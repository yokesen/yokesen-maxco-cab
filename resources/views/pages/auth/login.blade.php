<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-BMTCL2Z4WL"></script>
  <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
      dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'G-BMTCL2Z4WL');
  </script>
  <meta charset="UTF-8">
  <title>Maxco Futures | Prestigious Global Brokerage House</title>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

  <link rel="stylesheet" href="{{url('/')}}/cabinet/font/iconsmind-s/css/iconsminds.css" />
  <link rel="stylesheet" href="{{url('/')}}/cabinet/font/simple-line-icons/css/simple-line-icons.css" />
  <link rel="stylesheet" href="{{url('/')}}/cabinet/css/vendor/select2.min.css" />
  <link rel="stylesheet" href="{{url('/')}}/cabinet/css/vendor/select2-bootstrap.min.css" />
  <link rel="stylesheet" href="{{url('/')}}/cabinet/css/vendor/bootstrap.min.css" />
  <link rel="stylesheet" href="{{url('/')}}/cabinet/css/vendor/bootstrap.rtl.only.min.css" />
  <link rel="stylesheet" href="{{url('/')}}/cabinet/css/vendor/bootstrap-float-label.min.css" />
  <link rel="stylesheet" href="{{url('/')}}/cabinet/css/main.css" />
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <style>
    .btn-active {
      background-color: #fff !important;
      color: #017dc7 !important;
      border-color: #017dc7 !important;
    }
  </style>
  @if($agent->isMobile())
    <style>
      .loader{
        margin-top: -75px;
      }
    </style>
  @endif
  <!-- Facebook Pixel Code -->
  <script>
    ! function(f, b, e, v, n, t, s) {
      if (f.fbq) return;
      n = f.fbq = function() {
        n.callMethod ?
          n.callMethod.apply(n, arguments) : n.queue.push(arguments)
      };
      if (!f._fbq) f._fbq = n;
      n.push = n;
      n.loaded = !0;
      n.version = '2.0';
      n.queue = [];
      t = b.createElement(e);
      t.async = !0;
      t.src = v;
      s = b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t, s)
    }(window, document, 'script',
      'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '242949443388541');
    fbq('track', 'PageView');
  </script>
  <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=242949443388541&ev=PageView&noscript=1" /></noscript>
  <!-- End Facebook Pixel Code -->
</head>

<body class="background show-spinner no-footer">
  <div class="fixed-background" style="background:url({{url('/')}}/cabinet/img/webpage/background.jpg)"></div>
  <main id="app">
    <div class="container">
      <div class="row h-100">
        <div class="col-12 col-md-10 mx-auto my-auto">
          <div class="card auth-card">
            <div class="position-relative image-side" style="background:url({{url('/')}}/cabinet/img/webpage/background-kecil-final.jpg)">
              <p class="h2 text-white">MAXCO FUTURES CLIENT AREA</p>

              <p class="mb-0 text-white">
                Get Your trading accounts, deposit, withdrawal, and more features in one cabinet.
                <br>If you are not a member, please
                <a href="{{route('formregister')}}" class="btn btn-primary highlight">register</a>
              </p>
            </div>
            <div class="form-side">
              <a href="#">
                <span class="logo-single" style="background:url({{url('/')}}/cabinet/img/webpage/logo_maxco_W300px.png);background-size: cover;width: 300px;height: 66px;"></span>
              </a>

              <div class="mb-4">
                <div class="row">
                  <div class="col-md-4">
                    <button class="btn btn-primary btn-shadow btn-option-login" style="width:100% !important; border-radius: 0px;background-color: #fff;color:#9e9e9e;" onclick="renderText('Telephone');$('#areaCodeGrp').show();changeBtn(event);">Telephone</button>
                  </div>
                  <div class="col-md-4">
                    <button class="btn btn-primary btn-shadow btn-option-login btn-active" style="width:100% !important; border-radius: 0px;background-color: #fff;color:#9e9e9e;" onclick="renderText('Email');$('#areaCodeGrp').hide();changeBtn(event);">Email</button>
                  </div>
                  <div class="col-md-4">
                    <button class="btn btn-primary btn-shadow btn-option-login" style="width:100% !important; border-radius: 0px;background-color: #fff;color:#9e9e9e;" onclick="renderText('Client ID');$('#areaCodeGrp').hide();changeBtn(event);">Client ID</button>
                  </div>

                </div>
              </div>
              <h6 class="mb-4">Login</h6>
              <form id="login-form" class="" novalidate method="post" action="{{route('postLogin')}}">
                <div style="display: flex">
                  <label id="areaCodeGrp" class="form-group has-float-label mb-4" style="display: none;width: 118px;">
                    <select id="areaCode" name="AreaCode" class="form-control select2-single" style="min-width: 100px;">
                    </select>
                    <span>Code Area</span>
                  </label>
                  <label class="form-group has-float-label mb-4" style="width:100%;">
                    <input class="form-control" name="Username" />
                    <span id="label-text">E-mail</span>
                    <label id="Username-error" class="error" for="Username" style="top: 33px;position: absolute;color: red;opacity: 2;background: rgb(255, 255, 255);left: 0.75rem;"></label>
                  </label>
                </div>
                <label class="form-group has-float-label mb-4">
                  <input class="form-control" name="Password" type="password" placeholder="" />
                  <span>Password</span>
                  <label id="Password-error" class="error" for="Password" style="top: 33px;position: absolute;color: red;opacity: 2;background: rgb(255, 255, 255);left: 0.75rem;"></label>

                </label>

                @if (session('SuccessMessage'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                  <strong>Success!</strong> {{ session('SuccessMessage') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                @if (session('ErrorMessage'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                  <strong>Error!</strong> {{ session('ErrorMessage') }}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                @endif
                <input type="hidden" id="InputType" name="InputType" value="email">
                @csrf()
                <div class="d-flex justify-content-between align-items-center">
                  <a href="/forgot-password">Forget password?</a>
                  <input type="submit" class="btn btn-primary btn-lg btn-shadow" value="LOGIN" style="border-radius: 0px;background-color: #017dc7 !important;" />
                  <button id="btn-loading" onclick="event.preventDefault();" class="btn btn-primary btn-lg btn-shadow hidden" style="border-radius: 0px;background-color: #017dc7 !important;"><span class="loader"></span>  LOGIN</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>

  <script src="{{url('/')}}/cabinet/js/vendor/jquery-3.3.1.min.js"></script>
  <script src="{{url('/')}}/cabinet/js/vendor/jquery.validate/jquery.validate.min.js"></script>
  <script src="{{url('/')}}/cabinet/js/vendor/bootstrap.bundle.min.js"></script>
  <script src="{{url('/')}}/cabinet/js/vendor/select2.full.js"></script>
  <script src="{{url('/')}}/cabinet/js/dore.script.js"></script>
  <script src="{{url('/')}}/cabinet/js/scripts.js"></script>
  <script src="{{url('/')}}/cabinet/data/nationality.js"></script>
  <script>
    function changeBtn(event) {
      $('.btn-option-login').each(function() {
        $(this).removeClass('btn-active');
      })
      $(event.target).addClass('btn-active');
    }

    $("#login-form").validate({
      ignore: [],
      // errorElement: "div",
      rules: {
        Username: {
          required: true,
        },
        Password: {
          required: true
        },
      },
      messages: {

      }
    });

    function renderText(txt) {
      $('#label-text').text(txt);
      var text;
      if (txt === "Client ID") {
        text = 'id';
      } else if (txt === "Telephone") {
        text = 'phone';
      } else {
        text = 'email'
      }
      $('#InputType').val(text);
    };

    var optionAreaCd = "";
    nationalityDt.map(function(item) {
      const dt = `<option value="${item.AreaCode}">${item.AreaCode}</option>`;
      optionAreaCd += dt;
    });
    $('#areaCode').append(optionAreaCd)
    $('#areaCode').val('+ 62');

    $('[type="submit"]').on('click',function(){
      if($("#login-form").valid()){
        $(this).hide();
        $('#btn-loading').show();
      }
    });

  </script>
  @include('sweet::alert')
  <script>
    if ($('.swal-overlay').length > 0) {
      setTimeout(function() {
        $('.swal-overlay').css('opacity', "");
      }, 1000)
    }
  </script>
</body>

</html>