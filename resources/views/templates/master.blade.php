<!DOCTYPE html>
<html lang="en" dir="ltr">
@include('templates.head')

<body id="app-container" class="ltr rounded menu-sub-hidden sub-hidden show-spinner">

  @include('templates.topNav')
  @include('templates.sidebar')
  @yield('content')

  @include('templates.footer')
  <script src="{{url('/')}}/cabinet/js/vendor/jquery-3.3.1.min.js"></script>
  <script src="{{url('/')}}/cabinet/js/vendor/bootstrap.bundle.min.js"></script>
  <script src="{{url('/')}}/cabinet/js/vendor/Chart.bundle.min.js"></script>
  <script src="{{url('/')}}/cabinet/js/vendor/chartjs-plugin-datalabels.js"></script>
  <script src="{{url('/')}}/cabinet/js/vendor/moment.min.js"></script>
  <script src="{{url('/')}}/cabinet/js/vendor/fullcalendar.min.js"></script>
  <script src="{{url('/')}}/cabinet/js/vendor/datatables.min.js"></script>

  <script src="{{url('/')}}/cabinet/js/vendor/perfect-scrollbar.min.js"></script>
  <script src="{{url('/')}}/cabinet/js/vendor/bootstrap-tagsinput.min.js"></script>
  <script src="{{url('/')}}/cabinet/js/vendor/jquery.smartWizard.min.js"></script>
  <script src="{{url('/')}}/cabinet/js/vendor/jquery.validate/jquery.validate.min.js"></script>
  <script src="{{url('/')}}/cabinet/js/vendor/jquery.validate/additional-methods.min.js"></script>

  <script src="{{url('/')}}/cabinet/js/vendor/progressbar.min.js"></script>
  <script src="{{url('/')}}/cabinet/js/vendor/jquery.barrating.min.js"></script>
  <script src="{{url('/')}}/cabinet/js/vendor/select2.full.js"></script>
  <script src="{{url('/')}}/cabinet/js/vendor/nouislider.min.js"></script>
  <script src="{{url('/')}}/cabinet/js/vendor/bootstrap-datepicker.js"></script>
  <script src="{{url('/')}}/cabinet/js/vendor/Sortable.js"></script>
  <script src="{{url('/')}}/cabinet/js/vendor/mousetrap.min.js"></script>
  <script src="{{url('/')}}/cabinet/js/vendor/glide.min.js"></script>
  @yield('jsonpage')
  <script src="{{url('/')}}/cabinet/js/dore.script.js"></script>
  <script src="{{url('/')}}/cabinet/js/scripts.js"></script>
  <script>
    function getNotif() {
      $.get("{{route('apigetnotification')}}", {}, function(data, status) {
        if (data.Data.length > 0) {
          $('#countNotif').show();
          $('#countNotif').text(data.Data.length);
          data.Data.map(function(item) {
            if ($(`#contentnotif-${item.id}`).length === 0) {
              var initHtml = `
              <div class="d-flex flex-row mb-3 pb-3 border-bottom" id="contentnotif-${item.id}">
                  <!-- <a href="#">
                      <img src="{{url('/')}}/cabinet/img/profile-pic-l-2.jpg" alt="Notification Image"
                          class="img-thumbnail list-thumbnail xsmall border-0 rounded-circle" />
                  </a> -->
                  <div class="pl-3">
                      <a href="{{url('/')}}${item.url}">
                          <p class="font-weight-medium mb-1">${item.content}</p>
                          <p class="text-muted mb-0 text-small">${item.created_at}</p>
                      </a>
                  </div>
              </div>
            `;
              $('#notifContent').append(initHtml);
            }
          });
        }
      });
    };
    $(document).ready(function() {
      if ($('#countNotif').length > 0) {
        getNotif();
        setInterval(function() {
          getNotif();
        }, 60 * 1000)
      }
    });
  </script>
  @include('sweet::alert')
  <script>
    if ($('.swal-overlay').length > 0) {
      setTimeout(function(){
        $('.swal-overlay').css('opacity', "");
      },1000)
    }
  </script>
</body>

</html>