<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-BMTCL2Z4WL"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'G-BMTCL2Z4WL');
    </script>

    <meta charset="UTF-8">
    <title>Maxco Futures | Prestigious Global Brokerage House</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="stylesheet" href="{{url('/')}}/cabinet/font/iconsmind-s/css/iconsminds.css" />
    <link rel="stylesheet" href="{{url('/')}}/cabinet/font/simple-line-icons/css/simple-line-icons.css" />
    <link rel="stylesheet" href="{{url('/')}}/cabinet/css/vendor/select2.min.css" />
    <link rel="stylesheet" href="{{url('/')}}/cabinet/css/vendor/select2-bootstrap.min.css" />
    <link rel="stylesheet" href="{{url('/')}}/cabinet/css/vendor/bootstrap.min.css" />
    <link rel="stylesheet" href="{{url('/')}}/cabinet/css/vendor/bootstrap.rtl.only.min.css" />
    <link rel="stylesheet" href="{{url('/')}}/cabinet/css/vendor/bootstrap-float-label.min.css" />
    <link rel="stylesheet" href="{{url('/')}}/cabinet/css/main.css" />
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <!-- Facebook Pixel Code -->

</head>

<body class="background show-spinner no-footer">
    <div class="fixed-background" style="background:url({{url('/')}}/cabinet/img/webpage/background.jpg)"></div>
    <main>
        <div class="container">
            <div class="row h-100">
                <div class="col-12 col-md-10 mx-auto my-auto">
                    <div class="card auth-card">
                        <div class="position-relative image-side" style="background:url({{url('/')}}/cabinet/img/webpage/background-kecil-final.jpg)">
                            <p class="h2 text-white">MAXCO FUTURES<br> OPEN ACCOUNT</p>
                            <p class="mb-0 text-white">
                                Please use this form to register.
                                <br>If you are a member, please
                                <a href="{{route('login')}}" class="btn btn-primary highlight">login</a>
                            </p>
                        </div>
                        <div class="form-side">
                            <a href="{{route('formregister')}}">
                                <span class="logo-single" style="background:url({{url('/')}}/cabinet/img/webpage/logo_maxco_W300px.png);background-size: cover;width: 300px;height: 66px;"></span>
                            </a>
                            <h6 class="mb-4">Register</h6>
                            <form id="registerForm" class="tooltip-label-right" novalidate method="post" action="{{route('save')}}">
                                @csrf
                                <div class="row">
                                    <div class="col-md-6">
                                        <label class="form-group has-float-label mb-4">
                                            <select id="Nationality" name="Nationality" class="form-control select2-single" data-width="100%">

                                            </select>
                                            <span>Nationality</span>
                                        </label>
                                        <label class="form-group has-float-label mb-4">
                                            <input class="form-control" type="text" name="Realname" required value="@if(Session::get('demouser.Realname')){{Session::get('demouser.Realname')}}@endif" />
                                            <span>Full Name</span>
                                            <label id="Realname-error" class="error" for="Realname" style="top: 33px;position: absolute;color: red;opacity: 2;background: rgb(255, 255, 255);left: 0.75rem;"></label>

                                        </label>
                                        <label class="form-group has-float-label mb-4">
                                            <input class="form-control" type="text" name="IDNO" required value="@if(Session::get('demouser.IDNO')){{Session::get('demouser.IDNO')}}@endif" />
                                            <span>No ID</span>
                                            <label id="IDNO-error" class="error" for="IDNO" style="top: 33px;position: absolute;color: red;opacity: 2;background: rgb(255, 255, 255);left: 0.75rem;"></label>
                                        </label>
                                        <div style="display: flex">
                                            <label id="areaCodeGrp" class="form-group has-float-label mb-4 text-right" style="width: 118px;">
                                                <!-- <select id="AreaCodeSelect" name="AreaCodeSelect" class="form-control" data-width="100%">
                                                </select> -->
                                                <input class="form-control" id="AreaCodeInput" disabled style="background-color: transparent;">
                                                <span>Code Area</span>
                                            </label>
                                            <label class="form-group has-float-label mb-4" style="width:100%;">
                                                <input class="form-control" name="Mobile" required value="@if(Session::get('demouser.Mobile')){{Session::get('demouser.Mobile')}}@endif" />
                                                <span id="label-text">Phone</span>
                                                <label id="Mobile-error" class="error" for="Mobile" style="top: 33px;position: absolute;color: red;opacity: 2;background: rgb(255, 255, 255);left: 0.75rem;"></label>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mb-4">
                                            <label class="form-group has-float-label">
                                                <input class="form-control" type="email" name="Email" required autocomplete="off" value="@if(Session::get('demouser.Email')){{Session::get('demouser.Email')}}@endif" />
                                                <span>Email</span>
                                                <label id="Email-error" class="error" for="Email" style="top: 33px;position: absolute;color: red;opacity: 2;background: rgb(255, 255, 255);left: 0.75rem;"></label>
                                            </label>
                                        </div>

                                        <label class="form-group has-float-label mb-4">
                                            <input class="form-control" type="text" name="ParentId" value="{{env('APP_ENV')=='local'?3:10037}}" readonly />
                                            <span>Code Reference</span>
                                        </label>
                                        <label class="form-group has-float-label mb-4">
                                            <input class="form-control" type="password" name="Password" required value="@if(Session::get('demouser.Password')){{Session::get('demouser.Password')}}@endif" />
                                            <span>Password</span>
                                            <label id="Password-error" class="error" for="Password" style="top: 33px;position: absolute;color: red;opacity: 2;background: rgb(255, 255, 255);left: 0.75rem;"></label>
                                        </label>
                                        <label class="form-group has-float-label mb-4">
                                            <input class="form-control" type="password" name="ConfirmPassword" required value="@if(Session::get('demouser.ConfirmPassword')){{Session::get('demouser.ConfirmPassword')}}@endif" />
                                            <span>Confirm Password</span>
                                            <label id="ConfirmPassword-error" class="error" for="ConfirmPassword" style="top: 33px;position: absolute;color: red;opacity: 2;background: rgb(255, 255, 255);left: 0.75rem;"></label>
                                        </label>
                                    </div>
                                </div>
                                @if (session('ErrorMessage'))
                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                    <strong>Error!</strong> {{ session('ErrorMessage') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                @endif
                                <input class="" type="hidden" id="AreaCode" name="AreaCode" />
                                <input class="" type="hidden" id="InfoType" name="InfoType" value="0" />
                                <input type="hidden" name="ref" value="10037" />
                                <input type="hidden" name="so" value="<?php if (isset($_GET['so'])) {
                                                                            echo $_GET['so'];
                                                                        } else {
                                                                            echo '';
                                                                        } ?>" />
                                <input type="hidden" name="campaign" value="<?php if (isset($_GET['campaign'])) {
                                                                                echo $_GET['campaign'];
                                                                            } else {
                                                                                echo '';
                                                                            } ?>" />
                                @csrf
                                <div class="d-flex justify-content-end align-items-center">
                                    <button class="btn btn-primary btn-lg btn-shadow btn-maxco-blue" type="submit" style="border-radius: 0px;background-color: #017dc7 !important;">REGISTER</button>
                                    <button id="btn-loading" onclick="event.preventDefault();" class="btn btn-primary btn-lg btn-shadow hidden" style="border-radius: 0px;background-color: #017dc7 !important;"><span class="loader"></span>REGISTER</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <script src="{{url('/')}}/cabinet/js/vendor/jquery-3.3.1.min.js"></script>
    <script src="{{url('/')}}/cabinet/js/vendor/jquery.validate/jquery.validate.min.js"></script>
    <script src="{{url('/')}}/cabinet/js/vendor/bootstrap.bundle.min.js"></script>
    <script src="{{url('/')}}/cabinet/js/vendor/select2.full.js"></script>
    <script src="{{url('/')}}/cabinet/js/dore.script.js"></script>
    <script src="{{url('/')}}/cabinet/js/scripts.js"></script>
    <script src="{{url('/')}}/cabinet/data/nationality.js"></script>

    <script>
        // Wait for the DOM to be ready
        $(function() {
            $.validator.addMethod(
                "regex",
                function(value, element, regexp) {
                    var re = new RegExp(regexp);
                    return this.optional(element) || re.test(value);
                },
                "Gunakan minimal 8 karakter dengan campuran huruf kapital dan angka."
            );
            $("#registerForm").validate({
                ignore: [],
                // errorElement: "div",
                rules: {
                    Nationality: {
                        required: true
                    },
                    Realname: {
                        required: true
                    },
                    AreaCodeSelect: {
                        required: true
                    },
                    IDNO: {
                        required: true
                    },
                    areaCodeTxt: {
                        required: true
                    },
                    Mobile: {
                        required: true,
                        number: true
                    },
                    Email: {
                        required: true,
                        email: true
                    },
                    Password: {
                        required: true,
                        // minlength: 8,
                        // maxlength: 30,
                        regex: "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{5,30}$"
                    },
                    ConfirmPassword: {
                        required: true,
                        // minlength: 6,
                        equalTo: "[name='Password']"
                    },

                },
                messages: {
                    rulesPassword: {
                        // minlength: "Password must be at least {0} characters!"
                    },
                    ConfirmPassword: {
                        equalTo: "Passwords must match!",
                    }
                }
            });

            $('[type="submit"]').on('click',function(){
                if($('#registerForm').valid()){
                    $('[type="submit"]').hide();
                    $('#btn-loading').show();
                }
            });

        });

        var optionAreaCd = "";
        var NationalityDt = "";
        var areaCode = "";
        nationalityDt.map(function(item) {
            const dt = `<option value="${item.value}" arecode="${item.AreaCode}">${item.text}</option>`;
            const codeDt = `<option value="${item.AreaCode}">${item.AreaCode}</option>`;
            areaCode += codeDt;
            NationalityDt += dt;
        });
        $('#Nationality').append(NationalityDt)
        $('#AreaCodeSelect').append(areaCode)

        $('#Nationality').val('Indonesia');
        // $('#AreaCodeSelect').val('+ 62');
        $('#AreaCodeInput').val('+ 62')
        $('#AreaCode').val('62');

        $('#Nationality').on('change', function(event) {
            const value = event.currentTarget.value;
            var arecode = $("#Nationality").find(':selected').attr('arecode');
            arecode = arecode.replace("+ ", "");
            $('#AreaCodeInput').val(`+ ${arecode}`);
            $('#AreaCode').val(arecode);
        })

        $('#AreaCodeSelect').on('change', function(event) {
            var value = event.currentTarget.value;
            value = value.replace("+ ", "");
            $('#AreaCode').val(value);
        })
    </script>
    @include('sweet::alert')
    <script>
        if ($('.swal-overlay').length > 0) {
            setTimeout(function() {
                $('.swal-overlay').css('opacity', "");
            }, 1000)
        }
    </script>
</body>

</html>