@extends('templates.master')

@section('csslinkonhead')
<link rel="stylesheet" href="{{url('/')}}/cabinet/css/vendor/smart_wizard.min.css" />
<link rel="stylesheet" href="{{url('/')}}/cabinet/css/vendor/dropzone.min.css" />
<meta name="csrf-token" content="{{ csrf_token() }}" />
@if($agent->isMobile())
<style>
    .sw-main.sw-theme-check>ul.step-anchor>li>a:after,
    .sw-main.sw-theme-check>ul.step-anchor>li.active>a:after,
    .sw-main.sw-main.sw-theme-check>ul.step-anchor>li.done>a:after {
        margin-top: 42px;
    }
</style>
@endif
@endsection

@section('content')
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <h1>Opening Account Wizard</h1>
                <div class="separator mb-5"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 mb-4">
                <!-- <h5 class="mb-4">Open Live Account</h5> -->
                <div class="card mb-4">
                    <div id="" class="sw-main sw-theme-check">
                        <ul class="card-header mb-4 nav nav-tabs step-anchor" style="z-index: 0;">
                            <li class="nav-item  done">
                                <a class="nav-link">
                                    @if(!$agent->isMobile())
                                    Step 1
                                    <br>
                                    @endif
                                </a>
                            </li>
                            <li class="nav-item active">
                                <a class="nav-link">
                                    @if(!$agent->isMobile())
                                    Step 2
                                    <br>
                                    @endif
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link">
                                    @if(!$agent->isMobile())
                                    Step 3
                                    <br>
                                    @endif
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link">
                                    @if(!$agent->isMobile())
                                    Step 4
                                    <br>
                                    @endif
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link">
                                    @if(!$agent->isMobile())
                                    Step 5
                                    <br>
                                    @endif
                                </a>
                            </li>
                        </ul>

                        <div class="card-body">


                            <div id="step-1">
                                <form id="form-step-1" method="post" action="{{route('postNextStepOpenLiveAccount')}}" novalidate style="padding: 0px 22px;">
                                    <input name="NBCRMWEBAPI" type="hidden" value="{{$UserToken}}" />
                                    <input name="USERID" type="hidden" value="{{$UserId}}" />
                                    <input name="activeStep" type="hidden" value="{{$DataContent->activeStep?$DataContent->activeStep:0}}" />
                                    <input name="nextStep" type="hidden" value="3" />
                                    @csrf
                                    <h3 class="mb-4">
                                        Ketentuan Penyajian Aplikasi Pembukaan Rekening Transaksi Secara Elektronik
                                        Online - Formulir Nomor 107.PBK.03
                                    </h3>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="alert alert-dark" role="alert">Data Pribadi</div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Nama Lengkap</label>
                                                <div class="col-sm-8">
                                                    <span class="form-control-plaintext">{{$DataContent->full_name}}</span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Nomor Identitas</label>
                                                <div class="col-sm-8">
                                                    <span class="form-control-plaintext">{{$DataContent->identity_number}}</span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Tempat Lahir</label>
                                                <div class="col-sm-8">
                                                    <span class="form-control-plaintext" data-bind="place_of_birth">{{$DataContent->place_of_birth}}</span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Tangal Lahir</label>
                                                <div class="col-sm-8">
                                                    <span class="form-control-plaintext" data-bind="date_of_birth">{{$DataContent->date_of_birth}}</span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Alamat</label>
                                                <div class="col-sm-8">
                                                    <span class="form-control-plaintext" data-bind="home_address">{{$DataContent->home_address}}</span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Kode Pos</label>
                                                <div class="col-sm-8">
                                                    <span class="form-control-plaintext" data-bind="post_code">{{$DataContent->post_code}}</span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Alamat Email</label>
                                                <div class="col-sm-8">
                                                    <span class="form-control-plaintext">{{Session::get('user.email')}}</span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">No Handphone</label>
                                                <div class="col-sm-8">
                                                    <span class="form-control-plaintext">{{Session::get('user.mobile')}}</span>
                                                </div>
                                            </div>
                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">No NPWP</label>
                                                <input type="text" class="form-control" name="tax_file_number" placeholder="No NPWP" value="@isset($DataContent->tax_file_number){{trim($DataContent->tax_file_number)}}@endisset" required onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" />
                                            </div>
                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Jenis Kelamin</label>
                                                <div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="genderOptions" value="Male" required onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" @isset($DataContent->gender) @if($DataContent->gender==='Male') checked @endif @endisset/>
                                                        <label class="form-check-label" for="gender1">Laki-laki</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="genderOptions" value="Female" required onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" @isset($DataContent->gender) @if($DataContent->gender==='Female') checked @endif @endisset/>
                                                        <label class="form-check-label" for="gender2">Perempuan</label>
                                                    </div>
                                                </div>
                                                <label id="genderOptions-error" class="error" for="genderOptions" style="display: none;">This field is required.</label>
                                            </div>
                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Nama Ibu Kandung</label>
                                                <input type="text" class="form-control" name="mother_maiden_name" placeholder="Masukkan nama lengkap disini" value="@isset($DataContent->mother_maiden_name){{trim($DataContent->mother_maiden_name)}}@endisset" required onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" />
                                            </div>
                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Status Perkawinan</label>
                                                <div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="maritalstatusOptions" value="Married" onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" required @isset($DataContent->status) @if($DataContent->status==='Married') checked @endif @endisset/>
                                                        <label class="form-check-label" for="maritalstatus1">
                                                            Tidak
                                                            Kawin
                                                        </label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="maritalstatusOptions" value="Single" onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" required @isset($DataContent->status) @if($DataContent->status==='Single') checked @endif @endisset/>
                                                        <label class="form-check-label" for="maritalstatus2">Kawin</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="maritalstatusOptions" value="Divorced" onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" required @isset($DataContent->status) @if($DataContent->status==='Divorced') checked @endif @endisset/>
                                                        <label class="form-check-label" for="maritalstatus3">
                                                            Janda /
                                                            Duda
                                                        </label>
                                                    </div>
                                                </div>
                                                <label id="maritalstatusOptions-error" class="error" for="maritalstatusOptions" style="display: none;">This field is required.</label>
                                            </div>
                                            <div class="form-group position-relative error-l-100">
                                                <label>Nama Istri/Suami</label>
                                                <input type="text" class="form-control" name="husband_wife_spouse" placeholder="Masukkan nama lengkap (Opsional)" value="@isset($DataContent->husband_wife_spouse){{trim($DataContent->husband_wife_spouse)}}@endisset" onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" />
                                            </div>
                                            <div class="form-group position-relative error-l-100">
                                                <label>No Tlp Rumah</label>
                                                <input type="text" class="form-control" name="home_phone" placeholder="(Kode Area) No. (Opsional)" value="@isset($DataContent->home_phone){{trim($DataContent->home_phone)}}@endisset" onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" />
                                            </div>
                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Status Kepemilikan Rumah</label>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="houseownstatusOptions" required value="personal" @isset($DataContent->home_ownership_status) @if($DataContent->home_ownership_status->value==='personal') checked @endif @endisset
                                                            onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" />
                                                            <label class="form-check-label" for="houseownstatus1">Pribadi</label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="houseownstatusOptions" required value="family" @isset($DataContent->home_ownership_status) @if($DataContent->home_ownership_status->value==='family') checked @endif @endisset onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" />
                                                            <label class="form-check-label" for="houseownstatus2">Keluarga</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="houseownstatusOptions" required value="rent" @isset($DataContent->home_ownership_status) @if($DataContent->home_ownership_status->value==='rent') checked @endif @endisset onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" />
                                                            <label class="form-check-label" for="houseownstatus3">Sewa/Kontrak</label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="houseownstatusOptions" required value="other" @isset($DataContent->home_ownership_status) @if($DataContent->home_ownership_status->value==='other') checked @endif @endisset onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" />
                                                            <label class="form-check-label" for="houseownstatus4">Lainnya</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12" id="houseownstatusdescriptiondiv">
                                                        <input type="text" class="form-control" name="houseownstatusdescription" placeholder="Sebutkan" value="@isset($DataContent->home_ownership_status){{trim($DataContent->home_ownership_status->input)}}@endisset" onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" />
                                                    </div>

                                                </div>
                                                <label id="houseownstatusOptions-error" class="error ml-15" for="houseownstatusOptions" style="display: none;">This field is required.</label>
                                            </div>
                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Tujuan Pembukaan Rekening</label>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="openint_account_purpose" required value="protecvalue" onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" @isset($DataContent->openint_account_purpose)@if($DataContent->openint_account_purpose->value==='protecvalue') checked @endif @endisset />
                                                            <label class="form-check-label" for="openaccountfor1">
                                                                Lindungi
                                                                Nilai
                                                            </label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="openint_account_purpose" required value="gain" onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" @isset($DataContent->openint_account_purpose) @if($DataContent->openint_account_purpose->value==='gain') checked @endif @endisset />
                                                            <label class="form-check-label" for="openaccountfor2">Gain</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="openint_account_purpose" required value="specultion" onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" @isset($DataContent->openint_account_purpose) @if($DataContent->openint_account_purpose->value==='specultion') checked @endif @endisset />
                                                            <label class="form-check-label" for="openaccountfor3">Spekulasi</label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="openint_account_purpose" required value="other" onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" @isset($DataContent->openint_account_purpose) @if($DataContent->openint_account_purpose->value==='other') checked @endif @endisset />
                                                            <label class="form-check-label" for="openaccountfor4">Lainnya</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input type="text" class="form-control" name="openint_account_purpose_input" placeholder="Sebutkan" onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" value="@isset($DataContent->openint_account_purpose){{trim($DataContent->openint_account_purpose->input)}}@endisset" />
                                                    </div>
                                                    <label id="openint_account_purpose-error" class="error ml-15" for="openint_account_purpose" style="display: none;">This field is required.</label>
                                                </div>
                                            </div>

                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Pengalaman Investasi</label>
                                                <div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="experince_on_investment" required value="Yes" onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" @isset($DataContent->experince_on_investment) @if($DataContent->experince_on_investment->value==='Yes') checked @endif @endisset/>
                                                        <label class="form-check-label">Ya</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="experince_on_investment" required value="No" onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" @isset($DataContent->experince_on_investment) @if($DataContent->experince_on_investment->value==='No') checked @endif @endisset/>
                                                        <label class="form-check-label">Tidak</label>
                                                    </div>
                                                    <input type="text" class="form-control" name="experince_on_investment_input" placeholder="Sebutkan Bidang" onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" value="@isset($DataContent->experince_on_investment){{trim($DataContent->experince_on_investment->input)}}@endisset" />
                                                </div>
                                                <label id="experince_on_investment-error" class="error" for="experince_on_investment" style="display: none;">This field is required.</label>
                                            </div>

                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">
                                                    Apakah Anda Memiliki Anggota Keluarga Yang
                                                    Bekerja Di
                                                    Bappebti / Bursa Berjangka / Kliring Berjangka
                                                </label>
                                                <div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" required name="is_family_work_in_related_company" value="Yes" onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" @isset($DataContent->is_family_work_in_related_company) @if($DataContent->is_family_work_in_related_company->value==='Yes') checked @endif @endisset/>
                                                        <label class="form-check-label">Ya</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" required name="is_family_work_in_related_company" value="No" onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" @isset($DataContent->is_family_work_in_related_company) @if($DataContent->is_family_work_in_related_company->value==='No') checked @endif @endisset/>
                                                        <label class="form-check-label">Tidak</label>
                                                    </div>
                                                    <input type="text" class="form-control" name="is_family_work_in_related_company_input" placeholder="Sebutkan Nama" onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" value="@isset($DataContent->is_family_work_in_related_company){{trim($DataContent->is_family_work_in_related_company->input)}}@endisset" />
                                                </div>
                                                <label id="is_family_work_in_related_company-error" class="error" for="is_family_work_in_related_company" style="display: none;">This field is required.</label>
                                            </div>

                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">
                                                    Apakah Anda Dinyatakan Pailit Oleh Pengadilan
                                                    ?
                                                </label>
                                                <div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="is_declared_bankrupt" value="Yes" required onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" @isset($DataContent->is_declared_bankrupt) @if($DataContent->is_declared_bankrupt->value==='Yes') checked @endif @endisset/>
                                                        <label class="form-check-label" for="bankrupt1">Ya</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="is_declared_bankrupt" value="No" required onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" @isset($DataContent->is_declared_bankrupt) @if($DataContent->is_declared_bankrupt->value==='No') checked @endif @endisset/>
                                                        <label class="form-check-label" for="bankrupt2">Tidak</label>
                                                    </div>
                                                </div>
                                                <label id="is_declared_bankrupt-error" class="error" for="is_declared_bankrupt" style="display: none;">This field is required.</label>
                                            </div>
                                            <div class="alert alert-dark" role="alert">Pihak Yang Dihubungi Dalam Keadaan Darurat</div>
                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Nama</label>
                                                <input type="text" class="form-control" name="contactEmergencyName" value="@isset($DataContent->emergency_contact_data){{trim($DataContent->emergency_contact_data->name)}}@endisset" placeholder="Masukkan nama lengkap" onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" required />
                                            </div>

                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Alamat</label>
                                                <input type="text" class="form-control" name="contactEmergencyAddress" value="@isset($DataContent->emergency_contact_data){{trim($DataContent->emergency_contact_data->address)}}@endisset" placeholder="Masukkan Alamat" onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" required />
                                            </div>

                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Kode Pos</label>
                                                <input type="text" class="form-control" name="contactEmergencyPoscode" value="@isset($DataContent->emergency_contact_data){{trim($DataContent->emergency_contact_data->post_code)}}@endisset" placeholder="Kode Pos" onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" required />
                                            </div>

                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">No Telepon</label>
                                                <input type="text" class="form-control" name="contactEmergencyPhone" value="@isset($DataContent->emergency_contact_data){{trim($DataContent->emergency_contact_data->home_phone_number)}}@endisset" placeholder="(Kode Area) No" onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" required />
                                            </div>

                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Hubungan Dengan Anda</label>
                                                <input type="text" class="form-control" name="contactEmergencyRelationship" placeholder="Ketik Disini" value="@isset($DataContent->emergency_contact_data){{trim($DataContent->emergency_contact_data->relationship_with_the_contacts)}}@endisset" onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" required />
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="alert alert-dark" role="alert">Pekerjaan</div>
                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Pekerjaan</label>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="position_value" value="swasta" onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" required @isset($DataContent->job_details) @if($DataContent->job_details->position->value==='swasta') checked @endif @endisset />
                                                            <label class="form-check-label" for="jobtitle1">Swasta</label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="position_value" value="wiraswasta" onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" required @isset($DataContent->job_details) @if($DataContent->job_details->position->value==='wiraswasta') checked @endif @endisset />
                                                            <label class="form-check-label" for="jobtitle2">Wiraswata</label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="position_value" value="iburt" onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" required @isset($DataContent->job_details) @if($DataContent->job_details->position->value==='iburt') checked @endif @endisset/>
                                                            <label class="form-check-label" for="jobtitle3">
                                                                Ibu
                                                                RT
                                                            </label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="position_value" value="profesional" onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" required @isset($DataContent->job_details) @if($DataContent->job_details->position->value==='profesional') checked @endif @endisset/>
                                                            <label class="form-check-label" for="jobtitle4">Profesional</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="position_value" value="pns" onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" required @isset($DataContent->job_details) @if($DataContent->job_details->position->value==='pns') checked @endif @endisset/>
                                                            <label class="form-check-label" for="jobtitle5">PNS</label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="position_value" value="student" onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" required @isset($DataContent->job_details) @if($DataContent->job_details->position->value==='student') checked @endif @endisset/>
                                                            <label class="form-check-label" for="jobtitle6">Mahasiswa</label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="position_value" value="other" onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" required @isset($DataContent->job_details) @if($DataContent->job_details->position->value==='other') checked @endif @endisset/>
                                                            <label class="form-check-label" for="jobtitle7">Lainnya</label>
                                                        </div>
                                                    </div>
                                                    <label id="position_value-error" class="error ml-15" for="position_value" style="display: none;">This field is required.</label>

                                                    <div class="col-sm-12" id="position_value_div">
                                                        <input type="text" class="form-control" name="position_input" placeholder="Sebutkan" onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" value="@isset($DataContent->job_details){{trim($DataContent->job_details->position->input)}}@endisset" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Nama Perusahaan</label>
                                                <input type="text" class="form-control" name="employer_name" placeholder="Ketik Disini" value="@isset($DataContent->job_details){{trim($DataContent->job_details->more_details->employer_name)}}@endisset" onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" required />
                                            </div>

                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Bidang Usaha</label>
                                                <input type="text" class="form-control" name="business_fields" placeholder="Ketik Disini" value="@isset($DataContent->job_details){{trim($DataContent->job_details->more_details->business_fields)}}@endisset" onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" required />
                                            </div>

                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Jabatan</label>
                                                <input type="text" class="form-control" name="title" placeholder="Ketik Disini" onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" value="@isset($DataContent->job_details){{trim($DataContent->job_details->more_details->title)}}@endisset" required />
                                            </div>
                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Lama Bekerja</label>
                                                <div class="input-group">
                                                    <input type="number" class="form-control" name="length_of_work" min="1" placeholder="Ketik Disini" onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" required value="@isset($DataContent->job_details){{trim($DataContent->job_details->more_details->length_of_work)}}@endisset" />
                                                    <div class="input-group-append">
                                                        <div class="input-group-text" id="btnGroupAddon">Tahun</div>
                                                    </div>
                                                </div>
                                                <label id="length_of_work-error" class="error" for="length_of_work" style="display: none;">This field is required.</label>
                                            </div>
                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Kantor Sebelumnya</label>
                                                <div class="input-group">
                                                    <input type="number" class="form-control" name="previous_employer" min="1" placeholder="Ketik Disini" onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" value="@isset($DataContent->job_details){{trim($DataContent->job_details->more_details->previous_employer)}}@endisset" />
                                                    <div class="input-group-append">
                                                        <div class="input-group-text" id="btnGroupAddon">Tahun</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Alamat Kantor</label>
                                                <input type="text" class="form-control" name="office_address" placeholder="Masukkan Alamat Lengkap Disini" onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" value="@isset($DataContent->job_details){{trim($DataContent->job_details->more_details->office_address)}}@endisset" required />
                                            </div>
                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">No Tlp Kantor</label>
                                                <input type="text" class="form-control" name="office_tele" placeholder="Masukkan No Tlp Kantor" onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" required value="@isset($DataContent->job_details){{trim($DataContent->job_details->more_details->office_tele)}}@endisset" />
                                            </div>
                                            <div class="alert alert-dark" role="alert">Daftar Kekayaan</div>

                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Penghasilan Per Tahun ?</label>
                                                <div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="income_per_year" value="100-200" onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" required @isset($DataContent->wealth_list)@if($DataContent->wealth_list->income_per_year==='100-200') checked @endif @endisset
                                                        />
                                                        <label class="form-check-label" for="income1">
                                                            100-250
                                                            Juta
                                                        </label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="income_per_year" value="250-500" onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" required @isset($DataContent->wealth_list)@if($DataContent->wealth_list->income_per_year==='250-500') checked @endif @endisset/>
                                                        <label class="form-check-label" for="income2">
                                                            250-500
                                                            Juta
                                                        </label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="income_per_year" value=">500" onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" required @isset($DataContent->wealth_list)@if($DataContent->wealth_list->income_per_year==='>500') checked @endif @endisset/>
                                                        <label class="form-check-label" for="income3">
                                                            >500
                                                            Juta
                                                        </label>
                                                    </div>
                                                </div>
                                                <label id="income_per_year-error" class="error" for="income_per_year" style="display: none;">This field is required.</label>
                                            </div>
                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Lokasi Rumah</label>
                                                <input type="text" class="form-control" name="residence_location" placeholder="Masukkan Lokasi Rumah" value="@isset($DataContent->wealth_list){{trim($DataContent->wealth_list->residence_location)}}@endisset" onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" required />
                                            </div>
                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Nilai NJOP</label>
                                                <input type="text" class="form-control" name="tax_object_selling_value" placeholder="Total Semua NJOP Rumah" value="@isset($DataContent->wealth_list){{trim($DataContent->wealth_list->tax_object_selling_value)}}@endisset" onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" required />
                                            </div>
                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Nama Bank Deposito </label>
                                                <input type="text" class="form-control" name="bank_time_deposit_value" placeholder="Sebutkan Bank-bank Deposito" value="@isset($DataContent->wealth_list){{trim($DataContent->wealth_list->bank_time_deposit_value)}}@endisset" onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" required />
                                            </div>
                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Jumlah Deposito</label>
                                                <input type="text" class="form-control" name="bank_saving_value" placeholder="Sebutkan Total Nilai Deposito" value="@isset($DataContent->wealth_list){{trim($DataContent->wealth_list->bank_saving_value)}}@endisset" onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" required />
                                            </div>
                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Lainnya</label>
                                                <input type="text" class="form-control" name="wealth_list_other" placeholder="Sebutkan Aset-aset lainnya" value="@isset($DataContent->wealth_list){{trim($DataContent->wealth_list->others)}}@endisset" onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" required />
                                            </div>
                                            <div class="alert alert-dark" role="alert">
                                                Rekening Bank Untuk Penarikan
                                                <p>
                                                    Rekening Bank Nasabah Untuk Penyetoren Dana Penarikan Margin
                                                    (Hanya
                                                    Rekening Di Bawah Ini Yang Dapat Saudara Pergunakan Untuk
                                                    Lalulintas
                                                    Margin)
                                                </p>
                                            </div>
                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Nama Bank</label>
                                                <input type="text" class="form-control" name="withdrwlbankname" placeholder="Masukkan Nama Lengkap Disini" value="@isset($DataContent->bank_details_for_withdrawls){{trim($DataContent->bank_details_for_withdrawls->full_name)}}@endisset" onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" required />
                                            </div>
                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Cabang</label>
                                                <input type="text" class="form-control" name="withdrwlbankbranch" placeholder="Masukkan Nama Cabang Disini" value="@isset($DataContent->bank_details_for_withdrawls){{trim($DataContent->bank_details_for_withdrawls->branch)}}@endisset" onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" required />
                                            </div>
                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Nomor ACC</label>
                                                <input type="text" class="form-control" name="withdrwlbankaccno" placeholder="Sebutkan Nomor Rekening Dengan Benar" value="@isset($DataContent->bank_details_for_withdrawls){{trim($DataContent->bank_details_for_withdrawls->account_number)}}@endisset" onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" required />
                                            </div>
                                            <!-- <div class="form-group position-relative error-l-100">
                                                <label class="required">Nomor Telepon</label>
                                                <input type="text" class="form-control" name="withdrwlbankphone" placeholder="(Kode Area) No." value="@isset($DataContent->bank_details_for_withdrawls){{trim($DataContent->bank_details_for_withdrawls->phone_number)}}@endisset" onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" required />
                                            </div> -->
                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Jenis Akun</label>
                                                <div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="withdrwltypeaccbank" value="giro" onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" required @isset($DataContent->bank_details_for_withdrawls) @if($DataContent->bank_details_for_withdrawls->account_type->value==='giro') checked @endif @endisset"/>
                                                        <label class="form-check-label" for="typeakunbankforwithdrawl1">Giro</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="withdrwltypeaccbank" value="saving" onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" required @isset($DataContent->bank_details_for_withdrawls) @if($DataContent->bank_details_for_withdrawls->account_type->value==='saving') checked @endif @endisset"/>
                                                        <label class="form-check-label" for="typeakunbankforwithdrawl2">Tabungan</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="withdrwltypeaccbank" value="other" onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" required @isset($DataContent->bank_details_for_withdrawls) @if($DataContent->bank_details_for_withdrawls->account_type->value==='other') checked @endif @endisset"/>
                                                        <label class="form-check-label" for="typeakunbankforwithdrawl3">Lainnya</label>
                                                    </div>
                                                </div>
                                                <label id="withdrwltypeaccbank-error" class="error" for="withdrwltypeaccbank" style="display: none;">This field is required.</label>
                                                <input type="text" class="form-control" name="withdrwltypeaccbankinput" placeholder="Sebutkan" value="@isset($DataContent->bank_details_for_withdrawls){{trim($DataContent->bank_details_for_withdrawls->account_type->input)}}@endisset" onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" />
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="alert alert-dark" role="alert">Dokumen Yang Dilampirkan</div>
                                            <div class="form-group" style="display: none;">
                                                <div class="card mb-4">
                                                    <div class="card-body">
                                                        <form action="{{route('uploadimg')}}" enctype="multipart/form-data" class="dropzone coloured_photo">
                                                            @csrf
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="required">Fotokopi KTP/SIM/KITAS/PASSPOR</label>
                                                <input id="id_card" type="hidden">
                                                <div class="card mb-4">
                                                    <div class="card-body">
                                                        <h5 class="mb-4">Fotokopi KTP/SIM/KITAS/PASSPOR</h5>
                                                        <form action="{{route('uploadimg')}}" enctype="multipart/form-data" class="dropzone id_card">
                                                            @csrf
                                                        </form>
                                                        <br>
                                                        <div id="div-upload-image-ID" class="hidden">
                                                            <p class="mb-2">Uploading <span class="float-right text-muted progress-percent-card-id">0 %</span></p>
                                                            <div class="progress">
                                                                <div class="progress-bar progress-card-id" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Pasfoto Berwarna</label>
                                                <input id="coloured_photo" type="hidden">
                                                <div class="card mb-4">
                                                    <div class="card-body">
                                                        <h5 class="mb-4">Pasfoto Berwarna</h5>
                                                        <form action="{{route('uploadimg')}}" enctype="multipart/form-data" class="dropzone coloured_photo">
                                                            @csrf
                                                        </form>
                                                        <br>
                                                        <div id="div-upload-coloured-photo" class="hidden">
                                                            <p class="mb-2">Uploading <span class="float-right text-muted progress-percent-coloured-photo">0 %</span></p>
                                                            <div class="progress">
                                                                <div class="progress-bar progress-coloured-photo" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Tanda Tangan Spesimen</label>
                                                <input id="specimen_signature" type="hidden">
                                                <div class="card mb-4">
                                                    <div class="card-body">
                                                        <h5 class="mb-4">Tanda Tangan Spesimen</h5>
                                                        <form action="{{route('uploadimg')}}" enctype="multipart/form-data" class="dropzone specimen_signature">
                                                            @csrf
                                                        </form>
                                                        <br>
                                                        <div id="div-upload-specimen-signature" class="hidden">
                                                            <p class="mb-2">Uploading <span class="float-right text-muted progress-percent-specimen-signature">0 %</span></p>
                                                            <div class="progress">
                                                                <div class="progress-bar progress-specimen-signature" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- <div class="form-group position-relative error-l-100">
                                                <label class="required">Cover Buku Tabungan</label>
                                                <input id="bank_statement" type="hidden">
                                                <div class="card mb-4">
                                                    <div class="card-body">
                                                        <h5 class="mb-4">Cover Buku Tabungan</h5>
                                                        <form action="{{route('uploadimg')}}" enctype="multipart/form-data" class="dropzone bank_statement">
                                                            @csrf
                                                        </form>
                                                    </div>
                                                </div>
                                            </div> -->
                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Lainnya</label>
                                                <input id="other_image" type="hidden">
                                                <div class="card mb-4">
                                                    <div class="card-body">
                                                        <h5 class="mb-4">Lainnya</h5>
                                                        <form action="{{route('uploadimg')}}" enctype="multipart/form-data" class="dropzone other_image">
                                                            @csrf
                                                        </form>
                                                        <br>
                                                        <div id="div-upload-other-image" class="hidden">
                                                            <p class="mb-2">Uploading <span class="float-right text-muted progress-percent-other-image">0 %</span></p>
                                                            <div class="progress">
                                                                <div class="progress-bar progress-other-image" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                                            </div>
                                                        </div>
                                                        <br>
                                                        <p>Harap melampirkan setidaknya satu dari persyaratan ini:</p>
                                                        <ul>
                                                            <li>Cover Buku Tabungan</li>
                                                            <li>Surat Keterangan Bekerja</li>
                                                            <li>Surat Keterangan Pendapatan untuk calon nasabah berstatus pegawai atau laporan keuangan 3 (tiga) bulan terakhir (untuk calon Nasabah berstatus wiraswasta atau pemilik usaha)</li>
                                                            <li>Tagihan Kartu Kredit</li>
                                                            <li>Bukti Kepemilikan Tanah atau Bangunan atau Kendaraan Bermotor</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group position-relative error-l-100">
                                                <label>
                                                    PERNYATAAN KEBENARAN DAN TANGGUNG
                                                    JAWAB
                                                </label>
                                                <p class="text-justify">
                                                    Dengan mengisi kolom "YA" di bawah ini, saya menyatakan bahwa
                                                    semua
                                                    informasi dan semua dokumen yang saya lampirkan dalam APLIKASI
                                                    PEMBUKAAN
                                                    REKENING TRANSAKSI SECARA ELEKTRONIK ON-LINE adalah benar dan
                                                    tepat.
                                                    Saya akan bertanggung jawab penuh apabila dikemudian hari
                                                    terjadi
                                                    sesuatu hal sebuhungan dengan ketidakbenaran data yang saya
                                                    berikan.
                                                </p>
                                                <div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="truth_resposibility_agreement" value="yes" onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" required @isset($DataContent->truth_resposibility_agreement) @if($DataContent->truth_resposibility_agreement==='yes') checked @endif @endisset/>
                                                        <label class="form-check-label required">Ya</label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="truth_resposibility_agreement" value="no" onchange="savekyc()" onkeyup="savekyc()" onkeypress="savekyc()" required @isset($DataContent->truth_resposibility_agreement) @if($DataContent->truth_resposibility_agreement==='no') checked @endif @endisset/>
                                                        <label class="form-check-label">Tidak</label>
                                                    </div>
                                                </div>
                                                <label id="truth_resposibility_agreement-error" class="error" for="truth_resposibility_agreement" style="display: none;">Klik "Ya" untuk melanjutkan.</label>
                                                <p>Menyatakan pada tanggal : {{date('d-m-Y',strtotime(date("m.d.y")))}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="btn-toolbar custom-toolbar text-center card-body pt-0 form-footer-wizard mb-4 justify-content-md-center">
                                        <button class="btn btn-secondary next-btn" type="submit">Next</button>
                                        <button id="btn-loading" onclick="event.preventDefault();" class="btn btn-secondary hidden">
                                            <span class="loader-small" style="position: unset !important;"></span> Next
                                        </button>
                                    </div>
                                </form>
                            </div>

                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection

@section('jsonpage')
<script src="{{url('/')}}/cabinet/js/vendor/jquery.smartWizard.min.js"></script>
<script src="{{url('/')}}/cabinet/js/vendor/dropzone.min.js"></script>
<script src="{{url('/')}}/cabinet/js/vendor/sweetalert.min.js"></script>

<script>
    var idCard_imgUrl = '';
    var id_card = false;
    var colouredPhoto_imgUrl = '';
    var coloured_photo = false;
    var specimenSignature_imgUrl = '';
    var specimen_signature = false;
    var bankStatement_imgUrl = ''
    var bank_statement = false;
    var other_imgUrl = '';
    var other_image = false;
    var attachedImage = [];

    function getDetailKYC() {
        var contentDt = getContent();
        var content = JSON.stringify(contentDt)
        $.get("{{route('apigetKycDetail')}}", {}, function(data, status) {
            var dataKyc = data.Data;
            if (dataKyc.attached_documents.required_image.length > 0 && renderImgAttc !== true) {
                var attchments = dataKyc.attached_documents.required_image;
                var findIdxIdCardImg = attchments.findIndex(x => x.type === "id_card");
                if (attchments[findIdxIdCardImg]) {
                    var dataImageIdCard = attchments[findIdxIdCardImg];
                    if (dataImageIdCard.src.url !== "") {
                        idCard_imgUrl = dataImageIdCard.src.url;
                        $('#id_card').val(dataImageIdCard.src.url);
                        var imgName = idCard_imgUrl.split('/');
                        var idCardHtml = `
                            <div class="dz-preview mb-3 dz-processing dz-image-preview dz-success dz-complete id_card_preview">
                                <div class="d-flex flex-row ">
                                    <div class="p-0 w-30 position-relative">
                                        <div class="dz-error-mark"><span><i></i></span></div>
                                        <div class="dz-success-mark"><span><i></i></span></div>
                                        <div class="preview-container"><img data-dz-thumbnail="" class="img-thumbnail border-0"
                                                alt="${imgName[2]}"
                                                src="{{env('API_URL')}}${dataImageIdCard.src.url}"><i
                                                class="simple-icon-doc preview-icon"></i></div>
                                    </div>
                                    <div class="pl-3 pt-2 pr-2 pb-1 w-70 dz-details position-relative">
                                        <div><span data-dz-name="">${imgName[2]}</span></div>
                                        <!-- <div class="text-primary text-extra-small" data-dz-size=""><strong>0.8</strong> MB</div> -->
                                    </div>
                                    <a class="remove removeimg" data-dz-remove="id_card"><i class="glyph-icon simple-icon-trash"></i></a>
                                </div>
                            </div>
                            `;
                        $('.id_card').append(idCardHtml);
                        $('.id_card').addClass('dz-started');

                    }
                }

                var findIdxIdClrphto = attchments.findIndex(x => x.type === "coloured_photo");
                if (attchments[findIdxIdClrphto]) {
                    var dataImageClrPhoto = attchments[findIdxIdClrphto];
                    if (dataImageClrPhoto.src.url !== "") {
                        colouredPhoto_imgUrl = dataImageClrPhoto.src.url;
                        $('#coloured_photo').val(dataImageClrPhoto.src.url);

                        var imgName = colouredPhoto_imgUrl.split('/');
                        var idCardHtml = `
                            <div class="dz-preview mb-3 dz-processing dz-image-preview dz-success dz-complete coloured_photo_preview">
                                <div class="d-flex flex-row ">
                                    <div class="p-0 w-30 position-relative">
                                        <div class="dz-error-mark"><span><i></i></span></div>
                                        <div class="dz-success-mark"><span><i></i></span></div>
                                        <div class="preview-container"><img data-dz-thumbnail="" class="img-thumbnail border-0"
                                                alt="${imgName[2]}"
                                                src="{{env('API_URL')}}${dataImageClrPhoto.src.url}"><i
                                                class="simple-icon-doc preview-icon"></i></div>
                                    </div>
                                    <div class="pl-3 pt-2 pr-2 pb-1 w-70 dz-details position-relative">
                                        <div><span data-dz-name="">${imgName[2]}</span></div>
                                        <!-- <div class="text-primary text-extra-small" data-dz-size=""><strong>0.8</strong> MB</div>-->
                                    </div>
                                    <a class="remove removeimg" data-dz-remove="coloured_photo"><i class="glyph-icon simple-icon-trash"></i></a>
                                </div>
                            </div>
                            `;
                        $('.coloured_photo').append(idCardHtml);
                        $('.coloured_photo').addClass('dz-started');
                    }
                }

                var findIdxIdSign = attchments.findIndex(x => x.type === "specimen_signature");
                if (attchments[findIdxIdSign]) {
                    var dataImageSignPhoto = attchments[findIdxIdSign];
                    if (dataImageSignPhoto.src.url !== "") {
                        specimenSignature_imgUrl = dataImageSignPhoto.src.url;
                        $('#specimen_signature').val(dataImageSignPhoto.src.url);
                        var imgName = specimenSignature_imgUrl.split('/');
                        var idCardHtml = `
                            <div class="dz-preview mb-3 dz-processing dz-image-preview dz-success dz-complete specimen_signature_preview">
                                <div class="d-flex flex-row ">
                                    <div class="p-0 w-30 position-relative">
                                        <div class="dz-error-mark"><span><i></i></span></div>
                                        <div class="dz-success-mark"><span><i></i></span></div>
                                        <div class="preview-container"><img data-dz-thumbnail="" class="img-thumbnail border-0"
                                                alt="${imgName[2]}"
                                                src="{{env('API_URL')}}${dataImageSignPhoto.src.url}"><i
                                                class="simple-icon-doc preview-icon"></i></div>
                                    </div>
                                    <div class="pl-3 pt-2 pr-2 pb-1 w-70 dz-details position-relative">
                                        <div><span data-dz-name="">${imgName[2]}</span></div>
                                        <!-- <div class="text-primary text-extra-small" data-dz-size=""><strong>0.8</strong> MB</div> -->
                                    </div>
                                    <a class="remove removeimg" data-dz-remove="specimen_signature"><i class="glyph-icon simple-icon-trash"></i></a>
                                </div>
                            </div>
                            `;
                        $('.specimen_signature').append(idCardHtml);
                        $('.specimen_signature').addClass('dz-started');
                    }
                }

                // var findIdxBankStatement = attchments.findIndex(x => x.type === "bank_statement");
                // if (attchments[findIdxBankStatement]) {
                //     var dataImageBankStatement = attchments[findIdxBankStatement];
                //     if (dataImageBankStatement.src.url !== "") {
                //         bankStatement_imgUrl = dataImageBankStatement.src.url;
                //         $('#bank_statement').val(dataImageBankStatement.src.url);

                //         var imgName = bankStatement_imgUrl.split('/');
                //         var idCardHtml = `
                //             <div class="dz-preview mb-3 dz-processing dz-image-preview dz-success dz-complete bank_statement_preview">
                //                 <div class="d-flex flex-row ">
                //                     <div class="p-0 w-30 position-relative">
                //                         <div class="dz-error-mark"><span><i></i></span></div>
                //                         <div class="dz-success-mark"><span><i></i></span></div>
                //                         <div class="preview-container"><img data-dz-thumbnail="" class="img-thumbnail border-0"
                //                                 alt="${imgName[2]}"
                //                                 src="{{env('API_URL')}}${dataImageBankStatement.src.url}"><i
                //                                 class="simple-icon-doc preview-icon"></i></div>
                //                     </div>
                //                     <div class="pl-3 pt-2 pr-2 pb-1 w-70 dz-details position-relative">
                //                         <div><span data-dz-name="">${imgName[2]}</span></div>
                //                         <!-- <div class="text-primary text-extra-small" data-dz-size=""><strong>0.8</strong> MB</div> -->
                //                     </div>
                //                     <a class="remove removeimg" data-dz-remove="bank_statement"><i class="glyph-icon simple-icon-trash"></i></a>
                //                 </div>
                //             </div>
                //             `;
                //         $('.bank_statement').append(idCardHtml);
                //         $('.bank_statement').addClass('dz-started');
                //     }
                // }

                var findIdxOther = attchments.findIndex(x => x.type === "other");
                if (attchments[findIdxOther]) {
                    var dataImageOther = attchments[findIdxOther];
                    if (dataImageOther.src.url !== "") {
                        other_imgUrl = dataImageOther.src.url;
                        $('#other_image').val(dataImageOther.src.url);
                        var imgName = other_imgUrl.split('/');
                        var otherimghtml = `
                            <div class="dz-preview mb-3 dz-processing dz-image-preview dz-success dz-complete bank_statement_preview">
                                <div class="d-flex flex-row ">
                                    <div class="p-0 w-30 position-relative">
                                        <div class="dz-error-mark"><span><i></i></span></div>
                                        <div class="dz-success-mark"><span><i></i></span></div>
                                        <div class="preview-container"><img data-dz-thumbnail="" class="img-thumbnail border-0"
                                                alt="${imgName[2]}"
                                                src="{{env('API_URL')}}${dataImageOther.src.url}"><i
                                                class="simple-icon-doc preview-icon"></i></div>
                                    </div>
                                    <div class="pl-3 pt-2 pr-2 pb-1 w-70 dz-details position-relative">
                                        <div><span data-dz-name="">${imgName[2]}</span></div>
                                        <!-- <div class="text-primary text-extra-small" data-dz-size=""><strong>0.8</strong> MB</div> -->
                                    </div>
                                    <a class="remove removeimg" data-dz-remove="bank_statement"><i class="glyph-icon simple-icon-trash"></i></a>
                                </div>
                            </div>
                            `;
                        $('.other_image').append(otherimghtml);
                        $('.other_image').addClass('dz-started');
                    }
                }
                renderImgAttc = true;
            }

            // if (dataKyc.attached_documents.other_image && dataKyc.attached_documents.other_image.src) {
            //     if (dataKyc.attached_documents.other_image.src.url !== "") {
            //         other_imgUrl = dataKyc.attached_documents.other_image.src.url;
            //     }
            // }
            // if (dataKyc.attached_documents.other_image && dataKyc.attached_documents.other_image.src && renderImgAttcOther !== true) {
            //     if (dataKyc.attached_documents.other_image.src.url !== "") {
            //         var imgName = other_imgUrl.split('/');
            //         $('#other_image').val(other_imgUrl);
            //         var idCardHtml = `
            //                 <div class="dz-preview mb-3 dz-processing dz-image-preview dz-success dz-complete other_image_preview">
            //                     <div class="d-flex flex-row ">
            //                         <div class="p-0 w-30 position-relative">
            //                             <div class="dz-error-mark"><span><i></i></span></div>
            //                             <div class="dz-success-mark"><span><i></i></span></div>
            //                             <div class="preview-container"><img data-dz-thumbnail="" class="img-thumbnail border-0"
            //                                     alt="${imgName[2]}"
            //                                     src="{{env('API_URL')}}/${other_imgUrl}"><i
            //                                     class="simple-icon-doc preview-icon"></i></div>
            //                         </div>
            //                         <div class="pl-3 pt-2 pr-2 pb-1 w-70 dz-details position-relative">
            //                             <div><span data-dz-name="">${imgName[2]}</span></div>
            //                         </div><a class="remove removeimg" data-dz-remove="other_image"><i class="glyph-icon simple-icon-trash"></i></a>
            //                     </div>
            //                 </div>
            //                 `;
            //         $('.other_image').append(idCardHtml);
            //         $('.other_image').addClass('dz-started');
            //     }
            //     renderImgAttcOther = true;
            // }

        });
    };

    function getContent() {
        var id_card = $('#id_card').val();
        var id_cardName = id_card.split('/');

        var coloured_photo = $('#coloured_photo').val();
        var coloured_photoName = coloured_photo.split('/');

        var specimen_signature = $('#specimen_signature').val();
        var specimen_signatureName = specimen_signature.split('/');

        // var bank_statement = $('#bank_statement').val();
        // var bank_statementName = bank_statement.split('/');

        var other_image = $('#other_image').val();
        var other_imageName = other_image.split('/');

        var contentDt = {
            'job_details': {
                'more_details': {
                    'length_of_work': parseInt($('[name="length_of_work"]').val()),
                    'previous_employer': parseInt($('[name="previous_employer"]').val()),
                    'employer_name': $('[name="employer_name"]').val(),
                    'business_fields': $('[name="business_fields"]').val(),
                    'title': $('[name="title"]').val(),
                    'office_address': $('[name="office_address"]').val(),
                    'office_tele': $('[name="office_tele"]').val(),
                    'office_fax': 0
                },
                'position': {
                    'value': $($("input[name='position_value']:checked")).val() || '',
                    'input': $('[name="position_input"]').val()
                }
            },
            'wealth_list': {
                'income_per_year': $($("input[name='income_per_year']:checked")).val() || '',
                'residence_location': $('[name="residence_location"]').val(),
                'tax_object_selling_value': $('[name="tax_object_selling_value"]').val(),
                'bank_time_deposit_value': $('[name="bank_time_deposit_value"]').val(),
                'bank_saving_value': $('[name="bank_saving_value"]').val(),
                'others': $('[name="wealth_list_other"]').val()
            },
            'bank_details_for_withdrawls': {
                'full_name': $('[name="withdrwlbankname"]').val(),
                'branch': $('[name="withdrwlbankbranch"]').val(),
                'account_number': $('[name="withdrwlbankaccno"]').val(),
                'phone_number': 0,
                'account_type': {
                    'value': $($("input[name='withdrwltypeaccbank']:checked")).val() || '',
                    'input': $('[name="withdrwltypeaccbankinput"]').val()
                }
            },
            'emergency_contact_data': {
                'name': $('[name="contactEmergencyName"]').val(),
                'address': $('[name="contactEmergencyAddress"]').val(),
                'post_code': $('[name="contactEmergencyPoscode"]').val(),
                'home_phone_number': $('[name="contactEmergencyPhone"]').val(),
                'relationship_with_the_contacts': $('[name="contactEmergencyRelationship"]').val()
            },
            'attached_documents': {
                'id_card': true,
                'coloured_photo': true,
                'specimen_signature': true,
                'other_count': 1,
                'required_image': [{
                        'type': 'id_card',
                        'title': 'id_card_or_driving_license_passport',
                        'action_type': 0,
                        'src': {
                            'name': id_cardName[2],
                            'size': 116,
                            'url': id_card
                        }
                    },
                    {
                        'type': 'coloured_photo',
                        'title': 'coloured_photo',
                        'action_type': 0,
                        'src': {
                            'name': coloured_photoName[2],
                            'size': 116,
                            'url': coloured_photo
                        }
                    },
                    {
                        'type': 'specimen_signature',
                        'title': 'specimen_signature',
                        'action_type': 0,
                        'src': {
                            'name': specimen_signatureName[2],
                            'size': 116,
                            'url': specimen_signature
                        }
                    },
                    {
                        'type': 'other',
                        'title': 'other',
                        'action_type': 0,
                        'src': {
                            'name': other_imageName[2],
                            'size': 116,
                            'url': other_image
                        }
                    }
                    // {
                    //     'type': 'bank_statement',
                    //     'title': 'bank_statement',
                    //     'action_type': 0,
                    //     'src': {
                    //         'name': bank_statementName[2],
                    //         'size': 116,
                    //         'url': bank_statement
                    //     }
                    // }
                ],
                'option': 'yes'
                // 'other_image': {
                //     'type': 'other_image',
                //     'title': 'other_image',
                //     'action_type': 0,
                //     'src': {
                //         'name': other_imageName[2],
                //         'size': 116,
                //         'url': other_image
                //     }
                // },
            },
            'trading_statement': {
                'statement_for_trading_simulation_on_demo_account': "{{isset($DataContent->trading_statement) && !empty($DataContent->trading_statement->statement_for_trading_simulation_on_demo_account)?$DataContent->trading_statement->statement_for_trading_simulation_on_demo_account:''}}",
                'statement_letter_has_been_experienced_on_trading': "{{isset($DataContent->trading_statement) && !empty($DataContent->trading_statement->statement_letter_has_been_experienced_on_trading)?$DataContent->trading_statement->statement_letter_has_been_experienced_on_trading:''}}",
                'statement_has_been_received_information_futures_company': "{{isset($DataContent->trading_statement) && !empty($DataContent->trading_statement->statement_has_been_received_information_futures_company)?$DataContent->trading_statement->statement_has_been_received_information_futures_company:''}}",
            },
            'risk_document': {
                'risk_1_title_en_ref': "{{isset($DataContent->risk_document)&&!empty($DataContent->risk_document->risk_1_title_en_ref)?$DataContent->risk_document->risk_1_title_en_ref:''}}",
                'risk_2_title_en_ref': "{{isset($DataContent->risk_document)&&!empty($DataContent->risk_document->risk_2_title_en_ref)?$DataContent->risk_document->risk_2_title_en_ref:''}}",
                'risk_3_title_en_ref': "{{isset($DataContent->risk_document)&&!empty($DataContent->risk_document->risk_3_title_en_ref)?$DataContent->risk_document->risk_3_title_en_ref:''}}",
                'risk_4_title_en_ref': "{{isset($DataContent->risk_document)&&!empty($DataContent->risk_document->risk_4_title_en_ref)?$DataContent->risk_document->risk_4_title_en_ref:''}}",
                'risk_5_title_en_ref': "{{isset($DataContent->risk_document)&&!empty($DataContent->risk_document->risk_5_title_en_ref)?$DataContent->risk_document->risk_5_title_en_ref:''}}",
                'risk_6_title_en_ref': "{{isset($DataContent->risk_document)&&!empty($DataContent->risk_document->risk_6_title_en_ref)?$DataContent->risk_document->risk_6_title_en_ref:''}}",
                'risk_7_title_en_ref': "{{isset($DataContent->risk_document)&&!empty($DataContent->risk_document->risk_7_title_en_ref)?$DataContent->risk_document->risk_7_title_en_ref:''}}",
                'risk_8_title_en_ref': "{{isset($DataContent->risk_document)&&!empty($DataContent->risk_document->risk_8_title_en_ref)?$DataContent->risk_document->risk_8_title_en_ref:''}}",
                'risk_9_title_en_ref': "{{isset($DataContent->risk_document)&&!empty($DataContent->risk_document->risk_9_title_en_ref)?$DataContent->risk_document->risk_9_title_en_ref:''}}",
                'risk_10_title_en_ref': "{{isset($DataContent->risk_document)&&!empty($DataContent->risk_document->risk_10_title_en_ref)?$DataContent->risk_document->risk_10_title_en_ref:''}}",
                'risk_11_title_en_ref': "{{isset($DataContent->risk_document)&&!empty($DataContent->risk_document->risk_11_title_en_ref)?$DataContent->risk_document->risk_11_title_en_ref:''}}",
                'risk_12_title_en_ref': "{{isset($DataContent->risk_document)&&!empty($DataContent->risk_document->risk_12_title_en_ref)?$DataContent->risk_document->risk_12_title_en_ref:''}}",
                'risk_13_title_en_ref': "{{isset($DataContent->risk_document)&&!empty($DataContent->risk_document->risk_13_title_en_ref)?$DataContent->risk_document->risk_13_title_en_ref:''}}",
                'radio': "{{isset($DataContent->risk_document)&&!empty($DataContent->risk_document->radio)?$DataContent->risk_document->radio:''}}",
            },
            'trading_condition': {
                'trading_conditions_1_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_1_title_en_ref)?$DataContent->trading_condition->trading_conditions_1_title_en_ref:''}}",
                'trading_conditions_2_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_2_title_en_ref)?$DataContent->trading_condition->trading_conditions_2_title_en_ref:''}}",
                'trading_conditions_3_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_3_title_en_ref)?$DataContent->trading_condition->trading_conditions_3_title_en_ref:''}}",
                'trading_conditions_4_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_4_title_en_ref)?$DataContent->trading_condition->trading_conditions_4_title_en_ref:''}}",
                'trading_conditions_5_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_5_title_en_ref)?$DataContent->trading_condition->trading_conditions_5_title_en_ref:''}}",
                'trading_conditions_6_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_6_title_en_ref)?$DataContent->trading_condition->trading_conditions_6_title_en_ref:''}}",
                'trading_conditions_7_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_7_title_en_ref)?$DataContent->trading_condition->trading_conditions_7_title_en_ref:''}}",
                'trading_conditions_8_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_8_title_en_ref)?$DataContent->trading_condition->trading_conditions_8_title_en_ref:''}}",
                'trading_conditions_9_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_9_title_en_ref)?$DataContent->trading_condition->trading_conditions_9_title_en_ref:''}}",
                'trading_conditions_10_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_10_title_en_ref)?$DataContent->trading_condition->trading_conditions_10_title_en_ref:''}}",
                'trading_conditions_11_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_11_title_en_ref)?$DataContent->trading_condition->trading_conditions_11_title_en_ref:''}}",
                'trading_conditions_12_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_12_title_en_ref)?$DataContent->trading_condition->trading_conditions_12_title_en_ref:''}}",
                'trading_conditions_13_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_13_title_en_ref)?$DataContent->trading_condition->trading_conditions_13_title_en_ref:''}}",
                'trading_conditions_14_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_14_title_en_ref)?$DataContent->trading_condition->trading_conditions_14_title_en_ref:''}}",
                'trading_conditions_15_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_15_title_en_ref)?$DataContent->trading_condition->trading_conditions_15_title_en_ref:''}}",
                'trading_conditions_16_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_16_title_en_ref)?$DataContent->trading_condition->trading_conditions_16_title_en_ref:''}}",
                'trading_conditions_17_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_17_title_en_ref)?$DataContent->trading_condition->trading_conditions_17_title_en_ref:''}}",
                'trading_conditions_18_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_18_title_en_ref)?$DataContent->trading_condition->trading_conditions_18_title_en_ref:''}}",
                'trading_conditions_19_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_19_title_en_ref)?$DataContent->trading_condition->trading_conditions_19_title_en_ref:''}}",
                'trading_conditions_20_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_20_title_en_ref)?$DataContent->trading_condition->trading_conditions_20_title_en_ref:''}}",
                'trading_conditions_21_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_21_title_en_ref)?$DataContent->trading_condition->trading_conditions_21_title_en_ref:''}}",
                'trading_conditions_22_title_1': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_22_title_1)?$DataContent->trading_condition->trading_conditions_22_title_1:'0'}}",
                'trading_conditions_22_title_2': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_22_title_2)?$DataContent->trading_condition->trading_conditions_22_title_2:'0'}}",
                'trading_conditions_22_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_22_title_en_ref)?$DataContent->trading_condition->trading_conditions_22_title_en_ref:''}}",
                'trading_conditions_23_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_23_title_en_ref)?$DataContent->trading_condition->trading_conditions_23_title_en_ref:''}}",
                'step_4_footer': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->step_4_footer)?$DataContent->trading_condition->step_4_footer:''}}",
            },
            'pap_agreement': "{{!empty($DataContent->pap_agreement)?$DataContent->pap_agreement:''}}",
            'truth_resposibility_agreement': $($("input[name='truth_resposibility_agreement']:checked")).val() || '',
            'demoAccount': 0,
            'full_name': "{{!empty($DataContent->full_name)?$DataContent->full_name:''}}",
            'identity_number': "{{!empty($DataContent->identity_number)?$DataContent->identity_number:''}}",
            'future_contract': {
                'value': "{{isset($DataContent->future_contract)&&!empty($DataContent->future_contract->value)?$DataContent->future_contract->value:''}}",
                'input': "{{isset($DataContent->future_contract)&&!empty($DataContent->future_contract->input)?$DataContent->future_contract->input:''}}",
            },
            'account_type': "{{!empty($DataContent->account_type)?$DataContent->account_type:''}}",
            'place_of_birth': "{{!empty($DataContent->place_of_birth)?$DataContent->place_of_birth:''}}",
            'date_of_birth': "{{!empty($DataContent->date_of_birth)?$DataContent->date_of_birth:''}}",
            'post_code': "{{!empty($DataContent->post_code)?$DataContent->post_code:''}}",
            'page': 11,
            'activeStep': "{{!empty($DataContent->activeStep)?$DataContent->activeStep:''}}",
            'tax_file_number': $('[name="tax_file_number"]').val(),
            'mother_maiden_name': $('[name="mother_maiden_name"]').val(),
            'status': $($("input[name='maritalstatusOptions']:checked")).val() || '',
            'gender': $($("input[name='genderOptions']:checked")).val() || '',
            'home_address': "{{!empty($DataContent->home_address)?$DataContent->home_address:''}}",
            'home_ownership_status': {
                'value': $($("input[name='houseownstatusOptions']:checked")).val() || '',
                'input': $('[name="houseownstatusdescription"]').val()
            },
            'openint_account_purpose': {
                'value': $($("input[name='openint_account_purpose']:checked")).val() || '',
                'input': $("input[name='openint_account_purpose_input']").val(),
            },
            'experince_on_investment': {
                'value': $($("input[name='experince_on_investment']:checked")).val() || '',
                'input': $("input[name='experince_on_investment_input']").val()
            },
            'is_family_work_in_related_company': {
                'value': $($("input[name='is_family_work_in_related_company']:checked")).val() || '',
                'input': $("input[name='is_family_work_in_related_company_input']").val()
            },
            'is_declared_bankrupt': {
                'value': $($("input[name='is_declared_bankrupt']:checked")).val() || '',
                'input': ''
            },
            'trading_rules': "{{!empty($DataContent->trading_rules)?$DataContent->trading_rules:''}}",
            'ae_name_or_branch': "{{!empty($DataContent->ae_name_or_branch)?$DataContent->ae_name_or_branch:''}}",
            'husband_wife_spouse': $('[name="husband_wife_spouse"]').val(),
            'home_phone': $('[name="home_phone"]').val(),
            'home_fax': 0,
            'statement_data_correctly': "{{!empty($DataContent->statement_data_correctly)?$DataContent->statement_data_correctly:''}}",
            'KycStatus': 0
        };

        return contentDt;
    }

    $(function() {
        if ($().dropzone && !$(".dropzone").hasClass("disabled")) {
            $(".id_card").dropzone({
                url: "{{route('uploadimg')}}",
                dictResponseError: 'Cannot accept this type file',
                acceptedFiles: ".jpg, .jpeg, .pdf, .png",
                dictDefaultMessage: "Drop files here to upload <br> format (JPG/JPEG/PDF/PNG)",
                init: function() {
                    var drop = this;
                    this.on("success", function(file, responseText) {
                        idCard_imgUrl = responseText.Data;
                        id_card = true;
                        $('#id_card').val(responseText.Data);
                        savekyc();
                    });
                    this.on("error", function(file, error) {
                        console.log(error);
                        swal("Info", error, "info");
                        // i remove current file
                        drop.removeFile(file);
                        $('.progress-card-id').width(`0%`);
                        $('.progress-percent-card-id').text(`0 %`);
                    });
                    this.on('sending', function() {
                        $('#div-upload-image-ID').show();
                        $('.progress-card-id').width(`0%`);
                        $('.progress-percent-card-id').text(`0 %`);
                    });
                    this.on('canceled', function() {
                        $('.progress-card-id').width(`0%`);
                        $('.progress-percent-card-id').text(`0 %`);
                    });
                    this.on("totaluploadprogress", function(progress) {
                        var fixprogress = 0;
                        if (progress < 100) {
                            fixprogress = (progress).toFixed(2);
                        } else {
                            fixprogress = 100;
                        }

                        $('.progress-card-id').width(`${fixprogress}%`);
                        $('.progress-percent-card-id').text(`${fixprogress} %`);
                    });
                },
                thumbnailWidth: 160,
                previewTemplate: '<div class="dz-preview dz-file-preview mb-3"><div class="d-flex flex-row "><div class="p-0 w-30 position-relative"><div class="dz-error-mark"><span><i></i></span></div><div class="dz-success-mark"><span><i></i></span></div><div class="preview-container"><img data-dz-thumbnail class="img-thumbnail border-0" /><i class="simple-icon-doc preview-icon" ></i></div></div><div class="pl-3 pt-2 pr-2 pb-1 w-70 dz-details position-relative"><div><span data-dz-name></span></div><div class="text-primary text-extra-small" data-dz-size /><div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div><div class="dz-error-message"><span data-dz-errormessage></span></div></div></div><a href="#" class="remove" data-dz-remove><i class="glyph-icon simple-icon-trash"></i></a></div>'
            });
            $(".coloured_photo").dropzone({
                url: "{{route('uploadimg')}}",
                dictResponseError: 'Cannot accept this type file',
                acceptedFiles: ".jpg, .jpeg, .pdf, .png",
                dictDefaultMessage: "Drop files here to upload <br> format (JPG/JPEG/PDF/PNG)",
                init: function() {
                    var drop = this;
                    this.on('sending', function() {
                        $('#div-upload-coloured-photo').show();
                        $('.progress-coloured-photo').width(`0%`);
                        $('.progress-percent-coloured-photo').text(`0 %`);
                    });
                    this.on('canceled', function() {
                        $('.progress-coloured-photo').width(`0%`);
                        $('.progress-percent-coloured-photo').text(`0 %`);
                    });
                    this.on("totaluploadprogress", function(progress) {
                        var fixprogress = 0;
                        if (progress < 100) {
                            fixprogress = (progress).toFixed(2);
                        } else {
                            fixprogress = 100;
                        }
                        $('.progress-coloured-photo').width(`${fixprogress}%`);
                        $('.progress-percent-coloured-photo').text(`${fixprogress} %`);
                    });
                    this.on("success", function(file, responseText) {
                        colouredPhoto_imgUrl = responseText.Data;
                        $('#coloured_photo').val(responseText.Data);
                        coloured_photo = true;
                        savekyc();
                    });
                    this.on("error", function(file, error) {
                        console.log(error);
                        swal("Info", error, "info");
                        // i remove current file
                        drop.removeFile(file);
                        $('.progress-coloured-photo').width(`0%`);
                        $('.progress-percent-coloured-photo').text(`0 %`);
                    });
                },
                thumbnailWidth: 160,
                previewTemplate: '<div class="dz-preview dz-file-preview mb-3"><div class="d-flex flex-row "><div class="p-0 w-30 position-relative"><div class="dz-error-mark"><span><i></i></span></div><div class="dz-success-mark"><span><i></i></span></div><div class="preview-container"><img data-dz-thumbnail class="img-thumbnail border-0" /><i class="simple-icon-doc preview-icon" ></i></div></div><div class="pl-3 pt-2 pr-2 pb-1 w-70 dz-details position-relative"><div><span data-dz-name></span></div><div class="text-primary text-extra-small" data-dz-size /><div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div><div class="dz-error-message"><span data-dz-errormessage></span></div></div></div><a href="#" class="remove" data-dz-remove><i class="glyph-icon simple-icon-trash"></i></a></div>'
            });
            $(".specimen_signature").dropzone({
                url: "{{route('uploadimg')}}",
                dictResponseError: 'Cannot accept this type file',
                acceptedFiles: ".jpg, .jpeg, .pdf, .png",
                dictDefaultMessage: "Drop files here to upload <br> format (JPG/JPEG/PDF/PNG)",
                init: function() {
                    var drop = this;
                    this.on('sending', function() {
                        $('#div-upload-specimen-signature').show();
                        $('.progress-specimen-signature').width(`0%`);
                        $('.progress-percent-specimen-signature').text(`0 %`);
                    });
                    this.on('canceled', function() {
                        $('.progress-specimen-signature').width(`0%`);
                        $('.progress-percent-specimen-signature').text(`0 %`);
                    });
                    this.on("totaluploadprogress", function(progress) {
                        var fixprogress = 0;
                        if (progress < 100) {
                            fixprogress = (progress).toFixed(2);
                        } else {
                            fixprogress = 100;
                        }
                        $('.progress-specimen-signature').width(`${fixprogress}%`);
                        $('.progress-percent-specimen-signature').text(`${fixprogress} %`);
                    });
                    this.on("success", function(file, responseText) {
                        console.log('specimen_signature', responseText.Data);
                        specimenSignature_imgUrl = responseText.Data;
                        specimen_signature = true;
                        $('#specimen_signature').val(responseText.Data);
                        savekyc();
                    });
                    this.on("error", function(file, error) {
                        swal("Info", error, "info");
                        // i remove current file
                        drop.removeFile(file);
                        $('.progress-specimen-signature').width(`0%`);
                        $('.progress-percent-specimen-signature').text(`0 %`);
                    });
                },
                thumbnailWidth: 160,
                previewTemplate: '<div class="dz-preview dz-file-preview mb-3"><div class="d-flex flex-row "><div class="p-0 w-30 position-relative"><div class="dz-error-mark"><span><i></i></span></div><div class="dz-success-mark"><span><i></i></span></div><div class="preview-container"><img data-dz-thumbnail class="img-thumbnail border-0" /><i class="simple-icon-doc preview-icon" ></i></div></div><div class="pl-3 pt-2 pr-2 pb-1 w-70 dz-details position-relative"><div><span data-dz-name></span></div><div class="text-primary text-extra-small" data-dz-size /><div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div><div class="dz-error-message"><span data-dz-errormessage></span></div></div></div><a href="#" class="remove" data-dz-remove><i class="glyph-icon simple-icon-trash"></i></a></div>'
            });
            // $(".bank_statement").dropzone({
            //     url: "{{route('uploadimg')}}",
            //     init: function() {
            //         this.on("success", function(file, responseText) {
            //             bankStatement_imgUrl = responseText.Data;
            //             bank_statement = true;
            //             $('#bank_statement').val(responseText.Data);
            //             savekyc();
            //         });
            //         this.on("error", function(error) {
            //             console.log(error);
            //         });
            //     },
            //     thumbnailWidth: 160,
            //     previewTemplate: '<div class="dz-preview dz-file-preview mb-3"><div class="d-flex flex-row "><div class="p-0 w-30 position-relative"><div class="dz-error-mark"><span><i></i></span></div><div class="dz-success-mark"><span><i></i></span></div><div class="preview-container"><img data-dz-thumbnail class="img-thumbnail border-0" /><i class="simple-icon-doc preview-icon" ></i></div></div><div class="pl-3 pt-2 pr-2 pb-1 w-70 dz-details position-relative"><div><span data-dz-name></span></div><div class="text-primary text-extra-small" data-dz-size /><div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div><div class="dz-error-message"><span data-dz-errormessage></span></div></div></div><a href="#" class="remove" data-dz-remove><i class="glyph-icon simple-icon-trash"></i></a></div>'
            // });
            $(".other_image").dropzone({
                url: "{{route('uploadimg')}}",
                dictResponseError: 'Cannot accept this type file',
                acceptedFiles: ".jpg, .jpeg, .pdf, .png",
                dictDefaultMessage: "Drop files here to upload <br> format (JPG/JPEG/PDF/PNG)",
                init: function() {
                    var drop = this;
                    this.on('sending', function() {
                        $('#div-upload-other-image').show();
                        $('.progress-other-image').width(`0%`);
                        $('.progress-percent-other-image').text(`0 %`);
                    });
                    this.on('canceled', function() {
                        $('.progress-other-image').width(`0%`);
                        $('.progress-percent-other-image').text(`0 %`);
                    });
                    this.on("totaluploadprogress", function(progress) {
                        var fixprogress = 0;
                        if (progress < 100) {
                            fixprogress = (progress).toFixed(2);
                        } else {
                            fixprogress = 100;
                        }
                        $('.progress-other-image').width(`${fixprogress}%`);
                        $('.progress-percent-other-image').text(`${fixprogress} %`);
                    });
                    this.on("success", function(file, responseText) {
                        other_imgUrl = responseText.Data;
                        other_image = true;
                        $('#other_image').val(responseText.Data);
                        savekyc();
                    });
                    this.on("error", function(file, error) {
                        swal("Info", error, "info");
                        // i remove current file
                        drop.removeFile(file);
                        $('.progress-other-image').width(`0%`);
                        $('.progress-percent-other-image').text(`0 %`);
                    });
                },
                thumbnailWidth: 160,
                previewTemplate: '<div class="dz-preview dz-file-preview mb-3"><div class="d-flex flex-row "><div class="p-0 w-30 position-relative"><div class="dz-error-mark"><span><i></i></span></div><div class="dz-success-mark"><span><i></i></span></div><div class="preview-container"><img data-dz-thumbnail class="img-thumbnail border-0" /><i class="simple-icon-doc preview-icon" ></i></div></div><div class="pl-3 pt-2 pr-2 pb-1 w-70 dz-details position-relative"><div><span data-dz-name></span></div><div class="text-primary text-extra-small" data-dz-size /><div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div><div class="dz-error-message"><span data-dz-errormessage></span></div></div></div><a href="#" class="remove" data-dz-remove><i class="glyph-icon simple-icon-trash"></i></a></div>'
            });
        }
    });

    function savekyc() {
        var contentDt = getContent();
        if (contentDt.account_type === "reguler") {
            contentDt.account_type = "regular";
        }
        const content = JSON.stringify(contentDt)
        $.post("{{route('savekyc')}}", {
            content,
            NBCRMWEBAPI: $('[name="NBCRMWEBAPI"]').val(),
            USERID: $('[name="USERID"]').val(),
        }, function(data, status) {
            var {
                Code,
                Message
            } = data;
            if (data.Data) {
                var Data = data.Data;
                var MTUserRef = Data.MTUserRef;
                var idx = MTUserRef.findIndex(x => x.IsSimulate === false);
                if (MTUserRef[idx]) {
                    var liveAcc = MTUserRef[idx];
                    $('[data-bind="ac_live_no"]').text(liveAcc.Login);
                }
            }
        });
    };

    setTimeout(function() {
        $('.removeimg').on('click', function() {
            var getMsg = $($($(this).parents()[1]).siblings())[1];
            $(getMsg).show();
            $(`.${$(this).attr('data-dz-remove')}_preview`).remove();
            $(`#${$(this).attr('data-dz-remove')}`).val('');
            savekyc();
        })
    }, 2000)

    $('[name="truth_resposibility_agreement"]').on('change', function(event) {
        console.log(event.currentTarget.value);
        var val = event.currentTarget.value;
        if (val === 'no') {
            // $('.next-btn').prop('disabled',true);
            $('#truth_resposibility_agreement-error').text('Klik "Ya" untuk melanjutkan.');
            $('#truth_resposibility_agreement-error').show();

        } else {
            // $('.next-btn').prop('disabled',false);
            $('#truth_resposibility_agreement-error').hide();
        }
    });
    setTimeout(function() {
        $('.next-btn').on('click', function() {

            if (!$('#form-step-1').valid()) {
                var errors = $('.error:visible');
                $(errors[0]).focus();
                // swal("Perhatian!", "Untuk melanjutkan anda perlu mengisi form dengan benar dan lengkap", "warning");
            } else if ($('#id_card').val() === "") {
                swal("Perhatian!", "Untuk melanjutkan anda perlu melampirkan Fotokopi KTP/SIM/KITAS/PASSPOR", "warning");
            } else if ($('#coloured_photo').val() === "") {
                swal("Perhatian!", "Untuk melanjutkan anda perlu melampirkan Pasfoto Berwarna", "warning");
            } else if ($('#specimen_signature').val() === "") {
                swal("Perhatian!", "Untuk melanjutkan anda perlu melampirkan Tanda Tangan Spesimen", "warning");
            }
            // else if ($('#bank_statement').val() === "") {
            //     swal("Perhatian!", "Untuk melanjutkan anda perlu melampirkan Cover Buku Tabungan", "warning");
            // } 
            else if ($('#other_image').val() === "") {
                swal("Perhatian!", "Untuk melanjutkan anda perlu melampirkan lampiran pendukung lainnya", "warning");
            } else if ($($("input[name='truth_resposibility_agreement']:checked")).val() === 'no') {
                $('#truth_resposibility_agreement-error').show();
                swal("Perhatian!", "Untuk melanjutkan anda perlu mengklik 'Ya' pada PERNYATAAN KEBENARAN DAN TANGGUNG JAWAB", "warning");
            } else if ($($("input[name='truth_resposibility_agreement']:checked")).val() !== 'yes') {
                $('#truth_resposibility_agreement-error').text('Klik "Ya" untuk melanjutkan.');
                $('#truth_resposibility_agreement-error').show();
            } else {
                if ($('#form-step-1').valid()) {
                    $('.next-btn').hide();
                    $('#btn-loading').show();
                    $.post("{{route('postNextStepOpenLiveAccount')}}", {
                        NBCRMWEBAPI: $('[name="NBCRMWEBAPI"]').val(),
                        USERID: $('[name="USERID"]').val(),
                        nextStep: $('[name="nextStep"]').val(),
                    }, function(data, status) {
                        if (status === "success") {
                            location.reload();
                        } else {
                            $('.next-btn').show();
                            $('#btn-loading').hide();
                        }
                    });
                }
            }
        });

        $('.previous-btn').on('click', function() {
            $.post("{{route('postNextStepOpenLiveAccount')}}", {
                NBCRMWEBAPI: $('[name="NBCRMWEBAPI"]').val(),
                USERID: $('[name="USERID"]').val(),
                nextStep: '1',
            }, function(data, status) {
                if (status === "success") {
                    location.reload();
                }
            });
        });

    }, 1000);

    // Initialize Smart Wizard 
    //   $('#smartWizardValidation').smartWizardValidation();

    // Initialize the leaveStep event
    //   $("#smartWizardValidation").on("leaveStep", function(e, anchorObject, stepNumber, stepDirection) {
    //      return confirm("Do you want to leave the step "+stepNumber+"?");
    //   });

    // Initialize the showStep event
    getDetailKYC();
    // $("#smartWizardValidation").on("showStep", function(e, anchorObject, stepNumber, stepDirection) {
    //     atvStep = stepNumber;
    //     localStorage.setItem('activestepopnliveacc', stepNumber);
    //     if (stepDirection !== "") {
    //         savekyc();
    //     }
    //     if (stepNumber === 4) {
    //         addliveaccount();
    //     }
    //     getDetailKYC();
    // });

    //   // Initialize the beginReset event
    //   $("#smartWizardValidation").on("beginReset", function(e) {
    //  return confirm("Do you want to reset the wizard?");
    //   });

    //   // Initialize the endReset event
    //   $("#smartWizardValidation").on("endReset", function(e) {
    //  alert("endReset called");
    //   });  

    //   // Initialize the themeChanged event
    //   $("#smartWizardValidation").on("themeChanged", function(e, theme) {
    //  alert("Theme changed. New theme name: " + theme);
    //   });





    $(function() {
        $("#form-step-1").validate({
            ignore: [],
            // errorElement: "div",
            rules: {
                tax_file_number: {
                    required: true
                },
            },
            messages: {},
            submitHandler: function() {
                console.log('ini ya')
                // do other things for a valid form
                // form.submit();
            }
        });


    });
    var renderImgAttc = false;
    var renderImgAttcOther = false;
    var minAge = new Date();
    var getYear = minAge.getFullYear();
    minAge.setFullYear(getYear - 21);
    var dd = minAge.getDate();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    var mm = minAge.getMonth() + 1; //January is 0!
    var yyyy = minAge.getFullYear();
    var minDate = yyyy + '-' + mm + '-' + dd;
    $('[name="date_of_birth"]').prop('max', minDate);


    var atvStep = "{{$DataContent->activeStep?$DataContent->activeStep:0}}";
    localStorage.setItem('activestepopnliveacc', atvStep - 1);
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    const type = sessionStorage.getItem("register_account");
    if (type === "miniaccount") {
        $('.account_type_mini').prop('checked', true);
        $('.forex_idr').prop('checked', true);
        $('[name="future_contract"]').val('forex_idr');
        $('[name="account_type"]').val('mini');
    } else if (type === "usdaccount") {
        $('.account_type_reguler').prop('checked', true);
        $('.forex_usd').prop('checked', true);
        $('[name="future_contract"]').val('forex_usd');
        $('[name="account_type"]').val('regular');
    } else {
        // profesional
        $('.account_type_reguler').prop('checked', true);
        $('.forex_idr').prop('checked', true);
        $('[name="future_contract"]').val('forex_idr');
        $('[name="account_type"]').val('regular');
    }

    $('[name="future_contract_opt"]').on('change', function(event) {
        const val = event.currentTarget.value;
        $('[name="future_contract"]').val(val);
    });

    var position_value = "{{isset($DataContent->job_details)?$DataContent->job_details->position->value:''}}";
    if (position_value !== "other") {
        $('#position_value_div').hide();
    } else {
        $('[name="position_input"]').prop('required', true);
    }
    $('[name="position_value"]').on('click', function(event) {
        const value = event.currentTarget.value;
        if (value === "other") {
            $('#position_value_div').show();
            $('[name="position_input"]').prop('required', true);
        } else {
            $('#position_value_div').hide();
            $('[name="position_input"]').removeAttr('required');
        }
    });

    var home_ownership_status = "{{isset($DataContent->home_ownership_status)?$DataContent->home_ownership_status->value:''}}";
    if (home_ownership_status !== "other") {
        $('#houseownstatusdescriptiondiv').hide();
    } else {
        $('[name="houseownstatusdescription"]').prop('required', true);
    }
    $('[name="houseownstatusOptions"]').on('click', function(event) {
        const value = event.currentTarget.value;
        if (value === "other") {
            $('#houseownstatusdescriptiondiv').show();
            $('[name="houseownstatusdescription"]').prop('required', true);
        } else {
            $('#houseownstatusdescriptiondiv').hide();
            $('[name="houseownstatusdescription"]').removeAttr('required');
            $('#houseownstatusdescription-error').remove();
        }
    });

    var openint_account_purpose = "{{isset($DataContent->openint_account_purpose)?$DataContent->openint_account_purpose->value:''}}";
    if (openint_account_purpose !== "other") {
        $('[name="openint_account_purpose_input"]').hide();
    } else {
        $('[name="openint_account_purpose_input"]').prop('required', true);
    }
    $('[name="openint_account_purpose"]').on('change', function() {
        const val = $(this).val();
        if (val === 'other') {
            $('[name="openint_account_purpose_input"]').show();
            $('[name="openint_account_purpose_input"]').prop('required', true);
        } else {
            $('[name="openint_account_purpose_input"]').hide();
            $('[name="openint_account_purpose_input"]').removeAttr('required');
            $('#openint_account_purpose_input-error').remove();

        }
    });

    var experince_on_investment_input = "{{isset($DataContent->experince_on_investment)?$DataContent->experince_on_investment->value:''}}";
    if (experince_on_investment_input !== "Yes") {
        $('[name="experince_on_investment_input"]').hide();
    } else {
        $('[name="experince_on_investment_input"]').prop('required', true);
    }
    $('[name="experince_on_investment"]').on('change', function() {
        const val = $(this).val();
        if (val === 'Yes') {
            $('[name="experince_on_investment_input"]').show();
            $('[name="experince_on_investment_input"]').prop('required', true);
        } else {
            $('[name="experince_on_investment_input"]').hide();
            $('[name="experince_on_investment_input"]').removeAttr('required');
        }
    });

    var is_family_work_in_related_company = "{{isset($DataContent->is_family_work_in_related_company)?$DataContent->is_family_work_in_related_company->value:''}}";
    if (is_family_work_in_related_company !== "Yes") {
        $('[name="is_family_work_in_related_company_input"]').hide();
    } else {
        $('[name="is_family_work_in_related_company_input"]').prop('required', true);
    }
    $('[name="is_family_work_in_related_company"]').on('change', function() {
        const val = $(this).val();
        if (val === 'Yes') {
            $('[name="is_family_work_in_related_company_input"]').show();
            $('[name="is_family_work_in_related_company_input"]').prop('required', true);
        } else {
            $('[name="is_family_work_in_related_company_input"]').hide();
            $('[name="is_family_work_in_related_company_input"]').removeAttr('required');

        }
    });

    var withdrwltypeaccbank = "{{isset($DataContent->bank_details_for_withdrawls)?$DataContent->bank_details_for_withdrawls->account_type->value:''}}";
    if (withdrwltypeaccbank !== "other") {
        $('[name="withdrwltypeaccbankinput"]').hide();
    } else {
        $('[name="withdrwltypeaccbankinput"]').prop('required', true);
    }
    $('[name="withdrwltypeaccbank"]').on('change', function() {
        const val = $(this).val();
        if (val === 'other') {
            $('[name="withdrwltypeaccbankinput"]').show();
            $('[name="withdrwltypeaccbankinput"]').prop('required', true);
        } else {
            $('[name="withdrwltypeaccbankinput"]').hide();
            $('[name="withdrwltypeaccbankinput"]').removeAttr('required');
        }
    });
</script>
@endsection