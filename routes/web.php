<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

 Route::get('/lab', 'ApiController@lab')->name('lab');
 Route::get('/labs', 'ApiController@lab2')->name('lab2');
 Route::get('/lab3', 'ApiController@lab3')->name('lab3');
 Route::get('/redirect', 'PageController@redirect')->name('redirect');

Route::middleware('cookie')->group(function () {
    Route::get('/', function () {
        return redirect()->route('login');
    });
});
Route::get('/api/getnotification','ApiController@getnotification')->name('apigetnotification');

Route::get('/register', 'PageController@register')->name('formregister');
Route::post('/save/register', 'ApiController@save')->name('save');
Route::get('/register-verification', 'ApiController@sendVerificationCodeRegistration')->name('getRegisterVerification');
Route::post('/register-verification', 'ApiController@adddemouser')->name('postRegisterVerification');

Route::get('/login','PageController@login')->name('login');
Route::post('/maxco/login','ApiController@login')->name('postLogin');
Route::get('/login-from-web','PageController@loginfromweb')->name('loginfromweb');


Route::get('/forgot-password','PageController@forgotpassword')->name('forgotpasswordform');
Route::post('/send-code-forgot-password','ApiController@sendVerificationCodeForgotPass')->name('sendVerificationCodeForgotPass');
Route::post('/forgot-password','ApiController@forgotPassword')->name('postForgotPassword');

Route::get('/dashboard','PageController@dashboard')->name('dashboard');

Route::get('/profile','ApiController@profile')->name('dashboardProfile');


Route::get('/change-password', 'PageController@changepassword')->name('changepassword');
Route::post('/change-password','ApiController@changepassword')->name('postChangePassword');

Route::get('/open-live-account','ApiController@getKycDetail')->name('openLiveAccount');
Route::post('/next-step-open-live-account','ApiController@postNextStepOpenLiveAccount')->name('postNextStepOpenLiveAccount');
Route::get('/api/getKycDetail','ApiController@apigetKycDetail')->name('apigetKycDetail');

Route::post('/savekyc','ApiController@savekyc')->name('savekyc');
Route::post('/addliveaccount','ApiController@addliveaccount')->name('addliveaccount');

Route::get('/withdrawal','ApiController@withdrawal')->name('historywithdrawal');
Route::get('/withdrawal-request','PageController@withdrawalrequest');
Route::post('/withdrawal-request-post','ApiController@withdrawalrequest')->name('postWithdrawal');

Route::get('/deposit','ApiController@deposit')->name('historydeposit');
Route::get('/deposit-request','ApiController@depositrequestForm');
Route::post('/deposit-request-post','ApiController@depositrequest')->name('postdepositrequest');

Route::post('/upload-img','ApiController@uploadimg')->name('uploadimg');

Route::get('/session','PageController@session');

// ---------------------------------------

Route::get('/documents', function () {
    return view('pages.auth.documents');
})->name('documents');

Route::get('/live-account','ApiController@liveaccountproduct')->name('liveaccountproduct');

Route::get('/demo-account', function () {
    return view('pages.auth.demoaccount');
});

Route::get('/open-demo-account', function () {
    return view('pages.auth.opendemoaccount');
});

Route::get('/bank-account', function () {
    return view('pages.auth.bankaccount');
});

Route::get('/trading-contest-register', function () {
    return view('pages.auth.tradingcontestregister');
})->name('trading-contest-register');

Route::get('/trading-contest-account', function () {
    return view('pages.auth.tradingcontestaccount');
})->name('trading-contest-account');

Route::get('/trading-contest-standing', function () {
    return view('pages.auth.tradingconteststanding');
})->name('trading-contest-standing');

Route::get('/my-poin', function () {
    return view('pages.auth.mypoin');
})->name('my-poin');

Route::get('/redeem', function () {
    return view('pages.auth.redeem');
})->name('redeem');

Route::get('/history', function () {
    return view('pages.auth.history');
})->name('history');

Route::get('/open-account', function () {
    return view('index');
});

Route::get('/sign-out', 'PageController@signout');


Route::get('/lab-alert', 'LabController@alert')->name('labalert');
