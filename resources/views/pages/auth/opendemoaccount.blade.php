@extends('templates.master')

@section('csslinkonhead')
<link rel="stylesheet" href="{{url('/')}}/css/vendor/select2.min.css" />
<link rel="stylesheet" href="{{url('/')}}/css/vendor/select2-bootstrap.min.css" />
@endsection

@section('content')
<main id="app">
   <open-demoaccount />
</main>

@endsection

@section('jsonpage')
<script src="{{url('/')}}/js/vendor/select2.full.js"></script>
@endsection