@extends('templates.master')

@section('csslinkonhead')
<link rel="stylesheet" href="{{url('/')}}/css/vendor/select2.min.css" />
<link rel="stylesheet" href="{{url('/')}}/css/vendor/select2-bootstrap.min.css" />
@endsection

@section('content')
<main id="app">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <h1>Bank Account</h1>
                <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                    <ol class="breadcrumb pt-0">
                        <!-- <li class="breadcrumb-item">
                            <a href="#">Home</a>
                        </li> -->
                        <!-- <li class="breadcrumb-item">
                            <a href="#">Library</a>
                        </li> -->
                        <li class="breadcrumb-item active" aria-current="page">Data</li>
                    </ol>
                </nav>
                <div class="separator mb-5"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-lg-5 col-xl-4 col-left mt-5">
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="text-center pt-4">
                            <p class="list-item-heading pt-2">Bank Account</p>
                        </div>

                        <p class="text-muted text-small mb-2">Description</p>
                        <p class="mb-3">Description about how to make bank transfer, what bank/payment method is
                            acceptable
                        </p>


                    </div>
                </div>

            </div>
            <div class="col-12 col-lg-6 col-xl-6 col-left mt-5">
                <div class="card">
                    <div class="card-body">
                        <div class="card-title ">
                            <h5>
                                Bank Account</h5>
                            <p class="text-muted mb-2">Please Input Your Bank Account Information</p>
                        </div>
                        <div class="dashboard-quick-post">
                            <form class="needs-validation" novalidate>
                               
                                <div class="form-group row tooltip-right-top">
                                    <label class="col-sm-3 col-form-label">Bank Name</label>
                                    <div class="col-sm-9">
                                        <input type="email" class="form-control" placeholder="Please input bank name" required>
                                        <div class="invalid-tooltip">
                                            Bank Name is required!
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row tooltip-right-top">
                                    <label class="col-sm-3 col-form-label">Account Number </label>
                                    <div class="col-sm-9">
                                        <input type="email" class="form-control"
                                            placeholder="Please input account number" required>
                                            <div class="invalid-tooltip">
                                                Account Number is required!
                                            </div>
                                    </div>
                                </div>
                                <div class="form-group row tooltip-right-top">
                                    <label class="col-sm-3 col-form-label">Account Name</label>
                                    <div class="col-sm-9">
                                        <input type="email" class="form-control"
                                            placeholder="Please input account name" required>
                                            <div class="invalid-tooltip">
                                                Account Name is required!
                                            </div>
                                    </div>
                                </div>
                                <div class="form-group row mb-0 text-right">
                                    <div class="col-sm-12">
                                        <button type="submit"
                                            class="btn btn-primary btn-lg btn-shadow btn-maxco-blue borderradius-0">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</main>
@endsection

@section('jsonpage')
<script src="{{url('/')}}/js/vendor/select2.full.js"></script>
<script src="{{url('/')}}/js/vendor/jquery.validate/jquery.validate.min.js"></script>
    <script src="{{url('/')}}/js/vendor/jquery.validate/additional-methods.min.js"></script>
@endsection