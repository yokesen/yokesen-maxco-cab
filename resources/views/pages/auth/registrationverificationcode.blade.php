<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-BMTCL2Z4WL"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'G-BMTCL2Z4WL');
    </script>
    <meta charset="UTF-8">
    <title>Maxco Futures | Prestigious Global Brokerage House</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="stylesheet" href="{{url('/')}}/cabinet/font/iconsmind-s/css/iconsminds.css" />
    <link rel="stylesheet" href="{{url('/')}}/cabinet/font/simple-line-icons/css/simple-line-icons.css" />

    <link rel="stylesheet" href="{{url('/')}}/cabinet/css/vendor/bootstrap.min.css" />
    <link rel="stylesheet" href="{{url('/')}}/cabinet/css/vendor/bootstrap.rtl.only.min.css" />
    <link rel="stylesheet" href="{{url('/')}}/cabinet/css/vendor/bootstrap-float-label.min.css" />
    <link rel="stylesheet" href="{{url('/')}}/cabinet/css/main.css" />
</head>

<body class="background show-spinner no-footer">
    <div class="fixed-background" style="background:url({{url('/')}}/cabinet/img/webpage/background.jpg)"></div>
    <main>
        <div class="container">
            <div class="row h-100">
                <div class="col-12 col-md-10 mx-auto my-auto">
                    <div class="card auth-card">
                        <div class="position-relative image-side" style="background:url({{url('/')}}/cabinet/img/webpage/background-kecil-final.jpg)">
                            <p class="h2 text-white">MAXCO FUTURES<br> OPEN ACCOUNT</p>
                            <p class="mb-0 text-white">
                                Please use this form to register.
                                <br>If you are a member, please
                                <a href="{{route('login')}}" class="white">login</a>
                            </p>
                        </div>
                        <div class="form-side">
                            <a href="#">
                                <span class="logo-single" style="background:url({{url('/')}}/cabinet/img/webpage/logo_maxco_W300px.png);background-size: cover;width: 300px;height: 66px;"></span>
                            </a>
                            <h6 class="mb-4">Verification Code - Register</h6>

                            <form id="verificationcode-form" method="post" action="{{route('postRegisterVerification')}}">
                                @csrf
                                @if (session('ErrorMessage'))
                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                    <strong>Error!</strong> {{ session('ErrorMessage') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                @endif
                                <div class="justify-content-end align-items-center">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label class="form-group has-float-label mb-4">
                                                <input class="form-control" type="text" name="VerifyCode" required />
                                                <span>Verify Code</span>
                                                <label id="VerifyCode-error" class="error" for="VerifyCode" style="top: 33px;position: absolute;color: red;opacity: 2;background: rgb(255, 255, 255);left: 0.75rem;"></label>
                                            </label>
                                        </div>
                                        <div class="col-sm-6">
                                            <button class="btn btn-primary btn-shadow btn-maxco-blue" type="submit" style="border-radius: 0px;background-color: #017dc7 !important;">REGISTER</button>
                                            <button id="btn-loading" onclick="event.preventDefault();" class="btn btn-primary btn-lg btn-shadow hidden" style="border-radius: 0px;background-color: #017dc7 !important;"><span class="loader"></span>REGISTER</button>
                                        </div>
                                        <div class="col-md-12 text-center">
                                            <div class="alert alert-info">
                                                <span>Verification code has been sent to phone</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <script src="{{url('/')}}/cabinet/js/vendor/jquery-3.3.1.min.js"></script>
    <script src="{{url('/')}}/cabinet/js/vendor/jquery.validate/jquery.validate.min.js"></script>
    <script src="{{url('/')}}/cabinet/js/vendor/bootstrap.bundle.min.js"></script>
    <script src="{{url('/')}}/cabinet/js/dore.script.js"></script>
    <script src="{{url('/')}}/cabinet/js/scripts.js"></script>
    <script src="{{url('/')}}/cabinet/data/nationality.js"></script>

    <script>
        $(function() {
            var optionAreaCd = "";
            // nationalityDt.map(function(item) {
            //     const dt = `<option value="${item.AreaCode}">+${item.AreaCode}</option>`;
            //     optionAreaCd += dt;
            // });
            // $('#areaCode').append(optionAreaCd)

            var NationalityDt = ""
            nationalityDt.map(function(item) {
                const dt = `<option value="${item.value}">${item.text}</option>`;
                NationalityDt += dt;
            });
            $('#Nationality').append(NationalityDt)

            $('#Nationality').val('Indonesia');
            $('#areaCodeTxt').val('+ 62');
            $('#AreaCode').val('62');

            $('#Nationality').on('change', function(event) {
                const value = event.currentTarget.value;
                const areaCd = nationalityDt.find(x => x.value === value).AreaCode || '';
                $('#areaCodeTxt').val('+ ' + areaCd);
                $('#AreaCode').val(areaCd);
            })
            $("#verificationcode-form").validate({
                ignore: [],
                // errorElement: "div",
                rules: {
                    VerifyCode:{
                        required: true
                    }
                },
                messages: {
                    VerifyCode: {
                        required: "Masukkan kode verifikasi yang telah dikirimkan."
                    },
                }
            });

            $('[type="submit"]').on('click', function() {
                if ($('#verificationcode-form').valid()) {
                    $('[type="submit"]').hide();
                    $('#btn-loading').show();
                }
            });

        });
    </script>
</body>

</html>