@extends('templates.master')

@section('csslinkonhead')
<link rel="stylesheet" href="{{url('/')}}/cabinet/css/vendor/smart_wizard.min.css" />
<link rel="stylesheet" href="{{url('/')}}/cabinet/css/vendor/dropzone.min.css" />
<meta name="csrf-token" content="{{ csrf_token() }}" />
@if($agent->isMobile())
<style>
    .sw-main.sw-theme-check>ul.step-anchor>li>a:after,
    .sw-main.sw-theme-check>ul.step-anchor>li.active>a:after,
    .sw-main.sw-main.sw-theme-check>ul.step-anchor>li.done>a:after {
        margin-top: 42px;
    }
</style>
@endif
@endsection

@section('content')
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <h1>Opening Account Wizard</h1>
                <div class="separator mb-5"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 mb-4">
                <!-- <h5 class="mb-4">Open Live Account</h5> -->
                <div class="card mb-4">
                    <div id="" class="sw-main sw-theme-check">
                        <ul class="card-header mb-4 nav nav-tabs step-anchor" style="z-index: 0;">
                            <li class="nav-item  done">
                                <a class="nav-link">
                                    @if(!$agent->isMobile())
                                    Step 1
                                    <br>
                                    @endif
                                </a>
                            </li>
                            <li class="nav-item done">
                                <a class="nav-link">
                                    @if(!$agent->isMobile())
                                    Step 2
                                    <br>
                                    @endif
                                </a>
                            </li>
                            <li class="nav-item active">
                                <a class="nav-link">
                                    @if(!$agent->isMobile())
                                    Step 3
                                    <br>
                                    @endif
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#step-3" class="nav-link">
                                    @if(!$agent->isMobile())
                                    Step 4
                                    <br>
                                    @endif
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#step-4" class="nav-link">
                                    @if(!$agent->isMobile())
                                    Step 5
                                    <br>
                                    @endif
                                </a>
                            </li>
                        </ul>

                        <div class="card-body">
                            <div id="step-2">
                                <form id="form-step-3-1" method="post" action="{{route('postNextStepOpenLiveAccount')}}" class="tooltip-label-right" novalidate style="padding: 0px 22px;">
                                    <input name="NBCRMWEBAPI" type="hidden" value="{{$UserToken}}" />
                                    <input name="USERID" type="hidden" value="{{$UserId}}" />
                                    @csrf

                                    <h3>
                                        Ketentuan Penyajian Profil Perusahaan Pialang Berjangka - Formulir Nomor
                                        107.PBK.01
                                    </h3>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="alert alert-dark" role="alert">PROFIL PERUSAHAAN PIALANG BERJANGKA</div>

                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Nama</label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">PT. Maxco Futures</div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Alamat</label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">
                                                        Gedung Panin Pusat Lantai Dasar
                                                        <br />Jl. Jend. Sudirman Kav. 1. Senayan, Jakarta 10270
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">No Telepon</label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">021-7205868 (Hunting)</div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Faksimili</label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">021-5710974</div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Email</label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">cs@maxcofutures.co.id</div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Home Page</label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">www.maxcofutures.co.id</div>
                                                </div>
                                            </div>
                                            <div class="alert alert-dark" role="alert">Susunan Pengurus PT MAXCO FUTURES</div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Direktur Utama</label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">Muhammad Yanwar Pohan</div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Direktur</label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">Ade Manuel Ortega</div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Direktur Kepatuhan</label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">Rudi Anto, S, Sos</div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Komisaris Utama</label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">Ronald H.Lalamentik</div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Komisaris</label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">Tofvin</div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">
                                                    Susunan Pemegang Saham
                                                    Perusahaan
                                                </label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">
                                                        Ronald H.Lalamentik
                                                        <br />Tofvin
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">
                                                    Nomor dan Tanggal Izin Usaha
                                                    dari
                                                    Bappebti
                                                </label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">353/BAPPEBTI/SI/IV/2004 – Tanggal 30 April 2004</div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">
                                                    Nomor dan Tanggal Keanggotaan
                                                    Bursa Berjangka
                                                </label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">SPAB-057/BBJ/12/03 – Tanggal 30 Desember 2003</div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">
                                                    Nomor dan Tanggal Keanggotaan
                                                    Lembaga Kliring
                                                    Berjangka
                                                </label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">23/AK-KBI/V/2004 – Tanggal 16 November 2014</div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">
                                                    Nomor dan Tanggal Persetujuan
                                                    sebagai Peserta Sistem
                                                    Perdagangan Alternatif
                                                </label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">1220/BAPPEBTI/SP/5/2007 – Tanggal 31 Mei 2007</div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">
                                                    Nama Penyelengara Sistem
                                                    Perdagangan Alternatif
                                                </label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">PT. PAN EMPEROR</div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">
                                                    Kontrak Berjangka Yang
                                                    Diperdagangkan *)
                                                </label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext"></div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">
                                                    Kontrak Derivatif Syariah
                                                    Yang
                                                    Diperdagangkan *)
                                                </label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="alert alert-dark" role="alert">Kontrak Derivatif dalam Sistem Perdangangan Alternatif *)</div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Forex</label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">
                                                        GBP/USD, EUR/USD, AUD/USD, NZD/USD, USD/JPY, USD/CHF,
                                                        USD/CAD
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Cross Rate</label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">
                                                        GBP/JPY, GBP/AUD, EUR/AUD, AUD/JPY, AUD/NZD, EUR/CHF,
                                                        EUR/CAD,
                                                        Etc
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Loco</label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">London Gold & Precious Metals</div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Crude Oil</label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">NYMEX crude Oil</div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Stock Indexes</label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">Dow Jones, S&P500, Nasdaq, Indeks Saham Hong Kong, etc</div>
                                                </div>
                                            </div>

                                            <div class="alert alert-dark" role="alert">
                                                Kontrak Derivatif dalam Sistem Perdagangan Alternatif dengan volume
                                                minimum 0,1
                                                (nol koma satu) lot yang Diperdagangkan *)
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Forex</label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">
                                                        GBP/USD, EUR/USD, AUD/USD, NZD/USD, USD/JPY, USD/CHF,
                                                        USD/CAD
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Cross Rate</label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">
                                                        GBP/JPY, GBP/AUD, EUR/AUD, AUD/JPY, AUD/NZD, EUR/CHF,
                                                        EUR/CAD,
                                                        Etc
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Loco</label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">London Gold & Precious Metals</div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Crude Oil</label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">NYMEX crude Oil</div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Stock Indexes</label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">Dow Jones, S&P500, Nasdaq, Indeks Saham Hong Kong, etc</div>
                                                </div>
                                            </div>
                                            <!-- <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Biaya secara rinci yang dibebankan kepada Nasabah</label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">Diatur didalam Formulir Nomor 107.PBK.06<br> - Peraturan Perdagangan</div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Nomor atau alamat email jika terjadi keluhan	</label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">Tel: 021-7205868 (Hunting)<br>
                                                        Email: cs@maxcofutures.co.id
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Sarana penyelesaian perselisihan yang dipergunakan apabila terjadi perselisihan</label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">Mediasi BBJ, Mediasi BAPPEBTI, BAKTI dan Pengadilan Negri</div>
                                                </div>
                                            </div> -->

                                            <div class="alert alert-dark" role="alert">
                                                Nama-Nama Wakil Pialang Berjangka yang Bekerja di Perusahaan Pialang
                                                Berjangka
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-sm-12">
                                                    <div class="form-control-plaintext">
                                                        MUHAMMAD YANWAR POHAN
                                                        <br />SULUH ADIL WICAKSONO
                                                        <br />WIDIASTUTI
                                                        <br />BAMBANG PRAWOTO
                                                        <br />LINDA MAYASARI
                                                        <br />IRVAN SUTANTO
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="alert alert-dark" role="alert">
                                                Nama-Nama Wakil Pialang Berjangka yang secara khusus ditunjuk oleh
                                                Pialang
                                                Berjangka untuk melakukan verifikasi dalam rangka penerimaan
                                                Nasabah
                                                elektronik online
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-sm-12">
                                                    <div class="form-control-plaintext">
                                                        SULUH ADIL WICAKSONO
                                                        <br />WIDIASTUTI
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <!-- ================================================================ -->
                                        <div class="col-sm-4">
                                            <div class="alert alert-dark" role="alert">
                                                Nomor Rekening Terpisah (Segregeted Account) Perusahaan Pialang
                                                berjangka
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Nama</label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">PT. Maxco Futures</div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Alamat</label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">
                                                        Gedung Panin Pusat Lantai Dasar
                                                        <br />Jl. Jend. Sudirman Kav. 1. Senayan, Jakarta 10270
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Bank</label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">
                                                        BCA Sudirman (Jakarta)
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">No. Rekening Terpisah</label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">
                                                        <br />035 311 5666 (Rupiah)
                                                        <br />035 311 9777 (USD)
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="alert alert-dark" role="alert">PERNYATAAN TELAH MEMBACA PROFIL PERUSAHAAN PIALANG BERJANGKA</div>
                                            <div class="form-group position-relative error-l-100">
                                                <label>
                                                    <p class="text-justify">
                                                        Dengan mengisi kolom "YA" di bawah ini, saya menyatakan bahwa
                                                        saya
                                                        telah
                                                        membaca dan menerima informasi PROFIL PERUSAHAAN PIALANG
                                                        BERJANGKA,
                                                        mengerti
                                                        dan memahami isinya.
                                                    </p>
                                                </label>

                                                <div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="statement_has_been_received_information_futures_company" value="yes" onchange="savekyc()" onkeypress="savekyc()" @isset($DataContent->trading_statement) @if($DataContent->trading_statement->statement_has_been_received_information_futures_company==='Yes') checked @endif @endisset />
                                                        <label class="form-check-label required">Ya</label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="statement_has_been_received_information_futures_company" value="no" onchange="savekyc()" onkeypress="savekyc()" @isset($DataContent->trading_statement) @if($DataContent->trading_statement->statement_has_been_received_information_futures_company==='No') checked @endif @endisset />
                                                        <label class="form-check-label">Tidak</label>
                                                    </div>
                                                    <label id="statement_has_been_received_information_futures_company-error" class="error" for="statement_has_been_received_information_futures_company" style="display: none;">Klik "Ya" untuk melanjutkan.</label>
                                                </div>
                                                <p>Menyatakan pada tanggal : {{date('d-m-Y',strtotime(date("m.d.y")))}}</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="btn-toolbar custom-toolbar text-center card-body pt-0 form-footer-wizard mb-4 justify-content-md-center">
                                        <button class="btn btn-secondary prev-btn" type="button">Previous</button>
                                        <button onclick="event.preventDefault();" class="btn-loading-prev btn btn-secondary hidden">
                                            <span class="loader-small" style="position: unset !important;"></span> Previous
                                        </button>
                                        <input class="btn btn-secondary next-btn btn-form-step-3-1" type="button" value="Next" />
                                        <button onclick="event.preventDefault();" class="btn-loading-next btn btn-secondary hidden">
                                            <span class="loader-small" style="position: unset !important;"></span> Next
                                        </button>
                                    </div>
                                </form>

                                <form id="form-step-3-2" class="tooltip-label-right" method="post" action="{{route('postNextStepOpenLiveAccount')}}" novalidate style="padding: 0px 22px;display: none;">
                                    <input name="NBCRMWEBAPI" type="hidden" value="{{$UserToken}}" />
                                    <input name="USERID" type="hidden" value="{{$UserId}}" />
                                    @csrf

                                    <!-- next -->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h6>Yang mengisi formulir dibawah ini :</h6>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Nama Lengkap</label>
                                                <div class="col-sm-8">
                                                    <span class="form-control-plaintext" data-bind="fullname">{{$DataContent->full_name}}</span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Tempat Lahir</label>
                                                <div class="col-sm-8">
                                                    <span class="form-control-plaintext" data-bind="place_of_birth">{{$DataContent->place_of_birth}}</span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Tangal Lahir</label>
                                                <div class="col-sm-8">
                                                    <span class="form-control-plaintext" data-bind="date_of_birth">{{$DataContent->date_of_birth}}</span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Alamat</label>
                                                <div class="col-sm-8">
                                                    <span class="form-control-plaintext" data-bind="home_address">{{$DataContent->home_address}}</span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Kode Pos</label>
                                                <div class="col-sm-8">
                                                    <span class="form-control-plaintext" data-bind="post_code">{{$DataContent->post_code}}</span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Nomor Identitas</label>
                                                <div class="col-sm-8">
                                                    <span class="form-control-plaintext" data-bind="identity_number">{{$DataContent->identity_number}}</span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">No Demo Acc.</label>
                                                <div class="col-sm-8">
                                                    <span class="form-control-plaintext" data-bind="no_demo_acc">{{(Session::get('user.MTUserDemo')->Login)}}</span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <h3>
                                                Pernyataan Telah Melakukan Simulasi Perdagangan
                                                Berjangka
                                                Komoditi - Formulir : 107.PBK.02.1
                                            </h3>
                                            <div class="form-group position-relative error-l-100">
                                                <label></label>
                                                <p class="text-justify required">
                                                    Dengan mengisi kolom "YA" di bawah ini, saya menyatakan bahwa
                                                    saya
                                                    telah
                                                    melakukan simulasi bertransaksi di bidang Perdagangan Berjangka
                                                    Komoditi
                                                    pada PT. Maxco Futures, dan telah memahami tata cara
                                                    bertransaksi di
                                                    bidang
                                                    Perdagangan Berjangka Komoditi.
                                                    <br />Demikian Pernyataan ini dibuat dengan sebenarnya dalam keadaan
                                                    sadar, sehat
                                                    jasmani dan rohani serta tanpa paksaan apapun dari pihak
                                                    manapun.
                                                </p>
                                                <div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="statement_for_trading_simulation_on_demo_account" value="yes" onchange="savekyc()" onkeypress="savekyc()" onclick="savekyc()" @isset($DataContent->trading_statement) @if($DataContent->trading_statement->statement_for_trading_simulation_on_demo_account==='yes') checked @endif @endisset />
                                                        <label class="form-check-label required" for="trxsimulationyes">Ya</label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="statement_for_trading_simulation_on_demo_account" value="no" onchange="savekyc()" onkeypress="savekyc()" onclick="savekyc()" @isset($DataContent->trading_statement) @if($DataContent->trading_statement->statement_for_trading_simulation_on_demo_account==='no') checked @endif @endisset/>
                                                        <label class="form-check-label" for="trxsimulationno">Tidak</label>
                                                    </div>
                                                    <label id="statement_for_trading_simulation_on_demo_account-error" for="statement_for_trading_simulation_on_demo_account" class="error" style="display: none;">Klik "Ya" untuk melanjutkan.</label>
                                                </div>
                                                <p>Menyatakan pada tanggal : {{date('d-m-Y',strtotime(date("m.d.y")))}}</p>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="btn-toolbar custom-toolbar text-center card-body pt-0 form-footer-wizard mb-4 justify-content-md-center">
                                        <button class="btn btn-secondary prev-btn" type="button">Previous</button>
                                        <button onclick="event.preventDefault();" class="btn-loading-prev btn btn-secondary hidden">
                                            <span class="loader-small" style="position: unset !important;"></span> Previous
                                        </button>
                                        <input class="btn btn-secondary next-btn btn-form-step-3-2" type="button" value="Next" />
                                        <button onclick="event.preventDefault();" class="btn-loading-next btn btn-secondary hidden">
                                            <span class="loader-small" style="position: unset !important;"></span> Next
                                        </button>
                                    </div>
                                </form>

                                <form id="form-step-3-3" class="tooltip-label-right" method="post" action="{{route('postNextStepOpenLiveAccount')}}" novalidate style="padding: 0px 22px;display: none;">
                                    <input name="NBCRMWEBAPI" type="hidden" value="{{$UserToken}}" />
                                    <input name="USERID" type="hidden" value="{{$UserId}}" />
                                    @csrf

                                    <!-- next -->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h6>Yang mengisi formulir dibawah ini :</h6>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Nama Lengkap</label>
                                                <div class="col-sm-8">
                                                    <span class="form-control-plaintext" data-bind="fullname">{{$DataContent->full_name}}</span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Tempat Lahir</label>
                                                <div class="col-sm-8">
                                                    <span class="form-control-plaintext" data-bind="place_of_birth">{{$DataContent->place_of_birth}}</span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Tangal Lahir</label>
                                                <div class="col-sm-8">
                                                    <span class="form-control-plaintext" data-bind="date_of_birth">{{$DataContent->date_of_birth}}</span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Alamat</label>
                                                <div class="col-sm-8">
                                                    <span class="form-control-plaintext" data-bind="home_address">{{$DataContent->home_address}}</span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Kode Pos</label>
                                                <div class="col-sm-8">
                                                    <span class="form-control-plaintext" data-bind="post_code">{{$DataContent->post_code}}</span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Nomor Identitas</label>
                                                <div class="col-sm-8">
                                                    <span class="form-control-plaintext" data-bind="identity_number">{{$DataContent->identity_number}}</span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">No Demo Acc.</label>
                                                <div class="col-sm-8">
                                                    <span class="form-control-plaintext" data-bind="no_demo_acc">{{(Session::get('user.MTUserDemo')->Login)}}</span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <h3>
                                                Surat Penyataan Telah Berpengalaman Melaksanakan
                                                Transaksi
                                                Perdagangan Berjangka Komoditi - Formulir : 107.PBK.02.2
                                            </h3>
                                            <div class="form-group position-relative error-l-100">
                                                <label></label>
                                                <p class="text-justify required">
                                                    Dengan mengisi kolom "YA" di bawah ini, saya menyatakan bahwa
                                                    saya
                                                    telah
                                                    memiliki pengalaman yang mencukupi dalam melaksanakan transaksi
                                                    Perdagangan
                                                    Berjangka karena pernah bertransaksi pada Perusahaan Pialang
                                                    Berjangka **),
                                                    dan telah memahami tentang tata cara bertransaksi Perdagangan
                                                    Berjangka.
                                                    <br />Demikian Pernyataan ini dibuat dengan sebenarnya dalam keadaan
                                                    sadar, sehat
                                                    jasmani dan rohani serta tanpa paksaan apapun dari pihak
                                                    manapun.
                                                </p>
                                                <div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="statement_letter_has_been_experienced_on_trading" value="yes" onchange="savekyc()" onkeypress="savekyc()" onclick="savekyc()" @isset($DataContent->trading_statement) @if($DataContent->trading_statement->statement_letter_has_been_experienced_on_trading=='yes') checked @endif @endisset/>
                                                        <label class="form-check-label required" for="experttradingyes">Ya</label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="statement_letter_has_been_experienced_on_trading" value="no" onchange="savekyc()" onkeypress="savekyc()" onclick="savekyc()" @isset($DataContent->trading_statement) @if($DataContent->trading_statement->statement_letter_has_been_experienced_on_trading=='no') checked @endif @endisset/>
                                                        <label class="form-check-label" for="experttradingno">Tidak</label>
                                                    </div>
                                                    <label id="statement_letter_has_been_experienced_on_trading-error" for="statement_letter_has_been_experienced_on_trading" class="error" style="display: none;">Klik "Ya" untuk melanjutkan.</label>

                                                </div>
                                                <p>Menyatakan pada tanggal : {{date('d-m-Y',strtotime(date("m.d.y")))}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="btn-toolbar custom-toolbar text-center card-body pt-0 form-footer-wizard mb-4 justify-content-md-center">
                                        <button class="btn btn-secondary prev-btn" type="button">Previous</button>
                                        <button onclick="event.preventDefault();" class="btn-loading-prev btn btn-secondary hidden">
                                            <span class="loader-small" style="position: unset !important;"></span> Previous
                                        </button>
                                        <input class="btn btn-secondary next-btn btn-form-step-3-3" type="button" value="Next" />
                                        <button onclick="event.preventDefault();" class="btn-loading-next btn btn-secondary hidden">
                                            <span class="loader-small" style="position: unset !important;"></span> Next
                                        </button>
                                    </div>
                                </form>
                            </div>

                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection

@section('jsonpage')
<script src="{{url('/')}}/cabinet/js/vendor/jquery.smartWizard.min.js"></script>
<script src="{{url('/')}}/cabinet/js/vendor/dropzone.min.js"></script>
<script src="{{url('/')}}/cabinet/js/vendor/sweetalert.min.js"></script>

<script>
    var step = 1;
    $('.prev-btn').on('click', function() {
        step -= 1;
        $('.prev-btn').hide();
        $('.btn-loading-prev').show();
        if (step == 0) {
            $.post("{{route('postNextStepOpenLiveAccount')}}", {
                NBCRMWEBAPI: $('[name="NBCRMWEBAPI"]').val(),
                USERID: $('[name="USERID"]').val(),
                nextStep: '2',
                page: '0'
            }, function(data, status) {
                if (status === "success") {
                    location.reload();
                    $('.prev-btn').show();
                    $('.btn-loading-prev').hide();
                } else {
                    $('.prev-btn').show();
                    $('.btn-loading-prev').hide();
                }
            });
        } else if (step === 1) {
            setTimeout(function() {
                $('.prev-btn').show();
                $('.btn-loading-prev').hide();

                $('#form-step-3-1').show();
                $('#form-step-3-2').hide();
                $('#form-step-3-3').hide();

                document.body.scrollTop = 0; // For Safari
                document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
            }, 1000);
        } else if (step === 2) {
            setTimeout(function() {
                $('.prev-btn').show();
                $('.btn-loading-prev').hide();

                $('#form-step-3-1').hide();
                $('#form-step-3-2').show();
                $('#form-step-3-3').hide();

                document.body.scrollTop = 0; // For Safari
                document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera

            }, 1000);
        }

    });
    $(function() {
        $('.btn-form-step-3-1').on('click', function() {
            if ($("#form-step-3-1").valid()) {
                var value = $($("input[name='statement_has_been_received_information_futures_company']:checked")).val() || 'no';
                if (value === 'no') {
                    $('#statement_has_been_received_information_futures_company-error').show();
                } else {
                    step += 1;
                    $('.next-btn').hide();
                    $('.btn-loading-next').show();
                    $('#statement_has_been_received_information_futures_company-error').hide();
                    $.post("{{route('postNextStepOpenLiveAccount')}}", {
                        NBCRMWEBAPI: $('[name="NBCRMWEBAPI"]').val(),
                        USERID: $('[name="USERID"]').val(),
                        nextStep: '3',
                        page: '2'
                    }, function(data, status) {
                        if (status === "success") {
                            setTimeout(function() {
                                $('.next-btn').show();
                                $('.btn-loading-next').hide();
                                $('#form-step-3-1').hide();
                                $('#form-step-3-2').show();
                                $('#form-step-3-3').hide();

                                document.body.scrollTop = 0; // For Safari
                                document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera

                            }, 1000)
                        } else {
                            $('.next-btn').show();
                            $('.btn-loading-next').hide();
                        }
                    });
                }
            }
        });

        $('.btn-form-step-3-2').on('click', function() {

            if ($("#form-step-3-2").valid()) {
                var value1 = $($("input[name='statement_for_trading_simulation_on_demo_account']:checked")).val() || 'no';
                console.log('form-step-3-2 value', value1);
                if (value1 === 'no') {
                    $('#statement_for_trading_simulation_on_demo_account-error').show();
                }
                if (value1 === 'yes') {
                    step += 1;
                    $('.next-btn').hide();
                    $('.btn-loading-next').show();
                    $('#statement_for_trading_simulation_on_demo_account-error').hide();
                    $.post("{{route('postNextStepOpenLiveAccount')}}", {
                        NBCRMWEBAPI: $('[name="NBCRMWEBAPI"]').val(),
                        USERID: $('[name="USERID"]').val(),
                        nextStep: '3',
                        page: '2'
                    }, function(data, status) {
                        if (status === "success") {
                            $('.next-btn').show();
                            $('.btn-loading-next').hide();

                            $('#form-step-3-1').hide();
                            $('#form-step-3-2').hide();
                            $('#form-step-3-3').show();

                            document.body.scrollTop = 0; // For Safari
                            document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera

                        } else {
                            $('.next-btn').show();
                            $('.btn-loading-next').hide();
                        }
                    });
                }
            } else {
                var errors = $('.error:visible');
                $(errors[0]).focus();
            }
        });

        $('.btn-form-step-3-3').on('click', function() {
            if ($("#form-step-3-2").valid()) {
                var value2 = $($("input[name='statement_letter_has_been_experienced_on_trading']:checked")).val() || 'no';
                if (value2 === 'no') {
                    $('#statement_letter_has_been_experienced_on_trading-error').show();
                }

                if (value2 === 'yes') {
                    $('.next-btn').hide();
                    $('.btn-loading-next').show();
                    $('#statement_letter_has_been_experienced_on_trading-error').hide();
                    $.post("{{route('postNextStepOpenLiveAccount')}}", {
                        NBCRMWEBAPI: $('[name="NBCRMWEBAPI"]').val(),
                        USERID: $('[name="USERID"]').val(),
                        nextStep: '4',
                        page: '1'
                    }, function(data, status) {
                        if (status === "success") {
                            location.reload();
                        } else {
                            $('.next-btn').show();
                            $('.btn-loading-next').hide();
                        }
                    });
                }
            } else {
                var errors = $('.error:visible');
                $(errors[0]).focus();
            }
        });
    });

    $('[name="statement_has_been_received_information_futures_company"]').on('change', function(event) {
        var val = event.currentTarget.value;
        if (val === 'no') {
            // $('.next-btn').prop('disabled',true);
            $('#statement_has_been_received_information_futures_company-error').show();
        } else {
            // $('.next-btn').prop('disabled',false);
            $('#statement_has_been_received_information_futures_company-error').hide();
        }
    });

    $('[name="statement_for_trading_simulation_on_demo_account"]').on('change', function(event) {
        var val = event.currentTarget.value;
        if (val === 'no') {
            // $('.next-btn').prop('disabled',true);
            $('#statement_for_trading_simulation_on_demo_account-error').show();
        } else {
            // $('.next-btn').prop('disabled',false);
            $('#statement_for_trading_simulation_on_demo_account-error').hide();
        }
    });

    $('[name="statement_letter_has_been_experienced_on_trading"]').on('change', function(event) {
        var val = event.currentTarget.value;
        if (val === 'no') {
            // $('.next-btn').prop('disabled',true);
            $('#statement_letter_has_been_experienced_on_trading-error').show();
        } else {
            // $('.next-btn').prop('disabled',false);
            $('#statement_letter_has_been_experienced_on_trading-error').hide();
        }
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    // $(document).ready(function() {

    function getContent() {
        var contentDt = {
            'job_details': {
                'more_details': {
                    'length_of_work': "{{isset($DataContent->job_details) && !empty($DataContent->job_details->more_details->length_of_work)?$DataContent->job_details->more_details->length_of_work:''}}",
                    'previous_employer': "{{isset($DataContent->job_details) && !empty($DataContent->job_details->more_details->previous_employer)?$DataContent->job_details->more_details->previous_employer:0}}",
                    'employer_name': "{{isset($DataContent->job_details) && !empty($DataContent->job_details->more_details->employer_name)?$DataContent->job_details->more_details->employer_name:''}}",
                    'business_fields': "{{isset($DataContent->job_details) && !empty($DataContent->job_details->more_details->business_fields)?$DataContent->job_details->more_details->business_fields:''}}",
                    'title': "{{isset($DataContent->job_details) && !empty($DataContent->job_details->more_details->title)?$DataContent->job_details->more_details->title:''}}",
                    'office_address': "{{isset($DataContent->job_details) && !empty($DataContent->job_details->more_details->office_address)?$DataContent->job_details->more_details->office_address:''}}",
                    'office_tele': "{{isset($DataContent->job_details) && !empty($DataContent->job_details->more_details->office_tele)?$DataContent->job_details->more_details->office_tele:''}}",
                    'office_fax': 0
                },
                'position': {
                    'value': "{{isset($DataContent->job_details) && !empty($DataContent->job_details->position->value)?$DataContent->job_details->position->value:''}}",
                    'input': "{{isset($DataContent->job_details) && !empty($DataContent->job_details->position->input)?$DataContent->job_details->position->input:''}}",
                }
            },
            'wealth_list': {
                'income_per_year': "{{isset($DataContent->wealth_list)&&!empty($DataContent->wealth_list->income_per_year)?$DataContent->wealth_list->income_per_year:''}}",
                'residence_location': "{{isset($DataContent->wealth_list)&&!empty($DataContent->wealth_list->residence_location)?$DataContent->wealth_list->residence_location:''}}",
                'tax_object_selling_value': "{{isset($DataContent->wealth_list)&&!empty($DataContent->wealth_list->tax_object_selling_value)?$DataContent->wealth_list->tax_object_selling_value:''}}",
                'bank_time_deposit_value': "{{isset($DataContent->wealth_list)&&!empty($DataContent->wealth_list->bank_time_deposit_value)?$DataContent->wealth_list->bank_time_deposit_value:''}}",
                'bank_saving_value': "{{isset($DataContent->wealth_list)&&!empty($DataContent->wealth_list->bank_saving_value)?$DataContent->wealth_list->bank_saving_value:''}}",
                'others': "{{isset($DataContent->wealth_list)&&!empty($DataContent->wealth_list->others)?$DataContent->wealth_list->others:''}}",
            },
            'bank_details_for_withdrawls': {
                'full_name': "{{isset($DataContent->bank_details_for_withdrawls)&&!empty($DataContent->bank_details_for_withdrawls->full_name)?$DataContent->bank_details_for_withdrawls->full_name:''}}",
                'branch': "{{isset($DataContent->bank_details_for_withdrawls)&&!empty($DataContent->bank_details_for_withdrawls->branch)?$DataContent->bank_details_for_withdrawls->branch:''}}",
                'account_number': "{{isset($DataContent->bank_details_for_withdrawls)&&!empty($DataContent->bank_details_for_withdrawls->account_number)?$DataContent->bank_details_for_withdrawls->account_number:''}}",
                'phone_number': 0,
                'account_type': {
                    'value': "{{isset($DataContent->bank_details_for_withdrawls)&&!empty($DataContent->bank_details_for_withdrawls->account_type->value)?$DataContent->bank_details_for_withdrawls->account_type->value:''}}",
                    'input': "{{isset($DataContent->bank_details_for_withdrawls)&&!empty($DataContent->bank_details_for_withdrawls->account_type->input)?$DataContent->bank_details_for_withdrawls->account_type->input:''}}",
                }
            },
            'emergency_contact_data': {
                'name': "{{isset($DataContent->emergency_contact_data)&&!empty($DataContent->emergency_contact_data->name)?$DataContent->emergency_contact_data->name:''}}",
                'address': "{{isset($DataContent->emergency_contact_data)&&!empty($DataContent->emergency_contact_data->address)?$DataContent->emergency_contact_data->address:''}}",
                'post_code': "{{isset($DataContent->emergency_contact_data)&&!empty($DataContent->emergency_contact_data->post_code)?$DataContent->emergency_contact_data->post_code:''}}",
                'home_phone_number': "{{isset($DataContent->emergency_contact_data)&&!empty($DataContent->emergency_contact_data->home_phone_number)?$DataContent->emergency_contact_data->home_phone_number:''}}",
                'relationship_with_the_contacts': "{{isset($DataContent->emergency_contact_data)&&!empty($DataContent->emergency_contact_data->relationship_with_the_contacts)?$DataContent->emergency_contact_data->relationship_with_the_contacts:''}}"
            },
            'attached_documents': {
                'id_card': true,
                'coloured_photo': true,
                'specimen_signature': true,
                'other_count': 1,
                'required_image': [{
                        'type': 'id_card',
                        'title': 'id_card_or_driving_license_passport',
                        'action_type': 0,
                        'src': {
                            'name': "{{!empty($DataContent->attached_documents)?$DataContent->attached_documents->required_image[0]->src->name:''}}",
                            'size': 116,
                            'url': "{{!empty($DataContent->attached_documents)?$DataContent->attached_documents->required_image[0]->src->url:''}}",
                        }
                    },
                    {
                        'type': 'coloured_photo',
                        'title': 'coloured_photo',
                        'action_type': 0,
                        'src': {
                            'name': "{{!empty($DataContent->attached_documents)?$DataContent->attached_documents->required_image[1]->src->name:''}}",
                            'size': 116,
                            'url': "{{!empty($DataContent->attached_documents)?$DataContent->attached_documents->required_image[1]->src->url:''}}",
                        }
                    },
                    {
                        'type': 'specimen_signature',
                        'title': 'specimen_signature',
                        'action_type': 0,
                        'src': {
                            'name': "{{!empty($DataContent->attached_documents)?$DataContent->attached_documents->required_image[2]->src->name:''}}",
                            'size': 116,
                            'url': "{{!empty($DataContent->attached_documents)?$DataContent->attached_documents->required_image[2]->src->url:''}}",
                        }
                    },
                    {
                        'type': 'other',
                        'title': 'other',
                        'action_type': 0,
                        'src': {
                            'name': "{{!empty($DataContent->attached_documents)?$DataContent->attached_documents->required_image[3]->src->name:''}}",
                            'size': 116,
                            'url': "{{!empty($DataContent->attached_documents)?$DataContent->attached_documents->required_image[3]->src->url:''}}",
                        }
                    }
                ],
                'option': 'yes'
            },
            'trading_statement': {
                'statement_for_trading_simulation_on_demo_account': $($("input[name='statement_for_trading_simulation_on_demo_account']:checked")).val() || '',
                'statement_letter_has_been_experienced_on_trading': $($("input[name='statement_letter_has_been_experienced_on_trading']:checked")).val() || '',
                'statement_has_been_received_information_futures_company': $($("input[name='statement_has_been_received_information_futures_company']:checked")).val() || ''
            },
            'risk_document': {
                'risk_1_title_en_ref': "{{isset($DataContent->risk_document)&&!empty($DataContent->risk_document->risk_1_title_en_ref)?$DataContent->risk_document->risk_1_title_en_ref:''}}",
                'risk_2_title_en_ref': "{{isset($DataContent->risk_document)&&!empty($DataContent->risk_document->risk_2_title_en_ref)?$DataContent->risk_document->risk_2_title_en_ref:''}}",
                'risk_3_title_en_ref': "{{isset($DataContent->risk_document)&&!empty($DataContent->risk_document->risk_3_title_en_ref)?$DataContent->risk_document->risk_3_title_en_ref:''}}",
                'risk_4_title_en_ref': "{{isset($DataContent->risk_document)&&!empty($DataContent->risk_document->risk_4_title_en_ref)?$DataContent->risk_document->risk_4_title_en_ref:''}}",
                'risk_5_title_en_ref': "{{isset($DataContent->risk_document)&&!empty($DataContent->risk_document->risk_5_title_en_ref)?$DataContent->risk_document->risk_5_title_en_ref:''}}",
                'risk_6_title_en_ref': "{{isset($DataContent->risk_document)&&!empty($DataContent->risk_document->risk_6_title_en_ref)?$DataContent->risk_document->risk_6_title_en_ref:''}}",
                'risk_7_title_en_ref': "{{isset($DataContent->risk_document)&&!empty($DataContent->risk_document->risk_7_title_en_ref)?$DataContent->risk_document->risk_7_title_en_ref:''}}",
                'risk_8_title_en_ref': "{{isset($DataContent->risk_document)&&!empty($DataContent->risk_document->risk_8_title_en_ref)?$DataContent->risk_document->risk_8_title_en_ref:''}}",
                'risk_9_title_en_ref': "{{isset($DataContent->risk_document)&&!empty($DataContent->risk_document->risk_9_title_en_ref)?$DataContent->risk_document->risk_9_title_en_ref:''}}",
                'risk_10_title_en_ref': "{{isset($DataContent->risk_document)&&!empty($DataContent->risk_document->risk_10_title_en_ref)?$DataContent->risk_document->risk_10_title_en_ref:''}}",
                'risk_11_title_en_ref': "{{isset($DataContent->risk_document)&&!empty($DataContent->risk_document->risk_11_title_en_ref)?$DataContent->risk_document->risk_11_title_en_ref:''}}",
                'risk_12_title_en_ref': "{{isset($DataContent->risk_document)&&!empty($DataContent->risk_document->risk_12_title_en_ref)?$DataContent->risk_document->risk_12_title_en_ref:''}}",
                'risk_13_title_en_ref': "{{isset($DataContent->risk_document)&&!empty($DataContent->risk_document->risk_13_title_en_ref)?$DataContent->risk_document->risk_13_title_en_ref:''}}",
                'radio': "{{isset($DataContent->risk_document)&&!empty($DataContent->risk_document->radio)?$DataContent->risk_document->radio:''}}",
            },
            'trading_condition': {
                'trading_conditions_1_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_1_title_en_ref)?$DataContent->trading_condition->trading_conditions_1_title_en_ref:''}}",
                'trading_conditions_2_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_2_title_en_ref)?$DataContent->trading_condition->trading_conditions_2_title_en_ref:''}}",
                'trading_conditions_3_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_3_title_en_ref)?$DataContent->trading_condition->trading_conditions_3_title_en_ref:''}}",
                'trading_conditions_4_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_4_title_en_ref)?$DataContent->trading_condition->trading_conditions_4_title_en_ref:''}}",
                'trading_conditions_5_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_5_title_en_ref)?$DataContent->trading_condition->trading_conditions_5_title_en_ref:''}}",
                'trading_conditions_6_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_6_title_en_ref)?$DataContent->trading_condition->trading_conditions_6_title_en_ref:''}}",
                'trading_conditions_7_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_7_title_en_ref)?$DataContent->trading_condition->trading_conditions_7_title_en_ref:''}}",
                'trading_conditions_8_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_8_title_en_ref)?$DataContent->trading_condition->trading_conditions_8_title_en_ref:''}}",
                'trading_conditions_9_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_9_title_en_ref)?$DataContent->trading_condition->trading_conditions_9_title_en_ref:''}}",
                'trading_conditions_10_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_10_title_en_ref)?$DataContent->trading_condition->trading_conditions_10_title_en_ref:''}}",
                'trading_conditions_11_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_11_title_en_ref)?$DataContent->trading_condition->trading_conditions_11_title_en_ref:''}}",
                'trading_conditions_12_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_12_title_en_ref)?$DataContent->trading_condition->trading_conditions_12_title_en_ref:''}}",
                'trading_conditions_13_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_13_title_en_ref)?$DataContent->trading_condition->trading_conditions_13_title_en_ref:''}}",
                'trading_conditions_14_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_14_title_en_ref)?$DataContent->trading_condition->trading_conditions_14_title_en_ref:''}}",
                'trading_conditions_15_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_15_title_en_ref)?$DataContent->trading_condition->trading_conditions_15_title_en_ref:''}}",
                'trading_conditions_16_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_16_title_en_ref)?$DataContent->trading_condition->trading_conditions_16_title_en_ref:''}}",
                'trading_conditions_17_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_17_title_en_ref)?$DataContent->trading_condition->trading_conditions_17_title_en_ref:''}}",
                'trading_conditions_18_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_18_title_en_ref)?$DataContent->trading_condition->trading_conditions_18_title_en_ref:''}}",
                'trading_conditions_19_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_19_title_en_ref)?$DataContent->trading_condition->trading_conditions_19_title_en_ref:''}}",
                'trading_conditions_20_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_20_title_en_ref)?$DataContent->trading_condition->trading_conditions_20_title_en_ref:''}}",
                'trading_conditions_21_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_21_title_en_ref)?$DataContent->trading_condition->trading_conditions_21_title_en_ref:''}}",
                'trading_conditions_22_title_1': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_22_title_1)?$DataContent->trading_condition->trading_conditions_22_title_1:'0'}}",
                'trading_conditions_22_title_2': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_22_title_2)?$DataContent->trading_condition->trading_conditions_22_title_2:'0'}}",
                'trading_conditions_22_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_22_title_en_ref)?$DataContent->trading_condition->trading_conditions_22_title_en_ref:''}}",
                'trading_conditions_23_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_23_title_en_ref)?$DataContent->trading_condition->trading_conditions_23_title_en_ref:''}}",
                'step_4_footer': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->step_4_footer)?$DataContent->trading_condition->step_4_footer:''}}",
            },
            'pap_agreement': "{{!empty($DataContent->pap_agreement)?$DataContent->pap_agreement:''}}",
            'truth_resposibility_agreement': "{{!empty($DataContent->truth_resposibility_agreement)?$DataContent->truth_resposibility_agreement:''}}",
            'demoAccount': "{{(Session::get('user.MTUserDemo')->Login)}}",
            'full_name': "{{!empty($DataContent->full_name)?$DataContent->full_name:''}}",
            'identity_number': "{{!empty($DataContent->identity_number)?$DataContent->identity_number:''}}",
            'future_contract': {
                'value': "{{isset($DataContent->future_contract)&&!empty($DataContent->future_contract->value)?$DataContent->future_contract->value:''}}",
                'input': "{{isset($DataContent->future_contract)&&!empty($DataContent->future_contract->input)?$DataContent->future_contract->input:''}}",
            },
            'account_type': "{{!empty($DataContent->account_type)?$DataContent->account_type:''}}",
            'place_of_birth': "{{!empty($DataContent->place_of_birth)?$DataContent->place_of_birth:''}}",
            'date_of_birth': "{{!empty($DataContent->date_of_birth)?$DataContent->date_of_birth:''}}",
            'post_code': "{{!empty($DataContent->post_code)?$DataContent->post_code:''}}",
            'page': "{{!empty($DataContent->page)?$DataContent->page:0}}",
            'activeStep': "{{!empty($DataContent->activeStep)?$DataContent->activeStep:''}}",
            'tax_file_number': "{{!empty($DataContent->tax_file_number)?$DataContent->tax_file_number:''}}",
            'mother_maiden_name': "{{!empty($DataContent->mother_maiden_name)?$DataContent->mother_maiden_name:''}}",
            'status': "{{!empty($DataContent->status)?$DataContent->status:''}}",
            'gender': "{{!empty($DataContent->gender)?$DataContent->gender:''}}",
            'home_address': "{{!empty($DataContent->home_address)?$DataContent->home_address:''}}",
            'home_ownership_status': {
                'value': "{{isset($DataContent->home_ownership_status)&&!empty($DataContent->home_ownership_status->value)?$DataContent->home_ownership_status->value:''}}",
                'input': "{{isset($DataContent->home_ownership_status)&&!empty($DataContent->home_ownership_status->input)?$DataContent->home_ownership_status->input:''}}"
            },
            'openint_account_purpose': {
                'value': "{{isset($DataContent->openint_account_purpose)&&!empty($DataContent->openint_account_purpose->value)?$DataContent->openint_account_purpose->value:''}}",
                'input': "{{isset($DataContent->openint_account_purpose)&&!empty($DataContent->openint_account_purpose->input)?$DataContent->openint_account_purpose->input:''}}"
            },
            'experince_on_investment': {
                'value': "{{isset($DataContent->experince_on_investment)&&!empty($DataContent->experince_on_investment->value)?$DataContent->experince_on_investment->value:''}}",
                'input': "{{isset($DataContent->experince_on_investment)&&!empty($DataContent->experince_on_investment->input)?$DataContent->experince_on_investment->input:''}}"
            },
            'is_family_work_in_related_company': {
                'value': "{{isset($DataContent->is_family_work_in_related_company)&&!empty($DataContent->is_family_work_in_related_company->value)?$DataContent->is_family_work_in_related_company->value:''}}",
                'input': "{{isset($DataContent->is_family_work_in_related_company)&&!empty($DataContent->is_family_work_in_related_company->input)?$DataContent->is_family_work_in_related_company->input:''}}"
            },
            'is_declared_bankrupt': {
                'value': "{{isset($DataContent->is_declared_bankrupt)&&!empty($DataContent->is_declared_bankrupt->value)?$DataContent->is_declared_bankrupt->value:''}}",
                'input': "{{isset($DataContent->is_declared_bankrupt)&&!empty($DataContent->is_declared_bankrupt->input)?$DataContent->is_declared_bankrupt->input:''}}"
            },
            'trading_rules': "{{!empty($DataContent->trading_rules)?$DataContent->trading_rules:''}}",
            'ae_name_or_branch': "{{!empty($DataContent->ae_name_or_branch)?$DataContent->ae_name_or_branch:''}}",
            'husband_wife_spouse': "{{!empty($DataContent->husband_wife_spouse)?$DataContent->husband_wife_spouse:''}}",
            'home_phone': "{{!empty($DataContent->home_phone)?$DataContent->home_phone:''}}",
            'home_fax': 0,
            'statement_data_correctly': "{{!empty($DataContent->statement_data_correctly)?$DataContent->statement_data_correctly:''}}",
            'KycStatus': 0
        };

        return contentDt;
    }



    function savekyc() {
        var contentDt = getContent();
        if (contentDt.wealth_list.income_per_year === "&gt;500") {
            contentDt.wealth_list.income_per_year = ">500";
        }
        const content = JSON.stringify(contentDt)
        $.post("{{route('savekyc')}}", {
            content,
            NBCRMWEBAPI: $('[name="NBCRMWEBAPI"]').val(),
            USERID: $('[name="USERID"]').val(),
        }, function(data, status) {
            var {
                Code,
                Message
            } = data;
            if (data.Data) {
                var Data = data.Data;
                var MTUserRef = Data.MTUserRef;
                var idx = MTUserRef.findIndex(x => x.IsSimulate === false);
                if (MTUserRef[idx]) {
                    var liveAcc = MTUserRef[idx];
                    $('[data-bind="ac_live_no"]').text(liveAcc.Login);
                }
            }
        });
    };

    // });
</script>
@endsection