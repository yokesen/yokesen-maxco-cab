@extends('templates.master')

@section('csslinkonhead')
<link rel="stylesheet" href="{{url('/')}}/cabinet/css/vendor/smart_wizard.min.css" />
<link rel="stylesheet" href="{{url('/')}}/cabinet/css/vendor/dropzone.min.css" />
<meta name="csrf-token" content="{{ csrf_token() }}" />
<style>
    .form-check-input {
        margin-top: 0.7rem !important;
    }
</style>
@if($agent->isMobile())
<style>
    .sw-main.sw-theme-check>ul.step-anchor>li>a:after,
    .sw-main.sw-theme-check>ul.step-anchor>li.active>a:after,
    .sw-main.sw-main.sw-theme-check>ul.step-anchor>li.done>a:after {
        margin-top: 42px;
    }
</style>
@endif
@endsection

@section('content')
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <h1>Opening Account Wizard</h1>
                <div class="separator mb-5"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 mb-4">
                <!-- <h5 class="mb-4">Open Live Account</h5> -->
                <div class="card mb-4">
                    <div id="" class="sw-main sw-theme-check">
                        <ul class="card-header mb-4 nav nav-tabs step-anchor" style="z-index: 0;">
                            <li class="nav-item  done">
                                <a class="nav-link">
                                    @if(!$agent->isMobile())
                                    Step 1
                                    <br>
                                    @endif
                                </a>
                            </li>
                            <li class="nav-item  done">
                                <a class="nav-link">
                                    @if(!$agent->isMobile())
                                    Step 2
                                    <br>
                                    @endif
                                </a>
                            </li>
                            <li class="nav-item  done">
                                <a class="nav-link">
                                    @if(!$agent->isMobile())
                                    Step 3
                                    <br>
                                    @endif
                                </a>
                            </li>
                            <li class="nav-item active">
                                <a class="nav-link">
                                    @if(!$agent->isMobile())
                                    Step 4
                                    <br>
                                    @endif
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#step-4" class="nav-link">
                                    @if(!$agent->isMobile())
                                    Step 5
                                    <br>
                                    @endif
                                </a>
                            </li>
                        </ul>

                        <div class="card-body">
                            <form id="form-step-0" novalidate style="padding: 0px 22px;">
                                <input name="NBCRMWEBAPI" type="hidden" value="{{$UserToken}}" />
                                <input name="USERID" type="hidden" value="{{$UserId}}" />
                                @csrf
                                <div class="row">
                                    <!-- <h3>
                                        Dokumen Pemberitahuan Adanya Resiko Yang Harus Disampaikan Oleh Pialang
                                        Berjangka Untuk Transaksi Kontrak Derivatif Dalam Sistem Perdagangan
                                        Alternatif
                                        - Formulir Nomor 107.PBK.04.2
                                    </h3> -->
                                    <div class="col-md-4">
                                        <div class="alert alert-dark" role="alert">
                                            Dokumen Pemberitahuan Adanya Resiko Yang Harus Disampaikan Oleh
                                            Pialang
                                            Berjangka Untuk Transaksi Kontrak Derivatif Dalam Sistem Perdagangan
                                            Alternatif
                                            - Formulir Nomor 107.PBK.04.2
                                        </div>

                                        <h5>PERHATIAN!</h5>
                                        <h5 class="text-justify">
                                            MOHON UNTUK MEMBACA DENGAN TELITI DAN
                                            MENYETUJUI
                                            SETIAP
                                            ISI DOKUMEN DENGAN CARA
                                            "TICK / CENTANG" DI KOTAK YANG TERSEDIA
                                        </h5>
                                        <p class="text-justify">
                                            Dokumen Pemberitahuan Adanya Risiko ini disampaikan kepada Anda
                                            sesuai
                                            dengan
                                            Pasal
                                            50 ayat (2) Undang-Undang Nomor 32 Tahun 1997 tentang Perdagangan
                                            Berjangka
                                            Komoditi
                                            sebagaimana telah diubah dengan Undang-Undang Nomor 10 Tahun 2011
                                            tentang
                                            Perubahan
                                            Atas Undang-Undang Nomor 32 Tahun 1997 Tentang Perdagangan Berjangka
                                            Komoditi.
                                            <br />Maksud dokumen ini adalah memberitahukan bahwa kemungkinan kerugian
                                            atau
                                            keuntungan
                                            dalam perdagangan Kontrak Derivatif dalam Sistem Perdagangan
                                            Alternatif bisa
                                            mencapai jumlah yang sangat besar. Oleh karena itu, Anda harus
                                            berhati-hati
                                            dalam
                                            memutuskan untuk melakukan transaksi, apakah kondisi keuangan Anda
                                            mencukupi.
                                        </p>

                                        <div class="form-group position-relative error-l-100 text-justify">
                                            <label class="">
                                                1.
                                                Perdagangan Kontrak Derivatif dalam Sistem Perdagangan
                                                Alternatif belum
                                                tentu layak
                                                bagi semua investor.
                                                <br />Anda dapat menderita kerugian dalam jumlah besar dan dalam
                                                jangka waktu
                                                singkat.
                                                Jumlah kerugian uang dimungkinkan dapat melebihi jumlah uang
                                                yang
                                                pertama
                                                kali Anda
                                                setor (Margin Awal) ke Pialang Berjangka Anda. Anda mungkin
                                                menderita
                                                kerugian
                                                seluruh Margin dan Margin tambahan yang ditempatkan pada Pialang
                                                Berjangka
                                                untuk
                                                mempertahankan posisi Kontrak Derivatif dalam Sistem Perdagangan
                                                Alternatif
                                                Anda.
                                                Hal ini disebabkan Perdagangan Berjangka sangat dipengaruhi oleh
                                                mekanisme
                                                leverage,
                                                dimana dengan jumlah investasi dalam bentuk yang relatif kecil
                                                dapat
                                                digunakan untuk
                                                membuka posisi dengan aset yang bernilai jauh lebih tinggi.
                                                Apabila Anda
                                                tidak siap
                                                dengan risiko seperti ini, sebaiknya Anda tidak melakukan
                                                perdagangan
                                                Kontrak
                                                Derivatif dalam Sistem Perdagangan Alternatif.
                                            </label>

                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" name="risk_1_title_en_ref" value="1" onchange="savekyc()" onkeypress="savekyc()" onclick="savekyc()" @isset($DataContent->risk_document) @if($DataContent->risk_document->risk_1_title_en_ref==='1') checked @endif @endisset />
                                                <label class="form-check-label required"">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                            </div>
                                            <label id=" risk_1_title_en_ref-error" class="error" for="risk_1_title_en_ref"></label>

                                            </div>
                                            <div class="form-group position-relative error-l-100 text-justify hidden risk_2">
                                                <label class="">
                                                    2.
                                                    Perdagangan Kontrak Derivatif dalam Sistem Perdagangan
                                                    Alternatif
                                                    mempunyai
                                                    risiko
                                                    dan mempunyai kemungkinan kerugian yang tidak terbatas yang jauh
                                                    lebih
                                                    besar
                                                    dari
                                                    jumlah uang yang disetor (Margin) ke Pialang Berjangka.
                                                    <br />Kontrak Derivatif dalam Sistem Perdagangan Alternatif sama
                                                    dengan produk
                                                    keuangan
                                                    lainnya yang mempunyai risiko tinggi, Anda sebaiknya tidak
                                                    menaruh
                                                    risiko
                                                    terhadap
                                                    dana yang Anda tidak siap untuk menderita rugi, seperti tabungan
                                                    pensiun,
                                                    dana
                                                    kesehatan atau dana untuk keadaan darurat, dana yang disediakan
                                                    untuk
                                                    pendidikan
                                                    atau kepemilikan rumah, dana yang diperoleh dari pinjaman
                                                    pendidikan
                                                    atau
                                                    gadai,
                                                    atau dana yang digunakan untuk memenuhi kebutuhan
                                                    sehari-hari.
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="risk_2_title_en_ref" value="1" onchange="savekyc()" onkeypress="savekyc()" onclick="savekyc()" required @isset($DataContent->risk_document) @if($DataContent->risk_document->risk_2_title_en_ref==='1') checked @endif @endisset />
                                                    <label class="form-check-label required" for="pbk0402point2read">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                                <label id="risk_2_title_en_ref-error" class="error" for="risk_2_title_en_ref"></label>

                                            </div>
                                            <div class="form-group position-relative error-l-100 text-justify hidden risk_3">
                                                <label>
                                                    3.
                                                    Berhati-hatilah terhadap pernyataan bahwa Anda pasti mendapatkan
                                                    keuntungan
                                                    besar
                                                    dari perdagangan Kontrak Derivatif dalam Sistem Perdagangan
                                                    Alternatif.
                                                    <br />Meskipun perdagangan Kontrak Derivatif dalam Sistem Perdagangan
                                                    Alternatif
                                                    dapat
                                                    memberikan keuntungan yang besar dan cepat, namun hal tersebut
                                                    tidak
                                                    pasti,
                                                    bahkan
                                                    dapat menimbulkan kerugian yang besar dan cepat juga. Seperti
                                                    produk
                                                    keuangan
                                                    lainnya, tidak ada yang dinamakan “pasti untung”.
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="risk_3_title_en_ref" value="1" onchange="savekyc()" onkeypress="savekyc()" onclick="savekyc()" required @isset($DataContent->risk_document) @if($DataContent->risk_document->risk_3_title_en_ref==='1') checked @endif @endisset />
                                                    <label class="form-check-label required" for="pbk0402point3read">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                                <label id="risk_3_title_en_ref-error" class="error" for="risk_3_title_en_ref"></label>
                                            </div>
                                            <div class="form-group position-relative error-l-100 text-justify hidden risk_4">
                                                <label class="">
                                                    4.
                                                    Disebabkan adanya mekanisme leverage dan sifat dari transaksi
                                                    Kontrak
                                                    Derivatif
                                                    dalam Sistem Perdagangan Alternatif, Anda dapat merasakan dampak
                                                    bahwa
                                                    Anda
                                                    menderita kerugian dalam waktu cepat.
                                                    <br />Keuntungan maupun kerugian dalam transaksi akan langsung
                                                    dikredit atau
                                                    didebet ke
                                                    rekening Anda, paling lambat secara harian. Apabila pergerakan
                                                    di pasar
                                                    terhadap
                                                    Kontrak Derivatif dalam Sistem Perdagangan Alternatif menurunkan
                                                    nilai
                                                    posisi Anda
                                                    dalam Kontrak Derivatif dalam Sistem Perdagangan Alternatif,
                                                    dengan kata
                                                    lain
                                                    berlawanan dengan posisi yang Anda ambil, Anda diwajibkan untuk
                                                    menambah
                                                    dana untuk
                                                    pemenuhan kewajiban Margin ke perusahaan Pialang Berjangka.
                                                    Apabila
                                                    rekening
                                                    Anda
                                                    berada dibawah minimum Margin yang telah ditetapkan Lembaga
                                                    Kliring
                                                    Berjangka atau
                                                    Pialang Berjangka, maka posisi Anda dapat dilikuidasi pada saat
                                                    rugi,
                                                    dan
                                                    Anda wajib
                                                    menyelesaikan defisit (jika ada) dalam rekening Anda.
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="risk_4_title_en_ref" value="1" onchange="savekyc()" onkeypress="savekyc()" onclick="savekyc()" required @isset($DataContent->risk_document) @if($DataContent->risk_document->risk_4_title_en_ref==='1') checked @endif @endisset/>
                                                    <label class="form-check-label required" for="pbk0402point4read">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                                <label id="risk_4_title_en_ref-error" class="error" for="risk_4_title_en_ref"></label>
                                            </div>
                                            <div class="form-group position-relative error-l-100 text-justify hidden risk_5">
                                                <label class="">
                                                    5.
                                                    Pada saat pasar dalam keadaan tertentu, Anda mungkin akan sulit
                                                    atau
                                                    tidak
                                                    mungkin
                                                    melikuidasi posisi.
                                                    <br />Pada umumnya Anda harus melakukan transaksi mengambil posisi
                                                    yang
                                                    berlawanan
                                                    dengan
                                                    maksud melikuidasi posisi (offset) jika ingin melikuidasi posisi
                                                    dalam
                                                    Kontrak
                                                    Derivatif dalam Sistem Perdagangan Alternatif. Apabila Anda
                                                    tidak dapat
                                                    melikuidasi
                                                    posisi Kontrak Derivatif dalam Sistem Perdagangan Alternatif,
                                                    Anda tidak
                                                    dapat
                                                    merealisasikan keuntungan pada nilai posisi tersebut atau
                                                    mencegah
                                                    kerugian
                                                    yang
                                                    lebih tinggi. Kemungkinan tidak dapat melikuidasi dapat terjadi,
                                                    antara
                                                    lain: jika
                                                    perdagangan berhenti dikarenakan aktivitas perdagangan yang
                                                    tidak lazim
                                                    pada
                                                    Kontrak
                                                    Derivatif atau subjek Kontrak Derivatif, atau terjadi kerusakan
                                                    sistem
                                                    pada
                                                    Pialang
                                                    Berjangka Peserta Sistem Perdagangan Alternatif atau Pedagang
                                                    Berjangka
                                                    Penyelenggara Sistem Perdagangan Alternatif. Bahkan apabila Anda
                                                    dapat
                                                    melikuidasi
                                                    posisi tersebut, Anda mungkin terpaksa melakukannya pada harga
                                                    yang
                                                    menimbulkan
                                                    kerugian besar.
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="risk_5_title_en_ref" value="1" onchange="savekyc()" onkeypress="savekyc()" onclick="savekyc()" required @isset($DataContent->risk_document) @if($DataContent->risk_document->risk_5_title_en_ref==='1') checked @endif @endisset/>
                                                    <label class="form-check-label required" for="pbk0402point5read">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                                <label id="risk_5_title_en_ref-error" class="error" for="risk_5_title_en_ref"></label>

                                            </div>
                                            <div class="form-group position-relative error-l-100 text-justify hidden risk_6">
                                                <label class="">
                                                    6.
                                                    Pada saat pasar dalam keadaan tertentu, Anda mungkin akan sulit
                                                    atau
                                                    tidak
                                                    mungkin
                                                    mengelola risiko atas posisi terbuka Kontrak Derivatif dalam
                                                    Sistem
                                                    Perdagangan
                                                    Alternatif dengan cara membuka posisi dengan nilai yang sama
                                                    namun
                                                    dengan
                                                    posisi
                                                    yang berlawanan dalam kontrak bulan yang berbeda, dalam pasar
                                                    yang
                                                    berbeda
                                                    atau
                                                    dalam “subjek Kontrak Derivatif dalam Sistem Perdagangan
                                                    Alternatif”
                                                    yang
                                                    berbeda.
                                                    <br />Kemungkinan untuk tidak dapat mengambil posisi dalam rangka
                                                    membatasi
                                                    risiko
                                                    yang
                                                    timbul, contohnya; jika perdagangan dihentikan pada pasar yang
                                                    berbeda
                                                    disebabkan
                                                    aktivitas perdagangan yang tidak lazim pada Kontrak Derivatif
                                                    dalam
                                                    Sistem
                                                    Perdagangan Alternatif atau “Kontrak Derivatif dalam Sistem
                                                    Perdagangan
                                                    Alternatif”.
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="risk_6_title_en_ref" value="1" onchange="savekyc()" onkeypress="savekyc()" onclick="savekyc()" required @isset($DataContent->risk_document) @if($DataContent->risk_document->risk_6_title_en_ref==='1') checked @endif @endisset/>
                                                    <label class="form-check-label required" for="pbk0402point6read">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                                <label id="risk_6_title_en_ref-error" class="error" for="risk_6_title_en_ref"></label>

                                            </div>
                                            <div class="form-group position-relative error-l-100 text-justify hidden risk_7">
                                                <label class="">
                                                    7.
                                                    Anda dapat menderita kerugian yang disebabkan kegagalan sistem
                                                    informasi.
                                                    <br />Sebagaimana yang terjadi pada setiap transaksi keuangan, Anda
                                                    dapat
                                                    menderita
                                                    kerugian jika amanat untuk melaksanakan transaksi Kontrak
                                                    Derivatif
                                                    dalam
                                                    Sistem
                                                    Perdagangan Alternatif tidak dapat dilakukan karena kegagalan
                                                    sistem
                                                    informasi di
                                                    Bursa Berjangka, Pedagang Penyelenggara Sistem Perdagangan
                                                    Alternatif ,
                                                    maupun
                                                    sistem di Pialang Berjangka Peserta Sistem Perdagangan
                                                    Alternatif yang
                                                    mengelola
                                                    posisi Anda. Kerugian Anda akan semakin besar jika Pialang
                                                    Berjangka
                                                    yang
                                                    mengelola
                                                    posisi Anda tidak memiliki sistem informasi cadangan atau
                                                    prosedur yang
                                                    layak.
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="risk_7_title_en_ref" value="1" onchange="savekyc()" onkeypress="savekyc()" onclick="savekyc()" required @isset($DataContent->risk_document) @if($DataContent->risk_document->risk_7_title_en_ref==='1') checked @endif @endisset/>
                                                    <label class="form-check-label required" for="pbk0402point7read">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                                <label id="risk_7_title_en_ref-error" class="error" for="risk_7_title_en_ref"></label>

                                            </div>
                                            <div class="form-group position-relative error-l-100 text-justify hidden risk_8">
                                                <label class="">
                                                    8.
                                                    Semua Kontrak Derivatif dalam Sistem Perdagangan Alternatif
                                                    mempunyai
                                                    risiko, dan
                                                    tidak ada strategi berdagang yang dapat menjamin untuk
                                                    menghilangkan
                                                    risiko
                                                    tersebut.
                                                    <br />Strategi dengan menggunakan kombinasi posisi seperti spread,
                                                    dapat sama
                                                    berisiko
                                                    seperti posisi long atau short. Melakukan Perdagangan Berjangka
                                                    memerlukan
                                                    pengetahuan mengenai Kontrak Derivatif dalam Sistem Perdagangan
                                                    Alternatif
                                                    dan pasar
                                                    berjangka.
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="risk_8_title_en_ref" value="1" onchange="savekyc()" onkeypress="savekyc()" onclick="savekyc()" required @isset($DataContent->risk_document) @if($DataContent->risk_document->risk_8_title_en_ref==='1') checked @endif @endisset/>
                                                    <label class="form-check-label required" for="pbk0402point8read">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                                <label id="risk_8_title_en_ref-error" class="error" for="risk_8_title_en_ref"></label>
                                            </div>
                                            <div class="form-group position-relative error-l-100 text-justify hidden risk_9">
                                                <label class="">
                                                    9.
                                                    Strategi perdagangan harian dalam Kontrak Derivatif dalam Sistem
                                                    Perdagangan
                                                    Alternatif dan produk lainnya memiliki risiko khusus.
                                                    <br />Seperti pada produk keuangan lainnya, pihak yang ingin membeli
                                                    atau
                                                    menjual
                                                    Kontrak
                                                    Derivatif dalam Sistem Perdagangan Alternatif yang sama dalam
                                                    satu hari
                                                    untuk
                                                    mendapat keuntungan dari perubahan harga pada hari tersebut
                                                    (“day
                                                    traders”)
                                                    akan
                                                    memiliki beberapa risiko tertentu antara lain jumlah komisi yang
                                                    besar,
                                                    risiko
                                                    terkena efek pengungkit (“exposure to leverage”), dan persaingan
                                                    dengan
                                                    pedagang
                                                    profesional. Anda harus mengerti risiko tersebut dan memiliki
                                                    pengalaman
                                                    yang
                                                    memadai sebelum melakukan perdagangan harian (“day
                                                    trading”).
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="risk_9_title_en_ref" value="1" onchange="savekyc()" onkeypress="savekyc()" onclick="savekyc()" required @isset($DataContent->risk_document) @if($DataContent->risk_document->risk_9_title_en_ref==='1') checked @endif @endisset/>
                                                    <label class="form-check-label required" for="pbk0402point9read">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                                <label id="risk_9_title_en_ref-error" class="error" for="risk_9_title_en_ref"></label>

                                            </div>
                                            <div class="form-group position-relative error-l-100 text-justify hidden risk_10">
                                                <label class="">
                                                    10.
                                                    Menetapkan amanat bersyarat, Kontrak Derivatif dalam Sistem
                                                    Perdagangan
                                                    Alternatif
                                                    dilikuidasi pada keadaan tertentu untuk membatasi rugi (stop
                                                    loss),
                                                    mungkin
                                                    tidak
                                                    akan dapat membatasi kerugian Anda sampai jumlah tertentu
                                                    saja.
                                                    <br />Amanat bersyarat tersebut mungkin tidak dapat dilaksanakan
                                                    karena
                                                    terjadi
                                                    kondisi
                                                    pasar yang tidak memungkinkan melikuidasi Kontrak Derivatif
                                                    dalam Sistem
                                                    Perdagangan
                                                    Alternatif.
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="risk_10_title_en_ref" value="1" onchange="savekyc()" onkeypress="savekyc()" onclick="savekyc()" required @isset($DataContent->risk_document) @if($DataContent->risk_document->risk_10_title_en_ref==='1') checked @endif @endisset/>
                                                    <label class="form-check-label required" for="pbk0402point10read">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                                <label id="risk_10_title_en_ref-error" class="error" for="risk_10_title_en_ref"></label>

                                            </div>
                                            <div class="form-group position-relative error-l-100 text-justify hidden risk_11">
                                                <label class="">
                                                    11.
                                                    Anda harus membaca dengan seksama dan memahami Perjanjian
                                                    Pemberian
                                                    Amanat
                                                    Nasabah
                                                    dengan Pialang Berjangka Anda sebelum melakukan transaksi
                                                    Kontrak
                                                    Derivatif
                                                    dalam
                                                    Sistem Perdagangan Alternatif.
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="risk_11_title_en_ref" value="1" onchange="savekyc()" onkeypress="savekyc()" onclick="savekyc()" required @isset($DataContent->risk_document) @if($DataContent->risk_document->risk_11_title_en_ref==='1') checked @endif @endisset/>
                                                    <label class="form-check-label required" for="pbk0402point11read">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                                <label id="risk_11_title_en_ref-error" class="error" for="risk_11_title_en_ref"></label>
                                            </div>
                                            <div class="form-group position-relative error-l-100 text-justify hidden risk_12">
                                                <label class="">
                                                    12.
                                                    Pernyataan singkat ini tidak dapat memuat secara rinci seluruh
                                                    risiko
                                                    atau
                                                    aspek
                                                    penting lainnya tentang Perdagangan Berjangka. Oleh karena itu
                                                    Anda
                                                    harus
                                                    mempelajari kegiatan Perdagangan Berjangka secara cermat sebelum
                                                    memutuskan
                                                    melakukan transaksi.
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="risk_12_title_en_ref" value="1" onchange="savekyc()" onkeypress="savekyc()" onclick="savekyc()" required @isset($DataContent->risk_document) @if($DataContent->risk_document->risk_12_title_en_ref==='1') checked @endif @endisset />
                                                    <label class="form-check-label required" for="pbk0402point12read">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                                <label id="risk_12_title_en_ref-error" class="error" for="risk_12_title_en_ref"></label>
                                            </div>
                                            <div class="form-group position-relative error-l-100 text-justify hidden risk_13">
                                                <label class="">
                                                    13.
                                                    Dokumen Pemberitahuan Adanya Risiko (Risk Disclosure) ini dibuat
                                                    dalam
                                                    Bahasa
                                                    Indonesia.
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="risk_13_title_en_ref" value="1" onchange="savekyc()" onkeypress="savekyc()" onclick="savekyc()" required @isset($DataContent->risk_document) @if($DataContent->risk_document->risk_13_title_en_ref==='1') checked @endif @endisset/>
                                                    <label class="form-check-label required"" for=" pbk0402point13read">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                                <label id="risk_13_title_en_ref-error" class="error" for="risk_13_title_en_ref"></label>

                                            </div>
                                            <div class="form-group position-relative error-l-100 text-justify hidden risk_footer">
                                                <label class="">
                                                    SAYA SUDAH MEMBACA DAN MEMAHAMI
                                                    PERNYATAAN MENERIMA PEMBERITAHUAN ADANYA RISIKO
                                                    <br />Dengan mengisi kolom “YA” di bawah, saya menyatakan bahwa saya
                                                    telah
                                                    menerima
                                                    “DOKUMEN PEMBERITAHUAN ADANYA RISIKO” mengerti dan menyetujui
                                                    isinya.
                                                    Pernyataan menerima
                                                </label>
                                                <div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="risk_document_radio" value="yes" onchange="savekyc()" onkeypress="savekyc()" onclick="savekyc()" @isset($DataContent->risk_document) @if($DataContent->risk_document->radio==='yes') checked @endif @endisset />
                                                        <label class="form-check-label required">Ya</label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="risk_document_radio" value="no" onchange="savekyc()" onkeypress="savekyc()" onclick="savekyc()" @isset($DataContent->risk_document) @if($DataContent->risk_document->radio==='no') checked @endif @endisset />
                                                        <label class="form-check-label">Tidak</label>
                                                    </div>
                                                    <label id="risk_document_radio-error" class="error" for="risk_document_radio" style="display: none;">Klik "Ya" untuk melanjutkan.</label>
                                                </div>
                                                <p>Menyatakan pada tanggal : {{date('d-m-Y',strtotime(date("m.d.y")))}}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-4 hidden trading_condition">
                                            <div class="alert alert-dark" role="alert">
                                                Perjanjian Pemberian Amanat Secara Elektronik Online Untuk Transaksi
                                                Kontrak
                                                Derivatif Dalam Sistem Perdagangan Alternatif - Formulir Nomor
                                                107.PBK.05.2
                                            </div>
                                            <h5>PERHATIAN!</h5>
                                            <h5 class="text-justify">
                                                PERJANJIAN INI MERUPAKAN KONTRAK HUKUM. HARAP
                                                DIBACA
                                                DENGAN SEKSAMA.
                                                "TICK / CENTANG" DI KOTAK YANG TERSEDIA
                                            </h5>
                                            <p>
                                                Pada hari ini ,
                                                <b>
                                                    {{date('l',strtotime(date("m.d.y")))}}, tanggal {{date('d',strtotime(date("m.d.y")))}},bulan {{date('F',strtotime(date("m.d.y")))}}, tahun bulan {{date('Y',strtotime(date("m.d.y")))}} kami
                                                    yang
                                                    mengisi
                                                    perjanjian dibawah ini:
                                                </b>
                                            </p>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Nama</label>
                                                <div class="col-sm-8">
                                                    <span class="form-control-plaintext">{{$DataContent->full_name}}</span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Pekerjaan/Jabatan</label>
                                                <div class="col-sm-8">
                                                    <span class="form-control-plaintext" data-bind="job_details_title">{{isset($DataContent->job_details)?$DataContent->job_details->more_details->title:''}}</span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Alamat</label>
                                                <div class="col-sm-8">
                                                    <span class="form-control-plaintext" data-bind="home_address">{{$DataContent->home_address}}</span>
                                                </div>
                                            </div>
                                            <div class="form-group position-relative error-l-100 text-justify ">
                                                <label>
                                                    dalam hal ini bertindak untuk dan atas nama sendiri, yang selanjutnya disebut <strong>Nasabah</strong>,
                                                </label>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Nama</label>
                                                <div class="col-sm-8">
                                                    <span class="form-control-plaintext">PT. Maxco Futures</span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Pekerjaan/Jabatan</label>
                                                <div class="col-sm-8">
                                                    <span class="form-control-plaintext" data-bind="job_details_title">WAKIL PIALANG</span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Alamat</label>
                                                <div class="col-sm-8">
                                                    <span class="form-control-plaintext" data-bind="home_address">Gedung Panin Pusat lantai 5 (lima), Jl. Jend Sudirman Kav 1- Senayan Jakarta</span>
                                                </div>
                                            </div>
                                            <div class="form-group position-relative error-l-100 text-justify ">
                                                <label>
                                                    dalam hal ini bertindak untuk dan atas nama PT. Maxco Futures
                                                    yang
                                                    selanjutnya disebut Pialang Berjangka,
                                                    Nasabah dan Pialang Berjangka secara bersama – sama selanjutnya
                                                    disebut
                                                    Para Pihak.
                                                    Para pihak sepakat untuk mengadakan Perjanjian Pemberian Amanat
                                                    untuk
                                                    melakukan transaksi penjualan maupun pembelian Kontrak Derivatif
                                                    dalam
                                                    Sistem Perdagangan Alternatif dengan ketentuan sebagai
                                                    berikut:
                                                </label>
                                            </div>
                                            <div class="form-group position-relative error-l-100 text-justify ">
                                                <label class="">
                                                    1. Margin dan Pembayaran Lainnya
                                                    <br />(1)Nasabah menempatkan sejumlah dana(Margin) ke Rekening
                                                    Terpisah
                                                    (Segregated Account) Pialang Berjangka sebagai Margin Awal dan
                                                    wajib
                                                    mempertahankannya sebagaimana ditetapkan.
                                                    <br />(2)membayar biaya-biaya yang diperlukan untuk transaksi, yaitu
                                                    biaya
                                                    transaksi, pajak, komisi, dan biaya pelayanan, biaya bunga
                                                    sesuai
                                                    tingkat yang berlaku, dan biaya lainnya yang dapat
                                                    dipertanggungjawabkan berkaitan dengan transaksi sesuai amanat
                                                    Nasabah, maupun biaya rekening Nasabah.
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="trading_conditions_1_title_en_ref" value="1" onchange="savekyc()" onkeypress="savekyc()" onclick="savekyc()" required @isset($DataContent->trading_condition) @if($DataContent->trading_condition->trading_conditions_1_title_en_ref==='1') checked @endif @endisset/>
                                                    <label class="form-check-label required">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                                <label id="trading_conditions_1_title_en_ref-error" class="error" for="trading_conditions_1_title_en_ref"></label>
                                            </div>
                                            <div class="form-group position-relative error-l-100 text-justify hidden trading_con_2">
                                                <label class="">
                                                    2. Pelaksanaan Transaksi
                                                    <br />(1)Setiap transaksi Nasabah dilaksanakan secara elektronik
                                                    on-line oleh
                                                    Nasabah yang bersangkutan;
                                                    <br />(2)Setiap amanat Nasabah yang diterima dapat langsung
                                                    dilaksanakan
                                                    sepanjang nilai Margin yang tersedia pada rekeningnya mencukupi
                                                    dan eksekusinya dapat menimbulkan perbedaan waktu terhadap
                                                    proses pelaksanaan transaksi tersebut. Nasabah harus mengetahui
                                                    posisi Margin dan posisi terbuka sebelum memberikan amanat untuk
                                                    transaksi berikutnya.
                                                    <br />(3)Setiap transaksi Nasabah secara bilateral dilawankan dengan
                                                    Penyelenggara Sistem Perdagangan Alternatif PT. Maxco Futures
                                                    yang
                                                    bekerjasama dengan Pialang Berjangka.
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="trading_conditions_2_title_en_ref" value="1" onchange="savekyc()" onkeypress="savekyc()" onclick="savekyc()" required @isset($DataContent->trading_condition) @if($DataContent->trading_condition->trading_conditions_2_title_en_ref==='1') checked @endif @endisset />
                                                    <label class="form-check-label required" for="dotrax">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                                <label id="trading_conditions_2_title_en_ref-error" class="error" for="trading_conditions_2_title_en_ref"></label>
                                            </div>
                                            <div class="form-group position-relative error-l-100 text-justify hidden trading_con_3">
                                                <label class="">
                                                    3. Kewajiban Memelihara Margin
                                                    <br />(1)Nasabah wajib memelihara/memenuhi tingkat Margin yang harus
                                                    tersedia di rekening pada Pialang Berjangka sesuai dengan jumlah
                                                    yang telah ditetapkan baik diminta ataupun tidak oleh Pialang
                                                    Berjangka.
                                                    <br />(2)Apabila jumlah Margin memerlukan penambahan maka Pialang
                                                    Berjangka wajib memberitahukan dan memintakan kepada Nasabah
                                                    untuk menambah Margin segera.
                                                    <br />(3)Apabila jumlah Margin memerlukan tambahan (Call Margin) maka
                                                    Nasabah wajib melakukan penyerahan Call Margin selambat-
                                                    lambatnya sebelum dimulai hari perdagangan berikutnya. Kewajiban
                                                    Nasabah sehubungan dengan penyerahan Call Margin tidak terbatas
                                                    pada jumlah Margin awal.
                                                    <br />(4)Pialang Berjangka tidak berkewajiban melaksanakan amanat
                                                    untuk
                                                    melakukan transaksi yang baru dari Nasabah sebelum Call Margin
                                                    dipenuhi.
                                                    <br />(5)Untuk memenuhi kewajiban Call Margin dan keuangan lainnya
                                                    dari
                                                    Nasabah, Pialang Berjangka dapat mencairkan dana Nasabah yang
                                                    ada di Pialang Berjangka.
                                                    <br />
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="trading_conditions_3_title_en_ref" value="1" onchange="savekyc()" onkeypress="savekyc()" onclick="savekyc()" required @isset($DataContent->trading_condition) @if($DataContent->trading_condition->trading_conditions_3_title_en_ref==='1') checked @endif @endisset />
                                                    <label class="form-check-label required" for="margin1">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                                <label id="trading_conditions_3_title_en_ref-error" class="error" for="trading_conditions_3_title_en_ref"></label>
                                            </div>
                                            <div class="form-group position-relative error-l-100 text-justify hidden trading_con_4">
                                                <label class="">
                                                    4. Hak Pialang Berjangka Melikuidasi Posisi Nasabah
                                                    <br />Nasabah bertanggung jawab memantau/mengetahui posisi terbukanya
                                                    secara terus- menerus dan memenuhi kewajibannya. Apabila dalam
                                                    jangka
                                                    waktu tertentu dana pada rekening Nasabah kurang dari yang
                                                    dipersyaratkan, Pialang Berjangka dapat menutup posisi terbuka
                                                    Nasabah
                                                    secara keseluruhan atau sebagian, membatasi transaksi, atau
                                                    tindakan
                                                    lain untuk melindungi diri dalam pemenuhan Margin tersebut
                                                    dengan
                                                    terlebih dahulu memberitahu atau tanpa memberitahu Nasabah dan
                                                    Pialang Berjangka tidak bertanggung jawab atas kerugian yang
                                                    timbul
                                                    akibat tindakan tersebut.
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="trading_conditions_4_title_en_ref" required value="1" @isset($DataContent->trading_condition) @if($DataContent->trading_condition->trading_conditions_4_title_en_ref==='1') checked @endif @endisset />
                                                    <label class="form-check-label required" for="likuiditas1">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                                <label id="trading_conditions_4_title_en_ref-error" class="error" for="trading_conditions_4_title_en_ref"></label>
                                            </div>
                                            <div class="form-group position-relative error-l-100 text-justify hidden trading_con_5">
                                                <label class="">
                                                    5. Penggantian Kerugian Tidak Adanya Penutupan Posisi
                                                    Apabila Nasabah tidak mampu melakukan penutupan atas transaksi
                                                    yang
                                                    jatuh tempo, Pialang Berjangka dapat melakukan penutupan atas
                                                    transaksi
                                                    Nasabah yang terjadi. Nasabah wajib membayar biaya-biaya,
                                                    termasuk
                                                    biaya kerugian dan premi yang telah dibayarkan oleh Pialang
                                                    Berjangka,
                                                    dan apabila Nasabah lalai untuk membayar biaya-biaya tersebut,
                                                    Pialang
                                                    Berjangka berhak untuk mengambil pembayaran dari dana Nasabah.
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="trading_conditions_5_title_en_ref" value="1" onchange="savekyc()" onkeypress="savekyc()" onclick="savekyc()" required @isset($DataContent->trading_condition) @if($DataContent->trading_condition->trading_conditions_5_title_en_ref==='1') checked @endif @endisset />
                                                    <label class="form-check-label required" for="closeposition1">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                                <label id="trading_conditions_5_title_en_ref-error" class="error" for="trading_conditions_5_title_en_ref"></label>

                                            </div>
                                            <div class="form-group position-relative error-l-100 text-justify hidden trading_con_6">
                                                <label class="">
                                                    6. Pialang Berjangka Dapat Membatasi Posisi
                                                    <br />Nasabah mengakui hak Pialang Berjangka untuk membatasi posisi
                                                    terbuka
                                                    Kontrak dan Nasabah tidak melakukan transaksi melebihi batas
                                                    yang telah
                                                    ditetapkan tersebut.
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="trading_conditions_6_title_en_ref" value="1" onchange="savekyc()" onkeypress="savekyc()" onclick="savekyc()" required @isset($DataContent->trading_condition) @if($DataContent->trading_condition->trading_conditions_6_title_en_ref==='1') checked @endif @endisset />
                                                    <label class="form-check-label required" for="limitposisi1">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                                <label id="trading_conditions_6_title_en_ref-error" class="error" for="trading_conditions_6_title_en_ref"></label>

                                            </div>
                                            <div class="form-group position-relative error-l-100 text-justify hidden trading_con_7">
                                                <label class="">
                                                    7. Tidak Ada Jaminan atas Informasi atau Rekomendasi
                                                    Nasabah mengakui bahwa:
                                                    <br />(1)Informasi dan rekomendasi yang diberikan oleh Pialang
                                                    Berjangka
                                                    kepada Nasabah tidak selalu lengkap dan perlu diverifikasi.
                                                    <br />(2)Pialang Berjangka tidak menjamin bahwa informasi dan
                                                    rekomendasi
                                                    yang diberikan merupakan informasi yang akurat dan lengkap.
                                                    <br />(3)Informasi dan rekomendasi yang diberikan oleh Wakil Pialang
                                                    Berjangka
                                                    yang satu dengan yang lain mungkin berbeda karena perbedaan
                                                    analisis fundamental atau teknikal. Nasabah menyadari bahwa ada
                                                    kemungkinan Pialang Berjangka dan pihak terafiliasinya memiliki
                                                    posisi di pasar dan memberikan rekomendasi tidak konsisten
                                                    kepada
                                                    Nasabah.
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="trading_conditions_7_title_en_ref" required value="1" @isset($DataContent->trading_condition) @if($DataContent->trading_condition->trading_conditions_7_title_en_ref==='1') checked @endif @endisset />
                                                    <label class="form-check-label required" for="accurateinfo1">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                                <label id="trading_conditions_7_title_en_ref-error" class="error" for="trading_conditions_7_title_en_ref"></label>

                                            </div>
                                            <div class="form-group position-relative error-l-100 text-justify hidden trading_con_8">
                                                <label class="">
                                                    8. Pembatasan Tanggung Jawab Pialang Berjangka.
                                                    <br />(1)Pialang Berjangka tidak bertanggung jawab untuk memberikan
                                                    penilaian
                                                    kepada Nasabah mengenai iklim, pasar, keadaan politik dan
                                                    ekonomi
                                                    nasional dan internasional, nilai Kontrak Derivatif, kolateral,
                                                    atau
                                                    memberikan nasihat mengenai keadaan pasar. Pialang Berjangka
                                                    hanya memberikan pelayanan untuk melakukan transaksi secara
                                                    jujur
                                                    serta memberikan laporan atas transaksi tersebut.
                                                    <br />(2)Perdagangan sewaktu-waktu dapat dihentikan oleh pihak yang
                                                    memiliki
                                                    otoritas (Bappebti/Bursa Berjangka) tanpa pemberitahuan terlebih
                                                    dahulu kepada Nasabah. Atas posisi terbuka yang masih dimiliki
                                                    oleh
                                                    Nasabah pada saat perdagangan tersebut dihentikan, maka akan
                                                    diselesaikan (likuidasi) berdasarkan pada peraturan/ketentuan
                                                    yang
                                                    dikeluarkan dan ditetapkan oleh pihak otoritas tersebut, dan
                                                    semua
                                                    kerugian serta biaya yang timbul sebagai akibat dihentikannya
                                                    transaksi oleh pihak otoritas perdagangan tersebut, menjadi
                                                    beban
                                                    dan tanggung jawab Nasabah sepenuhnya.
                                                    <br />
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="trading_conditions_8_title_en_ref" required value="1" @isset($DataContent->trading_condition) @if($DataContent->trading_condition->trading_conditions_8_title_en_ref==='1') checked @endif @endisset />
                                                    <label class="form-check-label required" for="limitationofliability1">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                                <label id="trading_conditions_8_title_en_ref-error" class="error" for="trading_conditions_8_title_en_ref"></label>

                                            </div>
                                            <div class="form-group position-relative error-l-100 text-justify hidden trading_con_9">
                                                <label class="">
                                                    9. Transaksi Harus Mematuhi Peraturan Yang Berlaku
                                                    <br />Semua transaksi dilakukan sendiri oleh Nasabah dan wajib
                                                    mematuhi
                                                    peraturan perundang-undangan di bidang Perdagangan Berjangka,
                                                    kebiasaan dan interpretasi resmi yang ditetapkan oleh Bappebti
                                                    atau
                                                    Bursa
                                                    Berjangka.
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="trading_conditions_9_title_en_ref" value="1" onchange="savekyc()" onkeypress="savekyc()" onclick="savekyc()" required @isset($DataContent->trading_condition) @if($DataContent->trading_condition->trading_conditions_9_title_en_ref==='1') checked @endif @endisset />
                                                    <label class="form-check-label required" for="complyregulations1">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                                <label id="trading_conditions_9_title_en_ref-error" class="error" for="trading_conditions_9_title_en_ref"></label>

                                            </div>
                                            <div class="form-group position-relative error-l-100 text-justify hidden trading_con_10">
                                                <label class="">
                                                    10. Pialang Berjangka tidak Bertanggung jawab atas Kegagalan
                                                    Komunikasi
                                                    <br />Pialang Berjangka tidak bertanggung jawab atas keterlambatan
                                                    atau tidak
                                                    tepat waktunya pengiriman amanat atau informasi lainnya yang
                                                    disebabkan oleh kerusakan fasilitas komunikasi atau sebab lain
                                                    diluar
                                                    kontrol Pialang Berjangka.
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="trading_conditions_10_title_en_ref" value="1" onchange="savekyc()" onkeypress="savekyc()" onclick="savekyc()" required @isset($DataContent->trading_condition) @if($DataContent->trading_condition->trading_conditions_10_title_en_ref==='1') checked @endif @endisset />
                                                    <label class="form-check-label required" for="pbk0402point10read">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                                <label id="trading_conditions_10_title_en_ref-error" class="error" for="trading_conditions_10_title_en_ref"></label>

                                            </div>

                                            <div class="form-group position-relative error-l-100 text-justify hidden trading_con_11">
                                                <label class="">
                                                    11. Konfirmasi
                                                    <br />(1)Konfirmasi dari Nasabah dapat berupa surat, telex, media
                                                    lain, surat
                                                    elektronik, secara tertulis ataupun rekaman suara.
                                                    <br />(2)Pialang Berjangka berkewajiban menyampaikan konfirmasi
                                                    transaksi,
                                                    laporan rekening, permintaan Call Margin, dan pemberitahuan
                                                    lainnya
                                                    kepada Nasabah secara akurat, benar dan secepatnya pada alamat
                                                    (email) Nasabah sesuai dengan yang tertera dalam rekening
                                                    Nasabah.
                                                    Apabila dalam jangka waktu 2 x 24 jam setelah amanat jual atau
                                                    beli
                                                    disampaikan, tetapi Nasabah belum menerima konfirmasi melalui
                                                    alamat email Nasabah dan/atau sistem transaksi, Nasabah segera
                                                    memberitahukan hal tersebut kepada Pialang Berjangka melalui
                                                    telepon dan disusul dengan pemberitahuan tertulis.
                                                    <br />(3)Jika dalam waktu 2 x 24 jam sejak tanggal penerimaan
                                                    konfirmasi
                                                    tersebut tidak ada sanggahan dari Nasabah maka konfirmasi
                                                    Pialang
                                                    Berjangka dianggap benar dan sah.
                                                    <br />(4)Kekeliruan atas konfirmasi yang diterbitkan Pialang Berjangka
                                                    akan
                                                    diperbaiki oleh Pialang Berjangka sesuai keadaan yang sebenarnya
                                                    dan demi hukum konfirmasi yang lama batal.
                                                    <br />(5)Nasabah tidak bertanggung jawab atas transaksi yang
                                                    dilaksanakan atas
                                                    rekeningnya apabila konfirmasi tersebut tidak disampaikan secara
                                                    benar dan akurat.
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="trading_conditions_11_title_en_ref" value="1" onchange="savekyc()" onkeypress="savekyc()" onclick="savekyc()" required @isset($DataContent->trading_condition) @if($DataContent->trading_condition->trading_conditions_11_title_en_ref==='1') checked @endif @endisset />
                                                    <label class="form-check-label required" for="pbk0402point11read">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                                <label id="trading_conditions_11_title_en_ref-error" class="error" for="trading_conditions_11_title_en_ref"></label>

                                            </div>
                                            <div class="form-group position-relative error-l-100 text-justify hidden trading_con_12">
                                                <label class="">
                                                    12. Kebenaran Informasi Nasabah
                                                    <br />Nasabah memberikan informasi yang benar dan akurat mengenai data
                                                    Nasabah yang diminta oleh Pialang Berjangka dan akan
                                                    memberitahukan
                                                    paling lambat dalam waktu 3 (tiga) hari kerja setelah terjadi
                                                    perubahan,
                                                    termasuk perubahan kemampuan keuangannya untuk terus
                                                    melaksanakan
                                                    transaksi.
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="trading_conditions_12_title_en_ref" required value="1" @isset($DataContent->trading_condition) @if($DataContent->trading_condition->trading_conditions_12_title_en_ref==='1') checked @endif @endisset />
                                                    <label class="form-check-label required" for="pbk0402point12read">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                                <label id="trading_conditions_12_title_en_ref-error" class="error" for="trading_conditions_12_title_en_ref"></label>

                                            </div>

                                        </div>
                                        <div class="col-md-4">
                                            <div class="alert alert-dark" role="alert">
                                                Perjanjian Pemberian Amanat Secara Elektronik Online Untuk Transaksi
                                                Kontrak
                                                Derivatif Dalam Sistem Perdagangan Alternatif - Formulir Nomor
                                                107.PBK.05.2
                                            </div>
                                            <div class="form-group position-relative error-l-100 text-justify hidden trading_con_13">
                                                <label class="">
                                                    13. Komisi Transaksi
                                                    <br />Nasabah mengetahui dan menyetujui bahwa Pialang Berjangka berhak
                                                    untuk memungut komisi atas transaksi yang telah dilaksanakan,
                                                    dalam
                                                    jumlah sebagaimana akan ditetapkan dari waktu ke waktu oleh
                                                    Pialang
                                                    Berjangka. Perubahan beban (fees) dan biaya lainnya harus
                                                    disetujui
                                                    secara tertulis oleh Para Pihak.
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="trading_conditions_13_title_en_ref" required value="1" @isset($DataContent->trading_condition) @if($DataContent->trading_condition->trading_conditions_13_title_en_ref==='1') checked @endif @endisset />
                                                    <label class="form-check-label required" for="pbk0402point13read">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                                <label id="trading_conditions_13_title_en_ref-error" class="error" for="trading_conditions_13_title_en_ref"></label>

                                            </div>
                                            <div class="form-group position-relative error-l-100 text-justify hidden trading_con_14">
                                                <label class="">
                                                    14. Pemberian Kuasa
                                                    <br />Nasabah memberikan kuasa kepada Pialang Berjangka untuk
                                                    menghubungi bank, lembaga keuangan, Pialang Berjangka lain, atau
                                                    institusi lain yang terkait untuk memperoleh keterangan atau
                                                    verifikasi
                                                    mengenai informasi yang diterima dari Nasabah. Nasabah mengerti
                                                    bahwa
                                                    penelitian mengenai data hutang pribadi dan bisnis dapat
                                                    dilakukan oleh
                                                    Pialang Berjangka apabila diperlukan. Nasabah diberikan
                                                    kesempatan
                                                    untuk memberitahukan secara tertulis dalam jangka waktu yang
                                                    telah
                                                    disepakati untuk melengkapi persyaratan yang diperlukan.
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="trading_conditions_14_title_en_ref" value="1" onchange="savekyc()" onkeypress="savekyc()" onclick="savekyc()" required @isset($DataContent->trading_condition) @if($DataContent->trading_condition->trading_conditions_14_title_en_ref==='1') checked @endif @endisset />
                                                    <label class="form-check-label required" for="pbk0402point14read">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                                <label id="trading_conditions_14_title_en_ref-error" class="error" for="trading_conditions_14_title_en_ref"></label>

                                            </div>
                                            <div class="form-group position-relative error-l-100 text-justify hidden trading_con_15">
                                                <label class="">
                                                    15. Pemindahan Dana
                                                    <br />Pialang Berjangka dapat setiap saat mengalihkan dana dari satu
                                                    rekening
                                                    ke rekening lainnya berkaitan dengan kegiatan transaksi yang
                                                    dilakukan
                                                    Nasabah seperti pembayaran komisi, pembayaran biaya transaksi,
                                                    kliring
                                                    dan keterlambatan dalam memenuhi kewajibannya, tanpa terlebih
                                                    dahulu
                                                    memberitahukan kepada Nasabah. Transfer yang telah dilakukan
                                                    akan
                                                    segera diberitahukan secara tertulis kepada Nasabah.
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="trading_conditions_15_title_en_ref" value="1" onchange="savekyc()" onkeypress="savekyc()" onclick="savekyc()" required @isset($DataContent->trading_condition) @if($DataContent->trading_condition->trading_conditions_15_title_en_ref==='1') checked @endif @endisset />
                                                    <label class="form-check-label required" for="pbk0402point15read">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                                <label id="trading_conditions_15_title_en_ref-error" class="error" for="trading_conditions_15_title_en_ref"></label>

                                            </div>
                                            <div class="form-group position-relative error-l-100 text-justify hidden trading_con_16">
                                                <label class="">
                                                    16. Pemberitahuan
                                                    <br />(1)Semua komunikasi, uang, surat berharga, dan kekayaan lainnya
                                                    harus
                                                    dikirimkan langsung ke alamat Nasabah seperti tertera dalam
                                                    rekeningnya atau alamat lain yang ditetapkan/diberitahukan
                                                    secara
                                                    tertulis oleh Nasabah.
                                                    <br />(2)Semua uang, harus disetor atau ditransfer langsung oleh
                                                    Nasabah ke
                                                    Rekening Terpisah (Segregated Account) Pialang Berjangka:
                                                </label>
                                                <div class="form-group row">
                                                    <label class="col-sm-4 col-form-label">Nama</label>
                                                    <div class="col-sm-8">
                                                        <span class="form-control-plaintext">PT. Maxco Futures</span>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-sm-4 col-form-label">Alamat</label>
                                                    <div class="col-sm-8">
                                                        <span class="form-control-plaintext">Gedung Panin Pusat Lantai Dasar Jl. Jend. Sudirman kav. 1
                                                            Senayan
                                                            Jakarta
                                                            10270</span>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-sm-4 col-form-label">Bank</label>
                                                    <div class="col-sm-8">
                                                        <span class="form-control-plaintext">BCA Sudirman (Jakarta)</span>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-sm-4 col-form-label">No. Rekening Terpisah</label>
                                                    <div class="col-sm-8">
                                                        <span class="form-control-plaintext">035 311 5666 (IDR)
                                                            <br />035 311 9777 (USD)</span>
                                                    </div>
                                                </div>
                                                <label>
                                                    dan dianggap sudah diterima oleh Pialang Berjangka apabila sudah
                                                    ada tanda terima bukti setor atau transfer dari pegawai Pialang
                                                    Berjangka.
                                                    (3)Semua surat berharga, kekayaan lainnya, atau komunikasi harus
                                                    dikirim kepada Pialang Berjangka:
                                                </label>
                                                <div class="form-group row">
                                                    <label class="col-sm-4 col-form-label">Nama</label>
                                                    <div class="col-sm-8">
                                                        <span class="form-control-plaintext">PT. Maxco Futures</span>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-sm-4 col-form-label">Alamat</label>
                                                    <div class="col-sm-8">
                                                        <span class="form-control-plaintext">Gedung Panin Pusat Lantai Dasar Jl. Jend. Sudirman kav. 1
                                                            Senayan
                                                            Jakarta
                                                            10270</span>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-sm-4 col-form-label">Telepon</label>
                                                    <div class="col-sm-8">
                                                        <span class="form-control-plaintext">021-720 5868 (Hunting)</span>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-sm-4 col-form-label">Faksimili</label>
                                                    <div class="col-sm-8">
                                                        <span class="form-control-plaintext">021-571 0974</span>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-sm-4 col-form-label">Email</label>
                                                    <div class="col-sm-8">
                                                        <span class="form-control-plaintext">cs@maxcofutures.co.id</span>
                                                    </div>
                                                </div>
                                                <label>
                                                    dan dianggap sudah diterima oleh Pialang Berjangka apabila sudah
                                                    ada tanda bukti penerimaan dari pegawai Pialang Berjangka.
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="trading_conditions_16_title_en_ref" value="1" onchange="savekyc()" onkeypress="savekyc()" onclick="savekyc()" required @isset($DataContent->trading_condition) @if($DataContent->trading_condition->trading_conditions_16_title_en_ref==='1') checked @endif @endisset />
                                                    <label class="form-check-label required" for="pbk0402point16read">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                                <label id="trading_conditions_16_title_en_ref-error" class="error" for="trading_conditions_16_title_en_ref"></label>

                                            </div>
                                            <div class="form-group position-relative error-l-100 text-justify hidden trading_con_17">
                                                <label class="">
                                                    17. Dokumen Pemberitahuan Adanya Risiko
                                                    <br />Nasabah mengakui menerima dan mengerti Dokumen Pemberitahuan
                                                    Adanya Risiko.
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="trading_conditions_17_title_en_ref" value="1" onchange="savekyc()" onkeypress="savekyc()" onclick="savekyc()" required @isset($DataContent->trading_condition) @if($DataContent->trading_condition->trading_conditions_17_title_en_ref==='1') checked @endif @endisset />
                                                    <label class="form-check-label required" for="pbk0402point17read">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                                <label id="trading_conditions_17_title_en_ref-error" class="error" for="trading_conditions_17_title_en_ref"></label>

                                            </div>
                                            <div class="form-group position-relative error-l-100 text-justify hidden trading_con_18">
                                                <label class="">
                                                    18. Jangka Waktu Perjanjian dan Pengakhiran
                                                    <br />(1)Perjanjian ini mulai berlaku terhitung sejak tanggal
                                                    dilakukannya
                                                    konfirmasi oleh Pialang Berjangka dengan diterimanya Bukti
                                                    Konfirmasi
                                                    Penerimaan Nasabah dari Pialang Berjangka oleh Nasabah.
                                                    <br />(2)Nasabah dapat mengakhiri Perjanjian ini hanya jika Nasabah
                                                    sudah
                                                    tidak lagi memiliki posisi terbuka dan tidak ada kewajiban
                                                    Nasabah
                                                    yang diemban oleh atau terhutang kepada Pialang Berjangka.
                                                    <br />(3)Pengakhiran tidak membebaskan salah satu Pihak dari tanggung
                                                    jawab
                                                    atau kewajiban yang terjadi sebelum pemberitahuan tersebut.
                                                    <br />
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="trading_conditions_18_title_en_ref" value="1" onchange="savekyc()" onkeypress="savekyc()" onclick="savekyc()" required @isset($DataContent->trading_condition) @if($DataContent->trading_condition->trading_conditions_18_title_en_ref==='1') checked @endif @endisset />
                                                    <label class="form-check-label required" for="pbk0402point18read">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                                <label id="trading_conditions_18_title_en_ref-error" class="error" for="trading_conditions_18_title_en_ref"></label>

                                            </div>
                                            <div class="form-group position-relative error-l-100 text-justify hidden trading_con_19">
                                                <label class="">
                                                    19. Berakhirnya Perjanjian
                                                    <br />Perjanjian dapat berakhir dalam hal Nasabah:
                                                    <br />(1)dinyatakan pailit, memiliki hutang yang sangat besar, dalam
                                                    proses
                                                    peradilan, menjadi hilang ingatan, mengundurkan diri atau
                                                    meninggal;
                                                    <br />(2)tidak dapat memenuhi atau mematuhi perjanjian ini dan/atau
                                                    melakukan pelanggaran terhadapnya;
                                                    <br />(3)berkaitan dengan butir (1) dan (2) tersebut diatas, Pialang
                                                    Berjangka
                                                    dapat :
                                                    <br />i) meneruskan atau menutup posisi Nasabah tersebut
                                                    setelahmempertimbangkannya secara cermat dan jujur ; dan
                                                    <br />ii) menolaktransaksidari Nasabah.
                                                    <br />(4)Pengakhiran Perjanjian sebagaimana dimaksud dengan angka (1)
                                                    dan (2)
                                                    tersebut di atas tidak melepaskan kewajiban dari Para Pihak yang
                                                    berhubungan dengan penerimaan atau kewajiban pembayaran atau
                                                    pertanggungjawaban kewajiban lainnya yang timbul dari
                                                    Perjanjian.
                                                    <br />
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="trading_conditions_19_title_en_ref" value="1" onchange="savekyc()" onkeypress="savekyc()" onclick="savekyc()" required @isset($DataContent->trading_condition) @if($DataContent->trading_condition->trading_conditions_19_title_en_ref==='1') checked @endif @endisset />
                                                    <label class="form-check-label required" for="pbk0402point19read">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                                <label id="trading_conditions_19_title_en_ref-error" class="error" for="trading_conditions_19_title_en_ref"></label>

                                            </div>
                                            <div class="form-group position-relative error-l-100 text-justify hidden trading_con_20">
                                                <label class="">
                                                    20. Force Majeur
                                                    <br />Tidak ada satupun pihak di dalam Perjanjian dapat diminta
                                                    pertanggungjawabannya untuk suatu keterlambatan atau
                                                    terhalangnya
                                                    memenuhi kewajiban berdasarkan Perjanjian yang diakibatkan oleh
                                                    suatu
                                                    sebab yang berada di luar kemampuannya atau kekuasaannya (force
                                                    majeur), sepanjang pemberitahuan tertulis mengenai sebab itu
                                                    disampaikannya kepada pihak lain dalam Perjanjian dalam waktu
                                                    tidak
                                                    lebih dari 24 (dua puluh empat) jam sejak timbulnya sebab itu.
                                                    Yang dimaksud dengan Force Majeur dalam Perjanjian adalah
                                                    peristiwa
                                                    kebakaran, bencana alam (seperti gempa bumi, banjir, angin
                                                    topan,
                                                    petir),
                                                    pemogokan umum, huru hara, peperangan, perubahan terhadap
                                                    peraturan
                                                    perundang-undangan yang berlaku dan kondisi di bidang ekonomi,
                                                    keuangan dan Perdagangan Berjangka, pembatasan yang dilakukan
                                                    oleh
                                                    otoritas Perdagangan Berjangka dan Bursa Berjangka serta
                                                    terganggunya
                                                    sistem perdagangan, kliring dan penyelesaian transaksi Kontrak
                                                    Berjangka
                                                    di mana transaksi dilaksanakan yang secara langsung mempengaruhi
                                                    pelaksanaan pekerjaan berdasarkan Perjanjian.
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="trading_conditions_20_title_en_ref" value="1" onchange="savekyc()" onkeypress="savekyc()" onclick="savekyc()" required @isset($DataContent->trading_condition) @if($DataContent->trading_condition->trading_conditions_20_title_en_ref==='1') checked @endif @endisset />
                                                    <label class="form-check-label required" for="pbk0402point20read">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                                <label id="trading_conditions_20_title_en_ref-error" class="error" for="trading_conditions_20_title_en_ref"></label>

                                            </div>
                                            <div class="form-group position-relative error-l-100 text-justify hidden trading_con_21">
                                                <label class="">
                                                    21. Perubahan atas Isian dalam Perjanjian Pemberian Amanat
                                                    Perubahan atas isian dalam Perjanjian ini hanya dapat dilakukan
                                                    atas
                                                    persetujuan Para Pihak, atau Pialang Berjangka telah
                                                    memberitahukan
                                                    secara tertulis perubahan yang diinginkan, dan Nasabah tetap
                                                    memberikan
                                                    perintah untuk transaksi dengan tanpa memberikan tanggapan
                                                    secara
                                                    tertulis atas usul perubahan tersebut. Tindakan Nasabah tersebut
                                                    dianggap
                                                    setuju atas usul perubahan tersebut.
                                                </label>
                                                <div class=" form-check">
                                                    <input class="form-check-input" type="checkbox" name="trading_conditions_21_title_en_ref" value="1" onchange="savekyc()" onkeypress="savekyc()" onclick="savekyc()" required @isset($DataContent->trading_condition) @if($DataContent->trading_condition->trading_conditions_21_title_en_ref==='1') checked @endif @endisset />
                                                    <label class="form-check-label required" for="pbk0402point21read">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                                <label id="trading_conditions_21_title_en_ref-error" class="error" for="trading_conditions_21_title_en_ref"></label>
                                            </div>
                                            <div class="form-group position-relative error-l-100 text-justify hidden trading_con_22">
                                                <label class="">
                                                    22. Penyelesaian Perselisihan
                                                    <br />(1)Semua perselisihan dan perbedaan pendapat yang timbul dalam
                                                    pelaksanaan Perjanjian ini wajib diselesaikan terlebih dahulu
                                                    secara
                                                    musyawarah untuk mencapai mufakat antara Para Pihak.
                                                    <br />(2)Apabila perselisihan dan perbedaan pendapat yang timbul tidak
                                                    dapat
                                                    diselesaikan secara musyawarah untuk mencapai mufakat, Para
                                                    Pihak
                                                    wajib memanfaatkan sarana penyelesaian perselisihan yang
                                                    tersedia di
                                                    Bursa Berjangka.
                                                    <br />(3)Apabila perselisihan dan perbedaan pendapat yang timbul tidak
                                                    dapat
                                                    diselesaikan melalui cara sebagaimana dimaksud pada angka (1)
                                                    dan
                                                    angka (2), maka Para Pihak sepakat untuk menyelesaikan
                                                    perselisihan
                                                    melalui *):
                                                    <div class="form-check">
                                                        <input class="form-check-input account_type_reguler" type="radio" name="trading_conditions_22_title_1" value="1" checked />
                                                        <label class="form-check-label" for="regular">
                                                            a. Badan Arbitrase Perdagangan Berjangka Komoditi (BAKTI)
                                                            berdasarkan Peraturan dan Prosedur Badan Arbitrase Perdagangan
                                                            Berjangka Komoditi (BAKTI);atau
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input account_type_reguler" type="radio" name="trading_conditions_22_title_1" value="2" />
                                                        <label class="form-check-label" for="regular">
                                                            b. Pengadilan Negeri Jakarta Selatan
                                                        </label>
                                                    </div>

                                                    <br />
                                                    <br />(4)Kantor atau kantor cabang Pialang Berjangka terdekat dengan
                                                    domisili
                                                    Nasabah tempat penyelesaian dalam hal terjadi perselisihan.
                                                    Daftar Kantor Kantor yang dipilih (salah
                                                    satu):
                                                    <div class="form-check">
                                                        <input class="form-check-input account_type_reguler" type="radio" name="trading_conditions_22_title_2" value="1" checked />
                                                        <label class="form-check-label" for="regular">
                                                            a. Head Office - Address : Panin Bank Centre - First Floor Jl.
                                                            Jend.
                                                            Sudirman Kav. 1. Senayan, Jakarta Selatan 10270 Indonesia
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input account_type_reguler" type="radio" name="trading_conditions_22_title_2" value="2" />
                                                        <label class="form-check-label" for="regular">
                                                            b. Surabaya - Address : Jl. Dharmahusada no 60A, Mojo Gubeng,
                                                            Surabaya,Jawa Timur 60285 Indonesia
                                                        </label>
                                                    </div>
                                                    <br />
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="trading_conditions_22_title_en_ref" value="1" onchange="savekyc()" onkeypress="savekyc()" onclick="savekyc()" required @isset($DataContent->trading_condition) @if($DataContent->trading_condition->trading_conditions_22_title_en_ref==='1') checked @endif @endisset />
                                                    <label class="form-check-label required" for="pbk0402point22read">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                                <label id="trading_conditions_22_title_en_ref-error" class="error" for="trading_conditions_22_title_en_ref"></label>

                                            </div>
                                            <div class="form-group position-relative error-l-100 text-justify hidden trading_con_23">
                                                <label class="">
                                                    23. Bahasa
                                                    <br />Perjanjian ini dibuat dalam Bahasa Indonesia.
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="trading_conditions_23_title_en_ref" value="1" onchange="savekyc()" onkeypress="savekyc()" onclick="savekyc()" required @isset($DataContent->trading_condition) @if($DataContent->trading_condition->trading_conditions_23_title_en_ref==='1') checked @endif @endisset />
                                                    <label class="form-check-label required" for="pbk0402point23read">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                                <label id="trading_conditions_23_title_en_ref-error" class="error" for="trading_conditions_23_title_en_ref"></label>

                                            </div>
                                            <div class="form-group position-relative error-l-100 text-justify hidden trading_con_footer">
                                                <label class="">
                                                    Dengan mengisi kolom “YA” di bawah, saya menyatakan bahwa saya
                                                    telah
                                                    menerima "PERJANJIAN PEMBERIAN AMANAT TRANSAKSI KONTRAK
                                                    DERIVARIF SISTEM
                                                    PERDANGANGAN ALTERNATIF" mengerti dan menyetujui isinya.
                                                </label>
                                                <div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="step_4_footer" value="yes" onchange="savekyc()" onkeypress="savekyc()" onclick="savekyc()" @isset($DataContent->trading_condition) @if($DataContent->trading_condition->step_4_footer==='yes') checked @endif @endisset />
                                                        <label class="form-check-label required" for="authorizationTrxYes">Ya</label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="step_4_footer" value="no" onchange="savekyc()" onkeypress="savekyc()" onclick="savekyc()" @isset($DataContent->trading_condition) @if($DataContent->trading_condition->step_4_footer==='no') checked @endif @endisset />
                                                        <label class="form-check-label" for="authorizationTrxNo">Tidak</label>
                                                    </div>
                                                </div>
                                                <label id="step_4_footer-error" class="error" for="step_4_footer" style="display: none;">Klik "Ya" untuk melanjutkan.</label>
                                                <p>Menyatakan pada tanggal : {{date('d-m-Y',strtotime(date("m.d.y")))}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="btn-toolbar custom-toolbar text-center card-body pt-0 mb-4 justify-content-md-center">
                                        <button class="btn btn-secondary prev-btn" type="button">Previous</button>
                                        <button onclick="event.preventDefault();" class="btn-loading-prev btn btn-secondary hidden">
                                            <span class="loader-small" style="position: unset !important;"></span> Previous
                                        </button>
                                        <input class="btn btn-secondary next-btn" type="submit" value="Next" />
                                        <button onclick="event.preventDefault();" class="btn-loading-next btn btn-secondary hidden">
                                            <span class="loader-small" style="position: unset !important;"></span> Next
                                        </button>
                                    </div>
                            </form>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection

@section('jsonpage')
<script src="{{url('/')}}/cabinet/js/vendor/jquery.smartWizard.min.js"></script>
<script src="{{url('/')}}/cabinet/js/vendor/dropzone.min.js"></script>
<script src="{{url('/')}}/cabinet/js/vendor/sweetalert.min.js"></script>

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('[type="checkbox"]').on('change', function() {
        if ($(this).is(':checked')) {
            var name = $(this).attr('name').split("_");
            var seq = parseInt(name[1], 10)
            if (name[0] === "risk") {
                $(`.risk_${seq+1}`).show();
                if ($(`.risk_${seq+1}`).length == 0) {
                    $('.risk_footer').show();
                }
            }

            if (name[0] === "trading") {
                var seq = parseInt(name[2], 10)
                $(`.trading_con_${seq+1}`).show();
                if ($(`.trading_con_${seq+1}`).length == 0) {
                    $('.trading_con_footer').show();
                }
            }

        }
    });

    $('.prev-btn').on('click', function() {
        try {
            $('.prev-btn').hide();
            $('.btn-loading-prev').show();
            $.post("{{route('postNextStepOpenLiveAccount')}}", {
                NBCRMWEBAPI: $('[name="NBCRMWEBAPI"]').val(),
                USERID: $('[name="USERID"]').val(),
                nextStep: '3',
                page: '1'
            }, function(data, status) {
                if (status === "success") {
                    location.reload();
                } else {
                    $('.prev-btn').show();
                    $('.btn-loading-prev').hide();
                }
            });
        } catch (error) {
            console.log('error', error)
        }

    });

    $('[name="risk_document_radio"]').on('change', function(event) {
        var val = event.currentTarget.value;
        if (val === 'no') {
            $('#risk_document_radio-error').show();
            $('#risk_document_radio-error').text('Klik "Ya" untuk melanjutkan.');
        } else {
            $('#risk_document_radio-error').hide();
            $('.trading_condition').show();

        }
    });
    $('[name="step_4_footer"]').on('change', function(event) {
        var val = event.currentTarget.value;
        if (val === 'no') {
            $('#step_4_footer-error').show();
            $('#step_4_footer-error').text('Klik "Ya" untuk melanjutkan.');
        } else {
            $('#step_4_footer-error').hide();

        }
    });
    getDetailKYC();

    function getDetailKYC() {
        $.get("{{route('apigetKycDetail')}}", {}, function(data, status) {
            var risk_document = data.Data.risk_document;
            var val = risk_document['risk_1_title_en_ref'];
            for (const key in risk_document) {
                if (risk_document.hasOwnProperty(key)) {
                    val = risk_document[key];
                    if (val !== "") {
                        var name = key.split("_");
                        var seq = parseInt(name[1], 10)
                        if (name[0] === "risk") {
                            $(`.risk_${seq+1}`).show();
                            if ($(`.risk_${seq+1}`).length == 0) {
                                $('.risk_footer').show();
                                $('.trading_condition').show();
                            }
                        }
                    }

                }
            }

            var trading_condition = data.Data.trading_condition;
            for (const key in trading_condition) {
                if (trading_condition.hasOwnProperty(key)) {
                    var valtrad = trading_condition[key];
                    if (valtrad !== "" && key !== "trading_conditions_22_title_1" && key !== "trading_conditions_22_title_2") {
                        var name = key.split("_");
                        var seq = parseInt(name[2], 10)
                        $(`.trading_con_${seq+1}`).show();
                        if ($(`.trading_con_${seq+1}`).length == 0) {
                            $('.trading_con_footer').show();
                        }
                    }

                }
            }



        });
    }

    function getContent() {
        var contentDt = {
            'job_details': {
                'more_details': {
                    'length_of_work': "{{isset($DataContent->job_details) && !empty($DataContent->job_details->more_details->length_of_work)?$DataContent->job_details->more_details->length_of_work:''}}",
                    'previous_employer': "{{isset($DataContent->job_details) && !empty($DataContent->job_details->more_details->previous_employer)?$DataContent->job_details->more_details->previous_employer:0}}",
                    'employer_name': "{{isset($DataContent->job_details) && !empty($DataContent->job_details->more_details->employer_name)?$DataContent->job_details->more_details->employer_name:''}}",
                    'business_fields': "{{isset($DataContent->job_details) && !empty($DataContent->job_details->more_details->business_fields)?$DataContent->job_details->more_details->business_fields:''}}",
                    'title': "{{isset($DataContent->job_details) && !empty($DataContent->job_details->more_details->title)?$DataContent->job_details->more_details->title:''}}",
                    'office_address': "{{isset($DataContent->job_details) && !empty($DataContent->job_details->more_details->office_address)?$DataContent->job_details->more_details->office_address:''}}",
                    'office_tele': "{{isset($DataContent->job_details) && !empty($DataContent->job_details->more_details->office_tele)?$DataContent->job_details->more_details->office_tele:''}}",
                    'office_fax': 0
                },
                'position': {
                    'value': "{{isset($DataContent->job_details)&&!empty($DataContent->job_details->position->value)?$DataContent->job_details->position->value:''}}",
                    'input': "{{isset($DataContent->job_details)&&!empty($DataContent->job_details->position->input)?$DataContent->job_details->position->input:''}}",
                }
            },
            'wealth_list': {
                'income_per_year': "{{isset($DataContent->wealth_list)&&!empty($DataContent->wealth_list->income_per_year)?$DataContent->wealth_list->income_per_year:''}}",
                'residence_location': "{{isset($DataContent->wealth_list)&&!empty($DataContent->wealth_list->residence_location)?$DataContent->wealth_list->residence_location:''}}",
                'tax_object_selling_value': "{{isset($DataContent->wealth_list)&&!empty($DataContent->wealth_list->tax_object_selling_value)?$DataContent->wealth_list->tax_object_selling_value:''}}",
                'bank_time_deposit_value': "{{isset($DataContent->wealth_list)&&!empty($DataContent->wealth_list->bank_time_deposit_value)?$DataContent->wealth_list->bank_time_deposit_value:''}}",
                'bank_saving_value': "{{isset($DataContent->wealth_list)&&!empty($DataContent->wealth_list->bank_saving_value)?$DataContent->wealth_list->bank_saving_value:''}}",
                'others': "{{isset($DataContent->wealth_list)&&!empty($DataContent->wealth_list->others)?$DataContent->wealth_list->others:''}}",
            },
            'bank_details_for_withdrawls': {
                'full_name': "{{isset($DataContent->bank_details_for_withdrawls)&&!empty($DataContent->bank_details_for_withdrawls->full_name)?$DataContent->bank_details_for_withdrawls->full_name:''}}",
                'branch': "{{isset($DataContent->bank_details_for_withdrawls)&&!empty($DataContent->bank_details_for_withdrawls->branch)?$DataContent->bank_details_for_withdrawls->branch:''}}",
                'account_number': "{{isset($DataContent->bank_details_for_withdrawls)&&!empty($DataContent->bank_details_for_withdrawls->account_number)?$DataContent->bank_details_for_withdrawls->account_number:''}}",
                'phone_number': 0,
                'account_type': {
                    'value': "{{isset($DataContent->bank_details_for_withdrawls)&&!empty($DataContent->bank_details_for_withdrawls->account_type->value)?$DataContent->bank_details_for_withdrawls->account_type->value:''}}",
                    'input': "{{isset($DataContent->bank_details_for_withdrawls)&&!empty($DataContent->bank_details_for_withdrawls->account_type->input)?$DataContent->bank_details_for_withdrawls->account_type->input:''}}",
                }
            },
            'emergency_contact_data': {
                'name': "{{isset($DataContent->emergency_contact_data)&&!empty($DataContent->emergency_contact_data->name)?$DataContent->emergency_contact_data->name:''}}",
                'address': "{{isset($DataContent->emergency_contact_data)&&!empty($DataContent->emergency_contact_data->address)?$DataContent->emergency_contact_data->address:''}}",
                'post_code': "{{isset($DataContent->emergency_contact_data)&&!empty($DataContent->emergency_contact_data->post_code)?$DataContent->emergency_contact_data->post_code:''}}",
                'home_phone_number': "{{isset($DataContent->emergency_contact_data)&&!empty($DataContent->emergency_contact_data->home_phone_number)?$DataContent->emergency_contact_data->home_phone_number:''}}",
                'relationship_with_the_contacts': "{{isset($DataContent->emergency_contact_data)&&!empty($DataContent->emergency_contact_data->relationship_with_the_contacts)?$DataContent->emergency_contact_data->relationship_with_the_contacts:''}}"
            },
            'attached_documents': {
                'id_card': true,
                'coloured_photo': true,
                'specimen_signature': true,
                'other_count': 1,
                'required_image': [{
                        'type': 'id_card',
                        'title': 'id_card_or_driving_license_passport',
                        'action_type': 0,
                        'src': {
                            'name': "{{!empty($DataContent->attached_documents)?$DataContent->attached_documents->required_image[0]->src->name:''}}",
                            'size': 116,
                            'url': "{{!empty($DataContent->attached_documents)?$DataContent->attached_documents->required_image[0]->src->url:''}}",
                        }
                    },
                    {
                        'type': 'coloured_photo',
                        'title': 'coloured_photo',
                        'action_type': 0,
                        'src': {
                            'name': "{{!empty($DataContent->attached_documents)?$DataContent->attached_documents->required_image[1]->src->name:''}}",
                            'size': 116,
                            'url': "{{!empty($DataContent->attached_documents)?$DataContent->attached_documents->required_image[1]->src->url:''}}",
                        }
                    },
                    {
                        'type': 'specimen_signature',
                        'title': 'specimen_signature',
                        'action_type': 0,
                        'src': {
                            'name': "{{!empty($DataContent->attached_documents)?$DataContent->attached_documents->required_image[2]->src->name:''}}",
                            'size': 116,
                            'url': "{{!empty($DataContent->attached_documents)?$DataContent->attached_documents->required_image[2]->src->url:''}}",
                        }
                    },
                    {
                        'type': 'other',
                        'title': 'other',
                        'action_type': 0,
                        'src': {
                            'name': "{{!empty($DataContent->attached_documents)?$DataContent->attached_documents->required_image[3]->src->name:''}}",
                            'size': 116,
                            'url': "{{!empty($DataContent->attached_documents)?$DataContent->attached_documents->required_image[3]->src->url:''}}",
                        }
                    }
                ],
                'option': 'yes'
            },
            'trading_statement': {
                'statement_for_trading_simulation_on_demo_account': "{{isset($DataContent->trading_statement)&&!empty($DataContent->trading_statement->statement_for_trading_simulation_on_demo_account)?$DataContent->trading_statement->statement_for_trading_simulation_on_demo_account:''}}",
                'statement_letter_has_been_experienced_on_trading': "{{isset($DataContent->trading_statement)&&!empty($DataContent->trading_statement->statement_letter_has_been_experienced_on_trading)?$DataContent->trading_statement->statement_letter_has_been_experienced_on_trading:''}}",
                'statement_has_been_received_information_futures_company': "{{isset($DataContent->trading_statement)&&!empty($DataContent->trading_statement->statement_has_been_received_information_futures_company)?$DataContent->trading_statement->statement_has_been_received_information_futures_company:''}}",
            },
            'risk_document': {
                'risk_1_title_en_ref': $($("input[name='risk_1_title_en_ref']:checked")).val() || '',
                'risk_2_title_en_ref': $($("input[name='risk_2_title_en_ref']:checked")).val() || '',
                'risk_3_title_en_ref': $($("input[name='risk_3_title_en_ref']:checked")).val() || '',
                'risk_4_title_en_ref': $($("input[name='risk_4_title_en_ref']:checked")).val() || '',
                'risk_5_title_en_ref': $($("input[name='risk_5_title_en_ref']:checked")).val() || '',
                'risk_6_title_en_ref': $($("input[name='risk_6_title_en_ref']:checked")).val() || '',
                'risk_7_title_en_ref': $($("input[name='risk_7_title_en_ref']:checked")).val() || '',
                'risk_8_title_en_ref': $($("input[name='risk_8_title_en_ref']:checked")).val() || '',
                'risk_9_title_en_ref': $($("input[name='risk_9_title_en_ref']:checked")).val() || '',
                'risk_10_title_en_ref': $($("input[name='risk_10_title_en_ref']:checked")).val() || '',
                'risk_11_title_en_ref': $($("input[name='risk_11_title_en_ref']:checked")).val() || '',
                'risk_12_title_en_ref': $($("input[name='risk_12_title_en_ref']:checked")).val() || '',
                'risk_13_title_en_ref': $($("input[name='risk_13_title_en_ref']:checked")).val() || '',
                'radio': $($("input[name='risk_document_radio']:checked")).val() || ''
            },
            'trading_condition': {
                'trading_conditions_1_title_en_ref': $($("[name='trading_conditions_1_title_en_ref']:checked")).val() || '',
                'trading_conditions_2_title_en_ref': $($("[name='trading_conditions_2_title_en_ref']:checked")).val() || '',
                'trading_conditions_3_title_en_ref': $($("[name='trading_conditions_3_title_en_ref']:checked")).val() || '',
                'trading_conditions_4_title_en_ref': $($("[name='trading_conditions_4_title_en_ref']:checked")).val() || '',
                'trading_conditions_5_title_en_ref': $($("[name='trading_conditions_5_title_en_ref']:checked")).val() || '',
                'trading_conditions_6_title_en_ref': $($("[name='trading_conditions_6_title_en_ref']:checked")).val() || '',
                'trading_conditions_7_title_en_ref': $($("[name='trading_conditions_7_title_en_ref']:checked")).val() || '',
                'trading_conditions_8_title_en_ref': $($("[name='trading_conditions_8_title_en_ref']:checked")).val() || '',
                'trading_conditions_9_title_en_ref': $($("[name='trading_conditions_9_title_en_ref']:checked")).val() || '',
                'trading_conditions_10_title_en_ref': $($("[name='trading_conditions_10_title_en_ref']:checked")).val() || '',
                'trading_conditions_11_title_en_ref': $($("[name='trading_conditions_11_title_en_ref']:checked")).val() || '',
                'trading_conditions_12_title_en_ref': $($("[name='trading_conditions_12_title_en_ref']:checked")).val() || '',
                'trading_conditions_13_title_en_ref': $($("[name='trading_conditions_13_title_en_ref']:checked")).val() || '',
                'trading_conditions_14_title_en_ref': $($("[name='trading_conditions_14_title_en_ref']:checked")).val() || '',
                'trading_conditions_15_title_en_ref': $($("[name='trading_conditions_15_title_en_ref']:checked")).val() || '',
                'trading_conditions_16_title_en_ref': $($("[name='trading_conditions_16_title_en_ref']:checked")).val() || '',
                'trading_conditions_17_title_en_ref': $($("[name='trading_conditions_17_title_en_ref']:checked")).val() || '',
                'trading_conditions_18_title_en_ref': $($("[name='trading_conditions_18_title_en_ref']:checked")).val() || '',
                'trading_conditions_19_title_en_ref': $($("[name='trading_conditions_19_title_en_ref']:checked")).val() || '',
                'trading_conditions_20_title_en_ref': $($("[name='trading_conditions_20_title_en_ref']:checked")).val() || '',
                'trading_conditions_21_title_en_ref': $($("[name='trading_conditions_21_title_en_ref']:checked")).val() || '',
                'trading_conditions_22_title_1': $($("[name='trading_conditions_22_title_1']:checked")).val() || 0,
                'trading_conditions_22_title_2': $($("[name='trading_conditions_22_title_2']:checked")).val() || 0,
                'trading_conditions_22_title_en_ref': $($("[name='trading_conditions_22_title_en_ref']:checked")).val() || '',
                'trading_conditions_23_title_en_ref': $($("[name='trading_conditions_23_title_en_ref']:checked")).val() || '',
                'step_4_footer': $($("[name='step_4_footer']:checked")).val() || '',
            },
            'pap_agreement': "{{!empty($DataContent->pap_agreement)?$DataContent->pap_agreement:''}}",
            'truth_resposibility_agreement': "{{!empty($DataContent->truth_resposibility_agreement)?$DataContent->truth_resposibility_agreement:''}}",
            'demoAccount': "{{(Session::get('user.MTUserDemo')->Login)}}",
            'full_name': "{{!empty($DataContent->full_name)?$DataContent->full_name:''}}",
            'identity_number': "{{!empty($DataContent->identity_number)?$DataContent->identity_number:''}}",
            'future_contract': {
                'value': "{{isset($DataContent->future_contract)&&!empty($DataContent->future_contract->value)?$DataContent->future_contract->value:''}}",
                'input': "{{isset($DataContent->future_contract)&&!empty($DataContent->future_contract->input)?$DataContent->future_contract->input:''}}",
            },
            'account_type': "{{!empty($DataContent->account_type)?$DataContent->account_type:''}}",
            'place_of_birth': "{{!empty($DataContent->place_of_birth)?$DataContent->place_of_birth:''}}",
            'date_of_birth': "{{!empty($DataContent->date_of_birth)?$DataContent->date_of_birth:''}}",
            'post_code': "{{!empty($DataContent->post_code)?$DataContent->post_code:''}}",
            'page': "{{!empty($DataContent->page)?$DataContent->page:0}}",
            'activeStep': "{{!empty($DataContent->activeStep)?$DataContent->activeStep:''}}",
            'tax_file_number': "{{!empty($DataContent->tax_file_number)?$DataContent->tax_file_number:''}}",
            'mother_maiden_name': "{{!empty($DataContent->mother_maiden_name)?$DataContent->mother_maiden_name:''}}",
            'status': "{{!empty($DataContent->status)?$DataContent->status:''}}",
            'gender': "{{!empty($DataContent->gender)?$DataContent->gender:''}}",
            'home_address': "{{!empty($DataContent->home_address)?$DataContent->home_address:''}}",
            'home_ownership_status': {
                'value': "{{isset($DataContent->home_ownership_status)&&!empty($DataContent->home_ownership_status->value)?$DataContent->home_ownership_status->value:''}}",
                'input': "{{isset($DataContent->home_ownership_status)&&!empty($DataContent->home_ownership_status->input)?$DataContent->home_ownership_status->input:''}}"
            },
            'openint_account_purpose': {
                'value': "{{isset($DataContent->openint_account_purpose)&&!empty($DataContent->openint_account_purpose->value)?$DataContent->openint_account_purpose->value:''}}",
                'input': "{{isset($DataContent->openint_account_purpose)&&!empty($DataContent->openint_account_purpose->input)?$DataContent->openint_account_purpose->input:''}}"
            },
            'experince_on_investment': {
                'value': "{{isset($DataContent->experince_on_investment)&&!empty($DataContent->experince_on_investment->value)?$DataContent->experince_on_investment->value:''}}",
                'input': "{{isset($DataContent->experince_on_investment)&&!empty($DataContent->experince_on_investment->input)?$DataContent->experince_on_investment->input:''}}"
            },
            'is_family_work_in_related_company': {
                'value': "{{isset($DataContent->is_family_work_in_related_company)&&!empty($DataContent->is_family_work_in_related_company->value)?$DataContent->is_family_work_in_related_company->value:''}}",
                'input': "{{isset($DataContent->is_family_work_in_related_company)&&!empty($DataContent->is_family_work_in_related_company->input)?$DataContent->is_family_work_in_related_company->input:''}}"
            },
            'is_declared_bankrupt': {
                'value': "{{isset($DataContent->is_declared_bankrupt)&&!empty($DataContent->is_declared_bankrupt->value)?$DataContent->is_declared_bankrupt->value:''}}",
                'input': "{{isset($DataContent->is_declared_bankrupt)&&!empty($DataContent->is_declared_bankrupt->input)?$DataContent->is_declared_bankrupt->input:''}}"
            },
            'trading_rules': "{{!empty($DataContent->trading_rules)?$DataContent->trading_rules:''}}",
            'ae_name_or_branch': "{{!empty($DataContent->ae_name_or_branch)?$DataContent->ae_name_or_branch:''}}",
            'husband_wife_spouse': "{{!empty($DataContent->husband_wife_spouse)?$DataContent->husband_wife_spouse:''}}",
            'home_phone': "{{!empty($DataContent->home_phone)?$DataContent->home_phone:''}}",
            'home_fax': 0,
            'statement_data_correctly': "{{!empty($DataContent->statement_data_correctly)?$DataContent->statement_data_correctly:''}}",
            'KycStatus': 0
        };
        return contentDt;
    }

    function savekyc() {
        var contentDt = getContent();
        if (contentDt.wealth_list.income_per_year === "&gt;500") {
            contentDt.wealth_list.income_per_year = ">500";
        }
        const content = JSON.stringify(contentDt)
        $.post("{{route('savekyc')}}", {
            content,
            NBCRMWEBAPI: $('[name="NBCRMWEBAPI"]').val(),
            USERID: $('[name="USERID"]').val(),
        }, function(data, status) {
            var {
                Code,
                Message
            } = data;
            if (data.Data) {
                var Data = data.Data;
                var MTUserRef = Data.MTUserRef;
                var idx = MTUserRef.findIndex(x => x.IsSimulate === false);
                if (MTUserRef[idx]) {
                    var liveAcc = MTUserRef[idx];
                    $('[data-bind="ac_live_no"]').text(liveAcc.Login);
                }
            }
        });
    };

    // });
    $(function() {
        $("#form-step-0").validate({
            ignore: [],
            // errorElement: "div",
            rules: {
                risk_1_title_en_ref: {
                    required: true
                },
                risk_2_title_en_ref: {
                    required: true
                },
                risk_3_title_en_ref: {
                    required: true
                },
                risk_4_title_en_ref: {
                    required: true
                },
                risk_5_title_en_ref: {
                    required: true
                },
                risk_6_title_en_ref: {
                    required: true
                },
                risk_7_title_en_ref: {
                    required: true
                },
                risk_8_title_en_ref: {
                    required: true
                },
                risk_9_title_en_ref: {
                    required: true
                },
                risk_10_title_en_ref: {
                    required: true
                },
                risk_11_title_en_ref: {
                    required: true
                },
                risk_12_title_en_ref: {
                    required: true
                },
                risk_13_title_en_ref: {
                    required: true
                },
                trading_conditions_1_title_en_ref: {
                    required: true
                },
                trading_conditions_2_title_en_ref: {
                    required: true
                },
                trading_conditions_3_title_en_ref: {
                    required: true
                },
                trading_conditions_4_title_en_ref: {
                    required: true
                },
                trading_conditions_5_title_en_ref: {
                    required: true
                },
                trading_conditions_6_title_en_ref: {
                    required: true
                },
                trading_conditions_7_title_en_ref: {
                    required: true
                },
                trading_conditions_8_title_en_ref: {
                    required: true
                },
                trading_conditions_9_title_en_ref: {
                    required: true
                },
                trading_conditions_10_title_en_ref: {
                    required: true
                },
                trading_conditions_11_title_en_ref: {
                    required: true
                },
                trading_conditions_12_title_en_ref: {
                    required: true
                },
                trading_conditions_13_title_en_ref: {
                    required: true
                },
                trading_conditions_14_title_en_ref: {
                    required: true
                },
                trading_conditions_15_title_en_ref: {
                    required: true
                },
                trading_conditions_16_title_en_ref: {
                    required: true
                },
                trading_conditions_17_title_en_ref: {
                    required: true
                },
                trading_conditions_18_title_en_ref: {
                    required: true
                },
                trading_conditions_19_title_en_ref: {
                    required: true
                },
                trading_conditions_20_title_en_ref: {
                    required: true
                },
                trading_conditions_21_title_en_ref: {
                    required: true
                },
                trading_conditions_22_title_en_ref: {
                    required: true
                },
                trading_conditions_23_title_en_ref: {
                    required: true
                },


            },
            messages: {
                risk_1_title_en_ref: 'Klik SAYA SUDAH MEMBACA DAN MEMAHAMI untuk melanjutkan',
                risk_2_title_en_ref: 'Klik SAYA SUDAH MEMBACA DAN MEMAHAMI untuk melanjutkan',
                risk_3_title_en_ref: 'Klik SAYA SUDAH MEMBACA DAN MEMAHAMI untuk melanjutkan',
                risk_4_title_en_ref: 'Klik SAYA SUDAH MEMBACA DAN MEMAHAMI untuk melanjutkan',
                risk_5_title_en_ref: 'Klik SAYA SUDAH MEMBACA DAN MEMAHAMI untuk melanjutkan',
                risk_6_title_en_ref: 'Klik SAYA SUDAH MEMBACA DAN MEMAHAMI untuk melanjutkan',
                risk_7_title_en_ref: 'Klik SAYA SUDAH MEMBACA DAN MEMAHAMI untuk melanjutkan',
                risk_8_title_en_ref: 'Klik SAYA SUDAH MEMBACA DAN MEMAHAMI untuk melanjutkan',
                risk_9_title_en_ref: 'Klik SAYA SUDAH MEMBACA DAN MEMAHAMI untuk melanjutkan',
                risk_10_title_en_ref: 'Klik SAYA SUDAH MEMBACA DAN MEMAHAMI untuk melanjutkan',
                risk_11_title_en_ref: 'Klik SAYA SUDAH MEMBACA DAN MEMAHAMI untuk melanjutkan',
                risk_12_title_en_ref: 'Klik SAYA SUDAH MEMBACA DAN MEMAHAMI untuk melanjutkan',
                risk_13_title_en_ref: 'Klik SAYA SUDAH MEMBACA DAN MEMAHAMI untuk melanjutkan',
                trading_conditions_1_title_en_ref: 'Klik SAYA SUDAH MEMBACA DAN MEMAHAMI untuk melanjutkan',
                trading_conditions_2_title_en_ref: 'Klik SAYA SUDAH MEMBACA DAN MEMAHAMI untuk melanjutkan',
                trading_conditions_3_title_en_ref: 'Klik SAYA SUDAH MEMBACA DAN MEMAHAMI untuk melanjutkan',
                trading_conditions_4_title_en_ref: 'Klik SAYA SUDAH MEMBACA DAN MEMAHAMI untuk melanjutkan',
                trading_conditions_5_title_en_ref: 'Klik SAYA SUDAH MEMBACA DAN MEMAHAMI untuk melanjutkan',
                trading_conditions_6_title_en_ref: 'Klik SAYA SUDAH MEMBACA DAN MEMAHAMI untuk melanjutkan',
                trading_conditions_7_title_en_ref: 'Klik SAYA SUDAH MEMBACA DAN MEMAHAMI untuk melanjutkan',
                trading_conditions_8_title_en_ref: 'Klik SAYA SUDAH MEMBACA DAN MEMAHAMI untuk melanjutkan',
                trading_conditions_9_title_en_ref: 'Klik SAYA SUDAH MEMBACA DAN MEMAHAMI untuk melanjutkan',
                trading_conditions_10_title_en_ref: 'Klik SAYA SUDAH MEMBACA DAN MEMAHAMI untuk melanjutkan',
                trading_conditions_11_title_en_ref: 'Klik SAYA SUDAH MEMBACA DAN MEMAHAMI untuk melanjutkan',
                trading_conditions_12_title_en_ref: 'Klik SAYA SUDAH MEMBACA DAN MEMAHAMI untuk melanjutkan',
                trading_conditions_13_title_en_ref: 'Klik SAYA SUDAH MEMBACA DAN MEMAHAMI untuk melanjutkan',
                trading_conditions_14_title_en_ref: 'Klik SAYA SUDAH MEMBACA DAN MEMAHAMI untuk melanjutkan',
                trading_conditions_15_title_en_ref: 'Klik SAYA SUDAH MEMBACA DAN MEMAHAMI untuk melanjutkan',
                trading_conditions_16_title_en_ref: 'Klik SAYA SUDAH MEMBACA DAN MEMAHAMI untuk melanjutkan',
                trading_conditions_17_title_en_ref: 'Klik SAYA SUDAH MEMBACA DAN MEMAHAMI untuk melanjutkan',
                trading_conditions_18_title_en_ref: 'Klik SAYA SUDAH MEMBACA DAN MEMAHAMI untuk melanjutkan',
                trading_conditions_19_title_en_ref: 'Klik SAYA SUDAH MEMBACA DAN MEMAHAMI untuk melanjutkan',
                trading_conditions_20_title_en_ref: 'Klik SAYA SUDAH MEMBACA DAN MEMAHAMI untuk melanjutkan',
                trading_conditions_21_title_en_ref: 'Klik SAYA SUDAH MEMBACA DAN MEMAHAMI untuk melanjutkan',
                trading_conditions_22_title_en_ref: 'Klik SAYA SUDAH MEMBACA DAN MEMAHAMI untuk melanjutkan',
                trading_conditions_23_title_en_ref: 'Klik SAYA SUDAH MEMBACA DAN MEMAHAMI untuk melanjutkan',
            },
            submitHandler: function() {

                var val1 = $($("input[name='risk_document_radio']:checked")).val() || '';
                var val2 = $($("[name='step_4_footer']:checked")).val() || '';
                if (val1 === "yes" && val2 === "yes") {
                    $('.next-btn').hide();
                    $('.btn-loading-next').show();
                    $.post("{{route('postNextStepOpenLiveAccount')}}", {
                        NBCRMWEBAPI: $('[name="NBCRMWEBAPI"]').val(),
                        USERID: $('[name="USERID"]').val(),
                        nextStep: '4',
                        page: '2'
                    }, function(data, status) {
                        if (status === "success") {
                            location.reload();
                        } else {
                            $('.next-btn').show();
                            $('.btn-loading-next').hide();
                        }
                    });
                } else {
                    if (val1 === "no") {
                        $('#risk_document_radio-error').show();
                        $('#risk_document_radio-error').text('Klik "Ya" untuk melanjutkan.');
                    }
                    if (val2 === "no") {
                        $('#step_4_footer-error').show();
                        $('#step_4_footer-error').text('Klik "Ya" untuk melanjutkan.');
                    }

                }
            }
        });


    });
</script>
@endsection