<?php

namespace App\Http\Middleware;

use Closure;
use DB;
use Input;
use Cookie;
use Jenssegers\Agent\Agent;
use URL;
use Session;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class visitordata
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      $agent = new Agent();

      $browser = $agent->browser();
      $version_browser = $agent->version($browser);

      $platform = $agent->platform();
      $version_platform = $agent->version($platform);

      $device = $agent->device();

      $isMobile = $agent->isMobile();
      $request->session()->put('isMobile', $isMobile);

      $client = new Client();


      $ip = request()->ip();
      $time = time();
      $date = date('H:i d-m-Y',$time);
      if(empty(Cookie::get('ref'))){
        Cookie::queue('ref', '10037', 3243200);
        $ref = '10037';

        if(env('APP_ENV')=='local'){
          Cookie::queue('ref', '3', 3243200);
          $ref = '3';
        }
      }

      if(Input::get('ref')){
        // Cookie::queue('ref', Input::get('ref'), 3243200);
        Cookie::queue('ref', '10037', 3243200);
        $ref = Input::get('ref');

        if(env('APP_ENV')=='local'){
          Cookie::queue('ref', '3', 3243200);
          $ref = '3';
        }
      }else{
        $ref = Cookie::get('ref');
      }


      if(Input::get('uuid')){
        Cookie::queue('uuid', Input::get('uuid'), 3243200);
        $uuid = Input::get('uuid');
      }else{

        if(!Cookie::get('uuid')){
          $uuid = md5($ip.$time);
          Cookie::queue('uuid', $uuid , 3243200);
        }else{
          $uuid = Cookie::get('uuid');
        }
      }

      if(empty(Cookie::get('campaign'))){
        Cookie::queue('campaign', '1', 3243200);
        $campaign = '1';
      }



      if(empty($interest->title)){
        Cookie::queue('campaignTitle', 'none', 3243200);
      }else{
        Cookie::queue('campaignTitle', $interest->title, 3243200);
      }

      if(empty(Cookie::get('so'))){
        Cookie::queue('so', 'direct', 3243200);
        $so = 'direct';
      }

      if(Input::get('so')){
        Cookie::queue('so', Input::get('so'), 3243200);
        $so = Input::get('so');
      }else{
        $so = Cookie::get('so');
      }

        $agent = new Agent();

        $browser = $agent->browser();
        $version_browser = $agent->version($browser);

        $platform = $agent->platform();
        $version_platform = $agent->version($platform);

        $device = $agent->device();

        $url = URL::current();
        $before = URL::previous();

        return $next($request);
    }
}
