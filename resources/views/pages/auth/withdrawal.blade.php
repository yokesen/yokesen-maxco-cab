@extends('templates.master')

@section('csslinkonhead')
<link rel="stylesheet" href="{{url('/')}}/cabinet/css/vendor/select2.min.css" />
<link rel="stylesheet" href="{{url('/')}}/cabinet/css/vendor/select2-bootstrap.min.css" />
@endsection

@section('content')
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <h1>Withdrawal</h1>
                <div class="top-right-button-container">
                    <button class="btn btn-primary btn-lg top-right-button mr-1 btn-maxco-blue borderradius-0 text-uppercase" onclick="gotowithdrawal()">Withdrawal Request</button>
                </div>
                <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                    <ol class="breadcrumb pt-0">
                        <li class="breadcrumb-item active" aria-current="page">Data</li>
                    </ol>
                </nav>
                <div class="separator mb-5"></div>
            </div>
        </div>

        <div class="card">
            <div class="card-body">
                <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">DATE TIME</th>
                                <th scope="col">FROM ACOOUNT</th>
                                <th scope="col">CURRENCY</th>
                                <th scope="col">AMOUNT</th>
                                <th scope="col">STATUS</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $item)
                            <?php
                            switch ($item->Status) {
                                case '0':
                                    $statusWithDraw = "New withdraw request";
                                    break;
                                case '1':
                                    $statusWithDraw = "Freeze";
                                    break;
                                case '2':
                                    $statusWithDraw = "Approving";
                                    break;
                                case '3':
                                    $statusWithDraw = "Approved";
                                    break;
                                case '4':
                                    $statusWithDraw = "Withdrawing";
                                    break;
                                case '5':
                                    $statusWithDraw = "Withdrawn";
                                    break;
                                case '6':
                                    $statusWithDraw = "Rejected";
                                    break;
                                case '7':
                                    $statusWithDraw = "Failed";
                                    break;
                                case '8':
                                    $statusWithDraw = "Cancelled";
                                    break;
                            }

                            switch ($item->Currency) {
                                case '360':
                                    $currency = "IDR";
                                    break;
                                case '840':
                                    $currency = "USD";
                                    break;
                                default:
                                    $currency ='';
                                    break;
                            }

                            ?>
                            <tr>
                                <td>{{$item->CreateTime}}</td>
                                <td>{{$item->MTId}}</td>
                                <td>{{$currency}}</td>
                                <td>{{number_format($item->Amount,2)}}</td>
                                <td>{{$statusWithDraw}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="row view-pager">
                        <div class="col-sm-12">
                            <div class="text-center">
                                <div class="dataTables_paginate paging_simple_numbers" id="DataTables_Table_0_paginate">
                                    @php
                                        $page =!empty($_GET['page'])?$_GET['page']:1;
                                    @endphp
                                    <ul class="pagination pagination-sm">
                                        <li class="paginate_button page-item previous {{$page>1?'':'disabled'}}" id="DataTables_Table_0_previous"><a href="{{route('historywithdrawal').'?page='.($page-1)}}" aria-controls="DataTables_Table_0" data-dt-idx="0" tabindex="0" class="page-link prev"><i class="simple-icon-arrow-left"></i></a></li>
                                        @for($i = 1; $i < ($totalPages+1); $i++)
                                            <li class="paginate_button page-item {{$page==$i?'active':''}}"><a href="{{route('historywithdrawal').'?page='.$i}}" aria-controls="DataTables_Table_{{$i}}" data-dt-idx="1" tabindex="0" class="page-link">{{$i}}</a></li>
                                        @endfor
                                        <li class="paginate_button page-item next {{$page<$totalPages?'':'disabled'}}" id="DataTables_Table_0_next"><a href="{{route('historywithdrawal').'?page='.($page+1)}}" aria-controls="DataTables_Table_0" data-dt-idx="3" tabindex="0" class="page-link next"><i class="simple-icon-arrow-right"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</main>
@endsection

@section('jsonpage')
<script src="{{url('/')}}/cabinet/js/vendor/sweetalert.min.js"></script>
<script src="{{url('/')}}/cabinet/js/vendor/select2.full.js"></script>
<script src="{{url('/')}}/cabinet/js/vendor/jquery.validate/jquery.validate.min.js"></script>
<script src="{{url('/')}}/cabinet/js/vendor/jquery.validate/additional-methods.min.js"></script>
<script>
    function gotowithdrawal() {
        var kycStatus = "{{Session::get('user.KycStatus')}}";
        if (kycStatus === '2') {
            window.location.href = "{{url('/')}}/withdrawal-request";
        } else {
            swal("Perhatian!", "Anda belum memiliki akun live.", "warning");
        }
    };
</script>
@endsection