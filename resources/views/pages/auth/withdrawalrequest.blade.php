@extends('templates.master')


@section('csslinkonhead')
<link rel="stylesheet" href="{{url('/')}}/cabinet/css/vendor/select2.min.css" />
<link rel="stylesheet" href="{{url('/')}}/cabinet/css/vendor/select2-bootstrap.min.css" />
@endsection

@section('content')
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <h1>Withdrawal Request</h1>
                <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                    <ol class="breadcrumb pt-0"></ol>
                </nav>
                <div class="separator mb-5"></div>
            </div>
        </div>
        <div class="row" v-show="showform">
            <div class="col-12 col-lg-6 col-xl-6 col-left mt-5">
                <div class="card">
                    <div class="card-body">
                        <div class="card-title">
                            <h5>WITHDRAWAL REQUEST</h5>
                        </div>
                        <form id="withdrawal-form" class="needs-validation" novalidate ref="form" method="POST" action="{{route('postWithdrawal')}}">
                            @csrf
                            <div class="form-group row tooltip-right-top">
                                <label class="col-sm-3 col-form-label">From MT4 Account</label>
                                <div class="col-sm-9">
                                    <div class="form-control-plaintext">
                                        {{Session::get('user.MTUserRefObj')->Login}}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row tooltip-right-top">
                                <label class="col-sm-3 col-form-label">Available</label>
                                <div class="col-sm-9">
                                    <div class="form-control-plaintext">
                                        {{isset($databalacelive->FreeMargin)?$databalacelive->FreeMargin:0}} USD
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row tooltip-right-top">
                                <label class="col-sm-3 col-form-label">Margin</label>
                                <div class="col-sm-3">
                                    <div class="form-control-plaintext">
                                        {{isset($databalacelive->Margin)?$databalacelive->Margin:0}} USD
                                    </div>
                                </div>
                                <label class="col-sm-3 col-form-label text-right">Saldo</label>
                                <div class="col-sm-3">
                                    <div class="form-control-plaintext text-right">
                                        {{isset($databalacelive->Balance)?$databalacelive->Balance:0}} USD
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="form-group row tooltip-right-top">
                                <label class="col-sm-3 col-form-label">Saldo</label>
                                <div class="col-sm-9">
                                    <div class="form-control-plaintext">
                                        0,00 USD
                                    </div>
                                </div>
                            </div> -->
                            <div class="form-group row tooltip-right-top">
                                <label class="col-sm-3 col-form-label">Withdrawal Amount</label>
                                <div class="col-sm-9">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">USD</span>
                                            <!-- <select class="form-control" name="Currency" required>
                                                <option value="840" {{Session::get('user.Currency')===840?'selected':''}}>USD</option>
                                                <option value="360" {{Session::get('user.Currency')===360?'selected':''}}>IDR</option>
                                            </select> -->
                                            <input type="hidden" value="840" name="Currency" />
                                        </div>
                                        <input type="number" min="0" max="{{isset($databalacelive->FreeMargin)?$databalacelive->FreeMargin:0}}" class="form-control" aria-label="Amount (to the nearest dollar)" name="Amount" required>
                                        <div class="invalid-tooltip" style="margin-right: 100px;">Amount is required!</div>

                                        <div style="width:100px;display: flex;">
                                        <span style="padding: 0px 11px;margin-top: 20px;width:10px;"><strong>.</strong></span>
                                            <input type="number" min="0" max="99" class="form-control" aria-label="Amount (to the nearest dollar)" name="AmountDecimal" placeholder="00">
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="form-group row row tooltip-right-top">
                                <label class="col-sm-3 col-form-label">Bank Name</label>
                                <div class="col-sm-9">
                                    <input type="hidden" name="BankCardId" value="{{$UserBankCardLast->Id}}">
                                    <div class="form-control-plaintext">
                                        {{$UserBankCardLast->BankName}}
                                    </div>
                                    <!-- <select class="form-control" name="BankCardId" required>
                                        <option value="35">Panin Bank</option>
                                        <option value="36">BCA</option>
                                    </select> -->
                                </div>
                            </div>
                            <div class="form-group row row tooltip-right-top">
                                <label class="col-sm-3 col-form-label">Bank Account No.</label>
                                <div class="col-sm-9">
                                    <div class="form-control-plaintext">{{$UserBankCardLast->AccountNo}}</div>
                                </div>
                            </div>
                            <div class="form-group row tooltip-right-top">
                                <label class="col-sm-3 col-form-label">Remark</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="Remark" required>
                                    <div class="invalid-tooltip">Remark is required!</div>

                                </div>
                            </div>
                            @if (session('ErrorMessage'))
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <strong>Error!</strong> {{ session('ErrorMessage') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            @endif
                            <div class="form-group row mb-0 text-right">
                                <div class="col-sm-12">
                                    <a class="btn btn-warning btn-lg btn-shadow borderradius-0" href="/withdrawal">Cancel</a>
                                    <button id="btn-submit" type="submit" class="btn btn-primary btn-lg btn-shadow btn-maxco-blue borderradius-0">Submit</button>
                                    <button id="btn-loading" onclick="event.preventDefault();" class="btn btn-primary btn-lg btn-shadow hidden" style="border-radius: 0px;background-color: #017dc7 !important;"><span class="loader"></span> Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">DEPOSIT REQUEST</h5>
                    </div>
                    <div class="modal-body">Are you sure request deposit ?</div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-warning borderradius-0" data-dismiss="modal">No</button>
                        <button type="button" class="btn btn-primary btn-maxco-blue borderradius-0" data-dismiss="modal" @click="showform=false">Yes</button>
                    </div>
                </div>
            </div>
        </div> -->
    </div>
</main>
@endsection

@section('jsonpage')
<script src="{{url('/')}}/cabinet/js/vendor/select2.full.js"></script>
<script src="{{url('/')}}/cabinet/js/vendor/jquery.validate/jquery.validate.min.js"></script>
<script src="{{url('/')}}/cabinet/js/vendor/jquery.validate/additional-methods.min.js"></script>
<script>
    $(document).ready(function() {
        // $('#btn-submit').on('click',function(){
        //     if($('#withdrawal-form').valid()){
        //         $('#withdrawal-form').submit();
        //         $('#btn-submit').hide();
        //         $('#btn-loading').show();
        //     }
        // });
    });
</script>
@endsection