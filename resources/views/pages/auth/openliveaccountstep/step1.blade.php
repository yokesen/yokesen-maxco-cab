@extends('templates.master')

@section('csslinkonhead')
<link rel="stylesheet" href="{{url('/')}}/cabinet/css/vendor/smart_wizard.min.css" />
<link rel="stylesheet" href="{{url('/')}}/cabinet/css/vendor/dropzone.min.css" />
<meta name="csrf-token" content="{{ csrf_token() }}" />
@if($agent->isMobile())
<style>
    .sw-main.sw-theme-check>ul.step-anchor>li>a:after,
    .sw-main.sw-theme-check>ul.step-anchor>li.active>a:after,
    .sw-main.sw-main.sw-theme-check>ul.step-anchor>li.done>a:after {
        margin-top: 42px;
    }

    .tooltip-inner {
        background-color: red;
        color: #fff;
    }

    .bs-tooltip-auto[x-placement^=top] .arrow::after,
    .bs-tooltip-top .arrow::after {
        bottom: 1px;
        border-top-color: #eb1010;
    }
</style>
@endif
<style>
    .tooltip-inner {
        background-color: red;
        color: #fff;
    }

    .bs-tooltip-auto[x-placement^=top] .arrow::after,
    .bs-tooltip-top .arrow::after {
        bottom: 1px;
        border-top-color: red;
    }
</style>
@endsection

@section('content')
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <h1>Opening Account Wizard</h1>
                <div class="separator mb-5"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 mb-4">
                <!-- <h5 class="mb-4">Open Live Account</h5> -->
                <div class="card mb-4">
                    <div id="" class="sw-main sw-theme-check">
                        <ul class="card-header mb-4 nav nav-tabs step-anchor" style="z-index: 0;">
                            <li class="nav-item  {{intval($DataContent->activeStep?$DataContent->activeStep:0)}} @if(0<intval($DataContent->activeStep?$DataContent->activeStep:0)) done @endif">
                                <a class="nav-link">
                                    @if(!$agent->isMobile())
                                    Step 1
                                    <br>
                                    @endif
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#step-1" class="nav-link">
                                    @if(!$agent->isMobile())
                                    Step 2
                                    <br>
                                    @endif
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#step-2" class="nav-link">
                                    @if(!$agent->isMobile())
                                    Step 3
                                    <br>
                                    @endif
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#step-3" class="nav-link">
                                    @if(!$agent->isMobile())
                                    Step 4
                                    <br>
                                    @endif
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#step-4" class="nav-link">
                                    @if(!$agent->isMobile())
                                    Step 5
                                    <br>
                                    @endif
                                </a>
                            </li>
                        </ul>

                        <div class="card-body">
                            <form id="form-step-0" class="tooltip-label-right" novalidate style="padding: 0px 22px;">
                                <input name="NBCRMWEBAPI" type="hidden" value="{{$UserToken}}" />
                                <input name="USERID" type="hidden" value="{{$UserId}}" />
                                <input name="activeStep" type="hidden" value="1" />
                                <input name="nextStep" type="hidden" value="2" />
                                @csrf
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group position-relative error-l-100">
                                            <label for="email">Kontrak Berjangka</label>
                                            <!-- <div class="form-check">
                                                <input class="form-check-input forex_usd" type="radio" name="future_contract_opt" value="regular">
                                                Reguler
                                                <br />* Minimum Open : 1.0 Lot
                                                <br />* Minimal Setoran Awal : 10.000 USD
                                                <br />
                                                </label>
                                            </div> -->
                                            <div class="form-check">
                                                <input class="form-check-input forex_usd" type="radio" name="future_contract_opt" value="forex_usd" checked />
                                                <label class="form-check-label" for="regular">
                                                    Forex & Gold & Index (USD)
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input forex_idr" type="radio" name="future_contract_opt" value="forex_idr" />
                                                <label class="form-check-label" for="mini">
                                                    Forex & Gold & Index (IDR) - (Rp 10.000)
                                                </label>
                                            </div>
                                        </div>
                                        <input type="hidden" name="future_contract" value="forex_idr" />

                                        <div class="form-group position-relative error-l-100">
                                            <label for="email">Jenis Akun</label>
                                            <div class="form-check">
                                                <input class="form-check-input account_type_reguler" type="radio" name="account_type_opt" id="account_type_opt1" value="regular" checked />
                                                <label class="form-check-label" for="regular">
                                                    Reguler
                                                    <br />* Minimum Open : 1.0 Lot
                                                    <br />* Minimal Setoran Awal : 10.000 USD
                                                    <br />
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input account_type_mini" type="radio" name="account_type_opt" id="account_type_opt2" value="mini" />
                                                <label class="form-check-label" for="mini">
                                                    Mini
                                                    <br />* Minimum Open : 0.1 Lot
                                                    <br />* Minimal Setoran Awal : 500 USD
                                                    <br />
                                                </label>
                                            </div>
                                        </div>
                                        <input type="hidden" name="account_type" value="regular" />
                                    </div>
                                    <div class="col-md-4">
                                        <div class="alert alert-dark" role="alert">Data Pribadi</div>
                                        <div class="form-group position-relative error-l-100">
                                            <label class="required">Nama Lengkap</label>
                                            <input type="text" class="form-control" name="full_name" placeholder="Masukkan nama lengkap" onchange="savekyc()" onkeypress="savekyc()" required value="{{!empty($DataContent->full_name)?$DataContent->full_name:''}}" />
                                        </div>

                                        <div class="form-group position-relative error-l-100">
                                            <label for="idno" class="required">No Identitas</label>
                                            <input type="text" class="form-control" name="identity_number" placeholder="KTP/SIM/PASSPOR/KITAS" required onchange="savekyc()" onkeypress="savekyc()" value="{{!empty($DataContent->identity_number)?$DataContent->identity_number:''}}" />
                                        </div>

                                        <div class="form-group position-relative error-l-100">
                                            <label for="placeofbirth" class="required">Tempat Lahir</label>
                                            <input type="text" class="form-control" name="place_of_birth" placeholder="Masukkan nama kota disini" required onchange="savekyc()" onkeypress="savekyc()" value="{{!empty($DataContent->place_of_birth)?$DataContent->place_of_birth:''}}" />
                                            <div class="invalid-tooltip"> Tempat Lahir is required!</div>
                                        </div>

                                        <div class="form-group position-relative error-l-100">
                                            <label for="dateofbirth" class="required">Tanggal Lahir</label>
                                            <input type="date" class="form-control" name="date_of_birth" placeholder="Your dateofbirth address" required onchange="savekyc()" onkeypress="savekyc()" value="{{!empty($DataContent->date_of_birth)?$DataContent->date_of_birth:''}}" />
                                            <small class="form-text text-muted">Untuk memenuhi persyaratan dan peraturan kami hanya dapat menerima klien yang berusia lebih dari 21 tahun.</small>
                                        </div>

                                        <div class="form-group position-relative error-l-100">
                                            <label for="address" class="required">Alamat</label>
                                            <input type="text" class="form-control" name="home_address" placeholder="Masukkan alamat lengkap disini" required onchange="savekyc()" onkeypress="savekyc()" value="{{!empty($DataContent->home_address)?$DataContent->home_address:''}}" />
                                        </div>

                                        <div class="form-group position-relative error-l-100">
                                            <label for="poscode" class="required">Kode Pos</label>
                                            <input type="text" class="form-control" name="post_code" placeholder="Kode pos" required onchange="savekyc()" onkeypress="savekyc()" value="{{!empty($DataContent->post_code)?$DataContent->post_code:''}}" />
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group position-relative error-l-100">
                                            <label for="aename">AE Nama / Cabang</label>
                                            <input type="text" class="form-control" name="ae_name_or_branch" placeholder="Masukkan AE Nama / Cabang" onchange="savekyc()" onkeypress="savekyc()" value="@isset($DataContent->ae_name_or_branch){{trim($DataContent->ae_name_or_branch)}}@endisset" />
                                        </div>

                                        <div class="form-group position-relative error-l-100 row">
                                            <label class="col-sm-4 col-form-label">No Demo Acc.</label>
                                            <div class="col-sm-8">
                                                <span class="form-control-plaintext">{{(Session::get('user.MTUserDemo')->Login)}}</span>
                                            </div>
                                            <div class="col-sm-12">
                                                <p class="text-justify">
                                                    Semua data wajib diisi dan harus benar
                                                    adanya/sesuai dengan dokumen
                                                    pendukung yang sah. Semua data dan Pengaturan adalah merupakan
                                                    ketentuan yang mutlak dari Badan Pengawas Perdagangn Berjangka
                                                    Komoditi (BAPPEBTI) dan akan dikelola seta disimpan secara
                                                    Rahasia
                                                    (Highly Confidential) oleh Maxco Futures sebagai Wakil
                                                    Perusahaan
                                                    Pialang di Indonesia. Sebelum memulai proses Pembukaan Rekening
                                                    Transaksi Online bersama Maxco Futures.
                                                </p>
                                                <div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name='statement_data_correctly' required value="Yes" onchange="savekyc()" onkeypress="savekyc()" @isset($DataContent->statement_data_correctly) @if($DataContent->statement_data_correctly==='Yes') checked @endif @endisset/>
                                                        <label class="form-check-label required">Ya</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name='statement_data_correctly' required value="No" onchange="savekyc()" onkeypress="savekyc()" @isset($DataContent->statement_data_correctly) @if($DataContent->statement_data_correctly==='No') checked @endif @endisset/>
                                                        <label class="form-check-label">Tidak</label>
                                                    </div>
                                                </div>
                                                <label id="statement_data_correctly-error" class="error" for="statement_data_correctly" style="display: none;">Klik "Ya" untuk melanjutkan.</label>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="btn-toolbar custom-toolbar text-center card-body pt-0 mb-4 justify-content-md-center">
                                    <button class="btn btn-secondary next-btn">Next</button>
                                    <button id="btn-loading" onclick="event.preventDefault();" class="btn btn-secondary hidden">
                                        <span class="loader-small" style="position: unset !important;"></span> Next
                                    </button>
                                </div>
                            </form>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection

@section('jsonpage')
<script src="{{url('/')}}/cabinet/js/vendor/jquery.smartWizard.min.js"></script>
<script src="{{url('/')}}/cabinet/js/vendor/dropzone.min.js"></script>
<script src="{{url('/')}}/cabinet/js/vendor/sweetalert.min.js"></script>

<script>
    // $(document).ready(function() {
    var idCard_imgUrl = '';
    var id_card = false;
    var colouredPhoto_imgUrl = '';
    var coloured_photo = false;
    var specimenSignature_imgUrl = '';
    var specimen_signature = false;
    var bankStatement_imgUrl = ''
    var bank_statement = false;
    var other_imgUrl = '';
    var attachedImage = [];

    function getContent() {
        var contentDt = {
            'job_details': {
                'more_details': {
                    'length_of_work': "",
                    'previous_employer': "",
                    'employer_name': "",
                    'business_fields': "",
                    'title': "",
                    'office_address': "",
                    'office_tele': "",
                    'office_fax': ""
                },
                'position': {
                    'value': "",
                    'input': "",
                }
            },
            'wealth_list': {
                'income_per_year': "",
                'residence_location': "",
                'tax_object_selling_value': "",
                'bank_time_deposit_value': "",
                'bank_saving_value': "",
                'others': "",
            },
            'bank_details_for_withdrawls': {
                'full_name': "",
                'branch': "",
                'account_number': "",
                'phone_number': "",
                'account_type': {
                    'value': "",
                    'input': "",
                }
            },
            'emergency_contact_data': {
                'name': "",
                'address': "",
                'post_code': "",
                'home_phone_number': "",
                'relationship_with_the_contacts': ""
            },
            'attached_documents': {
                'id_card': true,
                'coloured_photo': true,
                'specimen_signature': true,
                'other_count': 1,
                'required_image': [{
                        'type': 'id_card',
                        'title': 'id_card_or_driving_license_passport',
                        'action_type': 0,
                        'src': {
                            'name': "",
                            'size': 116,
                            'url': "",
                        }
                    },
                    {
                        'type': 'coloured_photo',
                        'title': 'coloured_photo',
                        'action_type': 0,
                        'src': {
                            'name': "",
                            'size': 116,
                            'url': "",
                        }
                    },
                    {
                        'type': 'specimen_signature',
                        'title': 'specimen_signature',
                        'action_type': 0,
                        'src': {
                            'name': "",
                            'size': 116,
                            'url': "",
                        }
                    },
                    {
                        'type': 'bank_statement',
                        'title': 'bank_statement',
                        'action_type': 0,
                        'src': {
                            'name': "",
                            'size': 116,
                            'url': "",
                        }
                    },
                    {
                        'title': 'other',
                        'type': 'other',
                        'action_type': 0,
                        'src': {
                            'name': "",
                            'size': 116,
                            'url': "",
                        }
                    }
                ],
                'option': 'yes'
            },
            'trading_statement': {
                'statement_for_trading_simulation_on_demo_account': "",
                'statement_letter_has_been_experienced_on_trading': "",
                'statement_has_been_received_information_futures_company': "",
            },
            'risk_document': {
                'risk_1_title_en_ref': "",
                'risk_2_title_en_ref': "",
                'risk_3_title_en_ref': "",
                'risk_4_title_en_ref': "",
                'risk_5_title_en_ref': "",
                'risk_6_title_en_ref': "",
                'risk_7_title_en_ref': "",
                'risk_8_title_en_ref': "",
                'risk_9_title_en_ref': "",
                'risk_10_title_en_ref': "",
                'risk_11_title_en_ref': "",
                'risk_12_title_en_ref': "",
                'risk_13_title_en_ref': "",
                'radio': "",
            },
            'trading_condition': {
                'trading_conditions_1_title_en_ref': "",
                'trading_conditions_2_title_en_ref': "",
                'trading_conditions_3_title_en_ref': "",
                'trading_conditions_4_title_en_ref': "",
                'trading_conditions_5_title_en_ref': "",
                'trading_conditions_6_title_en_ref': "",
                'trading_conditions_7_title_en_ref': "",
                'trading_conditions_8_title_en_ref': "",
                'trading_conditions_9_title_en_ref': "",
                'trading_conditions_10_title_en_ref': "",
                'trading_conditions_11_title_en_ref': "",
                'trading_conditions_12_title_en_ref': "",
                'trading_conditions_13_title_en_ref': "",
                'trading_conditions_14_title_en_ref': "",
                'trading_conditions_15_title_en_ref': "",
                'trading_conditions_16_title_en_ref': "",
                'trading_conditions_17_title_en_ref': "",
                'trading_conditions_18_title_en_ref': "",
                'trading_conditions_19_title_en_ref': "",
                'trading_conditions_20_title_en_ref': "",
                'trading_conditions_21_title_en_ref': "",
                'trading_conditions_22_title_1': 0,
                'trading_conditions_22_title_2': 0,
                'trading_conditions_22_title_en_ref': "",
                'trading_conditions_23_title_en_ref': "",
                'step_4_footer': "",
            },
            'pap_agreement': "",
            'truth_resposibility_agreement': "",
            'demoAccount': "{{(Session::get('user.MTUserDemo')->Login)}}",
            'full_name': $('[name="full_name"]').val(),
            'identity_number': $('[name="identity_number"]').val(),
            'future_contract': {
                'value': $('[name="future_contract"]').val() || '',
                'input': ''
            },
            'account_type': $('[name="account_type"]').val() || '',
            'place_of_birth': $('[name="place_of_birth"]').val(),
            'date_of_birth': $('[name="date_of_birth"]').val(),
            'post_code': $('[name="post_code"]').val(),
            'page': 11,
            'activeStep': "{{!empty($DataContent->activeStep)?$DataContent->activeStep:''}}",
            'tax_file_number': "",
            'mother_maiden_name': "",
            'status': "",
            'gender': "",
            'home_address': $('[name="home_address"]').val(),
            'home_ownership_status': {
                'value': "",
                'input': ""
            },
            'openint_account_purpose': {
                'value': "",
                'input': ""
            },
            'experince_on_investment': {
                'value': "",
                'input': ""
            },
            'is_family_work_in_related_company': {
                'value': "",
                'input': ""
            },
            'is_declared_bankrupt': {
                'value': "",
                'input': ""
            },
            'trading_rules': "",
            'ae_name_or_branch': $('[name="ae_name_or_branch"]').val(),
            'husband_wife_spouse': "",
            'home_phone': "",
            'home_fax': "",
            'statement_data_correctly': $($("input[name='statement_data_correctly']:checked")).val() || '',
            'KycStatus': 0
        };
        return contentDt;
    }

    function savekyc() {
        var contentDt = getContent();
        const content = JSON.stringify(contentDt)
        $.post("{{route('savekyc')}}", {
            content,
            NBCRMWEBAPI: $('[name="NBCRMWEBAPI"]').val(),
            USERID: $('[name="USERID"]').val(),
        }, function(data, status) {
            var {
                Code,
                Message
            } = data;
            if (data.Data) {
                var Data = data.Data;
                var MTUserRef = Data.MTUserRef;
                var idx = MTUserRef.findIndex(x => x.IsSimulate === false);
                if (MTUserRef[idx]) {
                    var liveAcc = MTUserRef[idx];
                    $('[data-bind="ac_live_no"]').text(liveAcc.Login);
                }
            }
        });
    };

    // });
    $(function() {
        $("#form-step-0").validate({
            ignore: [],
            // errorElement: "div",
            rules: {
                future_contract_opt: {
                    required: true
                },
                account_type_opt: {
                    required: true
                },
                full_name: {
                    required: true
                },
                identity_number: {
                    required: true
                },
                place_of_birth: {
                    required: true
                },
                date_of_birth: {
                    required: true
                },
                home_address: {
                    required: true,
                },
                post_code: {
                    required: true,
                },
                ae_name_or_branch: {
                    required: false,
                },

            },
            messages: {

            },
            submitHandler: function() {
                var val = $($("input[name='statement_data_correctly']:checked")).val() || '';
                if (val !== "Yes") {
                    // $("[name='statement_data_correctly']").tooltip('show');
                    $('#statement_data_correctly-error').show();
                    $('#statement_data_correctly-error').text('Klik "Ya" untuk melanjutkan.');
                }

                if (val === "Yes") {
                    // $("[name='statement_data_correctly']").tooltip('hide');
                    $('.next-btn').hide();
                    $('#btn-loading').show();
                    $.post("{{route('postNextStepOpenLiveAccount')}}", {
                        NBCRMWEBAPI: $('[name="NBCRMWEBAPI"]').val(),
                        USERID: $('[name="USERID"]').val(),
                        nextStep: $('[name="nextStep"]').val(),
                    }, function(data, status) {
                        if (status === "success") {
                            location.reload();
                        } else {
                            $('.next-btn').show();
                            $('#btn-loading').hide();
                        }
                    });

                    setTimeout(function() {
                        $('.next-btn').show();
                        $('#btn-loading').hide();
                        location.reload();
                    }, 10000);
                }

            }
        });
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var renderImgAttc = false;
    var renderImgAttcOther = false;
    var minAge = new Date();
    var getYear = minAge.getFullYear();
    minAge.setFullYear(getYear - 21);
    var dd = minAge.getDate();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    var mm = minAge.getMonth() + 1; //January is 0!
    var yyyy = minAge.getFullYear();
    var minDate = yyyy + '-' + mm + '-' + dd;
    $('[name="date_of_birth"]').prop('max', minDate);



    const type = sessionStorage.getItem("register_account");
    if (type === "miniaccount") {
        $('.account_type_mini').prop('checked', true);
        $('.forex_idr').prop('checked', true);
        $('[name="future_contract"]').val('forex_idr');
        $('[name="account_type"]').val('mini');
    } else if (type === "usdaccount") {
        $('.account_type_reguler').prop('checked', true);
        $('.forex_usd').prop('checked', true);
        $('[name="future_contract"]').val('forex_usd');
        $('[name="account_type"]').val('regular');
    } else {
        // profesional
        $('.account_type_reguler').prop('checked', true);
        $('.forex_idr').prop('checked', true);
        $('[name="future_contract"]').val('forex_idr');
        $('[name="account_type"]').val('regular');
    }

    $('[name="statement_data_correctly"]').on('change', function(event) {
        var val = event.currentTarget.value;
        if (val === 'No') {
            // $('.next-btn').prop('disabled',true);
            $('#statement_data_correctly-error').show();
        } else {
            // $('.next-btn').prop('disabled',false);
            $('#statement_data_correctly-error').hide();
        }
    });

    $('[name="future_contract_opt"]').on('change', function(event) {
        const val = event.currentTarget.value;
        $('[name="future_contract"]').val(val);
    });


    $('[name="date_of_birth"]').on('change', function(event) {
        var date = event.currentTarget.value;
        var today = new Date();
        var birthday = new Date(date);
        var year = 0;
        if (today.getMonth() < birthday.getMonth()) {
            year = 1;
        } else if ((today.getMonth() == birthday.getMonth()) && today.getDate() < birthday.getDate()) {
            year = 1;
        }
        var age = today.getFullYear() - birthday.getFullYear() - year;
        if (age < 0) {
            age = 0;
        }
        if (age < 21) {
            $(this).val('');
            swal("Perhatian!", "Untuk memenuhi persyaratan dan peraturan  kami hanya dapat menerima klien yang berusia lebih dari 21 tahun.!", "warning");
        } else {

        }
    });
</script>
@endsection