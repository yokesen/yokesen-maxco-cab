<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/maxco/loginlab','ApiController@lab')->name('lab');

Route::post('/maxco/registerdemoaccount','ApiController@registerdemo')->name('postRegisterDemo');

Route::post('/maxco/detailuser','ApiController@detailuser')->name('postDetailUser');

Route::post('/savetokenfromweb','ApiController@detailuser')->name('postDetailUser');

