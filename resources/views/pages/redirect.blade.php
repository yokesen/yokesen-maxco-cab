<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-BMTCL2Z4WL"></script>
  <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
      dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'G-BMTCL2Z4WL');
  </script>

  <meta charset="UTF-8">
  <title>Maxco Futures | Prestigious Global Brokerage House</title>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

  <link rel="stylesheet" href="{{url('/')}}/cabinet/font/iconsmind-s/css/iconsminds.css" />
  <link rel="stylesheet" href="{{url('/')}}/cabinet/font/simple-line-icons/css/simple-line-icons.css" />

  <link rel="stylesheet" href="{{url('/')}}/cabinet/css/vendor/bootstrap.min.css" />
  <link rel="stylesheet" href="{{url('/')}}/cabinet/css/vendor/bootstrap.rtl.only.min.css" />
  <link rel="stylesheet" href="{{url('/')}}/cabinet/css/vendor/bootstrap-float-label.min.css" />
  <link rel="stylesheet" href="{{url('/')}}/cabinet/css/main.css" />
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <style>
    .btn-active {
      background-color: #fff !important;
      color: #017dc7 !important;
      border-color: #017dc7 !important;
    }
  </style>
  <!-- Facebook Pixel Code -->
  <script>
    ! function(f, b, e, v, n, t, s) {
      if (f.fbq) return;
      n = f.fbq = function() {
        n.callMethod ?
          n.callMethod.apply(n, arguments) : n.queue.push(arguments)
      };
      if (!f._fbq) f._fbq = n;
      n.push = n;
      n.loaded = !0;
      n.version = '2.0';
      n.queue = [];
      t = b.createElement(e);
      t.async = !0;
      t.src = v;
      s = b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t, s)
    }(window, document, 'script',
      'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '242949443388541');
    fbq('track', 'PageView');
  </script>
  <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=242949443388541&ev=PageView&noscript=1" /></noscript>
  <!-- End Facebook Pixel Code -->
</head>

<body class="background show-spinner no-footer">
  <div class="fixed-background" style="background:url({{url('/')}}/cabinet/img/webpage/background.jpg)"></div>
  <main id="app">
    <div class="container">
      <div class="row h-100">
        <div class="col-12 col-md-10 mx-auto my-auto">
          <div class="card">
            <div class="card-body text-center">
              You will redirect after <span id="second">30</span> Second. If it dooesn't please click <a style="color:blue;" href="{{url('/')}}/{{App::getLocale()}}/{{$pageredirect}}">Link<a />

            </div>
          </div>
        </div>
      </div>
    </div>
  </main>

  <script src="{{url('/')}}/cabinet/js/vendor/jquery-3.3.1.min.js"></script>
  <script src="{{url('/')}}/cabinet/js/vendor/bootstrap.bundle.min.js"></script>
  <script src="{{url('/')}}/cabinet/js/dore.script.js"></script>
  <script src="{{url('/')}}/cabinet/js/scripts.js"></script>
  <script src="{{url('/')}}/cabinet/data/nationality.js"></script>

  <script>
    $(document).ready(function() {
      var second = 30;
      var inter = setInterval(function() {
        second -= 1;
        rendertext(second);
      }, 1000);

      function rendertext(second) {
        $('#second').text(second);
        if (second === 0) {
          stopInter();
        }
      }

      function stopInter() {
        clearInterval(inter)
      }

    });
  </script>
</body>

</html>