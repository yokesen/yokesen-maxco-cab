@extends('templates.master')

@section('csslinkonhead')
<link rel="stylesheet" href="{{url('/')}}/cabinet/css/vendor/smart_wizard.min.css" />
<link rel="stylesheet" href="{{url('/')}}/cabinet/css/vendor/dropzone.min.css" />
<meta name="csrf-token" content="{{ csrf_token() }}" />
<style>
    .form-check-input {
        margin-top: 0.7rem !important;
    }
</style>
@if($agent->isMobile())
<style>
    .sw-main.sw-theme-check>ul.step-anchor>li>a:after,
    .sw-main.sw-theme-check>ul.step-anchor>li.active>a:after,
    .sw-main.sw-main.sw-theme-check>ul.step-anchor>li.done>a:after {
        margin-top: 42px;
    }
</style>
@endif
@endsection

@section('content')
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <h1>Opening Account Wizard</h1>
                <div class="separator mb-5"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 mb-4">
                <!-- <h5 class="mb-4">Open Live Account</h5> -->
                <div class="card mb-4">
                    <div id="" class="sw-main sw-theme-check">
                        <ul class="card-header mb-4 nav nav-tabs step-anchor" style="z-index: 0;">
                            <li class="nav-item  done">
                                <a class="nav-link">
                                    @if(!$agent->isMobile())
                                    Step 1
                                    <br>
                                    @endif
                                </a>
                            </li>
                            <li class="nav-item  done">
                                <a class="nav-link">
                                    @if(!$agent->isMobile())
                                    Step 2
                                    <br>
                                    @endif
                                </a>
                            </li>
                            <li class="nav-item  done">
                                <a class="nav-link">
                                    @if(!$agent->isMobile())
                                    Step 3
                                    <br>
                                    @endif
                                </a>
                            </li>
                            <li class="nav-item done">
                                <a class="nav-link">
                                    @if(!$agent->isMobile())
                                    Step 4
                                    <br>
                                    @endif
                                </a>
                            </li>
                            <li class="nav-item active">
                                <a class="nav-link">
                                    @if(!$agent->isMobile())
                                    Step 5
                                    <br>
                                    @endif
                                </a>
                            </li>
                        </ul>

                        <div class="card-body">
                            <form id="form-step-0" novalidate style="padding: 0px 22px;">
                                <input name="NBCRMWEBAPI" type="hidden" value="{{$UserToken}}" />
                                <input name="USERID" type="hidden" value="{{$UserId}}" />
                                @csrf
                                <div class="row">
                                    <div class="col-sm-12">
                                        <h4 class="text-center">Buka Akun Live</h4>
                                        <h2 class="text-center">Menunggu Persetujuan</h2>
                                        <!-- <h4 class="text-center">MT LIVE A/C NO. : <span data-bind="ac_live_no"></span></h4> -->
                                        <!-- <h4 class="text-center">Server : mt4-live.maxcofutures.co.id</h4> -->
                                        <!-- <div class="text-center">
                                            <a href="{{url('/')}}/deposit" class="btn btn-primary">DEPOSIT SEKARANG</a>
                                        </div> -->
                                        <h4 class="text-center">Anda telah berhasil mengirimkan permintaan untuk membuka akun live MT4.</h4>
                                        <h4 class="text-center">MAXCO FUTURES akan menghubungi Anda untuk langkah selanjutnya dalam kurun waktu 24 jam. (tidak termasuk hari libur)</h4>
                                    </div>
                                </div>
                            </form>
                        </div>
                        @if(Session::get('user.KycStatus')==0 || Session::get('user.KycStatus')==3 )
                        <div class="btn-toolbar custom-toolbar text-center card-body pt-0 mb-4 justify-content-md-center">
                            <button class="btn btn-secondary prev-btn" type="button">Back</button>
                            <button onclick="event.preventDefault();" class="btn-loading-prev btn btn-secondary hidden">
                                <span class="loader-small" style="position: unset !important;"></span> Back
                            </button>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection

@section('jsonpage')
<script src="{{url('/')}}/cabinet/js/vendor/jquery.smartWizard.min.js"></script>
<script src="{{url('/')}}/cabinet/js/vendor/dropzone.min.js"></script>
<script src="{{url('/')}}/cabinet/js/vendor/sweetalert.min.js"></script>

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('.prev-btn').on('click', function() {
        $('.prev-btn').hide();
        $('.btn-loading-prev').show();
        $.post("{{route('postNextStepOpenLiveAccount')}}", {
            NBCRMWEBAPI: $('[name="NBCRMWEBAPI"]').val(),
            USERID: $('[name="USERID"]').val(),
            nextStep: '2',
            page: '0'
        }, function(data, status) {
            if (status === "success") {
                location.reload();
            } else {
                $('.prev-btn').show();
                $('.btn-loading-prev').hide();
            }
        });
    });
</script>
@endsection