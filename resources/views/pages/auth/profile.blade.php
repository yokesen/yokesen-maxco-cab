@extends('templates.master')

@section('content')
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="mb-2">
                    <h1>{{Session::get('user.name')}}</h1>
                </div>
                <ul class="nav nav-tabs separator-tabs ml-0 mb-5" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="first-tab" data-toggle="tab" href="#first" role="tab" aria-controls="first" aria-selected="true">PROFILE</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane show active" id="first" role="tabpanel" aria-labelledby="first-tab">
                        <div class="row">
                            <div class="col-12 col-lg-5 col-xl-3 col-left mt-5">
                                <a class="lightbox">
                                    <img alt="Profile" src="{{url('/')}}/cabinet/img/profile-default.jpg" class="img-thumbnail card-img social-profile-img" />
                                </a>

                                <div class="card mb-4">
                                    <div class="card-body">
                                        <div class="text-center pt-4">
                                            <p class="list-item-heading pt-2">{{Session::get('user.name')}}</p>
                                        </div>

                                        <p class="text-muted text-small mb-2">Kode klien</p>
                                        <p class="mb-3">{{$User->UserId}}</p>

                                        <p class="text-muted text-small mb-2">Nama Lengkap</p>
                                        <p class="mb-3">{{$IdCard->RealName}}</p>

                                        <p class="text-muted text-small mb-2">Kebangsaan</p>
                                        <p class="mb-3">{{$User->Nationality}}</p>

                                        <p class="text-muted text-small mb-2">Tempat Lahir</p>
                                        <p class="mb-3">{{!empty($User->PlaceOfBirth) ? $User->PlaceOfBirth : '-'}}</p>

                                        <p class="text-muted text-small mb-2">Tanggal Lahir</p>
                                        <p class="mb-3">{{!empty($User->DateOfBirth) ? date('m/d/Y',strtotime($User->DateOfBirth)) : '-'}}</p>

                                        <p class="text-muted text-small mb-2">No Indentitas</p>
                                        <p class="mb-3">{{$IdCard->IDNO}}</p>

                                        <p class="text-muted text-small mb-2">Jenis Kelamin</p>
                                        <p class="mb-3">{{!empty($User->Gender) ? $User->Gender : '-'}}</p>

                                        <p class="text-muted text-small mb-2">Email</p>
                                        <p class="mb-3">{{$User->Email}}</p>

                                        <p class="text-muted text-small mb-2">Mobile No</p>
                                        <p class="mb-3">({{$User->AreaCode}}){{$User->Mobile}}</p>

                                        <a class="btn btn-primary btn-lg btn-shadow" style="border-radius: 0px;background-color: #017dc7 !important;width:100%;" href="/change-password">Change Password</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-lg-6 col-xl-6 mt-5">
                                <div class="card">
                                    <div class="position-absolute card-top-buttons">
                                        <button class="btn btn-header-light icon-button">
                                            <i class="simple-icon-refresh"></i>
                                        </button>
                                    </div>
                                    <div class="card-body">
                                        <h5 class="card-title">Profile Status</h5>
                                        <div class="row">
                                            <!-- <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                                <div class="dashboard-donut-chart chart">
                                                    <canvas id="categoryChart"></canvas>
                                                </div>
                                            </div> -->
                                            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                                <div class="mb-4">
                                                    <p class="mb-2">
                                                        Informasi Akun
                                                        <span class="float-right text-muted">
                                                            <span id="dataKycInfoaAkun">0</span>/<span id="dataMandatoryInfoaAkun"></span>
                                                        </span>
                                                    </p>
                                                    <div class="progress">
                                                        <div class="progress-bar infoakun-progress" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="0"></div>
                                                    </div>
                                                </div>

                                                <div class="mb-4">
                                                    <p class="mb-2">
                                                        Data Diri
                                                        <span class="float-right text-muted">
                                                            <span id="dataKycprofiledata">0</span>/<span id="dataMandatoryprofiledata"></span>
                                                        </span>

                                                    </p>
                                                    <div class="progress">
                                                        <div class="progress-bar profile-progress" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>

                                                <div class="mb-4">
                                                    <p class="mb-2">
                                                        Profil Pialang Berjangka
                                                        <span class="float-right text-muted">
                                                            <span id="dataKycfuturesprofile">0</span>/<span id="dataMandatoryfuturesprofile"></span>
                                                        </span>
                                                    </p>
                                                    <div class="progress">
                                                        <div class="progress-bar futuresprofile-progress" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>

                                                <div class="mb-4">
                                                    <p class="mb-2">
                                                        Pernyataan Demo Akun
                                                        <span class="float-right text-muted">
                                                            <span id="dataKyctradingsimulation">0</span>/<span id="dataMandatorytradingsimulation"></span>
                                                        </span>
                                                    </p>
                                                    <div class="progress">
                                                        <div class="progress-bar tradingsimulation-progress" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>

                                                <div class="mb-4">
                                                    <p class="mb-2">
                                                        Pernyataan Pengalaman Transaksi
                                                        <span class="float-right text-muted">
                                                            <span id="dataKyctradingexpert">0</span>/<span id="dataMandatorytradingexpert"></span>
                                                        </span>
                                                    </p>
                                                    <div class="progress">
                                                        <div class="progress-bar tradingexpert-progress" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>

                                                <div class="mb-4">
                                                    <p class="mb-2">
                                                        DPAR (Agreement)
                                                        <span class="float-right text-muted">
                                                            <span id="dataKycdpar">0</span>/<span id="dataMandatorydpar"></span>
                                                        </span>
                                                    </p>
                                                    <div class="progress">
                                                        <div class="progress-bar dparisk-progress" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>

                                                <div class="mb-4">
                                                    <p class="mb-2">
                                                        Trading Rules
                                                        <span class="float-right text-muted">
                                                            <span id="dataKyctradingrules">0</span>/<span id="dataMandatorytradingrules"></span>
                                                        </span>
                                                    </p>
                                                    <div class="progress">
                                                        <div class="progress-bar tradingrules-progress" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>

                                                <div class="mb-4">
                                                    <p class="mb-2">
                                                        Pernyataan Kerahasiaan Password
                                                        <span class="float-right text-muted">
                                                            <span id="dataKycpap">0</span>/<span id="dataMandatorypap"></span>

                                                        </span>
                                                    </p>
                                                    <div class="progress">
                                                        <div class="progress-bar pap-progress" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-lg-12 col-xl-3 mt-5">
                                <div class="row">
                                    <div class="col-12 mb-4">
                                        <div class="card dashboard-small-chart-analytics">
                                            <div class="card-body">
                                                <h5 class="mb-2">
                                                    Total DEPOSIT
                                                </h5>
                                                <?php
                                                $currency = "USD";
                                                // switch (Session::get('user.Currency')) {
                                                //     case '360':
                                                //         $currency = "IDR";
                                                //         break;
                                                //     case '840':
                                                //         $currency = "USD";
                                                //         break;
                                                // }
                                                ?>
                                                <h1>{{$currency}} {{!empty(Session::get('user.Summary'))?Session::get('user.Summary')->TotalDepAmt:0}}</h1>
                                                <a class="btn btn-primary btn-lg btn-shadow" style="border-radius: 0px;background-color: #017dc7 !important;width:100%;" href="{{route('historydeposit')}}">Deposit Now</a>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 mb-4">
                                        <div class="card dashboard-small-chart-analytics">
                                            <div class="card-body">
                                                <h5 class="mb-2">
                                                    Total WITHDRAWAL
                                                </h5>
                                                <h1>{{$currency}} {{!empty(Session::get('user.Summary'))?Session::get('user.Summary')->TotalWithdrawAmt:0}}</h1>
                                                <a class="btn btn-primary btn-lg btn-shadow" style="border-radius: 0px;background-color: #017dc7 !important;width:100%;" href="{{route('historywithdrawal')}}">Withdrawal Now</a>

                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection

@section('jsonpage')
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $(document).ready(function() {
        getDetailKYC();
    });
    var info_akun = ['account_type', 'future_contract.value', 'full_name', 'identity_number', 'place_of_birth', 'date_of_birth', 'post_code', 'home_address', 'statement_data_correctly']
    $('#dataMandatoryInfoaAkun').text(info_akun.length);
    var profile_data = [
        'job_details.more_details.length_of_work', 'job_details.more_details.previous_employer', 'job_details.more_details.employer_name', 'job_details.more_details.business_fields', 'job_details.more_details.title', 'job_details.more_details.office_address', 'job_details.more_details.office_tele', 'job_details.more_details.office_fax', 'job_details.position.value',
        // 'job_details.position.input',
        'wealth_list.income_per_year', 'wealth_list.residence_location', 'wealth_list.tax_object_selling_value', 'wealth_list.bank_time_deposit_value', 'wealth_list.bank_saving_value', 'wealth_list.others', 'bank_details_for_withdrawls.full_name', 'bank_details_for_withdrawls.branch', 'bank_details_for_withdrawls.account_number', 'bank_details_for_withdrawls.phone_number', 'bank_details_for_withdrawls.account_type.value',
        // 'bank_details_for_withdrawls.account_type.input',
        'emergency_contact_data.name', 'emergency_contact_data.address', 'emergency_contact_data.post_code', 'emergency_contact_data.home_phone_number', 'emergency_contact_data.relationship_with_the_contacts',
        'truth_resposibility_agreement', 'tax_file_number', 'mother_maiden_name', 'status', 'gender', 'home_ownership_status.value',
        'openint_account_purpose.value', 'experince_on_investment.value', 'is_family_work_in_related_company.value', 'is_declared_bankrupt.value'
    ];

    $('#dataMandatoryprofiledata').text(profile_data.length);

    var futures_profile = ['trading_statement.statement_has_been_received_information_futures_company'];
    $('#dataMandatoryfuturesprofile').text(futures_profile.length);

    var tradingSimulation = ['trading_statement.statement_for_trading_simulation_on_demo_account'];
    $('#dataMandatorytradingsimulation').text(tradingSimulation.length);

    var trading_expert = ['trading_statement.statement_letter_has_been_experienced_on_trading'];
    $('#dataMandatorytradingexpert').text(trading_expert.length);

    var dpa_risk = [
        'risk_document.risk_1_title_en_ref', 'risk_document.risk_2_title_en_ref', 'risk_document.risk_3_title_en_ref', 'risk_document.risk_4_title_en_ref', 'risk_document.risk_5_title_en_ref', 'risk_document.risk_6_title_en_ref', 'risk_document.risk_7_title_en_ref', 'risk_document.risk_8_title_en_ref', 'risk_document.risk_9_title_en_ref', 'risk_document.risk_10_title_en_ref', 'risk_document.risk_11_title_en_ref', 'risk_document.risk_12_title_en_ref', 'risk_document.risk_13_title_en_ref', 'risk_document.radio',
        'trading_condition.trading_conditions_1_title_en_ref', 'trading_condition.trading_conditions_2_title_en_ref', 'trading_condition.trading_conditions_3_title_en_ref', 'trading_condition.trading_conditions_4_title_en_ref', 'trading_condition.trading_conditions_5_title_en_ref', 'trading_condition.trading_conditions_6_title_en_ref', 'trading_condition.trading_conditions_7_title_en_ref', 'trading_condition.trading_conditions_8_title_en_ref', 'trading_condition.trading_conditions_9_title_en_ref', 'trading_condition.trading_conditions_10_title_en_ref', 'trading_condition.trading_conditions_11_title_en_ref', 'trading_condition.trading_conditions_12_title_en_ref', 'trading_condition.trading_conditions_13_title_en_ref', 'trading_condition.trading_conditions_14_title_en_ref', 'trading_condition.trading_conditions_15_title_en_ref', 'trading_condition.trading_conditions_16_title_en_ref', 'trading_condition.trading_conditions_17_title_en_ref', 'trading_condition.trading_conditions_18_title_en_ref', 'trading_condition.trading_conditions_19_title_en_ref', 'trading_condition.trading_conditions_20_title_en_ref', 'trading_condition.trading_conditions_21_title_en_ref', 'trading_condition.trading_conditions_22_title_en_ref', 'trading_condition.trading_conditions_23_title_en_ref', 'trading_condition.step_4_footer'
    ];
    $('#dataMandatorydpar').text(dpa_risk.length);

    var tradingrules = ["trading_rules"];
    $('#dataMandatorytradingrules').text(tradingrules.length);

    var pap = ["pap_agreement"];
    $('#dataMandatorypap').text(pap.length);

    function getDetailKYC() {

        $.get("{{route('apigetKycDetail')}}", {}, function(data, status) {
            var dataKyc = data.Data;
            if (dataKyc) {
                var scoreInfoAkun = 0;
                for (const iterator of info_akun) {
                    var val = dataKyc[iterator];
                    if (!val) {
                        var iteratorSplit = iterator.split(".");
                        var val;
                        iteratorSplit.map(function(item, index) {
                            val = !val ? dataKyc[item] : val[item];
                            if ((index + 1) === iteratorSplit.length) {
                                if (val && val !== "" && val !== "no" && val !== "No" && val !== "0") {
                                    scoreInfoAkun += 1;
                                }
                            }
                        });
                    } else {
                        if (val !== "" && val !== "no" && val !== "No" && val !== "0") {
                            scoreInfoAkun += 1;
                        }
                    }
                    $('#dataKycInfoaAkun').text(scoreInfoAkun);
                    var percentInfoAkun = (parseInt(scoreInfoAkun, 10) / info_akun.length) * 100;
                    $('.infoakun-progress').css('width', `${percentInfoAkun}%`);

                }

                var scoreprofile = 0;
                for (const iterator of profile_data) {
                    var val = dataKyc[iterator];
                    if (!val) {
                        var iteratorSplit = iterator.split(".");
                        var val;
                        iteratorSplit.map(function(item, index) {
                            val = !val ? dataKyc[item] : val[item];

                            if ((index + 1) === iteratorSplit.length) {
                                if (val && val !== "" && val !== "no" && val !== "No" && val !== "0") {
                                    scoreprofile += 1;
                                }

                                if (iterator === "is_declared_bankrupt.value" && val === "No") {
                                    scoreprofile += 1;
                                }

                                if (iterator === "is_declared_bankrupt.value" && val === "Yes") {
                                    scoreprofile -= 1;
                                }
                            }
                        });
                    } else {

                        if (val !== "" && val !== "no" && val !== "No" && val !== "0") {
                            scoreprofile += 1;
                        }
                    }
                }

                var required_image = dataKyc.attached_documents ? dataKyc.attached_documents.required_image : [];

                $('#dataMandatoryprofiledata').text(profile_data.length);
                required_image.map(function(item, index) {
                    console.log(item)
                    if (item.src && item.src.url !== "") {
                        scoreprofile += 1;
                    }
                });
                $('#dataKycprofiledata').text(scoreprofile);
                var percentprofile = (parseInt(scoreprofile, 10) / profile_data.length) * 100;
                $('.profile-progress').css('width', `${percentprofile}%`);


                var scorefuturesprofile = 0;
                for (const iterator of futures_profile) {
                    var val = dataKyc[iterator];
                    if (!val) {
                        var iteratorSplit = iterator.split(".");
                        var val;
                        iteratorSplit.map(function(item, index) {
                            val = !val ? dataKyc[item] : val[item];
                            if ((index + 1) === iteratorSplit.length) {
                                if (val && val !== "" && val !== "no" && val !== "No" && val !== "0") {
                                    scorefuturesprofile += 1;
                                }
                            }
                        });
                    } else {
                        if (val !== "" && val !== "no" && val !== "No" && val !== "0") {
                            scorefuturesprofile += 1;
                        }
                    }
                    $('#dataKycfuturesprofile').text(scorefuturesprofile);
                    var percentfutureprofile = (parseInt(scorefuturesprofile, 10) / futures_profile.length) * 100;
                    $('.futuresprofile-progress').css('width', `${percentfutureprofile}%`);

                }

                var scoretradingSimulation = 0;
                for (const iterator of tradingSimulation) {
                    var val = dataKyc[iterator];
                    if (!val) {
                        var iteratorSplit = iterator.split(".");
                        var val;
                        iteratorSplit.map(function(item, index) {
                            val = !val ? dataKyc[item] : val[item];
                            if ((index + 1) === iteratorSplit.length) {
                                if (val && val !== "" && val !== "no" && val !== "No" && val !== "0") {
                                    scoretradingSimulation += 1;
                                }
                            }
                        });
                    } else {
                        if (val !== "" && val !== "no" && val !== "No" && val !== "0") {
                            scoretradingSimulation += 1;
                        }
                    }
                    $('#dataKyctradingsimulation').text(scoretradingSimulation);
                    var percentfutureprofile = (parseInt(scoretradingSimulation, 10) / tradingSimulation.length) * 100;
                    $('.tradingsimulation-progress').css('width', `${percentfutureprofile}%`);
                }

                var scoretradingexpert = 0;
                for (const iterator of trading_expert) {
                    var val = dataKyc[iterator];
                    if (!val) {
                        var iteratorSplit = iterator.split(".");
                        var val;
                        iteratorSplit.map(function(item, index) {
                            val = !val ? dataKyc[item] : val[item];
                            if ((index + 1) === iteratorSplit.length) {
                                if (val && val !== "" && val !== "no" && val !== "No" && val !== "0") {
                                    scoretradingexpert += 1;
                                }
                            }
                        });
                    } else {
                        if (val !== "" && val !== "no" && val !== "No" && val !== "0") {
                            scoretradingexpert += 1;
                        }
                    }
                    $('#dataKyctradingexpert').text(scoretradingexpert);
                    var percentfutureprofile = (parseInt(scoretradingexpert, 10) / trading_expert.length) * 100;
                    $('.tradingexpert-progress').css('width', `${percentfutureprofile}%`);
                }

                var scoredparisk = 0;
                for (const iterator of dpa_risk) {
                    var val = dataKyc[iterator];
                    if (!val) {
                        var iteratorSplit = iterator.split(".");
                        var val;
                        iteratorSplit.map(function(item, index) {
                            val = !val ? dataKyc[item] : val[item];
                            if ((index + 1) === iteratorSplit.length) {
                                if (val && val !== "" && val !== "no" && val !== "No" && val !== "0") {
                                    scoredparisk += 1;
                                }
                            }
                        });
                    } else {
                        if (val !== "" && val !== "no" && val !== "No" && val !== "0") {
                            scoredparisk += 1;
                        }
                    }
                    $('#dataKycdpar').text(scoredparisk);
                    var percentfutureprofile = (parseInt(scoredparisk, 10) / dpa_risk.length) * 100;
                    $('.dparisk-progress').css('width', `${percentfutureprofile}%`);
                }

                var scoretradingrules = 0;
                for (const iterator of tradingrules) {
                    var val = dataKyc[iterator];
                    if (!val) {
                        var iteratorSplit = iterator.split(".");
                        var val;
                        iteratorSplit.map(function(item, index) {
                            val = !val ? dataKyc[item] : val[item];
                            if ((index + 1) === iteratorSplit.length) {
                                if (val && val !== "" && val !== "no" && val !== "No" && val !== "0") {
                                    scoretradingrules += 1;
                                }
                            }
                        });
                    } else {
                        if (val !== "" && val !== "no" && val !== "No" && val !== "0") {
                            scoretradingrules += 1;
                        }
                    }
                    $('#dataKyctradingrules').text(scoretradingrules);
                    var percentfutureprofile = (parseInt(scoretradingrules, 10) / tradingrules.length) * 100;
                    $('.tradingrules-progress').css('width', `${percentfutureprofile}%`);
                }

                var scorepap = 0;
                for (const iterator of pap) {
                    var val = dataKyc[iterator];
                    if (!val) {
                        var iteratorSplit = iterator.split(".");
                        var val;
                        iteratorSplit.map(function(item, index) {
                            val = !val ? dataKyc[item] : val[item];
                            if ((index + 1) === iteratorSplit.length) {
                                if (val && val !== "" && val !== "no" && val !== "No" && val !== "0") {
                                    scorepap += 1;
                                }
                            }
                        });
                    } else {
                        if (val !== "" && val !== "no" && val !== "No" && val !== "0") {
                            scorepap += 1;
                        }
                    }
                    $('#dataKycpap').text(scorepap);
                    var percentpap = (parseInt(scorepap, 10) / pap.length) * 100;
                    $('.pap-progress').css('width', `${percentpap}%`);
                }
            }
        });
    };
</script>
@endsection