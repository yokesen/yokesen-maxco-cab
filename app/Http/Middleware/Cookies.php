<?php

namespace App\Http\Middleware;

use Closure;
use Cookie;
use Redirect;
use Session;

class Cookies
{
  /**
   * Handle an incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \Closure  $next
   * @return mixed
   */
  public function handle($request, Closure $next)
  {
    // $token = Session::get('user.token');
    // if ($token === null) {
    //   return redirect()->route('login');
    // } else {
      // return $next($request);
      if (!empty(Cookie::get('lastUrl'))) {
        return Redirect::to(Cookie::get('lastUrl'));
      } else {
        return $next($request);
      }
    // }
  }
}
