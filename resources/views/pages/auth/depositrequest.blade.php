@extends('templates.master')


@section('csslinkonhead')
<link rel="stylesheet" href="{{url('/')}}/cabinet/css/vendor/select2.min.css" />
<link rel="stylesheet" href="{{url('/')}}/cabinet/css/vendor/select2-bootstrap.min.css" />
<link rel="stylesheet" href="{{url('/')}}/cabinet/css/vendor/dropzone.min.css" />

@endsection

@section('content')
<?php
switch (Session::get('user.Currency')) {
    case '360':
        $currency = "IDR";
        break;
    case '840':
        $currency = "USD";
        break;
    default:
        $currency ='';
        break;
}
?>
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <h1>Deposit Request</h1>
                <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                    <ol class="breadcrumb pt-0"></ol>
                </nav>
                <div class="separator mb-5"></div>
            </div>
        </div>
        <div class="row" v-show="showform">
            <div class="col-12 col-lg-6 col-xl-6 col-left mt-5">
                <div class="card">
                    <div class="card-body">
                        <div class="card-title">
                            <h5>DEPOSIT REQUEST</h5>
                        </div>
                        <form id="deposit-request-form" class="needs-validation" name="formrequestdeposit" novalidate>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">To Bank</label>
                                <div class="col-sm-9">
                                    <div id="text-panin-bank">
                                        <!-- <input type="hidden" name="BankId" value="36" /> -->
                                        <div class="form-control-plaintext">Panin Bank</div>
                                    </div>
                                    <div id="selectbankdeposit" style="display:none;">
                                        <select class="form-control bank-deposit" name="BankId" required>
                                            <option value="35">Panin Bank</option>
                                            <option value="36">BCA</option>
                                        </select>
                                        <div class="invalid-tooltip">To Bank is required!</div>
                                    </div>

                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Bank Account</label>
                                <div class="col-sm-9">
                                    <div class="form-control-plaintext deposit-to-panin">100 500 2928 (IDR) <br> 100 600 0788 (USD)</div>
                                    <div class="form-control-plaintext deposit-to-bca" style="display: none;">035 311 5666 (IDR) <br> 035 311 9777 (USD)</div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Amount</label>
                                <div class="col-sm-9">
                                    <div class="input-group">
                                        <input type="number" class="form-control" aria-label="Amount (to the nearest dollar)" name="Amount" min="1" required>
                                        <div class="input-group-append">
                                            <span class="input-group-text">{{$currency}}</span>
                                        </div>
                                    </div>
                                    <label id="Amount-error" class="error" for="Amount"></label>
                                </div>
                            </div>
                            <div class="form-group row row tooltip-right-top">
                                <label class="col-sm-3 col-form-label">MT4 Account</label>
                                <div class="col-sm-9">
                                    <div class="form-control-plaintext">{{Session::get('user.MTUserLive')->Login}}</div>
                                </div>
                            </div>
                            @if (session('ErrorMessage'))
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <strong>Error!</strong> {{ session('ErrorMessage') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            @endif
                            <input type="hidden" name="Method" value="BankTransfer" />
                            <input type="hidden" name="Receipt" value="" />
                        </form>
                        <div class="form-group">
                            <div class="card mb-4">
                                <div class="card-body">
                                    <h5 class="mb-4">Screenshot</h5>
                                    <form action="{{route('uploadimg')}}" enctype="multipart/form-data" class="dropzone upload_transfer">
                                        @csrf
                                    </form>
                                    <br>
                                    <div id="div-upload-screenshot" class="hidden">
                                        <p class="mb-2">Uploading <span class="float-right text-muted progress-percent-screenshot">0 %</span></p>
                                        <div class="progress">
                                            <div class="progress-bar progress-screenshot" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row mb-0 text-right">
                            <div class="col-sm-4">
                                <p onclick="otherdepositbank();" style="margin: 9px 0px;">Other deposit bank</p>
                            </div>
                            <div class="col-sm-8">
                                <a class="btn btn-warning btn-lg btn-shadow borderradius-0" href="/deposit">Cancel</a>
                                <button name="submitFormFooter" class="btn btn-primary btn-lg btn-shadow btn-maxco-blue borderradius-0">Submit</button>
                                <button id="btn-loading" onclick="event.preventDefault();" class="btn btn-primary btn-lg btn-shadow hidden" style="border-radius: 0px;background-color: #017dc7 !important;"><span class="loader"></span> Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</main>
@endsection

@section('jsonpage')
<script src="{{url('/')}}/cabinet/js/vendor/select2.full.js"></script>
<script src="{{url('/')}}/cabinet/js/vendor/jquery.validate/jquery.validate.min.js"></script>
<script src="{{url('/')}}/cabinet/js/vendor/jquery.validate/additional-methods.min.js"></script>
<script src="{{url('/')}}/cabinet/js/vendor/dropzone.min.js"></script>
<script src="{{url('/')}}/cabinet/js/vendor/sweetalert.min.js"></script>

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var BankId = "35";
    $('.bank-deposit').on('change', function() {
        var value = $(this).val();
        if (value == 35) {
            BankId = value;
            $('.deposit-to-bca').hide();
            $('.deposit-to-panin').show();
        } else {
            BankId = value;
            $('.deposit-to-bca').show();
            $('.deposit-to-panin').hide();
        }
    })

    function otherdepositbank() {
        $('#selectbankdeposit').show();
        $('.bank-deposit').focus();
        $('#text-panin-bank').hide();
    }
    $(function() {
        $('[name="formrequestdeposit"]').validate({
            ignore: [],
            // errorElement: "div",
            rules: {
                Amount: {
                    required: true,
                    number: true,
                    min: 1
                },
            },
            messages: {}
        });
    });
    if ($().dropzone && !$(".dropzone").hasClass("disabled")) {
        $(".upload_transfer").dropzone({
            url: "{{route('uploadimg')}}",
            dictResponseError: 'Cannot accept this type file',
            acceptedFiles: ".jpg, .jpeg, .pdf, .png",
            dictDefaultMessage: "Drop files here to upload <br> format (JPG/JPEG/PDF/PNG)",
            maxFiles: 1,
            init: function() {
                var drop = this;
                this.on("success", function(file, responseText) {
                    $('[name="Receipt"]').val(responseText.Data);
                });
                this.on("error", function(file, error) {
                    swal("Info", error, "info");
                    // i remove current file
                    drop.removeFile(file);
                    $('.progress-screenshot').width(`0%`);
                    $('.progress-percent-screenshot').text(`0 %`);
                });
                this.on('sending', function() {
                    $('#div-upload-screenshot').show();
                    $('.progress-screenshot').width(`0%`);
                    $('.progress-percent-screenshot').text(`0 %`);
                });
                this.on('canceled', function() {
                    $('.progress-screenshot').width(`0%`);
                    $('.progress-percent-screenshot').text(`0 %`);
                });
                this.on("totaluploadprogress", function(progress) {
                    var fixprogress = 0;
                    if (progress < 100) {
                        fixprogress = (progress).toFixed(2);
                    } else {
                        fixprogress = 100;
                    }

                    $('.progress-screenshot').width(`${fixprogress}%`);
                    $('.progress-percent-screenshot').text(`${fixprogress} %`);
                });
            },
            thumbnailWidth: 160,
            previewTemplate: '<div class="dz-preview dz-file-preview mb-3"><div class="d-flex flex-row "><div class="p-0 w-30 position-relative"><div class="dz-error-mark"><span><i></i></span></div><div class="dz-success-mark"><span><i></i></span></div><div class="preview-container"><img data-dz-thumbnail class="img-thumbnail border-0" /><i class="simple-icon-doc preview-icon" ></i></div></div><div class="pl-3 pt-2 pr-2 pb-1 w-70 dz-details position-relative"><div><span data-dz-name></span></div><div class="text-primary text-extra-small" data-dz-size /><div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div><div class="dz-error-message"><span data-dz-errormessage></span></div></div></div><a href="#/" class="remove" data-dz-remove><i class="glyph-icon simple-icon-trash"></i></a></div>'
        });
    }
    $(document).ready(function() {
        $('[name="submitFormFooter"]').on('click', function() {
            if ($('[name="formrequestdeposit"]').valid()) {
                if ($('[name="Receipt"]').val() === "") {
                    swal("Perhatian!", "Mohon lampirkan bukti transfer", "warning");
                } else {
                    $('[name="submitFormFooter"]').hide();
                    $('#btn-loading').show();
                    $.post("{{route('postdepositrequest')}}", {
                        'Amount': $('[name="Amount"]').val(),
                        'BankId': BankId, //$('[name="BankId"]').val(),
                        'Receipt': $('[name="Receipt"]').val(),
                        'Method': $('[name="Method"]').val(),
                    }, function(data, status) {
                        if (status === "success") {
                            window.location.href = "{{url('/')}}/deposit";
                        } else {
                            $('[name="submitFormFooter"]').show();
                            $('#btn-loading').hide();
                        }
                    });
                }
            } else {
                var errors = $('.error:visible');
                $(errors[0]).focus();
            }
        });
    });
</script>
@endsection