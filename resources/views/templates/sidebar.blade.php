<div class="menu">
    <div class="main-menu">
        <div class="scroll">
            <ul class="list-unstyled">
                <li class="{{Route::currentRouteName()=='dashboardProfile'||Route::currentRouteName()=='changepassword'||Route::currentRouteName()=='documents'?'active':''}}">
                    <a href="#dashboard">
                        <i class="iconsminds-monitor-3"></i>
                        <span>Dashboards</span>
                    </a>
                </li>
                <li class="{{Route::currentRouteName()=='liveaccountproduct'?'active':''}}">
                    <a href="{{route('liveaccountproduct')}}">
                        <i class="iconsminds-id-card"></i>
                        <span>Accounts</span>
                    </a>
                </li>
                <li class="{{Route::currentRouteName()=='historydeposit' || Route::currentRouteName()=='historywithdrawal'?'active':''}}">
                    <a href="#finance">
                        <i class="iconsminds-money-bag"></i>
                        <span>Finance</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <div class="sub-menu">
        <div class="scroll">
            <ul class="list-unstyled" data-link="dashboard">
                <li class="{{Route::currentRouteName()=='dashboardProfile'?'active':''}}">
                    <a href="{{route('dashboardProfile')}}">
                        <i class="simple-icon-user"></i> <span class="d-inline-block">Profile</span>
                    </a>
                </li>
                <li class="{{Route::currentRouteName()=='changepassword'?'active':''}}">
                    <a href="{{route('changepassword')}}">
                        <i class="iconsminds-lock-2" style="margin-left: -4px;"></i> <span class="d-inline-block"
                            style="margin-left: -3px;">Change password</span>
                    </a>
                </li>
                @if(env('APP_ENV')=='local')
                <li class="{{Route::currentRouteName()=='documents'?'active':''}}">
                    <a href="{{route('documents')}}">
                        <i class="simple-icon-docs"></i> <span class="d-inline-block">Documents</span>
                    </a>
                </li>
                @endif
            </ul>
            <ul class="list-unstyled" data-link="finance">
                <li class="{{Route::currentRouteName()=='historydeposit'?'active':''}}">
                    <a href="{{route('historydeposit')}}">
                        <i class="iconsminds-coins-2"></i> <span class="d-inline-block">Deposit</span>
                    </a>
                </li>
                <li class="{{Route::currentRouteName()=='historywithdrawal'?'active':''}}">
                    <a href="{{route('historywithdrawal')}}">
                        <i class="iconsminds-financial"></i> <span class="d-inline-block">Withdrawal</span>
                    </a>
                </li>
            </ul>
            <ul class="list-unstyled" data-link="trading-contest">
                <li>
                    <a href="{{route('trading-contest-register')}}">
                        <i class="simple-icon-user-follow"></i> <span class="d-inline-block">Information &
                            Register</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('trading-contest-account')}}">
                        <i class="simple-icon-organization"></i> <span class="d-inline-block">Contest Account</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('trading-contest-standing')}}">
                        <i class="simple-icon-badge"></i> <span class="d-inline-block">Standing</span>
                    </a>
                </li>
            </ul>
            <ul class="list-unstyled" data-link="poin-rewards">
                <li class="">
                    <a href="{{route('my-poin')}}">
                        <i class="simple-icon-star"></i> <span class="d-inline-block">My Poin</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('redeem')}}">
                        <i class="simple-icon-tag atau simple-icon-drawer"></i> <span
                            class="d-inline-block">Redeem</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('history')}}">
                        <i class="simple-icon-hourglass"></i> <span class="d-inline-block">History</span>
                    </a>
                </li>
            </ul>


            <!-- <ul class="list-unstyled" data-link="layouts" id="layouts">
                <li>
                    <a href="#" data-toggle="collapse" data-target="#collapseAuthorization" aria-expanded="true"
                        aria-controls="collapseAuthorization" class="rotate-arrow-icon opacity-50">
                        <i class="simple-icon-arrow-down"></i> <span class="d-inline-block">Authorization</span>
                    </a>
                    <div id="collapseAuthorization" class="collapse show">
                        <ul class="list-unstyled inner-level-menu">
                            <li>
                                <a href="Pages.Auth.Login.html">
                                    <i class="simple-icon-user-following"></i> <span
                                        class="d-inline-block">Login</span>
                                </a>
                            </li>
                            <li>
                                <a href="Pages.Auth.Register.html">
                                    <i class="simple-icon-user-follow"></i> <span
                                        class="d-inline-block">Register</span>
                                </a>
                            </li>
                            <li>
                                <a href="Pages.Auth.ForgotPassword.html">
                                    <i class="simple-icon-user-unfollow"></i> <span class="d-inline-block">Forgot
                                        Password</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <a href="#" data-toggle="collapse" data-target="#collapseProduct" aria-expanded="true"
                        aria-controls="collapseProduct" class="rotate-arrow-icon opacity-50">
                        <i class="simple-icon-arrow-down"></i> <span class="d-inline-block">Product</span>
                    </a>
                    <div id="collapseProduct" class="collapse show">
                        <ul class="list-unstyled inner-level-menu">
                            <li>
                                <a href="Pages.Product.List.html">
                                    <i class="simple-icon-credit-card"></i> <span class="d-inline-block">Data
                                        List</span>
                                </a>
                            </li>
                            <li>
                                <a href="Pages.Product.Thumbs.html">
                                    <i class="simple-icon-list"></i> <span class="d-inline-block">Thumb
                                        List</span>
                                </a>
                            </li>
                            <li>
                                <a href="Pages.Product.Images.html">
                                    <i class="simple-icon-grid"></i> <span class="d-inline-block">Image
                                        List</span>
                                </a>
                            </li>
                            <li>
                                <a href="Pages.Product.Detail.html">
                                    <i class="simple-icon-book-open"></i> <span class="d-inline-block">Detail</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <a href="#" data-toggle="collapse" data-target="#collapseProfile" aria-expanded="true"
                        aria-controls="collapseProfile" class="rotate-arrow-icon opacity-50">
                        <i class="simple-icon-arrow-down"></i> <span class="d-inline-block">Profile</span>
                    </a>
                    <div id="collapseProfile" class="collapse show">
                        <ul class="list-unstyled inner-level-menu">
                            <li>
                                <a href="Pages.Profile.Social.html">
                                    <i class="simple-icon-share"></i> <span class="d-inline-block">Social</span>
                                </a>
                            </li>
                            <li>
                                <a href="Pages.Profile.Portfolio.html">
                                    <i class="simple-icon-link"></i> <span class="d-inline-block">Portfolio</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <a href="#" data-toggle="collapse" data-target="#collapseBlog" aria-expanded="true"
                        aria-controls="collapseBlog" class="rotate-arrow-icon opacity-50">
                        <i class="simple-icon-arrow-down"></i> <span class="d-inline-block">Blog</span>
                    </a>
                    <div id="collapseBlog" class="collapse show">
                        <ul class="list-unstyled inner-level-menu">
                            <li>
                                <a href="Pages.Blog.html">
                                    <i class="simple-icon-list"></i> <span class="d-inline-block">List</span>
                                </a>
                            </li>
                            <li>
                                <a href="Pages.Blog.Detail.html">
                                    <i class="simple-icon-book-open"></i> <span class="d-inline-block">Detail</span>
                                </a>
                            </li>
                            <li>
                                <a href="Pages.Blog.Detail.Alt.html">
                                    <i class="simple-icon-picture"></i> <span class="d-inline-block">Detail
                                        Alt</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <a href="#" data-toggle="collapse" data-target="#collapseMisc" aria-expanded="true"
                        aria-controls="collapseMisc" class="rotate-arrow-icon opacity-50">
                        <i class="simple-icon-arrow-down"></i> <span class="d-inline-block">Miscellaneous</span>
                    </a>
                    <div id="collapseMisc" class="collapse show">
                        <ul class="list-unstyled inner-level-menu">
                            <li>
                                <a href="Pages.Misc.Coming.Soon.html">
                                    <i class="simple-icon-hourglass"></i> <span class="d-inline-block">Coming
                                        Soon</span>
                                </a>
                            </li>
                            <li>
                                <a href="Pages.Misc.Error.html">
                                    <i class="simple-icon-exclamation"></i> <span
                                        class="d-inline-block">Error</span>
                                </a>
                            </li>
                            <li>
                                <a href="Pages.Misc.Faq.html">
                                    <i class="simple-icon-question"></i> <span class="d-inline-block">Faq</span>
                                </a>
                            </li>
                            <li>
                                <a href="Pages.Misc.Invoice.html">
                                    <i class="simple-icon-bag"></i> <span class="d-inline-block">Invoice</span>
                                </a>
                            </li>
                            <li>
                                <a href="Pages.Misc.Knowledge.Base.html">
                                    <i class="simple-icon-graduation"></i> <span class="d-inline-block">Knowledge
                                        Base</span>
                                </a>
                            </li>
                            <li>
                                <a href="Pages.Misc.Mailing.html">
                                    <i class="simple-icon-envelope-open"></i> <span
                                        class="d-inline-block">Mailing</span>
                                </a>
                            </li>
                            <li>
                                <a href="Pages.Misc.Pricing.html">
                                    <i class="simple-icon-diamond"></i> <span class="d-inline-block">Pricing</span>
                                </a>
                            </li>
                            <li>
                                <a href="Pages.Misc.Search.html">
                                    <i class="simple-icon-magnifier"></i> <span class="d-inline-block">Search</span>
                                </a>
                            </li>

                        </ul>
                    </div>
                </li>
            </ul>
            <ul class="list-unstyled" data-link="applications">
                <li>
                    <a href="Apps.MediaLibrary.html">
                        <i class="simple-icon-picture"></i> <span class="d-inline-block">Library</span>
                    </a>
                </li>
                <li>
                    <a href="Apps.Todo.List.html">
                        <i class="simple-icon-check"></i> <span class="d-inline-block">Todo</span>
                    </a>
                </li>
                <li>
                    <a href="Apps.Survey.List.html">
                        <i class="simple-icon-calculator"></i> <span class="d-inline-block">Survey</span>
                    </a>
                </li>
                <li>
                    <a href="Apps.Chat.html">
                        <i class="simple-icon-bubbles"></i> <span class="d-inline-block">Chat</span>
                    </a>
                </li>
            </ul>
            <ul class="list-unstyled" data-link="ui">
                <li>
                    <a href="#" data-toggle="collapse" data-target="#collapseForms" aria-expanded="true"
                        aria-controls="collapseForms" class="rotate-arrow-icon opacity-50">
                        <i class="simple-icon-arrow-down"></i> <span class="d-inline-block">Forms</span>
                    </a>
                    <div id="collapseForms" class="collapse show">
                        <ul class="list-unstyled inner-level-menu">
                            <li>
                                <a href="Ui.Forms.Components.html">
                                    <i class="simple-icon-event"></i> <span class="d-inline-block">Components</span>
                                </a>
                            </li>
                            <li>
                                <a href="Ui.Forms.Layouts.html">
                                    <i class="simple-icon-doc"></i> <span class="d-inline-block">Layouts</span>
                                </a>
                            </li>
                            <li>
                                <a href="Ui.Forms.Validation.html">
                                    <i class="simple-icon-check"></i> <span class="d-inline-block">Validation</span>
                                </a>
                            </li>
                            <li>
                                <a href="Ui.Forms.Wizard.html">
                                    <i class="simple-icon-magic-wand"></i> <span
                                        class="d-inline-block">Wizard</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <a href="#" data-toggle="collapse" data-target="#collapseDataTables" aria-expanded="true"
                        aria-controls="collapseDataTables" class="rotate-arrow-icon opacity-50">
                        <i class="simple-icon-arrow-down"></i> <span class="d-inline-block">Datatables</span>
                    </a>
                    <div id="collapseDataTables" class="collapse show">
                        <ul class="list-unstyled inner-level-menu">
                            <li>
                                <a href="Ui.Datatables.Rows.html">
                                    <i class="simple-icon-screen-desktop"></i> <span class="d-inline-block">Full
                                        Page UI</span>
                                </a>
                            </li>
                            <li>
                                <a href="Ui.Datatables.Scroll.html">
                                    <i class="simple-icon-mouse"></i> <span class="d-inline-block">Scrollable</span>
                                </a>
                            </li>
                            <li>
                                <a href="Ui.Datatables.Pagination.html">
                                    <i class="simple-icon-notebook"></i> <span
                                        class="d-inline-block">Pagination</span>
                                </a>
                            </li>
                            <li>
                                <a href="Ui.Datatables.Default.html">
                                    <i class="simple-icon-grid"></i> <span class="d-inline-block">Default</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <a href="#" data-toggle="collapse" data-target="#collapseComponents" aria-expanded="true"
                        aria-controls="collapseComponents" class="rotate-arrow-icon opacity-50">
                        <i class="simple-icon-arrow-down"></i> <span class="d-inline-block">Components</span>
                    </a>
                    <div id="collapseComponents" class="collapse show">
                        <ul class="list-unstyled inner-level-menu">
                            <li>
                                <a href="Ui.Components.Alerts.html">
                                    <i class="simple-icon-bell"></i> <span class="d-inline-block">Alerts</span>
                                </a>
                            </li>
                            <li>
                                <a href="Ui.Components.Badges.html">
                                    <i class="simple-icon-badge"></i> <span class="d-inline-block">Badges</span>
                                </a>
                            </li>
                            <li>
                                <a href="Ui.Components.Buttons.html">
                                    <i class="simple-icon-control-play"></i> <span
                                        class="d-inline-block">Buttons</span>
                                </a>
                            </li>
                            <li>
                                <a href="Ui.Components.Cards.html">
                                    <i class="simple-icon-layers"></i> <span class="d-inline-block">Cards</span>
                                </a>
                            </li>

                            <li>
                                <a href="Ui.Components.Carousel.html">
                                    <i class="simple-icon-picture"></i> <span class="d-inline-block">Carousel</span>
                                </a>
                            </li>
                            <li>
                                <a href="Ui.Components.Charts.html">
                                    <i class="simple-icon-chart"></i> <span class="d-inline-block">Charts</span>
                                </a>
                            </li>
                            <li>
                                <a href="Ui.Components.Collapse.html">
                                    <i class="simple-icon-arrow-up"></i> <span
                                        class="d-inline-block">Collapse</span>
                                </a>
                            </li>
                            <li>
                                <a href="Ui.Components.Dropdowns.html">
                                    <i class="simple-icon-arrow-down"></i> <span
                                        class="d-inline-block">Dropdowns</span>
                                </a>
                            </li>
                            <li>
                                <a href="Ui.Components.Editors.html">
                                    <i class="simple-icon-book-open"></i> <span
                                        class="d-inline-block">Editors</span>
                                </a>
                            </li>
                            <li>
                                <a href="Ui.Components.Icons.html">
                                    <i class="simple-icon-star"></i> <span class="d-inline-block">Icons</span>
                                </a>
                            </li>
                            <li>
                                <a href="Ui.Components.InputGroups.html">
                                    <i class="simple-icon-note"></i> <span class="d-inline-block">Input
                                        Groups</span>
                                </a>
                            </li>
                            <li>
                                <a href="Ui.Components.Jumbotron.html">
                                    <i class="simple-icon-screen-desktop"></i> <span
                                        class="d-inline-block">Jumbotron</span>
                                </a>
                            </li>
                            <li>
                                <a href="Ui.Components.Modal.html">
                                    <i class="simple-icon-docs"></i> <span class="d-inline-block">Modal</span>
                                </a>
                            </li>
                            <li>
                                <a href="Ui.Components.Navigation.html">
                                    <i class="simple-icon-cursor"></i> <span
                                        class="d-inline-block">Navigation</span>
                                </a>
                            </li>

                            <li>
                                <a href="Ui.Components.PopoverandTooltip.html">
                                    <i class="simple-icon-pin"></i> <span class="d-inline-block">Popover &
                                        Tooltip</span>
                                </a>
                            </li>
                            <li>
                                <a href="Ui.Components.Sortable.html">
                                    <i class="simple-icon-shuffle"></i> <span class="d-inline-block">Sortable</span>
                                </a>
                            </li>
                            <li>
                                <a href="Ui.Components.Tables.html">
                                    <i class="simple-icon-grid"></i> <span class="d-inline-block">Tables</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>

            </ul>

            <ul class="list-unstyled" data-link="menu" id="menuTypes">
                <li>
                    <a href="#" data-toggle="collapse" data-target="#collapseMenuTypes" aria-expanded="true"
                        aria-controls="collapseMenuTypes" class="rotate-arrow-icon">
                        <i class="simple-icon-arrow-down"></i> <span class="d-inline-block">Menu Types</span>
                    </a>
                    <div id="collapseMenuTypes" class="collapse show" data-parent="#menuTypes">
                        <ul class="list-unstyled inner-level-menu">
                            <li>
                                <a href="Menu.Default.html">
                                    <i class="simple-icon-control-pause"></i> <span
                                        class="d-inline-block">Default</span>
                                </a>
                            </li>
                            <li>
                                <a href="Menu.Subhidden.html">
                                    <i class="simple-icon-arrow-left mi-subhidden"></i> <span
                                        class="d-inline-block">Subhidden</span>
                                </a>
                            </li>
                            <li>
                                <a href="Menu.Hidden.html">
                                    <i class="simple-icon-control-start mi-hidden"></i> <span
                                        class="d-inline-block">Hidden</span>
                                </a>
                            </li>
                            <li>
                                <a href="Menu.Mainhidden.html">
                                    <i class="simple-icon-control-rewind mi-hidden"></i> <span
                                        class="d-inline-block">Mainhidden</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <a href="#" data-toggle="collapse" data-target="#collapseMenuLevel" aria-expanded="true"
                        aria-controls="collapseMenuLevel" class="rotate-arrow-icon collapsed">
                        <i class="simple-icon-arrow-down"></i> <span class="d-inline-block">Menu Levels</span>
                    </a>
                    <div id="collapseMenuLevel" class="collapse" data-parent="#menuTypes">
                        <ul class="list-unstyled inner-level-menu">
                            <li>
                                <a href="#">
                                    <i class="simple-icon-layers"></i> <span class="d-inline-block">Sub
                                        Level</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" data-toggle="collapse" data-target="#collapseMenuLevel2"
                                    aria-expanded="true" aria-controls="collapseMenuLevel2"
                                    class="rotate-arrow-icon collapsed">
                                    <i class="simple-icon-arrow-down"></i> <span class="d-inline-block">Another
                                        Level</span>
                                </a>
                                <div id="collapseMenuLevel2" class="collapse">
                                    <ul class="list-unstyled inner-level-menu">
                                        <li>
                                            <a href="#">
                                                <i class="simple-icon-layers"></i> <span class="d-inline-block">Sub
                                                    Level</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <a href="#" data-toggle="collapse" data-target="#collapseMenuDetached" aria-expanded="true"
                        aria-controls="collapseMenuDetached" class="rotate-arrow-icon collapsed">
                        <i class="simple-icon-arrow-down"></i> <span class="d-inline-block">Detached</span>
                    </a>
                    <div id="collapseMenuDetached" class="collapse">
                        <ul class="list-unstyled inner-level-menu">
                            <li>
                                <a href="#">
                                    <i class="simple-icon-layers"></i> <span class="d-inline-block">Sub
                                        Level</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul> -->

        </div>
    </div>
</div>