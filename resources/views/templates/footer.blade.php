<footer class="page-footer">
    <div class="footer-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-sm-12">
                    <h4>Disclaimer</h4>
                    <p class="text-justify">
                        Informasi yang disediakan dalam situs web ini dimaksudkan secara eksklusif untuk tujuan informatif dan diperoleh dari sumber yang dapat dipercaya. PT. Maxco Futures tidak menjamin kelengkapan dan keakuratan informasi dan tidak akan bertanggung jawab karena menggunakan informasi yang disediakan di situs web ini.
                    </p>
                    <h4>Kebijakan Privasi</h4>
                    <p class="text-justify">
                        Untuk keperluan internal, PT. Maxco Futures memerlukan informasi pribadi bagi mereka yang mendaftar demo dan akun live Maxco. PT. Maxco Futures dan karyawan diharuskan untuk menjaga kerahasiaan informasi klien dan dilarang untuk berbagi kepada pihak ketiga. Namun, jika diwajibkan oleh hukum, PT. Maxco Futures dapat memberikan informasi kepada otoritas publik.</p>
                    <h4>PERINGATAN RISIKO TENTANG PERDANGAN</h4>
                    <p class="text-justify">Transaksi melalui margin adalah produk dengan penggunaan mekanisme leverage, memiliki resiko tinggi dan mungkin tidak cocok untuk semua orang. TIDAK ADA JAMINAN KEUNTUNGAN atas investasi Anda dan oleh karena itu berhati-hatilah terhadap pihak yang memberi jaminan keuntungan pada perdagangan. Anda disarankan utnuk tidak menggunakan dana jika Anda tidak siap menanggung kerugian. Sebelum memutuskan untuk berdagang, pastikan Anda memahami risiko yang terjadi dan pertimbangkan juga pengalaman Anda. </p>
                </div>
                <!-- <div class="col-4 col-sm-4">
                    <h4>Disclaimer</h4>
                    <p class="text-justify">
                        Informasi yang disediakan dalam situs web ini dimaksudkan secara eksklusif untuk tujuan informatif dan diperolah dari sumber yang dapad dipercaya. PT. Maxco Futures tidak menjamin kelengkapan dan keakuratan informasi dan tidak akan bertanggung jawab karena menggunakan informasi yang disediakan di situs web ini.
                    </p>
                </div>
                <div class="col-4 col-sm-4">
                    <h4>Kebijakan Privasi</h4>
                    <p class="text-justify">Untuk keperluan internal , PT. Maxco Futures memerlukan informasi pribadi bagi mereka yang mendaftar demo dan akun live Maxco. PT. Maxco Futures dan karyawan diharuskan untuk menjaga kerahasiaan informasi klien dan dilarang untuk berbagi kepada pihak ketiga. Namun, jika diwajibkan oleh hukum, PT. Maxco Futures dapat memberikan informasi kepada otoritas publik.</p>
                </div>
                <div class="col-4 col-sm-4">
                    <h4>PERINGATAN RISIKO TENTANG PERDANGAN</h4>
                    <p class="text-justify">Transaksi melalui margin adalah produk dengan penggunaan mekanisme leverage, memiliki resiko tinggi dan mungkin tidak cocok untuk semua orang. TIDAK ADA JAMINAN KEUNTUNGAN atas investasi Anda dan oleh karena itu berhati-hatilah terhadap pihak yang memberi jaminan keuntungan pada perdagangan. Anda disarankan utnuk tidak menggunakan dana jika Anda tidak siap menanggung kerugian. Sebelum memutuskan untuk berdagang, pastikan Anda memahami risiko yang terjadi dan pertimbangkan juga pengalaman Anda. </p>
                </div> -->

            </div>
            <!-- <div class="row">
                <div class="col-12 col-sm-6">
                    <p class="mb-0 text-muted">ColoredStrategies 2019</p>
                </div>
                <div class="col-sm-6 d-none d-sm-block">
                    <ul class="breadcrumb pt-0 pr-0 float-right">
                        <li class="breadcrumb-item mb-0">
                            <a href="#" class="btn-link">Docs</a>
                        </li>
                    </ul>
                </div>
            </div> -->
        </div>
    </div>
</footer>
