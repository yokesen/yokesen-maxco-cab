@extends('templates.master')

@section('csslinkonhead')
<link rel="stylesheet" href="{{url('/')}}/cabinet/css/vendor/smart_wizard.min.css" />
<link rel="stylesheet" href="{{url('/')}}/cabinet/css/vendor/dropzone.min.css" />
<meta name="csrf-token" content="{{ csrf_token() }}" />
<style>
    .form-check-input {
        margin-top: 0.7rem !important;
    }
</style>
@if($agent->isMobile())
<style>
    .sw-main.sw-theme-check>ul.step-anchor>li>a:after,
    .sw-main.sw-theme-check>ul.step-anchor>li.active>a:after,
    .sw-main.sw-main.sw-theme-check>ul.step-anchor>li.done>a:after {
        margin-top: 42px;
    }
</style>
@endif
@endsection

@section('content')
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <h1>Opening Account Wizard</h1>
                <div class="separator mb-5"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 mb-4">
                <!-- <h5 class="mb-4">Open Live Account</h5> -->
                <div class="card mb-4">
                    <div id="" class="sw-main sw-theme-check">
                        <ul class="card-header mb-4 nav nav-tabs step-anchor" style="z-index: 0;">
                            <li class="nav-item done">
                                <a class="nav-link">
                                    @if(!$agent->isMobile())
                                    Step 1
                                    <br>
                                    @endif
                                </a>
                            </li>
                            <li class="nav-item done">
                                <a href="#step-1" class="nav-link">
                                    @if(!$agent->isMobile())
                                    Step 2
                                    <br>
                                    @endif
                                </a>
                            </li>
                            <li class="nav-item done">
                                <a href="#step-2" class="nav-link">
                                    @if(!$agent->isMobile())
                                    Step 3
                                    <br>
                                    @endif
                                </a>
                            </li>
                            <li class="nav-item active">
                                <a href="#step-3" class="nav-link">
                                    @if(!$agent->isMobile())
                                    Step 4
                                    <br>
                                    @endif
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#step-4" class="nav-link">
                                    @if(!$agent->isMobile())
                                    Step 5
                                    <br>
                                    @endif
                                </a>
                            </li>
                        </ul>

                        <div class="card-body">
                            <form id="form-step-0" novalidate style="padding: 0px 22px;">
                                <input name="NBCRMWEBAPI" type="hidden" value="{{$UserToken}}" />
                                <input name="USERID" type="hidden" value="{{$UserId}}" />
                                @csrf
                                <div class="row">
                                    <div class="offset-md-1 col-md-10">
                                        <div class="alert alert-dark" role="alert">
                                            Pernyataan Bertanggung Jawab Atas Kode Akses Transaksi Nasabah
                                            (Personal
                                            Access Password) - Formulir Nomor 107.PBK.07
                                        </div>
                                        <div class="form-group">
                                            <h5>PT MAXCO FUTURES Online Trading Rules</h5>
                                            <table class="table">
                                                <tbody>
                                                    <tr>
                                                        <th>Nama Lengkap</th>
                                                        <td>{{$DataContent->full_name}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Tempat Lahir</th>
                                                        <td>{{$DataContent->place_of_birth}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Tanggal Lahir</th>
                                                        <td>{{$DataContent->date_of_birth}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Alamat</th>
                                                        <td>{{$DataContent->home_address}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Kode Pos</th>
                                                        <td>{{$DataContent->post_code}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>No. Indentitas</th>
                                                        <td>{{$DataContent->identity_number}}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>No Demo Acc.</th>
                                                        <td>{{(Session::get('user.MTUserDemo')->Login)}}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                        <div class="form-group position-relative error-l-100 text-justify">
                                            <label class="required">
                                                PERINGATAN !!! Pialang Berjangka, Wakil
                                                Pialang
                                                Berjangka,
                                                pegawai Pialang Berjangka, atau pihak yang mimiliki kepentingan
                                                dengan
                                                Pialang Berjangka dilarang menerima kode akses transaksi Nasabah
                                                (Personal
                                                Access Password).
                                                <br>
                                                <br />Dengan mengisi kolom "YA" di bawah ini, saya menyatakan bahwa
                                                saya
                                                bertanggungjawab sepenuhnya terhadap kode akses transaksi
                                                Nasabah
                                                (Personal
                                                Access Password) dan tidak menyerahkan kode akses transaksi
                                                Nasabah
                                                (Personal Access Password) ke pihak lain, terutama kepada
                                                pegawai
                                                Pialang
                                                Berjangka atau pihak yang memiliki kepentingan dengan Pialang
                                                Berjangka.
                                                <br />Demikian Pernyataan ini dibuat dengan sebenarnya dalam keadaan
                                                sadar,
                                                sehat
                                                jasmani dan rohani serta tanpa paksaan apapun dari pihak manapun.
                                            </label>
                                            <div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="pap_agreement" value="yes" onchange="savekyc()" onkeypress="savekyc()" onclick="savekyc()" @isset($DataContent->pap_agreement) @if($DataContent->pap_agreement==='yes') checked @endif @endisset/>
                                                    <label class="form-check-label required">Ya</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="pap_agreement" value="no" onchange="savekyc()" onkeypress="savekyc()" onclick="savekyc()" @isset($DataContent->pap_agreement) @if($DataContent->pap_agreement==='no') checked @endif @endisset/>
                                                    <label class="form-check-label" style="margin-top: 0.7rem !important;">Tidak</label>
                                                </div><br>
                                                <label id="pap_agreement-error" class="error" for="pap_agreement" style="display: none;">Klik "Ya" untuk melanjutkan.</label>
                                            </div>
                                            <label>Menyatakan pada tanggal : {{date('d-m-Y',strtotime(date("m.d.y")))}}</label>
                                        </div>



                                    </div>
                                </div>
                                <div class="btn-toolbar custom-toolbar text-center card-body pt-0 mb-4 justify-content-md-center">
                                    <button class="btn btn-secondary prev-btn" type="button">Previous</button>
                                    <button onclick="event.preventDefault();" class="btn-loading-prev btn btn-secondary hidden">
                                        <span class="loader-small" style="position: unset !important;"></span> Previous
                                    </button>
                                    <input class="btn btn-secondary next-btn" type="submit" value="Next" />
                                    <button onclick="event.preventDefault();" class="btn-loading-next btn btn-secondary hidden">
                                        <span class="loader-small" style="position: unset !important;"></span> Next
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection

@section('jsonpage')
<script src="{{url('/')}}/cabinet/js/vendor/jquery.smartWizard.min.js"></script>
<script src="{{url('/')}}/cabinet/js/vendor/dropzone.min.js"></script>
<script src="{{url('/')}}/cabinet/js/vendor/sweetalert.min.js"></script>

<script>
    $('[name="pap_agreement"]').on('change', function(event) {
        var val = event.currentTarget.value;
        if (val === 'no') {
            $('#pap_agreement-error').show();
            $('#pap_agreement-error').text('Klik "Ya" untuk melanjutkan.')
        } else {
            $('#pap_agreement-error').hide();

        }
    });

    function getContent() {
        var contentDt = {
            'job_details': {
                'more_details': {
                    'length_of_work': "{{isset($DataContent->job_details) && !empty($DataContent->job_details->more_details->length_of_work)?$DataContent->job_details->more_details->length_of_work:''}}",
                    'previous_employer': "{{isset($DataContent->job_details) && !empty($DataContent->job_details->more_details->previous_employer)?$DataContent->job_details->more_details->previous_employer:0}}",
                    'employer_name': "{{isset($DataContent->job_details) && !empty($DataContent->job_details->more_details->employer_name)?$DataContent->job_details->more_details->employer_name:''}}",
                    'business_fields': "{{isset($DataContent->job_details) && !empty($DataContent->job_details->more_details->business_fields)?$DataContent->job_details->more_details->business_fields:''}}",
                    'title': "{{isset($DataContent->job_details) && !empty($DataContent->job_details->more_details->title)?$DataContent->job_details->more_details->title:''}}",
                    'office_address': "{{isset($DataContent->job_details) && !empty($DataContent->job_details->more_details->office_address)?$DataContent->job_details->more_details->office_address:''}}",
                    'office_tele': "{{isset($DataContent->job_details) && !empty($DataContent->job_details->more_details->office_tele)?$DataContent->job_details->more_details->office_tele:''}}",
                    'office_fax': 0
                },
                'position': {
                    'value': "{{isset($DataContent->job_details)&&!empty($DataContent->job_details->position->value)?$DataContent->job_details->position->value:''}}",
                    'input': "{{isset($DataContent->job_details)&&!empty($DataContent->job_details->position->input)?$DataContent->job_details->position->input:''}}",
                }
            },
            'wealth_list': {
                'income_per_year': "{{isset($DataContent->wealth_list)&&!empty($DataContent->wealth_list->income_per_year)?$DataContent->wealth_list->income_per_year:''}}",
                'residence_location': "{{isset($DataContent->wealth_list)&&!empty($DataContent->wealth_list->residence_location)?$DataContent->wealth_list->residence_location:''}}",
                'tax_object_selling_value': "{{isset($DataContent->wealth_list)&&!empty($DataContent->wealth_list->tax_object_selling_value)?$DataContent->wealth_list->tax_object_selling_value:''}}",
                'bank_time_deposit_value': "{{isset($DataContent->wealth_list)&&!empty($DataContent->wealth_list->bank_time_deposit_value)?$DataContent->wealth_list->bank_time_deposit_value:''}}",
                'bank_saving_value': "{{isset($DataContent->wealth_list)&&!empty($DataContent->wealth_list->bank_saving_value)?$DataContent->wealth_list->bank_saving_value:''}}",
                'others': "{{isset($DataContent->wealth_list)&&!empty($DataContent->wealth_list->others)?$DataContent->wealth_list->others:''}}",
            },
            'bank_details_for_withdrawls': {
                'full_name': "{{isset($DataContent->bank_details_for_withdrawls)&&!empty($DataContent->bank_details_for_withdrawls->full_name)?$DataContent->bank_details_for_withdrawls->full_name:''}}",
                'branch': "{{isset($DataContent->bank_details_for_withdrawls)&&!empty($DataContent->bank_details_for_withdrawls->branch)?$DataContent->bank_details_for_withdrawls->branch:''}}",
                'account_number': "{{isset($DataContent->bank_details_for_withdrawls)&&!empty($DataContent->bank_details_for_withdrawls->account_number)?$DataContent->bank_details_for_withdrawls->account_number:''}}",
                'phone_number': 0,
                'account_type': {
                    'value': "{{isset($DataContent->bank_details_for_withdrawls)&&!empty($DataContent->bank_details_for_withdrawls->account_type->value)?$DataContent->bank_details_for_withdrawls->account_type->value:''}}",
                    'input': "{{isset($DataContent->bank_details_for_withdrawls)&&!empty($DataContent->bank_details_for_withdrawls->account_type->input)?$DataContent->bank_details_for_withdrawls->account_type->input:''}}",
                }
            },
            'emergency_contact_data': {
                'name': "{{isset($DataContent->emergency_contact_data)&&!empty($DataContent->emergency_contact_data->name)?$DataContent->emergency_contact_data->name:''}}",
                'address': "{{isset($DataContent->emergency_contact_data)&&!empty($DataContent->emergency_contact_data->address)?$DataContent->emergency_contact_data->address:''}}",
                'post_code': "{{isset($DataContent->emergency_contact_data)&&!empty($DataContent->emergency_contact_data->post_code)?$DataContent->emergency_contact_data->post_code:''}}",
                'home_phone_number': "{{isset($DataContent->emergency_contact_data)&&!empty($DataContent->emergency_contact_data->home_phone_number)?$DataContent->emergency_contact_data->home_phone_number:''}}",
                'relationship_with_the_contacts': "{{isset($DataContent->emergency_contact_data)&&!empty($DataContent->emergency_contact_data->relationship_with_the_contacts)?$DataContent->emergency_contact_data->relationship_with_the_contacts:''}}"
            },
            'attached_documents': {
                'id_card': true,
                'coloured_photo': true,
                'specimen_signature': true,
                'other_count': 1,
                'required_image': [{
                        'type': 'id_card',
                        'title': 'id_card_or_driving_license_passport',
                        'action_type': 0,
                        'src': {
                            'name': "{{!empty($DataContent->attached_documents)?$DataContent->attached_documents->required_image[0]->src->name:''}}",
                            'size': 116,
                            'url': "{{!empty($DataContent->attached_documents)?$DataContent->attached_documents->required_image[0]->src->url:''}}",
                        }
                    },
                    {
                        'type': 'coloured_photo',
                        'title': 'coloured_photo',
                        'action_type': 0,
                        'src': {
                            'name': "{{!empty($DataContent->attached_documents)?$DataContent->attached_documents->required_image[1]->src->name:''}}",
                            'size': 116,
                            'url': "{{!empty($DataContent->attached_documents)?$DataContent->attached_documents->required_image[1]->src->url:''}}",
                        }
                    },
                    {
                        'type': 'specimen_signature',
                        'title': 'specimen_signature',
                        'action_type': 0,
                        'src': {
                            'name': "{{!empty($DataContent->attached_documents)?$DataContent->attached_documents->required_image[2]->src->name:''}}",
                            'size': 116,
                            'url': "{{!empty($DataContent->attached_documents)?$DataContent->attached_documents->required_image[2]->src->url:''}}",
                        }
                    },
                    {
                        'type': 'other_image',
                        'title': 'other_image',
                        'action_type': 0,
                        'src': {
                            'name': "{{!empty($DataContent->attached_documents)?$DataContent->attached_documents->required_image[3]->src->name:''}}",
                            'size': 116,
                            'url': "{{!empty($DataContent->attached_documents)?$DataContent->attached_documents->required_image[3]->src->url:''}}",
                        }
                    }
                ],
                'option': 'yes'
            },
            'trading_statement': {
                'statement_for_trading_simulation_on_demo_account': "{{isset($DataContent->trading_statement)&&!empty($DataContent->trading_statement->statement_for_trading_simulation_on_demo_account)?$DataContent->trading_statement->statement_for_trading_simulation_on_demo_account:''}}",
                'statement_letter_has_been_experienced_on_trading': "{{isset($DataContent->trading_statement)&&!empty($DataContent->trading_statement->statement_letter_has_been_experienced_on_trading)?$DataContent->trading_statement->statement_letter_has_been_experienced_on_trading:''}}",
                'statement_has_been_received_information_futures_company': "{{isset($DataContent->trading_statement)&&!empty($DataContent->trading_statement->statement_has_been_received_information_futures_company)?$DataContent->trading_statement->statement_has_been_received_information_futures_company:''}}",
            },
            'risk_document': {
                'risk_1_title_en_ref': "{{isset($DataContent->risk_document)&&!empty($DataContent->risk_document->risk_1_title_en_ref)?$DataContent->risk_document->risk_1_title_en_ref:''}}",
                'risk_2_title_en_ref': "{{isset($DataContent->risk_document)&&!empty($DataContent->risk_document->risk_2_title_en_ref)?$DataContent->risk_document->risk_2_title_en_ref:''}}",
                'risk_3_title_en_ref': "{{isset($DataContent->risk_document)&&!empty($DataContent->risk_document->risk_3_title_en_ref)?$DataContent->risk_document->risk_3_title_en_ref:''}}",
                'risk_4_title_en_ref': "{{isset($DataContent->risk_document)&&!empty($DataContent->risk_document->risk_4_title_en_ref)?$DataContent->risk_document->risk_4_title_en_ref:''}}",
                'risk_5_title_en_ref': "{{isset($DataContent->risk_document)&&!empty($DataContent->risk_document->risk_5_title_en_ref)?$DataContent->risk_document->risk_5_title_en_ref:''}}",
                'risk_6_title_en_ref': "{{isset($DataContent->risk_document)&&!empty($DataContent->risk_document->risk_6_title_en_ref)?$DataContent->risk_document->risk_6_title_en_ref:''}}",
                'risk_7_title_en_ref': "{{isset($DataContent->risk_document)&&!empty($DataContent->risk_document->risk_7_title_en_ref)?$DataContent->risk_document->risk_7_title_en_ref:''}}",
                'risk_8_title_en_ref': "{{isset($DataContent->risk_document)&&!empty($DataContent->risk_document->risk_8_title_en_ref)?$DataContent->risk_document->risk_8_title_en_ref:''}}",
                'risk_9_title_en_ref': "{{isset($DataContent->risk_document)&&!empty($DataContent->risk_document->risk_9_title_en_ref)?$DataContent->risk_document->risk_9_title_en_ref:''}}",
                'risk_10_title_en_ref': "{{isset($DataContent->risk_document)&&!empty($DataContent->risk_document->risk_10_title_en_ref)?$DataContent->risk_document->risk_10_title_en_ref:''}}",
                'risk_11_title_en_ref': "{{isset($DataContent->risk_document)&&!empty($DataContent->risk_document->risk_11_title_en_ref)?$DataContent->risk_document->risk_11_title_en_ref:''}}",
                'risk_12_title_en_ref': "{{isset($DataContent->risk_document)&&!empty($DataContent->risk_document->risk_12_title_en_ref)?$DataContent->risk_document->risk_12_title_en_ref:''}}",
                'risk_13_title_en_ref': "{{isset($DataContent->risk_document)&&!empty($DataContent->risk_document->risk_13_title_en_ref)?$DataContent->risk_document->risk_13_title_en_ref:''}}",
                'radio': "{{isset($DataContent->risk_document)&&!empty($DataContent->risk_document->radio)?$DataContent->risk_document->radio:''}}",
            },
            'trading_condition': {
                'trading_conditions_1_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_1_title_en_ref)?$DataContent->trading_condition->trading_conditions_1_title_en_ref:''}}",
                'trading_conditions_2_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_2_title_en_ref)?$DataContent->trading_condition->trading_conditions_2_title_en_ref:''}}",
                'trading_conditions_3_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_3_title_en_ref)?$DataContent->trading_condition->trading_conditions_3_title_en_ref:''}}",
                'trading_conditions_4_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_4_title_en_ref)?$DataContent->trading_condition->trading_conditions_4_title_en_ref:''}}",
                'trading_conditions_5_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_5_title_en_ref)?$DataContent->trading_condition->trading_conditions_5_title_en_ref:''}}",
                'trading_conditions_6_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_6_title_en_ref)?$DataContent->trading_condition->trading_conditions_6_title_en_ref:''}}",
                'trading_conditions_7_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_7_title_en_ref)?$DataContent->trading_condition->trading_conditions_7_title_en_ref:''}}",
                'trading_conditions_8_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_8_title_en_ref)?$DataContent->trading_condition->trading_conditions_8_title_en_ref:''}}",
                'trading_conditions_9_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_9_title_en_ref)?$DataContent->trading_condition->trading_conditions_9_title_en_ref:''}}",
                'trading_conditions_10_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_10_title_en_ref)?$DataContent->trading_condition->trading_conditions_10_title_en_ref:''}}",
                'trading_conditions_11_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_11_title_en_ref)?$DataContent->trading_condition->trading_conditions_11_title_en_ref:''}}",
                'trading_conditions_12_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_12_title_en_ref)?$DataContent->trading_condition->trading_conditions_12_title_en_ref:''}}",
                'trading_conditions_13_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_13_title_en_ref)?$DataContent->trading_condition->trading_conditions_13_title_en_ref:''}}",
                'trading_conditions_14_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_14_title_en_ref)?$DataContent->trading_condition->trading_conditions_14_title_en_ref:''}}",
                'trading_conditions_15_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_15_title_en_ref)?$DataContent->trading_condition->trading_conditions_15_title_en_ref:''}}",
                'trading_conditions_16_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_16_title_en_ref)?$DataContent->trading_condition->trading_conditions_16_title_en_ref:''}}",
                'trading_conditions_17_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_17_title_en_ref)?$DataContent->trading_condition->trading_conditions_17_title_en_ref:''}}",
                'trading_conditions_18_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_18_title_en_ref)?$DataContent->trading_condition->trading_conditions_18_title_en_ref:''}}",
                'trading_conditions_19_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_19_title_en_ref)?$DataContent->trading_condition->trading_conditions_19_title_en_ref:''}}",
                'trading_conditions_20_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_20_title_en_ref)?$DataContent->trading_condition->trading_conditions_20_title_en_ref:''}}",
                'trading_conditions_21_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_21_title_en_ref)?$DataContent->trading_condition->trading_conditions_21_title_en_ref:''}}",
                'trading_conditions_22_title_1': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_22_title_1)?$DataContent->trading_condition->trading_conditions_22_title_1:'0'}}",
                'trading_conditions_22_title_2': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_22_title_2)?$DataContent->trading_condition->trading_conditions_22_title_2:'0'}}",
                'trading_conditions_22_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_22_title_en_ref)?$DataContent->trading_condition->trading_conditions_22_title_en_ref:''}}",
                'trading_conditions_23_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_23_title_en_ref)?$DataContent->trading_condition->trading_conditions_23_title_en_ref:''}}",
                'step_4_footer': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->step_4_footer)?$DataContent->trading_condition->step_4_footer:''}}",
            },
            'pap_agreement': $($("input[name='pap_agreement']:checked")).val() || '',
            'truth_resposibility_agreement': "{{!empty($DataContent->truth_resposibility_agreement)?$DataContent->truth_resposibility_agreement:''}}",
            'demoAccount': "{{(Session::get('user.MTUserDemo')->Login)}}",
            'full_name': "{{!empty($DataContent->full_name)?$DataContent->full_name:''}}",
            'identity_number': "{{!empty($DataContent->identity_number)?$DataContent->identity_number:''}}",
            'future_contract': {
                'value': "{{isset($DataContent->future_contract)&&!empty($DataContent->future_contract->value)?$DataContent->future_contract->value:''}}",
                'input': "{{isset($DataContent->future_contract)&&!empty($DataContent->future_contract->input)?$DataContent->future_contract->input:''}}",
            },
            'account_type': "{{!empty($DataContent->account_type)?$DataContent->account_type:''}}",
            'place_of_birth': "{{!empty($DataContent->place_of_birth)?$DataContent->place_of_birth:''}}",
            'date_of_birth': "{{!empty($DataContent->date_of_birth)?$DataContent->date_of_birth:''}}",
            'post_code': "{{!empty($DataContent->post_code)?$DataContent->post_code:''}}",
            'page': "{{!empty($DataContent->page)?$DataContent->page:0}}",
            'activeStep': "{{!empty($DataContent->activeStep)?$DataContent->activeStep:''}}",
            'tax_file_number': "{{!empty($DataContent->tax_file_number)?$DataContent->tax_file_number:''}}",
            'mother_maiden_name': "{{!empty($DataContent->mother_maiden_name)?$DataContent->mother_maiden_name:''}}",
            'status': "{{!empty($DataContent->status)?$DataContent->status:''}}",
            'gender': "{{!empty($DataContent->gender)?$DataContent->gender:''}}",
            'home_address': "{{!empty($DataContent->home_address)?$DataContent->home_address:''}}",
            'home_ownership_status': {
                'value': "{{isset($DataContent->home_ownership_status)&&!empty($DataContent->home_ownership_status->value)?$DataContent->home_ownership_status->value:''}}",
                'input': "{{isset($DataContent->home_ownership_status)&&!empty($DataContent->home_ownership_status->input)?$DataContent->home_ownership_status->input:''}}"
            },
            'openint_account_purpose': {
                'value': "{{isset($DataContent->openint_account_purpose)&&!empty($DataContent->openint_account_purpose->value)?$DataContent->openint_account_purpose->value:''}}",
                'input': "{{isset($DataContent->openint_account_purpose)&&!empty($DataContent->openint_account_purpose->input)?$DataContent->openint_account_purpose->input:''}}"
            },
            'experince_on_investment': {
                'value': "{{isset($DataContent->experince_on_investment)&&!empty($DataContent->experince_on_investment->value)?$DataContent->experince_on_investment->value:''}}",
                'input': "{{isset($DataContent->experince_on_investment)&&!empty($DataContent->experince_on_investment->input)?$DataContent->experince_on_investment->input:''}}"
            },
            'is_family_work_in_related_company': {
                'value': "{{isset($DataContent->is_family_work_in_related_company)&&!empty($DataContent->is_family_work_in_related_company->value)?$DataContent->is_family_work_in_related_company->value:''}}",
                'input': "{{isset($DataContent->is_family_work_in_related_company)&&!empty($DataContent->is_family_work_in_related_company->input)?$DataContent->is_family_work_in_related_company->input:''}}"
            },
            'is_declared_bankrupt': {
                'value': "{{isset($DataContent->is_declared_bankrupt)&&!empty($DataContent->is_declared_bankrupt->value)?$DataContent->is_declared_bankrupt->value:''}}",
                'input': "{{isset($DataContent->is_declared_bankrupt)&&!empty($DataContent->is_declared_bankrupt->input)?$DataContent->is_declared_bankrupt->input:''}}"
            },
            'trading_rules': "{{!empty($DataContent->trading_rules)?$DataContent->trading_rules:''}}",
            'ae_name_or_branch': "{{!empty($DataContent->ae_name_or_branch)?$DataContent->ae_name_or_branch:''}}",
            'husband_wife_spouse': "{{!empty($DataContent->husband_wife_spouse)?$DataContent->husband_wife_spouse:''}}",
            'home_phone': "{{!empty($DataContent->home_phone)?$DataContent->home_phone:''}}",
            'home_fax': 0,
            'statement_data_correctly': "{{!empty($DataContent->statement_data_correctly)?$DataContent->statement_data_correctly:''}}",
            'KycStatus': 0
        };
        return contentDt;
    }

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function savekyc() {
        var contentDt = getContent();
        if (contentDt.wealth_list.income_per_year === "&gt;500") {
            contentDt.wealth_list.income_per_year = ">500";
        }
        const content = JSON.stringify(contentDt)
        $.post("{{route('savekyc')}}", {
            content,
            NBCRMWEBAPI: $('[name="NBCRMWEBAPI"]').val(),
            USERID: $('[name="USERID"]').val(),
        }, function(data, status) {
            var {
                Code,
                Message
            } = data;
            if (data.Data) {
                var Data = data.Data;
                var MTUserRef = Data.MTUserRef;
                var idx = MTUserRef.findIndex(x => x.IsSimulate === false);
                if (MTUserRef[idx]) {
                    var liveAcc = MTUserRef[idx];
                    $('[data-bind="ac_live_no"]').text(liveAcc.Login);
                }
            }
        });
    };
    $('.prev-btn').on('click', function() {
        $('.prev-btn').hide();
        $('.btn-loading-prev').show();
        $.post("{{route('postNextStepOpenLiveAccount')}}", {
            NBCRMWEBAPI: $('[name="NBCRMWEBAPI"]').val(),
            USERID: $('[name="USERID"]').val(),
            nextStep: '4',
            page: '2'
        }, function(data, status) {
            if (status === "success") {
                location.reload();
            } else {
                $('.prev-btn').show();
                $('.btn-loading-prev').hide();
            }
        });
    });
    // });
    $(function() {
        $("#form-step-0").validate({
            ignore: [],
            // errorElement: "div",
            rules: {
                pap_agreement: {
                    required: true
                },
            },
            messages: {
                pap_agreement: 'Klik "Ya" untuk melanjutkan',
            },
            submitHandler: function() {
                var val = $($("input[name='pap_agreement']:checked")).val() || '';
                if (val === "yes") {
                    $('.next-btn').hide();
                    $('.btn-loading-next').show();
                    var contentDt = getContent();
                    const content = JSON.stringify(contentDt)
                    $.post("{{route('postNextStepOpenLiveAccount')}}", {
                        NBCRMWEBAPI: $('[name="NBCRMWEBAPI"]').val(),
                        USERID: $('[name="USERID"]').val(),
                        nextStep: '5',
                        page: '0'
                    }, function(data, status) {
                        if (status === "success") {
                            location.reload();
                        } else {
                            $('.next-btn').show();
                            $('.btn-loading-next').hide();
                        }
                    });
                } else {
                    $('#pap_agreement-error').show();
                    $('#pap_agreement-error').text('Klik "Ya" untuk melanjutkan.')
                }
            }
        });
    });
</script>
@endsection