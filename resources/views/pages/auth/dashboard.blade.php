@extends('templates.master')

@section('content')
<main id="app">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <h1>Dashboard</h1>
                <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                    <ol class="breadcrumb pt-0">
                        <!-- <li class="breadcrumb-item">
                            <a href="#">Home</a>
                        </li> -->
                        <li class="breadcrumb-item">
                            <a href="#">Library</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Data</li>
                    </ol>
                </nav>
                <div class="separator mb-5"></div>
            </div>
        </div>
        <div class="row">
          <div class="col-md-12 col-lg-12 col-xl-12 mb-4">
            <div style="background:url({{url('/')}}/cabinet/img/webpage/whymaxco1.png);    background-size: cover;
    min-height: 650px;
    max-height: 650px;
    background-position: 50% 50%;
    background-repeat: no-repeat;">
            </div>
              <!-- <img src=""></img> -->
          </div>
        </div>
        <!-- <div class="row">
           <div class="col-md-12 col-lg-6 col-xl-4 mb-4">
               <div class="card h-100">
                   <div class="card-body">
                       <h5 class="card-title">Profile Meter</h5>
                       <div class="dashboard-donut-chart chart">
                           <canvas id="categoryChart"></canvas>
                       </div>
                   </div>
               </div>
           </div>
           <div class="col-md-12 col-lg-6 col-xl-4 mb-4">
               <div class="card dashboard-progress">
                   <div class="position-absolute card-top-buttons">
                       <button class="btn btn-header-light icon-button">
                           <i class="simple-icon-refresh"></i>
                       </button>
                   </div>
                   <div class="card-body">
                       <h5 class="card-title">Profile Status</h5>
                       <div class="mb-4">
                           <p class="mb-2">Maxco Mobile Easy to Use
                               <span class="float-right text-muted">12/18</span>
                           </p>
                           <div class="progress">
                               <div class="progress-bar" role="progressbar" aria-valuenow="75" aria-valuemin="0"
                                   aria-valuemax="100"></div>
                           </div>
                       </div>

                       <div class="mb-4">
                           <p class="mb-2">Basic Information
                               <span class="float-right text-muted">1/8</span>
                           </p>
                           <div class="progress">
                               <div class="progress-bar" role="progressbar" aria-valuenow="15" aria-valuemin="0"
                                   aria-valuemax="100"></div>
                           </div>
                       </div>

                       <div class="mb-4">
                           <p class="mb-2">Occupation
                               <span class="float-right text-muted">2/6</span>
                           </p>
                           <div class="progress">
                               <div class="progress-bar" role="progressbar" aria-valuenow="20" aria-valuemin="0"
                                   aria-valuemax="100"></div>
                           </div>
                       </div>

                       <div class="mb-4">
                           <p class="mb-2">Bank Account
                               <span class="float-right text-muted">0/8</span>
                           </p>
                           <div class="progress">
                               <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0"
                                   aria-valuemax="100"></div>
                           </div>
                       </div>

                       <div class="mb-4">
                           <p class="mb-2">Legal Document
                               <span class="float-right text-muted">1/2</span>
                           </p>
                           <div class="progress">
                               <div class="progress-bar" role="progressbar" aria-valuenow="50" aria-valuemin="0"
                                   aria-valuemax="100"></div>
                           </div>
                       </div>

                   </div>
               </div>
           </div>
           <div class="col-md-12 col-lg-12 col-xl-4">
               <div class="row">
                   <div class="col-6 mb-4">
                       <div class="card dashboard-small-chart-analytics">
                           <div class="card-body">
                               <p class="lead color-theme-1 mb-1 value"></p>
                               <p class="mb-0 label text-small"></p>
                               <div class="chart">
                                   <canvas id="smallChart1"></canvas>
                               </div>
                           </div>
                       </div>
                   </div>
                   <div class="col-6 mb-4">
                       <div class="card dashboard-small-chart-analytics">
                           <div class="card-body">
                               <p class="lead color-theme-1 mb-1 value"></p>
                               <p class="mb-0 label text-small"></p>
                               <div class="chart">
                                   <canvas id="smallChart2"></canvas>
                               </div>
                           </div>

                       </div>
                   </div>
                   <div class="col-6 mb-4">
                       <div class="card dashboard-small-chart-analytics">
                           <div class="card-body">
                               <p class="lead color-theme-1 mb-1 value"></p>
                               <p class="mb-0 label text-small"></p>
                               <div class="chart">
                                   <canvas id="smallChart3"></canvas>
                               </div>
                           </div>
                       </div>
                   </div>
                   <div class="col-6 mb-4">
                       <div class="card dashboard-small-chart-analytics">
                           <div class="card-body">
                               <p class="lead color-theme-1 mb-1 value"></p>
                               <p class="mb-0 label text-small"></p>
                               <div class="chart">
                                   <canvas id="smallChart4"></canvas>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>

           </div>

       </div> -->

    </div>
</main>
@endsection
