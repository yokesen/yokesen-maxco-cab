<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use DB;
use Session;
use Jenssegers\Agent\Agent;
use Alert;
use Cookie;

class PageController extends Controller
{

    public function register()
    {
        return view('pages.auth.register');
    }

    public function login()
    {
        $agent = new Agent();
        return view('pages.auth.login', compact('agent'));
    }

    public function loginfromweb(Request $request)
    {
        if (!empty($request->query('webtoken'))) {
            Cookie::queue('NBCRMWEBAPI.SESSION', 'NBCRMWEBAPI.SESSION='.$request->query('webtoken'));
            Cookie::queue('__cfduid', $request->query('cfduid'));
            Session::put('user.token','NBCRMWEBAPI.SESSION', 'NBCRMWEBAPI.SESSION='.$request->query('webtoken'));
            $api_url = env('API_URL');
            $url = $api_url . 'api/account/Detail';
            $client = new Client();
            $account_detail = $client->post($url, [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Language' => 'en',
                    'Cookie' => 'NBCRMWEBAPI.SESSION='.$request->query('webtoken')
                ],
                'json' => [
                    'IsLoadComm' => false,
                    'LoadStatistics' => true,
                ]
            ]);
            $accounts = json_decode($account_detail->getBody());
            $Data = $accounts->Data;
            $User = $Data->User;
            $UserBankCard = $Data->UserBankCard;
            $IdCard = $Data->IdCard;

            if (count($UserBankCard) > 0) {
                Session::put('userbank.BankName', $UserBankCard[0]->BankName);
                Session::put('userbank.AccountNo', $UserBankCard[0]->AccountNo);
                Session::put('userbank.AccountName', $UserBankCard[0]->AccountName);
                Session::put('userbank.BranchName', $UserBankCard[0]->BranchName);
            }
            if (isset($accounts->Data->User->Summary)) {
                Session::put('user.Summary', $accounts->Data->User->Summary);
            }
            return view('pages.auth.profile', compact('User', 'UserBankCard', 'IdCard'));
        } else {
            return redirect()->route('login');
        }
    }
    // public function loginfromweb(Request $request)
    // {
    //     if (!empty($request->query('webtoken'))) {
    //         Cookie::queue('NBCRMWEBAPI.SESSION', 'NBCRMWEBAPI.SESSION=' . $request->query('webtoken'));
    //         Cookie::queue('__cfduid', $request->query('cfduid'));
    //         Session::put('user.token', 'NBCRMWEBAPI.SESSION', 'NBCRMWEBAPI.SESSION=' . $request->query('webtoken'));
    //         $api_url = env('API_URL');
    //         $url = $api_url . 'api/account/Detail';
    //         $client = new Client();
    //         $account_detail = $client->post($url, [
    //             'headers' => [
    //                 'Content-Type' => 'application/json',
    //                 'Language' => 'en',
    //                 'Cookie' => 'NBCRMWEBAPI.SESSION=' . $request->query('webtoken')
    //             ],
    //             'json' => [
    //                 'IsLoadComm' => false,
    //                 'LoadStatistics' => true,
    //             ]
    //         ]);
    //         $accounts = json_decode($account_detail->getBody());
    //         Session::put('user.id', $accounts->Data->User->UserId);
    //         Session::put('user.parent', $accounts->Data->User->ParentId);
    //         Session::put('user.type', $accounts->Data->User->UserType);
    //         Session::put('user.email', $accounts->Data->User->Email);
    //         Session::put('user.mobile', $accounts->Data->User->Mobile);
    //         Session::put('user.gender', $accounts->Data->User->Gender);
    //         Session::put('user.name', $accounts->Data->IdCard->RealName);
    //         Session::put('user.IDNO', $accounts->Data->IdCard->IDNO);
    //         Session::put('user.PlaceOfBirth', $accounts->Data->User->PlaceOfBirth);
    //         Session::put('user.DateOfBirth', $accounts->Data->User->DateOfBirth);
    //         Session::put('user.Address', $accounts->Data->User->Address);
    //         Session::put('user.KycStatus', $accounts->Data->User->KycStatus);
    //         Session::put('user.Currency', $accounts->Data->User->Currency);
    //         if (isset($accounts->Data->User->Summary)) {
    //             Session::put('user.Summary', $accounts->Data->User->Summary);
    //         }
    //         $findIndexLiveAcc = array_search(false, array_column($accounts->Data->MTUserRef, 'IsSimulate'));
    //         $findIndexDemoAcc = array_search(true, array_column($accounts->Data->MTUserRef, 'IsSimulate'));

    //         Session::put('user.MTUserRefObj', $accounts->Data->MTUserRef[0]);
    //         Session::put('user.MTUserLive', $accounts->Data->MTUserRef[$findIndexLiveAcc]);
    //         Session::put('user.MTUserDemo', $accounts->Data->MTUserRef[$findIndexDemoAcc]);


    //         $Data = $accounts->Data;
    //         $User = $Data->User;
    //         $UserBankCard = $Data->UserBankCard;
    //         $IdCard = $Data->IdCard;

    //         if (count($UserBankCard) > 0) {
    //             Session::put('userbank.BankName', $UserBankCard[0]->BankName);
    //             Session::put('userbank.AccountNo', $UserBankCard[0]->AccountNo);
    //             Session::put('userbank.AccountName', $UserBankCard[0]->AccountName);
    //             Session::put('userbank.BranchName', $UserBankCard[0]->BranchName);
    //         }
    //         if (isset($accounts->Data->User->Summary)) {
    //             Session::put('user.Summary', $accounts->Data->User->Summary);
    //         }
    //         return view('pages.auth.profile', compact('User', 'UserBankCard', 'IdCard'));
    //         // return redirect()->route('dashboardProfile');
    //     } else {
    //         return redirect()->route('login');
    //     }
    // }

    public function signout()
    {
        return redirect()->route('login');
    }
    public function changepassword()
    {
        return view('pages.auth.changepassword');
    }
    public function dashboard()
    {
        return view('pages.auth.dashboard');
    }
    public function session()
    {
        dd(Session::all());
    }
    public function redirect(Request $request)
    {
        $pageredirect = $request->pageurl;
        // dd($request);
        return view('pages.redirect', compact('pageredirect'));
    }
    public function withdrawalrequest()
    {
        $api_url = env('API_URL');
        $url = $api_url . 'api/account/Detail';
        $client = new Client();
        $account_detail = $client->post($url, [
            'headers' => [
                'Content-Type' => 'application/json',
                'Language' => 'en',
                'Cookie' => Session::get('user.token')
            ],
            'json' => [
                'IsLoadComm' => false,
                'LoadStatistics' => true,
            ]
        ]);
        $accounts = json_decode($account_detail->getBody());
        if (isset($accounts->Data->User->Summary)) {
            Session::put('user.Summary', $accounts->Data->User->Summary);
        }

        $UserBankCardLast = [];
        if (count($accounts->Data->UserBankCard) > 0) {
            $UserBankCardLast = $accounts->Data->UserBankCard[0];
        }

        $url_usersbalance = $api_url . "api/bill/usersbalance";
        $usersbalance_detail = $client->post($url_usersbalance, [
            'headers' => [
                'Content-Type' => 'application/json',
                'Language' => 'en',
                'Cookie' => Session::get('user.token')
            ],
            'json' => [
                'MType' => 4,
                "IsSimulate" => false
            ]
        ]);

        $usersbalance = json_decode($usersbalance_detail->getBody());

        if ($usersbalance->Code == 0) {
            $databalacelive = [];
            foreach ($usersbalance->Data as $data) {
                if ($data->IsSimulate == false) {
                    $databalacelive = $data;
                }
            }
            return view('pages.auth.withdrawalrequest', compact('databalacelive', 'UserBankCardLast'));
        } else {
            alert()->error('Perhatian !', $usersbalance->Message);
            return redirect()->back();
        }
    }
    public function forgotpassword()
    {
        $agent = new Agent();
        return view('pages.auth.forgotpassword', compact('agent'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
