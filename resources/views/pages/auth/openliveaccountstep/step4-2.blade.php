@extends('templates.master')

@section('csslinkonhead')
<link rel="stylesheet" href="{{url('/')}}/cabinet/css/vendor/smart_wizard.min.css" />
<link rel="stylesheet" href="{{url('/')}}/cabinet/css/vendor/dropzone.min.css" />
<meta name="csrf-token" content="{{ csrf_token() }}" />
<style>
    .form-check-input {
        margin-top: 0.7rem !important;
    }
</style>
@if($agent->isMobile())
<style>
    .sw-main.sw-theme-check>ul.step-anchor>li>a:after,
    .sw-main.sw-theme-check>ul.step-anchor>li.active>a:after,
    .sw-main.sw-main.sw-theme-check>ul.step-anchor>li.done>a:after {
        margin-top: 42px;
    }
</style>
@endif
@endsection

@section('content')
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <h1>Opening Account Wizard</h1>
                <div class="separator mb-5"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 mb-4">
                <!-- <h5 class="mb-4">Open Live Account</h5> -->
                <div class="card mb-4">
                    <div id="" class="sw-main sw-theme-check">
                        <ul class="card-header mb-4 nav nav-tabs step-anchor" style="z-index: 0;">
                            <li class="nav-item done">
                                <a class="nav-link">
                                    @if(!$agent->isMobile())
                                    Step 1
                                    <br>
                                    @endif
                                </a>
                            </li>
                            <li class="nav-item done">
                                <a class="nav-link">
                                    @if(!$agent->isMobile())
                                    Step 2
                                    <br>
                                    @endif
                                </a>
                            </li>
                            <li class="nav-item done">
                                <a class="nav-link">
                                    @if(!$agent->isMobile())
                                    Step 3
                                    <br>
                                    @endif
                                </a>
                            </li>
                            <li class="nav-item active">
                                <a class="nav-link">
                                    @if(!$agent->isMobile())
                                    Step 4
                                    <br>
                                    @endif
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link">
                                    @if(!$agent->isMobile())
                                    Step 5
                                    <br>
                                    @endif
                                </a>
                            </li>
                        </ul>

                        <div class="card-body">
                            <form id="form-step-0" novalidate style="padding: 0px 22px;">
                                <input name="NBCRMWEBAPI" type="hidden" value="{{$UserToken}}" />
                                <input name="USERID" type="hidden" value="{{$UserId}}" />
                                @csrf
                                <div class="row">
                                    <div class="col-12 col-md-12">
                                        <div class="alert alert-dark" role="alert">
                                            Peraturan Perdangangan (Trading Rules) Dalam Sistem Aplikasi
                                            Penerimaan
                                            Nasabah Secara Elektronik On-Line - Formulir Nomor 107.PBK.06
                                        </div>
                                        <div class="form-group">
                                            <h5>PT MAXCO FUTURES Online Trading Rules</h5>
                                            <!-- <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">No</Fth>
                                                        <th scope="col">Nama Kontrak</th>
                                                        <th scope="col">Satuan Kontrak</th>
                                                        <th scope="col">Kode Kontrak</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <th>1</th>
                                                        <td>Kontrak EUR/USD (FOREX)</td>
                                                        <td>EUR 100,000 Fixed rate 10,000 EUR 100,000 Floating</td>
                                                        <td>
                                                            EU 1010_BBJ
                                                            <br />EU 10F_BBJ
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>2</th>
                                                        <td>Kontrak GBP/USD (FOREX)</td>
                                                        <td>GBP 100,000 Fixed rate 10,000 GBP 100,000 Floating</td>
                                                        <td>
                                                            GU 1010_BBJ
                                                            <br />GU 10F_BBJ
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>3</th>
                                                        <td>Kontrak AUD/USD (FOREX)</td>
                                                        <td>AUD 100,000 Fixed rate 10,000 AUD 100,000 Floating</td>
                                                        <td>
                                                            AU 1010_BBJ
                                                            <br />AU 10F_BBJ
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>4</th>
                                                        <td>Kontrak NZD/USD (FOREX)</td>
                                                        <td>NZD 100,000 Fixed rate 10,000 NZD 100,000 Floating</td>
                                                        <td>
                                                            NU 1010_BBJ
                                                            <br />NU 10F_BBJ
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>5</th>
                                                        <td>Kontrak USD/JPY (FOREX)</td>
                                                        <td>JPY 100,000 Fixed rate 10,000 JPY 100,000 Floating</td>
                                                        <td>
                                                            UJ 1010_BBJ
                                                            <br />UJ 10F_BBJ
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>6</th>
                                                        <td>Kontrak USD/CHF (FOREX)</td>
                                                        <td>CHF 100,000 Fixed rate 10,000 CHF 100,000 Floating</td>
                                                        <td>
                                                            UC 1010_BBJ
                                                            <br />UC 10F_BBJ
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>7</th>
                                                        <td>Kontrak USD/CAD (FOREX)</td>
                                                        <td>CAD 100,000 Fixed rate 10,000 CAD 100,000 Floating</td>
                                                        <td>
                                                            UK 1010_BBJ
                                                            <br />UK 10F_BBJ
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>8</th>
                                                        <td>Kontrak UAD/NZD (Crossrate)</td>
                                                        <td>AUD 100,000 Fixed rate 10,000 AUD 100,000 Floating</td>
                                                        <td>
                                                            AN 1010_BBJ
                                                            <br />AN 10F_BBJ
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>9</th>
                                                        <td>Kontrak EUR/JPY (Crossrate)</td>
                                                        <td>EUR 100,000 Fixed rate 10,000 EUR 100,000 Floating</td>
                                                        <td>
                                                            EJ 1H10_BBJ
                                                            <br />EJ 10F_BBJ
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>10</th>
                                                        <td>Kontrak EUR/GBP (Crossrate)</td>
                                                        <td>GBP 100,000 Fixed rate 10,000 GBP 100,000 Floating</td>
                                                        <td>
                                                            EG 1H10_BBJ
                                                            <br />EG 10F_BBJ
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>11</th>
                                                        <td>Kontrak EUR/AUD(Crossrate)</td>
                                                        <td>EUR 100,000 Fixed rate 10,000 EUR 100,000 Floating</td>
                                                        <td>
                                                            EA 1H10_BBJ
                                                            <br />EA 10F_BBJ
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>12</th>
                                                        <td>Kontrak EUR/CAD (Crossrate)</td>
                                                        <td>
                                                            EUR 100,000 Fixed rate 10,000/USD EUR 100,000
                                                            Floating
                                                        </td>
                                                        <td>
                                                            EU 1010_BBJ
                                                            <br />EU 10F_BBJ
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>13</th>
                                                        <td>Kontrak NZD/JPY (Crossrate)</td>
                                                        <td>
                                                            NZD 100,000 Fixed rate 10,000/USD NZD 100,000
                                                            Floating
                                                        </td>
                                                        <td>
                                                            NJ 1010_BBJ
                                                            <br />NJ 10F_BBJ
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>14</th>
                                                        <td>Kontrak EUR/CHF (Crossrate)</td>
                                                        <td>EUR 100,000 Fixed rate 10,000 EUR 100,000 Floating</td>
                                                        <td>
                                                            EC 1010_BBJ
                                                            <br />EC 10F_BBJ
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>15</th>
                                                        <td>Kontrak AUD/JPY(Crossrate)</td>
                                                        <td>AUD 100,000 Fixed rate 10,000 AUD 100,000 Floating</td>
                                                        <td>
                                                            AJ 1010_BBJ
                                                            <br />AJ 10F_BBJ
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>16</th>
                                                        <td>Kontrak CHF/JPY (Crossrate)</td>
                                                        <td>
                                                            CHF 100,000 Fixed rate 10,000/USD CHF 100,000
                                                            Floating
                                                        </td>
                                                        <td>
                                                            CJ 1H10_BBJ
                                                            <br />CJ 10F_BBJ
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>17</th>
                                                        <td>Kontrak GBP/JPY (Crossrate)</td>
                                                        <td>GBP 100,000 Fixed rate 10,000 GBP 100,000 Floating</td>
                                                        <td>
                                                            GJ 1H10_BBJ
                                                            <br />GJ 10F_BBJ
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>18</th>
                                                        <td>Kontrak GBP/AUD (Crossrate)</td>
                                                        <td>GBP 100,000 Fixed rate 10,000 GBP 100,000 Floating</td>
                                                        <td>
                                                            GA 1H10_BBJ
                                                            <br />GA 10F_BBJ
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>19</th>
                                                        <td>Kontrak GBP/CHF (Crossrate)</td>
                                                        <td>GBP 100,000 Fixed rate 10,000 GBP 100,000 Floating</td>
                                                        <td>
                                                            GC 1010_BBJ
                                                            <br />GC 10F_BBJ
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>20</th>
                                                        <td>Kontrak XAU/USD (Precious Metal)</td>
                                                        <td>
                                                            XAU 100 Troy Ounce Fixed rate 10,000 XAU 100 Troy
                                                            Ounce
                                                            Floating
                                                        </td>
                                                        <td>
                                                            XUL10
                                                            <br />XULF
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>21</th>
                                                        <td>Kontrak XAG/USD (Precious Metal)</td>
                                                        <td>
                                                            XAU 5000 Troy Ounce Fixed rate 10,000 XAU 5000 Troy
                                                            Ounce
                                                            Floating
                                                        </td>
                                                        <td>
                                                            XAG10_BBJ
                                                            <br />XAGF_BBJ
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table> -->

                                            <h3>Pemberitahuan</h3>
                                            <p class="text-justify">
                                                Peraturan Perdagangan (Trading Rules) ini harus dibaca bersama dengan Ketentuan Bisnis kami. Maxco Futures dapat sewaktu-waktu merubah peraturan tanpa pemberitahuan terlebih dahulu, maka dari itu peraturan ini selayaknya hanya dijadikan sebagai panduan. Jika Anda memiliki pertanyaan lebih lanjut, silahkan hubungi MAXCO FUTURES secara langsung.
                                            </p>
                                            <p class="text-justify">
                                                MAXCO FUTURES tidak mengizinkan praktik arbitrase saat memperdagangkan Contract for Difference (CFD). Transaksi yang bergantung pada peluang arbitrase latensi harga dapat dicabut dan MAXCO FUTURES berhak untuk membuat koreksi atau penyesuaian yang diperlukan pada akun tersebut tanpa pemberitahuan sebelumnya. Sesuai dengan Ketentuan Bisnis MAXCO FUTURES, akun yang mengandalkan strategi arbitrase dapat dikenai intervensi, yang dapat mencakup pelebaran spread pada akun Anda, akun yang ditempatkan dalam praktik eksekusi yang berbeda, penolakan pesanan, tutup hanya dengan meja perdagangan. Manipulasi pasar seperti spiking pasar berjangka yang mendasari untuk memindahkan harga yang menguntungkan pedagang akan ditandai dan perdagangan yang dianggap telah mengambil keuntungan dari praktik manipulatif tersebut dapat dihapus dengan kebijaksanaan Maxco Futures tanpa pemberitahuan sebelumnya. Penyalahgunaan likuiditas selama waktu perdagangan yang tenang juga merupakan praktik perdagangan yang tidak akan ditoleransi di Maxco Futures. Pesanan besar dengan eksekusi yang cepat selama jam-jam tenang dan ditutup dalam waktu singkat dapat dianggap sebagai perilaku penyalahgunaan likuiditas.

                                            </p>
                                            <h3>Peringatan Resiko</h3>
                                            <p class="text-justify">
                                                Perdagangan Contract for Difference (CFD) dan foreign exchange (forex) mempunyai tingkat risiko yang tinggi, dan mungkin tidak cocok untuk semua investor. Tingkat leverage yang tinggi dapat merugikan Anda dan juga sebaliknya. Sebelum memutuskan untuk berdagang CFD dan / atau valuta asing, Anda harus mempertimbangkan tujuan investasi, tingkat pengalaman, dan selera risiko (risk appetite) Anda dengan cermat.
                                            </p>
                                            <p class="text-justify">
                                                Perlu dicatat bahwa perdagangan ini dapat mengurangi dana bahkan bisa menghabiskan dana keseluruhan yang dimiliki, maka dari itu Anda tidak disarankan untuk menginvestasikan dana atau modal yang Anda miliki secara keseluruhan. Anda harus mengetahui dan memahami semua risiko yang terkait dengan perdagangan CFD dan valas, dan mencari nasihat dari penasihat keuangan independen jika Anda ragu atau memiliki pertanyaan Produk CFD MAXCO FUTURES tidak ditawarkan dengan cara apa pun sehubungan dengan, atau dengan pengesahan dari pertukaran mendasar yang relevan. Penggunaan kata kontrak berjangka dan pertukaran yang relevan oleh MAXCO FUTURES hanya untuk menunjukkan karakteristik produk yang ditawarkan dan karakteristik layanan.
                                            </p>
                                            <p class="text-justify">
                                                Perdagangan Contract For Difference ("CFD") memungkinkan Anda untuk mengalami untung atau rugi dari fluktuasi harga instrumen yang mendasarinya. Harga CFD didasarkan pada harga instrumen yang mendasarinya dan tidak diperdagangkan di bursa penerbitan berjangka, terlepas dari status, atau lokasi instrumen yang mendasarinya. Karenanya, CFD adalah produk Over-the-counter (OTC), dan Anda berdagang dengan MAXCO FUTURES sebagai agen pialang dan Pedagang sebagai mitra untuk semua transaksi yang Anda lakukan. Pedagang memiliki hak dan kemampuan untuk beralih likuiditas CFD dan penyedia harga setiap saat tanpa pemberitahuan sebelumnya.
                                            </p>
                                            <p class="text-justify">
                                                Harap dicatat bahwa sepadan dengan pembukaan / penutupan pasar untuk instrumen yang mendasarinya, pedagang dapat mengalami kesenjangan dalam harga pasar (open gap). Karena volatilitas yang tinggi selama periode waktu tersebut. Perdagangan di pembukaan atau penutupan, dapat melibatkan risiko tambahan dan harus diperhitungkan dalam setiap keputusan perdagangan. Periode waktu ini secara khusus disebutkan karena terkait dengan tingkat likuiditas pasar terendah dan dapat diikuti oleh pergerakan harga yang signifikan untuk CFD, dan instrumen yang mendasarinya.
                                            </p>
                                            <p class="text-justify">
                                                Ada risiko besar bahwa pemasangan stop-loss, yang ditujukan untuk melindungi posisi terbuka yang diadakan dalam semalam, dapat dieksekusi pada tingkat yang jauh lebih buruk daripada harga yang telah Anda tentukan.
                                            </p>
                                            <br>
                                            <h3><strong>INDEX</strong></h3>
                                            <!-- <p class="text-justify">(Native GBP EUR CHF NZD AUD JPY CAD)*</p> -->
                                            <div class="table-responsive">
                                                <table class="table table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th scope="col">Instrument Name</Fth>
                                                            <th scope="col">Product Type</th>
                                                            <th scope="col">Maximum Lots Per Trade</th>
                                                            <th scope="col">PIP Value USD</th>
                                                            <th scope="col">Minimum Spread</th>
                                                            <th scope="col">Minimum Swap</th>
                                                            <th scope="col">Day Trading Margin Requirements<br>(USD)</th>
                                                            <th scope="col">Overnight Margin Requirements <br>(USD)</th>
                                                            <th scope="col">Hedged Margin Requirements</th>
                                                            <th scope="col">Overnight hours</th>
                                                            <th scope="col">Cut Loss Level</th>
                                                            <th scope="col">Pending Order Expiry</th>
                                                            <th scope="col">Summer Trading Hours GMT+7</th>
                                                            <th scope="col">Winter Trading Hours GMT+7</th>
                                                            <th scope="col">Summer Break Time</th>
                                                            <th scope="col">Winter Break TIme</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>US30</td>
                                                            <td>CFD</td>
                                                            <td>20</td>
                                                            <td>5</td>
                                                            <td>5</td>
                                                            <td>-3%</td>
                                                            <td>1.000</td>
                                                            <td>1.000</td>
                                                            <td>200</td>
                                                            <td>n/a</td>
                                                            <td>20%</td>
                                                            <td>GTF</td>
                                                            <td>Monday 06:10 to Saturday 03:15</td>
                                                            <td>Monday 06:10 to Saturday 04:15</td>
                                                            <td>03:15-05:00</td>
                                                            <td>04:15-06:00</td>
                                                        </tr>
                                                        <tr>
                                                            <td>SPX500</td>
                                                            <td>CFD</td>
                                                            <td>20</td>
                                                            <td>50</td>
                                                            <td>0,8</td>
                                                            <td>-3%</td>
                                                            <td>1.000</td>
                                                            <td>1.000</td>
                                                            <td>200</td>
                                                            <td>n/a</td>
                                                            <td>20%</td>
                                                            <td>GTF</td>
                                                            <td>Monday 06:10 to Saturday 03:15</td>
                                                            <td>Monday 06:10 to Saturday 04:15</td>
                                                            <td>03:15-05:00</td>
                                                            <td>04:15-06:00</td>
                                                        </tr>
                                                        <tr>
                                                            <td>NAS100</td>
                                                            <td>CFD</td>
                                                            <td>20</td>
                                                            <td>20</td>
                                                            <td>2</td>
                                                            <td>-3%</td>
                                                            <td>$ 1.000,00</td>
                                                            <td>$ 1.000,00</td>
                                                            <td>$ 200,00</td>
                                                            <td>n/a</td>
                                                            <td>20%</td>
                                                            <td>GTF</td>
                                                            <td>Monday 06:10 to Saturday 03:15</td>
                                                            <td>Monday 06:10 to Saturday 04:15</td>
                                                            <td>03:15-05:00</td>
                                                            <td>04:15-06:00</td>
                                                        </tr>
                                                        <tr>
                                                            <td>JPN225</td>
                                                            <td>CFD</td>
                                                            <td>20</td>
                                                            <td>5</td>
                                                            <td>5</td>
                                                            <td>-3%</td>
                                                            <td>$ 1.000,00</td>
                                                            <td>$ 1.000,00</td>
                                                            <td>$ 200,00</td>
                                                            <td>n/a</td>
                                                            <td>20%</td>
                                                            <td>GTF</td>
                                                            <td>Monday 07:10 to Saturday 03:15</td>
                                                            <td>Monday 07:10 to Saturday 04:15</td>
                                                            <td>03:15-05:00</td>
                                                            <td>04:15-06:00</td>
                                                        </tr>
                                                        <tr>
                                                            <td>HKG50</td>
                                                            <td>CFD</td>
                                                            <td>20</td>
                                                            <td>5</td>
                                                            <td>6</td>
                                                            <td>-3%</td>
                                                            <td>$ 500,00</td>
                                                            <td>$ 2.000,00</td>
                                                            <td>$ 400,00</td>
                                                            <td>02:00-08:30</td>
                                                            <td>20%</td>
                                                            <td>GTF</td>
                                                            <td>Daily 08:15-11:00,12:00-15:30, 16:20-02:00(next day)</td>
                                                            <td>Daily 08:15-11:00,12:00-15:30, 16:20-02:00(next day)</td>
                                                            <td>02:00-08:15</td>
                                                            <td>02:00-08:15</td>
                                                        </tr>
                                                        <tr>
                                                            <td>GER30</td>
                                                            <td>CFD</td>
                                                            <td>20</td>
                                                            <td>5</td>
                                                            <td>2</td>
                                                            <td>-3%</td>
                                                            <td>$ 1.850,00</td>
                                                            <td>$ 3.700,00</td>
                                                            <td>$ 740,00</td>
                                                            <td>03:01-13:59</td>
                                                            <td>20%</td>
                                                            <td>GTF</td>
                                                            <td>Daily 14:00-13:00</td>
                                                            <td>Daily 15:00-04:00</td>
                                                            <td>03:00-14:00 (next day)</td>
                                                            <td>04:00-15:00 (next day)</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                            <h3><strong>1. Jam Perdagangan</strong></h3>
                                            <p class="text-justify">
                                                Jam perdagangan indeks MAXCO FUTURES berpedoman pada jam dari pasar referensi yang mendasarinya. Perlu dicatat bahwa Pasar Indeks MAXCO FUTURES tidak akan terbuka untuk perdagangan selama liburan ketika pasar referensi ditutup. Anda juga dapat mencatat bahwa beberapa indeks memiliki waktu istirahat intraday selain penutupan harian. Selama waktu ini Anda TIDAK akan dapat menempatkan stop dan limit order. Anda juga tidak akan dapat menutup posisi yang ada atau membuka yang baru selama waktu istirahat. Semua fungsi perdagangan akan berhenti pada akhir minggu penutupan.
                                            </p>
                                            <p class="text-justify">
                                                Jika pasar referensi yang mendasari mengenai batas harga Limit-Up atau Limit-Down , semua perdagangan akan dihentikan. Perdagangan akan dilanjutkan dengan kebijakan Maxco Futures karena ini adalah peristiwa dari volatilitas ekstrem dan likuiditas berikutnya mungkin tidak tersedia.
                                            </p>
                                            <!-- Minimum -->
                                            <!-- Trading hours** -->
                                            <h3><strong>2. Kontrak / Ukuran Perdagangan & Eksekusi Perdagangan</strong></h3>
                                            <p class="text-justify">
                                                MAXCO FUTURES menggunakan sistem perdagangan berbasis lot. Karena itu, Anda hanya dapat berdagang berdasarkan ‘Ukuran Perdagangan Minimum’ atau kelipatannya. Dimana nilai pip dikaitkan dengan setiap lot untuk memberikan perhitungan laba dan rugi yang tepat secara real time. Biaya pip yang terkait dengan masing-masing instrumen otomatis akan ditambahkan otomotis kedalam untung dan rugi didalam akun, oleh karena itu, meniadakan risiko fluktuasi mata uang apa pun.
                                            </p>
                                            <h3><strong>2.1 Jumlah Perdagangan Maksimum</strong></h3>
                                            <p class="text-justify">
                                                MAXCO FUTURES dapat mengurangi standard lot maksimum per perdagangan atas kebijakannya sendiri untuk alasan manajemen risiko yang tepat.
                                            </p>
                                            <h3><strong>2.2 Overtrading</strong></h3>
                                            <p class="text-justify">
                                                Dalam hal over-trading, posisi akan dilikuidasi dengan harga pasar.
                                            </p>
                                            <h3><strong>2.3 Limit Orders & Stop Orders</strong></h3>
                                            <p class="text-justify">
                                                Limit Order & Stop Order dapat dimasukkan, dimodifikasi, atau dibatalkan kapan saja selama sesi perdagangan aktif. Selama waktu istirahat, liburan dan akhir pekan nasabah tidak akan bisa mengeksekusi/melaksanakan hal-hal tersebut.
                                            </p>
                                            <p class="text-justify">
                                                Semua Limit Order & Stop Order akan dihapus setelah sesi perdagangan mingguan selesai pada Sabtu pagi atau hari sebelumnya jika perdagangan terakhir dalam minggu tersebut adalah hari libur.
                                            </p>
                                            <p class="text-justify">
                                                Limit Order akan dieksekusi pada harga permintaan ketika harga Bid atau Ask masing-masing tersentuh.
                                            </p>
                                            <p class="text-justify">
                                                Stop Order akan terlaksanakan pada harga pasar ketika harga Bid atau Ask tersentuh, dan perlu dicatat bahwa sewaktu-waktu pasar dapat bergerak dengan volatilitas yang tinggi sehingga dapat menyebabkan slip harga pada saat eksekusi oleh sistem trading PT. Maxco Futures. Apabila ketika pasar dalam kondisi yang tidak normal/atau tidak menentu, maka pada situasi ini ada kemungkinan terjadinya market gap sehingga Stop Order akan tereksekusi di harga gap price terbaru mengingat tidak adanya likuiditas sehingga menyebabkan fluktuasi harga yang tidak wajar.
                                            </p>
                                            <h3><strong>2.3 Limit-Up & Limit-Down (Batas Harga)</strong></h3>
                                            <p class="text-justify">
                                                Jika pasar referensi masing-masing dari produk indeks telah mencapai Limit-Up atau Limit-Down, semua aktivitas perdagangan produk indeks tersebut akan dihentikan. Periode istirahat dapat bervariasi tergantung pada kondisi pasar dan aturan pasar referensi yang mendasarinya.
                                            </p>
                                            <h3><strong>US30, SP500 and NAS100</strong>: Ketentuan Limit Up & Down</h3>
                                            <p class="text-justify">
                                                Ketika harga bergerak sebesar 5% ke atas atau ke bawah dari harga penutupan hari sebelumnya, perdagangan akan otomatis dihentikan sementara selama 15 menit atau lebih. Jika pasar referensi masih mengalami penundaan setelah kembali dibuka maka pasar akan kembali dihentikan hingga pasar referensi kembali dibuka.
                                            </p>
                                            <p class="text-justify">
                                                Jika pasar referensi kembali dibuka dan masih melanjutkan penurunan sebesar 7%, maka secara otomatis akan memicu perhentian perdagangan selama 15 menit. Jika setelah pasar dibuka masih memperpanjang total penurunan menjadi 13%, maka perdagangan akan Kembali dihentikan selama 15 menit.
                                            </p>
                                            <p class="text-justify">
                                                Penurunan sebesar 20% akan memicu penghentian hingga akhir sesi perdagangan.
                                            </p>
                                            <h3><strong>Batas Harga Harian Nikkei</strong></h3>
                                            <p class="text-justify">
                                                Setiap kali harga bergerak sebesar 7,5% ke atas maupun ke bawah dari harga penyelesaian hari sebelumnya, perdagangan akan dihentikan sementara selama 15 menit. Perdagangan akan kembali dihentikan selama 15 menit jika pergerakan harga melanjutkan penurunan atau kenaikan hingga 12,5% dari harga penyelesaian hari sebelumnya.
                                            </p>
                                            <p class="text-justify">
                                                Sering kali likuiditas menjadi sangat minim sehingga menyebabkan kondisi pasar hanya bergerak satu arah. Contohnya tidak adanya Penawaran dan hanya ada Permintaan atau sebaliknya. MAXCO FUTURES berhak dan atas kebijakan sendiri untuk tidak membuka kembali produk indeks yang baru saja mencapai waktu istirahat Limit-Up atau Limit-Down atau memperpanjang waktu istirahat melebihi 15 menit. Jika pasar referensi yang mendasari memutuskan untuk menutup untuk sisa sesi, maka MAXCO FUTURES juga akan menghentikan perdagangan produk indeks tertentu sampai pasar perdagangan tunai masing-masing terbuka.
                                            </p>
                                            <h3><strong>3. Nilai Pip</strong></h3>
                                            <p class="text-justify">
                                                Dikarenakan laba dan rugi dikonversi ke dalam mata uang akun, maka nilai pip akan dikaitkan dengan setiap produk. Misalnya, jika akun perdagangan didominasi dalam USD, untung dan rugi akan dihitung dalam USD. Jika nasabah mempuyai posisi JPN200, yang dihargai dalam JPY, MAXCO FUTURES akan secara otomatis mengkonversi laba dan rugi menjadi USD pada tingkat bunga tetap. Kurs tetap dapat berubah dari waktu ke waktu karena pergerakan valuasi mata uang yang signifikan.
                                            </p>
                                            <p class="text-justify">
                                                Semua produk Indeks dengan pengecualian US500 & NAS100 & GER30 akan dikutip dalam bilangan bulat di mana digit terakhir dari harga adalah pergerakan harga minimum. Misalnya, Permintaan HKG50 adalah 23729, maka pergerakan harga minimum dimulai dari digit terakhir, 9. Jika Permintaan HKG50 bergerak dari 23729 ke 23730, maka harga telah naik sebesar 1 pip.
                                            </p>
                                            <p class="text-justify">
                                                US500, NAS100, GER30 dikutip dalam 1/10 dari pip dengan 1 digit setelah desimal.
                                            </p>
                                            <p class="text-justify">
                                                Pergerakan harga US500 & NAS100 dimulai dari 0,1 pip dan seterusnya. Misalnya, US500 Bid adalah 2526.8 dan Ask sebesar 2529.7, maka spread adalah sebesar 2.9 pips
                                            </p>
                                            <p class="text-justify">
                                                Sedangkan GER30 dikutip sebesar 0.5 pip. Contohnya, GER30 Bid 9959 Ask 9962.50, maka spread adalah sebesar 3.5 pips.
                                            </p>
                                            <h3><strong>4. Persyaratan Margin Minimum</strong></h3>
                                            <p class="text-justify">
                                                Persyaratan Margin Minimum MAXCO FUTURES ditampilkan dalam detail produk dan kewajiban modal nasabah untuk membeli atau menjual kontrak minimum satu lot. MAXCO FUTURES memiliki ukuran perdagangan minimum standar untuk setiap instrumen agar menghitung margin yang diperlukan untuk melakukan perdagangan secara langsung.
                                            </p>
                                            <p class="text-justify">
                                                Detail margin adalah persyaratan margin default MAXCO FUTURES.
                                            </p>
                                            <h3><strong>4.1 Persyaratan Margin Overnight</strong></h3>
                                            <p class="text-justify">
                                                Peraturan overnight margin adalah ketika hari perdagangan pasar referensi telah tutup, dan kemudian perdagangan derivative semalam Kembali dilanjutkan setelah waktu istirahat.
                                            </p>
                                            <p class="text-justify">
                                                Mengingat risiko yang lebih tinggi dari margin perdagangan semalam mungkin dinaikkan dari waktu ke waktu tergantung pada kondisi pasar dan volatilitas. Peningkatan margin semalam dapat berlaku segera tanpa pemberitahuan sebelumnya.
                                            </p>
                                            <p class="text-justify">
                                                Overnight margin bisa mencapai EMPAT kali margin perdagangan hari tergantung pada kondisi pasar. MAXCO FUTURES memiliki hak untuk meningkatkan margin semalam lebih lanjut jika dianggap perlu untuk meminimalkan risiko kerugian lebih kepada pemegang rekening.
                                            </p>
                                            <p class="text-justify">
                                                Jika Anda ingin agar margin semalam tidak diterapkan ke akun Anda, silakan hubungi MAXCO FUTURES untuk mengajukan permintaan ini. Permintaan disetujui secara individual dan MAXCO FUTURES memiliki keleluasaan untuk menolak atau membalikkan persetujuan.
                                            </p>
                                            <h3><strong>4.2 Margin Call</strong></h3>
                                            <p class="text-justify">
                                                Semua posisi semalam harus mempertahankan margin yang cukup jika tidak semua posisi dalam akun dapat dilikuidasi jika tingkat margin bebas akun menyentuh tingkat likuidasi 20%. Jika terjadi kelalaian dalam penyelesaian atau margin minimum yang tidak terpenuhi atau setoran yang tidak diselesaikan karena alasan apa pun, MAXCO FUTURES, atas kebijakannya sendiri berhak untuk melikuidasi sebagian atau semua posisi pada harga pasar ketika tingkat likuidasi tercapai.
                                            </p>
                                            <p class="text-justify">
                                                Jika margin call dipicu oleh open gap maka tidak menutup kemungkinan adanya risiko saldo negatif karena tingkat kesenjangan harga.
                                            </p>
                                            <h3><strong>5. Spread Minimum</strong></h3>
                                            <p class="text-justify">
                                                Ini adalah yang paling minim (perbedaan terkecil antara harga jual dan beli) MAXCO FUTURES akan tampilkan dalam harga kami. Spread dapat berfluktuasi tergantung pada kondisi pasar karena kami mengadaptasi spread mengambang (floating spread).
                                            </p>
                                            <h3><strong>6. Biaya</strong></h3>
                                            <p class="text-justify">
                                                Biaya penyimpanan dan dividen merupakan kredit / debit semalam. Nilai kedua variabel ini tidak tergantung satu sama lain. Kredit / debit keseluruhan ke akun Anda akan tergantung pada ukuran perdagangan terbuka. Ini akan tercermin dalam area swap dan dihitung berdasarkan poin swap di jendela detail produk. Kredit atau debit dividen akan tercermin sebagai setoran atau penarikan di akun Anda ketika peristiwa itu terjadi.
                                            </p>
                                            <h3><strong>7. Biaya SWAP</strong></h3>
                                            <p class="text-justify">
                                                Suku bunga adalah faktor yang penting di pasar mana pun. Debit bunga atau jumlah kredit harian MAXCO FUTURES (selanjutnya disebut "swap") didasarkan pada total nilai nominal rollover rate yang dihitung dengan merujuk LIBOR 3 bulan yang relevan untuk semua produk indeks. Setiap hari, jumlah rollover per lot menunjukkan nilai tukar yang sederhana.
                                            </p>
                                            <p class="text-justify">
                                                Posisi indeks yang terbuka pada penutupan bisnis pada hari Jumat Anda akan dikenakan rollover 3 hari.
                                            </p>
                                            <p class="text-justify">
                                                Misalnya, jika 3 bulan USD LIBOR adalah 4,50% dan MAXCO FUTURES menerapkan potongan + 3 / -3% posisi beli (long) akan membayar 7,50% / 360 per hari, di mana posisi jual (short) akan menerima 1,50% / 360 per hari.
                                            </p>
                                            <p class="text-justify">
                                                Perlu dicatat bahwa MAXCO FUTURES menggunakan referensi tingkat LIBOR 3 bulan sesuai dengan mata uang yang dikutip oleh instrumen. Contohnya, untuk mata uang Euro, MAXCO FUTURES akan merujuk terhadap LIBOR Euro 3 bulan. Demikian pula, untuk nasabah yang memiliki posisi terbuka GER30, akan merujuk terhadap LIBOR Euro 3 bulan dan sebagainya.
                                            </p>
                                            <p class="text-justify">
                                                <i>Perhitungan Biaya</i>
                                            </p>
                                            <p class="text-justify">
                                                f = Biaya SWAP s = Ukuran perdagangan p = harga penutupan sebagaimana ditentukan oleh MAXCO FUTURES r = Tingkat LIBOR yang relevan, tambahkan 300 basis poin untuk posisi buy, atau kurangi 300 basis poin untuk posisi sell (6.00% - 3.00%) = 3% d = Jumlah hari, yaitu, 365 untuk produk GBP dan 360 untuk produk lainnya
                                            </p>
                                            <p class="text-justify">
                                                Dan dihitung sebagai berikut: f = (s x p x r) / d
                                            </p>
                                            <p class="text-justify">
                                                Biaya SWAP dapa berubah sewaktu-waktu dikarenakan tingkat volalitas yang tinggi. Penyedia Likuiditas (Liquidity Provider) dapat menaikan biaya tersebut dan MAXCO FUTURES harus turut menyesuaikan biaya untuk mencerminkan kondisi pasar yang tidak stabil.
                                            </p>
                                            <h3><strong>8. Dividends</strong></h3>
                                            <p class="text-justify">
                                                Berlaku untuk sebagian besar indeks tunai, pembayaran dividen akan diterapkan sebagai debit / kredit bersamaan dengan swap ke posisi terbuka Anda. Penyesuaian akan berlaku pada tanggal ex-dividend anggota konstituen dari Indeks yang relevan. Penyesuaian akan muncul sebagai bagian dari roll over debit / kredit pada laporan Anda.
                                            </p>
                                            <p class="text-justify">
                                                Ketika ekuitas menjadi ex-dividen, harga ekuitas itu secara teoritis berkurang dengan jumlah dividen. Dalam praktiknya, ini tidak selalu terjadi kekuatan pasar yang mempengaruhi harga ekuitas. Jumlah poin yang CFD tunai indeks turun tergantung pada bobot ekuitas dalam indeks. Jika lebih dari satu ekuitas konstituen dari CFD indeks berjalan ex-dividen pada hari yang sama, jumlah poin setiap ekuitas secara teoritis akan menyebabkan sektor atau indeks turun ditambahkan bersama-sama untuk menghitung jumlah total poin dividen atau 'poin drop' ' MAXCO FUTURES akan mengumpulkan atau membayar dividen pada posisi lindung nilai yang telah kami lakukan terhadap CFD yang diterbitkan klien.
                                            </p>
                                            <p class="text-justify">
                                                Di mana indeks adalah Total Return Index, pembayaran dividen tidak akan dikreditkan / didebit.
                                            </p>
                                            <p class="text-justify">
                                                Contoh dari indeks pengembalian total adalah GER 30 di mana pencairan kas diinvestasikan kembali ke dalam indeks
                                            </p>
                                            <p class="text-justify">
                                                <strong>Contoh:</strong>
                                            </p>
                                            <ul>
                                                <li>Client mempunyai posisi beli (long) 100 US 30.</li>
                                                <li>Biaya Swap beli (Long) adalah -0.88 (seperti yang ditampilkan di jendela detail produk)</li>
                                                <li>Dengan asumsi klien adalah pemegang posisi ini hingga pukul 17:00 (Waktu NY), mereka akan dinilai biaya $ 88,00 untuk hari perdagangan tertentu.</li>
                                            </ul>
                                            <h3><strong>Masa Kontrak Indeks</strong></h3>
                                            <p class="text-justify">
                                                Semua posisi indeks akan tetap terbuka sampai ditutup oleh nasabah atau posisi dilikuidasi karena margin yang tidak mencukupi untuk mendukung posisi terbuka.
                                            </p>
                                            <br>
                                            <h3><strong>Energi</strong></h3>
                                            <div class="table-responsive">
                                                <table class="table table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th scope="col">Instrument Name</Fth>
                                                            <th scope="col">Product Type</th>
                                                            <th scope="col">Maximum Lots Per Trade</th>
                                                            <th scope="col">PIP Value USD</th>
                                                            <th scope="col">Minimum Spread</th>
                                                            <th scope="col">Minimum Swap</th>
                                                            <th scope="col">Day Trading Margin Requirements<br>(USD)</th>
                                                            <th scope="col">Overnight Margin Requirements <br>(USD)</th>
                                                            <th scope="col">Hedged Margin Requirements</th>
                                                            <th scope="col">Overnight hours</th>
                                                            <th scope="col">Cut Loss Level</th>
                                                            <th scope="col">Pending Order Expiry</th>
                                                            <th scope="col">Summer Trading Hours GMT+7</th>
                                                            <th scope="col">Winter Trading Hours GMT+7</th>
                                                            <th scope="col">Summer Break Time</th>
                                                            <th scope="col">Winter Break TIme</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>USOIL</td>
                                                            <td>CFD</td>
                                                            <td>10</td>
                                                            <td>10</td>
                                                            <td>4</td>
                                                            <td>-15%</td>
                                                            <td>1.000</td>
                                                            <td>2.000</td>
                                                            <td>400</td>
                                                            <td>Sat: 02:01 - Mon: 06:30</td>
                                                            <td>20%</td>
                                                            <td>GTF</td>
                                                            <td>Mon: 06:10 - Sat: 02:00</td>
                                                            <td>Mon: 06:10 - Sat: 03:00</td>
                                                            <td>04:00-05:00</td>
                                                            <td>05:00-06:00</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                            <h3><strong>10. Jam Perdagangan</strong></h3>
                                            <p class="text-justify">
                                                Jam Perdagangan untuk produk Energi berpedoman pada jam dari pasar referensi yang mendasarinya. Produk Energy tidak akan dibuka untuk perdagangan ketika pasar referensi tutup. Serupa dengan produk Index, Produk Energi juga mempunyai waktu istirahat, selama jam ini nasabah tidak akan bisa untuk menempatkan Stop dan Limit Order. Nasabah juga tidak akan dapat menutup atau membuka posisi baru. Semua fungsi perdagangan akan berhenti pada penutupan akhir minggu.
                                            </p>
                                            <h3><strong>11. Kuotasi Harga Produk Energi</strong></h3>
                                            <p class="text-justify">
                                                MAXCO FUTURES menerima kuotasi harga dari Panin Emperor dan Panin Emperor mendapatkan kuotasi harga dari berbagai Penyedia Likuiditas (Liquidity Provider). Perlu dicatat dengan kondisi ini, tidak menutup kemungkinan adanya Perbedaan harga yang signifikan antara MAXCO FUTURES dengan Penyedia Likuiditas dikarenakan kami memiliki likuiditas minimum per harga untuk penyedia likuiditas. Jika kondisi pasar memburuk dalam likuiditas di atas harga buku, harga kami akan lebih luas karena persyaratan likuiditas minimum di mana kami harus menggabungkan beragam harga untuk menawarkan likuiditas yang dapat dieksekusi kepada nasabah kami.
                                            </p>
                                            <h3><strong>12. Ukuran Kontrak</strong></h3>
                                            <p class="text-justify">
                                                MAXCO FUTURES menggunakan sistem perdagangan 'berbasis lot'. Ini berarti bahwa semua produk MAXCO FUTURES digabungkan ke dalam ukuran perdagangan standar. Ukuran ini umumnya mereplikasi instrumen referensi yang mendasarinya (futures atau cash instrument) atau sebagian kecil dari angka itu. Ini menyederhanakan perdagangan dengan memungkinkan nasabah untuk melakukan perdagangan dalam peningkatan lot, dan juga memberikan harga untuk setiap ukuran lot daripada rata-rata harga buka dan tutup ketika banyak posisi diambil dalam instrumen yang sama. Nilai tick atau pip dikaitkan dengan setiap lot untuk perhitungan yang tepat, dan keuntungan dan kerugian pada suatu posisi secara otomatis dikonversi ke dalam mata uang akun tertentu.
                                            </p>
                                            <!-- Minimum Stop Distance (Points) -->
                                            <h3><strong>12.1 Jumlah Perdagangan Maksimum</strong></h3>
                                            <p class="text-justify">
                                                MAXCO FUTURES dapat mengurangi standard lot maksimum per perdagangan atas kebijakannya sendiri untuk alasan manajemen risiko yang tepat.
                                            </p>
                                            <h3><strong>12.2 Overtrading</strong></h3>
                                            <p class="text-justify">
                                                Dalam hal over-trading, posisi akan dilikuidasi dengan harga pasar.
                                            </p>
                                            <h3><strong>12.3 Limit Orders & Stop Orders</strong></h3>
                                            <p class="text-justify">
                                                Limit Order & Stop Order dapat dimasukkan, dimodifikasi, atau dibatalkan kapan saja selama sesi perdagangan aktif. Selama waktu istirahat, liburan dan akhir pekan nasabah tidak akan bisa mengeksekusi/melaksanakan hal-hal tersebut.
                                            </p>
                                            <p class="text-justify">
                                                Semua Limit Order & Stop Order akan dihapus setelah sesi perdagangan mingguan selesai pada Sabtu pagi atau hari sebelumnya jika perdagangan terakhir dalam minggu tersebut adalah hari libur.
                                            </p>
                                            <p class="text-justify">
                                                Limit Order akan dieksekusi pada harga permintaan ketika harga Bid atau Ask masing-masing tersentuh.
                                            </p>
                                            <p class="text-justify">
                                                Stop Order akan terlaksanakan pada harga pasar ketika harga Bid atau Ask tersentuh, dan perlu dicatat bahwa sewaktu-waktu pasar dapat bergerak dengan volatilitas yang tinggi sehingga dapat menyebabkan slip harga pada saat eksekusi oleh sistem trading PT. Maxco Futures. Apabila ketika pasar dalam kondisi yang tidak normal/atau tidak menentu, maka pada situasi ini ada kemungkinan terjadinya market gap sehingga Stop Order akan tereksekusi di harga gap price terbaru mengingat tidak adanya likuiditas sehingga menyebabkan fluktuasi harga yang tidak wajar.
                                            </p>
                                            <h3><strong>13. Nilai Pip</strong></h3>
                                            <p class="text-justify">
                                                Nilai Pip ditentukan oleh ukuran lot perdagangan. Pergerakan harga minimum dianggap sebagai 1 pip, ini adalah digit kedua setelah desimal, yaitu 1 sen. 1 lot sama dengan 1000 barel, maka setiap satu pip bernilai $ 10 USD.
                                            </p>
                                            <h3><strong>14. Persyaratan Margin Minimum</strong></h3>
                                            <p class="text-justify">
                                                Persyaratan Margin Minimum MAXCO FUTURES ditampilkan dalam detail produk dan kewajiban modal nasabah untuk membeli atau menjual kontrak minimum satu lot Minyak AS. MAXCO FUTURES memiliki ukuran perdagangan minimum standar untuk setiap instrumen agar menghitung margin yang diperlukan untuk melakukan perdagangan secara langsung.
                                            </p>
                                            <ul>
                                                <li>Ukuran perdagangan Minimum Minyak AS adalah 1 kontrak (1000 barel)</li>
                                                <li>Margin adalah USD $ 1000 per kontrak</li>
                                                <li>1 kontrak x $1000 = $1000</li>
                                            </ul>
                                            <p class="text-justify">
                                                Detail margin diatas adalah syarat margin oleh MAXCO FUTURES. Jika Anda ingin mengubah persyaratan margin Anda, silakan berbicara dengan spesialis produk.
                                            </p>
                                            <h3><strong>14.1 Persyaratan Margin Overnight</strong></h3>
                                            <p class="text-justify">
                                                Peraturan margin overnight adalah ketika hari perdagangan pasar referensi telah tutup, dan kemudian perdagangan derivative semalam Kembali dilanjutkan setelah waktu istirahat.
                                            </p>
                                            <p class="text-justify">
                                                Mengingat risiko yang lebih tinggi dari margin perdagangan semalam mungkin dinaikkan dari waktu ke waktu tergantung pada kondisi pasar dan volatilitas. Peningkatan margin semalam dapat berlaku segera tanpa pemberitahuan sebelumnya.
                                            </p>
                                            <p class="text-justify">
                                                Overnight margin bisa mencapai EMPAT kali margin perdagangan hari tergantung pada kondisi pasar. MAXCO FUTURES memiliki hak untuk meningkatkan margin semalam lebih lanjut jika dianggap perlu untuk meminimalkan risiko kerugian lebih kepada pemegang rekening.
                                            </p>
                                            <p class="text-justify">
                                                Jika Anda ingin agar margin semalam tidak diterapkan ke akun Anda, silakan hubungi MAXCO FUTURES untuk mengajukan permintaan ini. Permintaan disetujui secara individual dan MAXCO FUTURES memiliki keleluasaan untuk menolak atau membalikkan persetujuan.
                                            </p>
                                            <h3><strong>14.2 Margin Call</strong></h3>
                                            <p class="text-justify">
                                                Semua posisi semalam harus mempertahankan margin yang cukup jika tidak semua posisi dalam akun dapat dilikuidasi jika tingkat margin bebas akun menyentuh tingkat likuidasi 20%. Jika terjadi kelalaian dalam penyelesaian atau margin minimum yang tidak terpenuhi atau setoran yang tidak diselesaikan karena alasan apa pun, MAXCO FUTURES, atas kebijakannya sendiri berhak untuk melikuidasi sebagian atau semua posisi pada harga pasar ketika tingkat likuidasi tercapai.
                                            </p>
                                            <p class="text-justify">
                                                Jika margin call dipicu oleh open gap maka tidak menutup kemungkinan adanya risiko saldo negatif karena tingkat kesenjangan harga.
                                            </p>
                                            <h3><strong>15. Spread Minimum</strong></h3>
                                            <p class="text-justify">
                                                Ini adalah yang paling minim (perbedaan terkecil antara harga jual dan beli) MAXCO FUTURES akan tampilkan dalam harga kami. Spread dapat berfluktuasi tergantung pada kondisi pasar karena kami mengadaptasi spread mengambang (floating spread).
                                            </p>
                                            <h3><strong>16. Biaya SWAP</strong></h3>
                                            <p class="text-justify">
                                                Karena produk energi adalah instrumen spot, maka akan diberlakukan biaya swap kredit atau debit
                                            </p>
                                            <p class="text-justify">
                                                Produk-produk energi tidak memiliki tanggal kedaluwarsa bulanan dan digulir secara otomatis setiap hari. Menjelang akhir kontrak, harga spot minyak mungkin berbeda secara signifikan dari harga pasar referensi yang mendasari karena perbedaan harga antara bulan ini dan harga pasar referensi bulan berikutnya.
                                            </p>
                                            <br>
                                            <h3><strong>LOGAM MULIA (METAL)</strong></h3>
                                            <!-- GBP Requirement
                                            Per Min T rade Size EUR CHF NZD Target MAXCO FUTURES
                                            AUD JPY CAD
                                            Spread ding hours** Br -->
                                            <div class="table-responsive">
                                                <table class="table table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th scope="col">Instrument Name</Fth>
                                                            <th scope="col">Product Type</th>
                                                            <th scope="col">Maximum Lots Per Trade</th>
                                                            <th scope="col">PIP Value USD</th>
                                                            <th scope="col">Minimum Spread</th>
                                                            <th scope="col">Minimum Swap</th>
                                                            <th scope="col">Day Trading Margin Requirements<br>(USD)</th>
                                                            <th scope="col">Overnight Margin Requirements <br>(USD)</th>
                                                            <th scope="col">Hedged Margin Requirements</th>
                                                            <th scope="col">Overnight hours</th>
                                                            <th scope="col">Cut Loss Level</th>
                                                            <th scope="col">Pending Order Expiry</th>
                                                            <th scope="col">Summer Trading Hours GMT+7</th>
                                                            <th scope="col">Winter Trading Hours GMT+7</th>
                                                            <th scope="col">Summer Break Time</th>
                                                            <th scope="col">Winter Break TIme</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>XAUUSD</td>
                                                            <td>CFD</td>
                                                            <td>50</td>
                                                            <td>10</td>
                                                            <td>28</td>
                                                            <td>-3%</td>
                                                            <td>1.000</td>
                                                            <td>1.000</td>
                                                            <td>200</td>
                                                            <td>n/a</td>
                                                            <td>20%</td>
                                                            <td>GTF</td>
                                                            <td>Mon 06:10 - Sat 02:00</td>
                                                            <td>Mon 06:10 - Sat 03:00</td>
                                                            <td>04:00-05:00</td>
                                                            <td>05:00-06:00</td>
                                                        </tr>
                                                        <tr>
                                                            <td>XAUUSD</td>
                                                            <td>CFD</td>
                                                            <td>50</td>
                                                            <td>50</td>
                                                            <td>4</td>
                                                            <td>-3%</td>
                                                            <td>1.000</td>
                                                            <td>1.000</td>
                                                            <td>200</td>
                                                            <td>n/a</td>
                                                            <td>20%</td>
                                                            <td>GTF</td>
                                                            <td>Mon 06:10 - Sat 02:00</td>
                                                            <td>Mon 06:10 - Sat 03:00</td>
                                                            <td>04:00-05:00</td>
                                                            <td>05:00-06:00</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <h3><strong>18. Jam Perdagangan</strong></h3>
                                            <p class="text-justify">
                                                Produk Metal dapat diperdagangkan selama 23 jam per hari. Selama periode ini berlangsung, nasabah dapat membuka dan menutup posisi dan juga menaruh limit dan stop orders. Ketika perdanganan ditutup, nasabah tidak akan dapat untuk membuka posisi, limit atau stop order.
                                            </p>
                                            <h3><strong>19. Kuotasi Harga Produk Metal</strong></h3>
                                            <p class="text-justify">
                                                Tujuan kami adalah memberi Anda biaya transaksi yang sangat kompetitif — memberikan spread bid / ask yang minim pada setiap produk metal. Spread yang digunakan adalah spread mengambang (floating spread).
                                            </p>
                                            <h3><strong>20. Ukuran Kontrak</strong></h3>
                                            <p class="text-justify">
                                                MAXCO FUTURES menggunakan sistem perdagangan 'berbasis lot'. Ini berarti bahwa semua produk MAXCO FUTURES digabungkan ke dalam ukuran perdagangan standar. Ukuran ini umumnya mereplikasi instrumen referensi yang mendasarinya (futures atau cash instrument) atau sebagian kecil dari angka itu. Ini menyederhanakan perdagangan dengan memungkinkan nasabah untuk melakukan perdagangan dalam peningkatan lot, dan juga memberikan harga untuk setiap ukuran lot daripada rata-rata harga buka dan tutup ketika banyak posisi diambil dalam instrumen yang sama. Nilai tick atau pip dikaitkan dengan setiap lot untuk perhitungan yang tepat, dan keuntungan dan kerugian pada suatu posisi secara otomatis dikonversi ke dalam mata uang akun tertentu.
                                            </p>
                                            <h3><strong>12.1 Jumlah Perdagangan Maksimum</strong></h3>
                                            <p class="text-justify">
                                                MAXCO FUTURES dapat mengurangi standard lot maksimum per perdagangan atas kebijakannya sendiri untuk alasan manajemen risiko yang tepat.
                                            </p>
                                            <h3><strong>20.1 Overtrading</strong></h3>
                                            <p class="text-justify">
                                                Dalam hal over-trading, posisi akan dilikuidasi dengan harga pasar.
                                            </p>
                                            <h3><strong>12.3 Limit Orders & Stop Orders</strong></h3>
                                            <p class="text-justify">
                                                Limit Order & Stop Order dapat dimasukkan, dimodifikasi, atau dibatalkan kapan saja selama sesi perdagangan aktif. Selama waktu istirahat, liburan dan akhir pekan nasabah tidak akan bisa mengeksekusi/melaksanakan hal-hal tersebut.
                                            </p>
                                            <p class="text-justify">
                                                Semua Limit Order & Stop Order akan dihapus setelah sesi perdagangan mingguan selesai pada Sabtu pagi atau hari sebelumnya jika perdagangan terakhir dalam minggu tersebut adalah hari libur.
                                            </p>
                                            <p class="text-justify">
                                                Limit Order akan dieksekusi pada harga permintaan ketika harga Bid atau Ask masing-masing tersentuh.
                                            </p>
                                            <p class="text-justify">
                                                Stop Order akan terlaksanakan pada harga pasar ketika harga Bid atau Ask tersentuh, dan perlu dicatat bahwa sewaktu-waktu pasar dapat bergerak dengan volatilitas yang tinggi sehingga dapat menyebabkan slip harga pada saat eksekusi oleh sistem trading PT. Maxco Futures. Apabila ketika pasar dalam kondisi yang tidak normal/atau tidak menentu, maka pada situasi ini ada kemungkinan terjadinya market gap sehingga Stop Order akan tereksekusi di harga gap price terbaru mengingat tidak adanya likuiditas sehingga menyebabkan fluktuasi harga yang tidak wajar.
                                            </p>
                                            <p class="text-justify">
                                                Minimum Stop Distance (Points)
                                            </p>
                                            <h3><strong>21. Nilai Pip</strong></h3>
                                            <p class="text-justify">
                                                Nilai Pip ditentukan oleh ukuran lot perdagangan. Pergerakan harga minimum dianggap sebagai 1 pip, ini adalah digit kedua setelah desimal, 1 lot sama dengan 100 troy ounces, maka dari itu satu pip adalah $1 USD.
                                            </p>
                                            <h3><strong>22. Persyaratan Margin Minimum</strong></h3>
                                            <p class="text-justify">
                                                Tingkat Margin MAXCO FUTURES ditampilkan di jendela nilai tukar pada TSII dan merinci kewajiban modal klien untuk membeli atau menjual ukuran perdagangan minimum. MAXCO FUTURES memiliki ukuran perdagangan minimum / tambahan standar untuk setiap instrumen. Untuk menghitung margin yang diperlukan untuk menempatkan ukuran perdagangan minimum, ukuran perdagangan dengan margin yang diperlukan (per kontrak), yang ditampilkan dalam kurs transaksi.
                                            </p>
                                            <p class="text-justify">
                                                Detail margin adalah persyaratan margin default MAXCO FUTURES. Jika Anda ingin mengubah persyaratan margin Anda, silakan masuk ke myMaxco Futures.com untuk meningkatkan margin default Anda untuk produk minyak, logam dan indeks
                                            </p>
                                            <h3><strong>14.2 Persyaratan Margin Overnight</strong></h3>
                                            <p class="text-justify">
                                                Peraturan margin overnight adalah ketika hari perdagangan pasar referensi telah tutup, dan kemudian perdagangan derivative semalam Kembali dilanjutkan setelah waktu istirahat.
                                            </p>
                                            <p class="text-justify">
                                                Mengingat risiko yang lebih tinggi dari margin perdagangan semalam mungkin dinaikkan dari waktu ke waktu tergantung pada kondisi pasar dan volatilitas. Peningkatan margin semalam dapat berlaku segera tanpa pemberitahuan sebelumnya.
                                            </p>
                                            <p class="text-justify">
                                                Overnight margin bisa mencapai EMPAT kali margin perdagangan hari tergantung pada kondisi pasar. MAXCO FUTURES memiliki hak untuk meningkatkan margin semalam lebih lanjut jika dianggap perlu untuk meminimalkan risiko kerugian lebih kepada pemegang rekening.
                                            </p>
                                            <p class="text-justify">
                                                Jika Anda ingin agar margin semalam tidak diterapkan ke akun Anda, silakan hubungi MAXCO FUTURES untuk mengajukan permintaan ini. Permintaan disetujui secara individual dan MAXCO FUTURES memiliki keleluasaan untuk menolak atau membalikkan persetujuan
                                            </p>
                                            <h3><strong>14.1 Margin Call</strong></h3>
                                            <p class="text-justify">
                                                Semua posisi semalam harus mempertahankan margin yang cukup jika tidak semua posisi dalam akun dapat dilikuidasi jika tingkat margin bebas akun menyentuh tingkat likuidasi 20%. Jika terjadi kelalaian dalam penyelesaian atau margin minimum yang tidak terpenuhi atau setoran yang tidak diselesaikan karena alasan apa pun, MAXCO FUTURES, atas kebijakannya sendiri berhak untuk melikuidasi sebagian atau semua posisi pada harga pasar ketika tingkat likuidasi tercapai.
                                            </p>
                                            <p class="text-justify">
                                                Jika margin call dipicu oleh open gap maka tidak menutup kemungkinan adanya risiko saldo negatif karena tingkat kesenjangan harga.
                                            </p>
                                            <h3><strong>23. Spread Minimum</strong></h3>
                                            <p class="text-justify">
                                                Ini adalah yang paling minim (perbedaan terkecil antara harga jual dan beli) MAXCO FUTURES akan tampilkan dalam harga kami.
                                            </p>
                                            <h3><strong>24. Overnight Swaps (Swap)</strong></h3>
                                            <p class="text-justify">
                                                Semua posisi terbuka akan digulir ke hari perdagangan berikutnya. Tergantung pada apakah Anda mempunyai long (beli) atau short (jual). Biaya ini akan didebit atau dikreditkan ke akun Anda setiap hari. Detail dari nilai tukar MAXCO FUTURES diperinci di platform kami secara transparan. Harap dicatat bahwa semua posisi terbuka pada penutupan Rabu pukul 17.00 EST menimbulkan debit / kredit rollover 3 hari, dan hari libur bank akan memengaruhi jumlah hari suatu posisi bergulir.
                                            </p>
                                            <h3><strong>25. Expiration</strong></h3>
                                            <p class="text-justify">
                                                Semua perdagangan produk metal akan tetap terbuka sampai nasabah menutup posisi atau ada margin yang tidak cukup untuk mendukung posisi terbuka. Dalam hal ini akan ditutup oleh MAXCO FUTURES.
                                            </p>
                                            <h3><strong>Foreign Exchange (FX)</strong></h3>
                                            <div class="table-responsive">
                                                <table class="table table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th scope="col">Instrument Name</Fth>
                                                            <th scope="col">Product Type</th>
                                                            <th scope="col">Maximum Lots Per Trade</th>
                                                            <th scope="col">PIP Value USD</th>
                                                            <th scope="col">Minimum Spread</th>
                                                            <th scope="col">Minimum Swap</th>
                                                            <th scope="col">Day Trading Margin Requirements<br>(USD)</th>
                                                            <th scope="col">Overnight Margin Requirements <br>(USD)</th>
                                                            <th scope="col">Hedged Margin Requirements</th>
                                                            <th scope="col">Overnight hours</th>
                                                            <th scope="col">Cut Loss Level</th>
                                                            <th scope="col">Pending Order Expiry</th>
                                                            <th scope="col">Summer Trading Hours GMT+7</th>
                                                            <th scope="col">Winter Trading Hours GMT+7</th>
                                                            <th scope="col">Summer Break Time</th>
                                                            <th scope="col">Winter Break TIme</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>AUD/CAD</td>
                                                            <td>FX</td>
                                                            <td>50</td>
                                                            <td>7,6</td>
                                                            <td>1,2</td>
                                                            <td>-1,25%</td>
                                                            <td>1.000</td>
                                                            <td>1.000</td>
                                                            <td>200</td>
                                                            <td>n/a</td>
                                                            <td>20%</td>
                                                            <td>GTF</td>
                                                            <td>Mon 06:10 - Sat 02:00</td>
                                                            <td>Mon 06:10 - Sat 03:00</td>
                                                            <td>04:00-05:00</td>
                                                            <td>05:00-06:00</td>
                                                        </tr>
                                                        <tr>
                                                            <td>AUD/JPY</td>
                                                            <td>FX</td>
                                                            <td>50</td>
                                                            <td>9,1</td>
                                                            <td>1,2</td>
                                                            <td>0,00%</td>
                                                            <td>1.000</td>
                                                            <td>1.000</td>
                                                            <td>200</td>
                                                            <td>n/a</td>
                                                            <td>20%</td>
                                                            <td>GTF</td>
                                                            <td>Mon 06:10 - Sat 02:00</td>
                                                            <td>Mon 06:10 - Sat 03:00</td>
                                                            <td>04:00-05:00</td>
                                                            <td>05:00-06:00</td>
                                                        </tr>
                                                        <tr>
                                                            <td>AUD/NZD</td>
                                                            <td>FX</td>
                                                            <td>50</td>
                                                            <td>6,6</td>
                                                            <td>1,2</td>
                                                            <td>-0.75</td>
                                                            <td>1.000</td>
                                                            <td>1.000</td>
                                                            <td>200</td>
                                                            <td>n/a</td>
                                                            <td>20%</td>
                                                            <td>GTF</td>
                                                            <td>Mon 06:10 - Sat 02:00</td>
                                                            <td>Mon 06:10 - Sat 03:00</td>
                                                            <td>04:00-05:00</td>
                                                            <td>05:00-06:00</td>
                                                        </tr>
                                                        <tr>
                                                            <td>AUD/USD</td>
                                                            <td>FX</td>
                                                            <td>50</td>
                                                            <td>10</td>
                                                            <td>0,8</td>
                                                            <td>-1,50%</td>
                                                            <td>1.000</td>
                                                            <td>1.000</td>
                                                            <td>200</td>
                                                            <td>n/a</td>
                                                            <td>20%</td>
                                                            <td>GTF</td>
                                                            <td>Mon 06:10 - Sat 02:00</td>
                                                            <td>Mon 06:10 - Sat 03:00</td>
                                                            <td>04:00-05:00</td>
                                                            <td>05:00-06:00</td>
                                                        </tr>
                                                        <tr>
                                                            <td>CAD/JPY</td>
                                                            <td>FX</td>
                                                            <td>50</td>
                                                            <td>9,1</td>
                                                            <td>1,2</td>
                                                            <td>0,00%</td>
                                                            <td>1.000</td>
                                                            <td>1.000</td>
                                                            <td>200</td>
                                                            <td>n/a</td>
                                                            <td>20%</td>
                                                            <td>GTF</td>
                                                            <td>Mon 06:10 - Sat 02:00</td>
                                                            <td>Mon 06:10 - Sat 03:00</td>
                                                            <td>04:00-05:00</td>
                                                            <td>05:00-06:00</td>
                                                        </tr>
                                                        <tr>
                                                            <td>CHF/JPY</td>
                                                            <td>FX</td>
                                                            <td>50</td>
                                                            <td>9,1</td>
                                                            <td>1,2</td>
                                                            <td>0,00%</td>
                                                            <td>1.000</td>
                                                            <td>1.000</td>
                                                            <td>200</td>
                                                            <td>n/a</td>
                                                            <td>20%</td>
                                                            <td>GTF</td>
                                                            <td>Mon 06:10 - Sat 02:00</td>
                                                            <td>Mon 06:10 - Sat 03:00</td>
                                                            <td>04:00-05:00</td>
                                                            <td>05:00-06:00</td>
                                                        </tr>
                                                        <tr>
                                                            <td>EUR/AUD</td>
                                                            <td>FX</td>
                                                            <td>50</td>
                                                            <td>6,9</td>
                                                            <td>1,2</td>
                                                            <td>-0,50%</td>
                                                            <td>1.000</td>
                                                            <td>1.000</td>
                                                            <td>200</td>
                                                            <td>n/a</td>
                                                            <td>20%</td>
                                                            <td>GTF</td>
                                                            <td>Mon 06:10 - Sat 02:00</td>
                                                            <td>Mon 06:10 - Sat 03:00</td>
                                                            <td>04:00-05:00</td>
                                                            <td>05:00-06:00</td>
                                                        </tr>
                                                        <tr>
                                                            <td>EUR/CAD</td>
                                                            <td>FX</td>
                                                            <td>50</td>
                                                            <td>7,6</td>
                                                            <td>1,2</td>
                                                            <td>-0,25%</td>
                                                            <td>1.000</td>
                                                            <td>1.000</td>
                                                            <td>200</td>
                                                            <td>n/a</td>
                                                            <td>20%</td>
                                                            <td>GTF</td>
                                                            <td>Mon 06:10 - Sat 02:00</td>
                                                            <td>Mon 06:10 - Sat 03:00</td>
                                                            <td>04:00-05:00</td>
                                                            <td>05:00-06:00</td>
                                                        </tr>
                                                        <tr>
                                                            <td>EUR/CHF</td>
                                                            <td>FX</td>
                                                            <td>50</td>
                                                            <td>10</td>
                                                            <td>1,2</td>
                                                            <td>-1,00%</td>
                                                            <td>1.000</td>
                                                            <td>1.000</td>
                                                            <td>200</td>
                                                            <td>n/a</td>
                                                            <td>20%</td>
                                                            <td>GTF</td>
                                                            <td>Mon 06:10 - Sat 02:00</td>
                                                            <td>Mon 06:10 - Sat 03:00</td>
                                                            <td>04:00-05:00</td>
                                                            <td>05:00-06:00</td>
                                                        </tr>
                                                        <tr>
                                                            <td>EUR/GBP</td>
                                                            <td>FX</td>
                                                            <td>50</td>
                                                            <td>13</td>
                                                            <td>1,2</td>
                                                            <td>-0,50%</td>
                                                            <td>1.000</td>
                                                            <td>1.000</td>
                                                            <td>200</td>
                                                            <td>n/a</td>
                                                            <td>20%</td>
                                                            <td>GTF</td>
                                                            <td>Mon 06:10 - Sat 02:00</td>
                                                            <td>Mon 06:10 - Sat 03:00</td>
                                                            <td>04:00-05:00</td>
                                                            <td>05:00-06:00</td>
                                                        </tr>
                                                        <tr>
                                                            <td>EUR/JPY</td>
                                                            <td>FX</td>
                                                            <td>50</td>
                                                            <td>9,1</td>
                                                            <td>1,2</td>
                                                            <td>0,00%</td>
                                                            <td>1.000</td>
                                                            <td>1.000</td>
                                                            <td>200</td>
                                                            <td>n/a</td>
                                                            <td>20%</td>
                                                            <td>GTF</td>
                                                            <td>Mon 06:10 - Sat 02:00</td>
                                                            <td>Mon 06:10 - Sat 03:00</td>
                                                            <td>04:00-05:00</td>
                                                            <td>05:00-06:00</td>
                                                        </tr>
                                                        <tr>
                                                            <td>EUR/USD</td>
                                                            <td>FX</td>
                                                            <td>50</td>
                                                            <td>10</td>
                                                            <td>0,6</td>
                                                            <td>-0,01%</td>
                                                            <td>1.000</td>
                                                            <td>1.000</td>
                                                            <td>200</td>
                                                            <td>n/a</td>
                                                            <td>20%</td>
                                                            <td>GTF</td>
                                                            <td>Mon 06:10 - Sat 02:00</td>
                                                            <td>Mon 06:10 - Sat 03:00</td>
                                                            <td>04:00-05:00</td>
                                                            <td>05:00-06:00</td>
                                                        </tr>
                                                        <tr>
                                                            <td>GBP/AUD</td>
                                                            <td>FX</td>
                                                            <td>50</td>
                                                            <td>6,9</td>
                                                            <td>1,2</td>
                                                            <td>-0,75%</td>
                                                            <td>1.000</td>
                                                            <td>1.000</td>
                                                            <td>200</td>
                                                            <td>n/a</td>
                                                            <td>20%</td>
                                                            <td>GTF</td>
                                                            <td>Mon 06:10 - Sat 02:00</td>
                                                            <td>Mon 06:10 - Sat 03:00</td>
                                                            <td>04:00-05:00</td>
                                                            <td>05:00-06:00</td>
                                                        </tr>
                                                        <tr>
                                                            <td>GBP/CHF</td>
                                                            <td>FX</td>
                                                            <td>50</td>
                                                            <td>10</td>
                                                            <td>1,2</td>
                                                            <td>-1,00%</td>
                                                            <td>1.000</td>
                                                            <td>1.000</td>
                                                            <td>200</td>
                                                            <td>n/a</td>
                                                            <td>20%</td>
                                                            <td>GTF</td>
                                                            <td>Mon 06:10 - Sat 02:00</td>
                                                            <td>Mon 06:10 - Sat 03:00</td>
                                                            <td>04:00-05:00</td>
                                                            <td>05:00-06:00</td>
                                                        </tr>
                                                        <tr>
                                                            <td>GBP/JPY</td>
                                                            <td>FX</td>
                                                            <td>50</td>
                                                            <td>9,1</td>
                                                            <td>1,2</td>
                                                            <td>0,00%</td>
                                                            <td>1.000</td>
                                                            <td>1.000</td>
                                                            <td>200</td>
                                                            <td>n/a</td>
                                                            <td>20%</td>
                                                            <td>GTF</td>
                                                            <td>Mon 06:10 - Sat 02:00</td>
                                                            <td>Mon 06:10 - Sat 03:00</td>
                                                            <td>04:00-05:00</td>
                                                            <td>05:00-06:00</td>
                                                        </tr>
                                                        <tr>
                                                            <td>GBP/USD</td>
                                                            <td>FX</td>
                                                            <td>50</td>
                                                            <td>10</td>
                                                            <td>0,8</td>
                                                            <td>-0,50%</td>
                                                            <td>1.000</td>
                                                            <td>1.000</td>
                                                            <td>200</td>
                                                            <td>n/a</td>
                                                            <td>20%</td>
                                                            <td>GTF</td>
                                                            <td>Mon 06:10 - Sat 02:00</td>
                                                            <td>Mon 06:10 - Sat 03:00</td>
                                                            <td>04:00-05:00</td>
                                                            <td>05:00-06:00</td>
                                                        </tr>
                                                        <tr>
                                                            <td>NZD/CAD</td>
                                                            <td>FX</td>
                                                            <td>50</td>
                                                            <td>7,6</td>
                                                            <td>1,2</td>
                                                            <td>0,00%</td>
                                                            <td>1.000</td>
                                                            <td>1.000</td>
                                                            <td>200</td>
                                                            <td>n/a</td>
                                                            <td>20%</td>
                                                            <td>GTF</td>
                                                            <td>Mon 06:10 - Sat 02:00</td>
                                                            <td>Mon 06:10 - Sat 03:00</td>
                                                            <td>04:00-05:00</td>
                                                            <td>05:00-06:00</td>
                                                        </tr>
                                                        <tr>
                                                            <td>NZD/JPY</td>
                                                            <td>FX</td>
                                                            <td>50</td>
                                                            <td>9,1</td>
                                                            <td>1,2</td>
                                                            <td>0,00%</td>
                                                            <td>1.000</td>
                                                            <td>1.000</td>
                                                            <td>200</td>
                                                            <td>n/a</td>
                                                            <td>20%</td>
                                                            <td>GTF</td>
                                                            <td>Mon 06:10 - Sat 02:00</td>
                                                            <td>Mon 06:10 - Sat 03:00</td>
                                                            <td>04:00-05:00</td>
                                                            <td>05:00-06:00</td>
                                                        </tr>
                                                        <tr>
                                                            <td>NZD/USD</td>
                                                            <td>FX</td>
                                                            <td>50</td>
                                                            <td>10</td>
                                                            <td>0,8</td>
                                                            <td>0,00%</td>
                                                            <td>1.000</td>
                                                            <td>1.000</td>
                                                            <td>200</td>
                                                            <td>n/a</td>
                                                            <td>20%</td>
                                                            <td>GTF</td>
                                                            <td>Mon 06:10 - Sat 02:00</td>
                                                            <td>Mon 06:10 - Sat 03:00</td>
                                                            <td>04:00-05:00</td>
                                                            <td>05:00-06:00</td>
                                                        </tr>
                                                        <tr>
                                                            <td>USD/CAD</td>
                                                            <td>FX</td>
                                                            <td>50</td>
                                                            <td>7,6</td>
                                                            <td>0,8</td>
                                                            <td>-1,00%</td>
                                                            <td>1.000</td>
                                                            <td>1.000</td>
                                                            <td>200</td>
                                                            <td>n/a</td>
                                                            <td>20%</td>
                                                            <td>GTF</td>
                                                            <td>Mon 06:10 - Sat 02:00</td>
                                                            <td>Mon 06:10 - Sat 03:00</td>
                                                            <td>04:00-05:00</td>
                                                            <td>05:00-06:00</td>
                                                        </tr>
                                                        <tr>
                                                            <td>USD/CHF</td>
                                                            <td>FX</td>
                                                            <td>50</td>
                                                            <td>10</td>
                                                            <td>0,8</td>
                                                            <td>0,00%</td>
                                                            <td>1.000</td>
                                                            <td>1.000</td>
                                                            <td>200</td>
                                                            <td>n/a</td>
                                                            <td>20%</td>
                                                            <td>GTF</td>
                                                            <td>Mon 06:10 - Sat 02:00</td>
                                                            <td>Mon 06:10 - Sat 03:00</td>
                                                            <td>04:00-05:00</td>
                                                            <td>05:00-06:00</td>
                                                        </tr>
                                                        <tr>
                                                            <td>USD/JPY</td>
                                                            <td>FX</td>
                                                            <td>50</td>
                                                            <td>9,1</td>
                                                            <td>0,8</td>
                                                            <td>-1,00%</td>
                                                            <td>1.000</td>
                                                            <td>1.000</td>
                                                            <td>200</td>
                                                            <td>n/a</td>
                                                            <td>20%</td>
                                                            <td>GTF</td>
                                                            <td>Mon 06:10 - Sat 02:00</td>
                                                            <td>Mon 06:10 - Sat 03:00</td>
                                                            <td>04:00-05:00</td>
                                                            <td>05:00-06:00</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <H3><strong>26. Jam Perdagangan</strong></H3>
                                            <p class="text-justify">
                                                Produk Forex dapat diperdagangkan selama 23 jam per hari. Selama periode ini berlangsung, nasabah dapat membuka dan menutup posisi dan juga menaruh limit dan stop orders. Ketika perdanganan ditutup, nasabah tidak akan dapat untuk membuka posisi, limit atau stop order.
                                            </p>
                                            <H3><strong>27. Kuotasi Harga</strong></H3>
                                            <p class="text-justify">
                                                Tujuan kami adalah memberi Anda biaya transaksi yang sangat kompetitif — spread bid / ask ketat pada setiap produk FX. Spread FX adalah spread mengambang.
                                            </p>
                                            <H3><strong>28. Contract Size/Trade Size</strong></H3>
                                            <p class="text-justify">
                                                MAXCO FUTURES menggunakan sistem perdagangan 'berbasis lot'. Ini berarti bahwa semua produk MAXCO FUTURES digabungkan ke dalam ukuran perdagangan standar. Ukuran ini umumnya diterima secara internasional untuk perdagangan spot FX. Ini menyederhanakan perdagangan dengan memungkinkan klien untuk melakukan perdagangan dalam peningkatan lot, dan juga, memberikan harga untuk setiap ukuran lot daripada rata-rata harga buka dan tutup ketika banyak posisi diambil dalam instrumen yang sama. Nilai tick atau pip dikaitkan dengan setiap lot untuk perhitungan yang tepat, dan keuntungan dan kerugian pada suatu posisi secara otomatis dikonversi ke dalam mata uang akun tertentu.
                                            </p>
                                            <p class="text-justify">
                                                Bahkan ketika perdagangan mata uang silang berpasangan (cross-rate), keuntungan atau kerugian akan secara otomatis dikonversi ke mata uang dalam mata uang akun untuk menghindari adanya selisih.
                                            </p>
                                            <H3><strong>28.1 Jumlah Perdagangan Maksimum</strong></H3>
                                            <p class="text-justify">
                                                MAXCO FUTURES dapat mengurangi standard lot maksimum per perdagangan atas kebijakannya sendiri untuk alasan manajemen risiko yang tepat.
                                            </p>
                                            <H3><strong>28.2 Overtrading</strong></H3>
                                            <p class="text-justify">
                                                Dalam hal over-trading, posisi akan dilikuidasi dengan harga pasar.
                                            </p>
                                            <h3><strong>29. Limit Orders & Stop Orders</strong></h3>
                                            <p class="text-justify">
                                                Limit Order & Stop Order dapat dimasukkan, dimodifikasi, atau dibatalkan kapan saja selama sesi perdagangan aktif. Selama waktu istirahat, liburan dan akhir pekan nasabah tidak akan bisa mengeksekusi/melaksanakan hal-hal tersebut.
                                            </p>
                                            <p class="text-justify">
                                                Semua Limit Order & Stop Order akan dihapus setelah sesi perdagangan mingguan selesai pada Sabtu pagi atau hari sebelumnya jika perdagangan terakhir dalam minggu tersebut adalah hari libur.
                                            </p>
                                            <p class="text-justify">
                                                Limit Order akan dieksekusi pada harga permintaan ketika harga Bid atau Ask masing-masing tersentuh.
                                            </p>
                                            <p class="text-justify">
                                                Stop Order akan terlaksanakan pada harga pasar ketika harga Bid atau Ask tersentuh, dan perlu dicatat bahwa sewaktu-waktu pasar dapat bergerak dengan volatilitas yang tinggi sehingga dapat menyebabkan slip harga pada saat eksekusi oleh sistem trading PT. Maxco Futures. Apabila ketika pasar dalam kondisi yang tidak normal/atau tidak menentu, maka pada situasi ini ada kemungkinan terjadinya market gap sehingga Stop Order akan tereksekusi di harga gap price terbaru mengingat tidak adanya likuiditas sehingga menyebabkan fluktuasi harga yang tidak wajar.
                                            </p>
                                            <!-- Minimum Stop Distance (Points) -->
                                            <h3><strong>30. Nilai Pip</strong></h3>
                                            <p class="text-justify">
                                            Nilai Pip ditentukan oleh ukuran lot perdagangan. Pergerakan harga minimum dianggap sebagai 1 pip, untuk pasangan FX yang dikutip dengan 5 digit setelah desimal, digit ke-4 dihitung sebagai satu pip penuh. Pip umumnya 1/1000 sen. Misalnya, EUR / USD Bid 1.20018 Ask 1.20029, maka spreadnya adalah 1.1 pips.
                                            </p>
                                            <p class="text-justify">
                                            Pasangan JPY adalah pengecualian, di mana seluruh pip berada pada digit ke-2 setelah desimal. Semua pasangan JPY dikutip hingga 1/100 sen. Misalnya, Bid GBP / JPY 134.068 134.082,Ask maka spread adalah 1,4 pips.
                                            </p>
                                            <p class="text-justify">
                                            Semua pasangan FX dikutip pada 1/10 pip.
                                            </p>
                                            <h3><strong>31. Persyaratan Margin Minimum</strong></h3>
                                            <p class="text-justify">
                                            Tingkat Margin MAXCO FUTURES ditampilkan di jendela nilai tukar pada TSII dan merinci kewajiban modal klien untuk membeli atau menjual ukuran perdagangan minimum. MAXCO FUTURES memiliki ukuran perdagangan minimum / tambahan standar untuk setiap instrumen. Untuk menghitung margin yang diperlukan untuk menempatkan ukuran perdagangan minimum, ukuran perdagangan dengan margin yang diperlukan (per kontrak), yang ditampilkan dalam kurs transaksi.
                                            </p>
                                            <p class="text-justify">
                                            Detail margin adalah persyaratan margin default MAXCO FUTURES. Jika Anda ingin mengubah persyaratan margin Anda, silakan hubungi Perwakilan Akun di MAXCO FUTURES.
                                            </p>
                                            <h3><strong>31.1 Over the Weekend Margin Requirements</strong></h3>
                                            <p class="text-justify">
                                            Syarat Margin Akhir Pekan (Over the Weekend) adalah ketika posisi FX akan diadakan selama akhir pekan setelah pasar tutup pada hari Sabtu. Mengingat risiko yang lebih tinggi selama perdagangan akhir pekan, margin dapat dinaikkan dari waktu ke waktu tergantung pada kondisi pasar dan volatilitas. Selama akhir pekan kenaikan margin dapat segera berlaku dengan pemberitahuan minimal, seperti pada hari Jumat.
                                            </p>
                                            <p class="text-justify">
                                            Over the Weekend margin dapat mencapai EMPAT kali margin perdagangan hari tergantung pada kondisi pasar. MAXCO FUTURES memiliki hak untuk meningkatkan margin semalam lebih lanjut jika dianggap perlu untuk meminimalkan risiko kerugian lebih kepada pemegang rekening.
                                            </p>
                                            <p class="text-justify">
                                            Jika Anda ingin tidak menerapkan aturan margin akhir pekan ke akun Anda, silakan hubungi MAXCO FUTURES untuk mengajukan permintaan ini. Permintaan disetujui secara individual dan MAXCO FUTURES memiliki keleluasaan untuk menolak atau membalikkan persetujuan.
                                            </p>
                                            <h3><strong>32.2 Margin Call</strong></h3>
                                            <p class="text-justify">
                                            Semua posisi semalam harus mempertahankan margin yang cukup jika tidak semua posisi dalam akun dapat dilikuidasi jika tingkat margin bebas akun menyentuh tingkat likuidasi 20%. Jika terjadi kelalaian dalam penyelesaian atau margin minimum yang tidak terpenuhi atau setoran yang tidak diselesaikan karena alasan apa pun, MAXCO FUTURES, atas kebijakannya sendiri berhak untuk melikuidasi sebagian atau semua posisi pada harga pasar ketika tingkat likuidasi tercapai.
                                            </p>
                                            <p class="text-justify">
                                            Jika margin call dipicu oleh open gap maka tidak menutup kemungkinan adanya risiko saldo negatif karena tingkat kesenjangan harga.
                                            </p>
                                            <h3><strong>Spread Minimum</strong></h3>
                                           
                                            <p class="text-justify">
                                            Tujuan kami adalah memberi Anda biaya transaksi yang sangat kompetitif — spread bid / ask ketat pada setiap produk FX. Spread FX adalah spread mengambang.
                                            </p>
                                            <h3><strong>34. Overnight Swaps (Swap)</strong></h3>
                                            
                                            <p class="text-justify">
                                            Semua posisi terbuka akan digulir ke hari perdagangan berikutnya. Tergantung pada apakah Anda mempunyai long (beli) atau short (jual). Biaya ini akan didebit atau dikreditkan ke akun Anda setiap hari. Detail dari nilai tukar MAXCO FUTURES diperinci di platform kami secara transparan. Harap dicatat bahwa semua posisi terbuka pada penutupan Rabu pukul 17.00 EST menimbulkan debit / kredit rollover 3 hari, dan hari libur bank akan memengaruhi jumlah hari suatu posisi bergulir.
                                            </p>
                                            <h3><strong>35. Expiration</strong></h3>
                                            <p class="text-justify">
                                            Semua perdagangan FX akan tetap terbuka sampai klien menutup posisi atau ada margin yang tidak cukup untuk mendukung posisi terbuka. Dalam hal ini akan ditutup oleh pihak MAXCO FUTURES.
                                            </p>
                                        </div>

                                        <div class="form-group position-relative error-l-100 text-justify">
                                            <label>
                                                Dengan menigisi kolom "YA" di bawah ini, saya
                                                menyatakan
                                                bahwa saya telah membaca tentang PERATURAN PERDAGANGAN (Trading
                                                Rules)
                                                mengerti dan menerima ketentuan dalam bertransaksi. Demikian
                                                Pernyataan
                                                ini
                                                dibuat dengan sebenarnya dalam keadaan sadar,sehat jasmani dan
                                                rohani
                                                serta
                                                tanpa paksaan apapun dari pihak manapun. Penyataan
                                                menerima
                                            </label>
                                            <div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="trading_rules" value="yes" onchange="savekyc()" onkeypress="savekyc()" onclick="savekyc()" @isset($DataContent->trading_rules) @if($DataContent->trading_rules==='yes') checked @endif @endisset/>
                                                    <label class="form-check-label required">Ya</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="trading_rules" value="no" onchange="savekyc()" onkeypress="savekyc()" onclick="savekyc()" @isset($DataContent->trading_rules) @if($DataContent->trading_rules==='no') checked @endif @endisset/>
                                                    <label class="form-check-label" style="margin-top: 0.7rem !important;">Tidak</label>
                                                </div><br>
                                                <label id="trading_rules-error" class="error" for="trading_rules" style="display: none;">Klik "Ya" untuk melanjutkan.</label>

                                            </div>

                                            <label>Menyatakan pada tanggal : {{date('d-m-Y',strtotime(date("m.d.y")))}}</label>
                                        </div>



                                    </div>
                                </div>
                                <div class="btn-toolbar custom-toolbar text-center card-body pt-0 mb-4 justify-content-md-center">
                                    <button class="btn btn-secondary prev-btn" type="button">Previous</button>
                                    <button onclick="event.preventDefault();" class="btn-loading-prev btn btn-secondary hidden">
                                        <span class="loader-small" style="position: unset !important;"></span> Previous
                                    </button>
                                    <input class="btn btn-secondary next-btn" type="submit" value="Next" />
                                    <button onclick="event.preventDefault();" class="btn-loading-next btn btn-secondary hidden">
                                        <span class="loader-small" style="position: unset !important;"></span> Next
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection

@section('jsonpage')
<script src="{{url('/')}}/cabinet/js/vendor/jquery.smartWizard.min.js"></script>
<script src="{{url('/')}}/cabinet/js/vendor/dropzone.min.js"></script>
<script src="{{url('/')}}/cabinet/js/vendor/sweetalert.min.js"></script>

<script>
    $('[name="trading_rules"]').on('change', function(event) {
        var val = event.currentTarget.value;
        if (val === 'no') {
            $('#trading_rules-error').show();
            $('#trading_rules-error').text('Klik "Ya" untuk melanjutkan.')
        } else {
            $('#trading_rules-error').hide();

        }
    });

    $('.prev-btn').on('click', function() {
        $('.prev-btn').hide();
        $('.btn-loading-prev').show();
        $.post("{{route('postNextStepOpenLiveAccount')}}", {
            NBCRMWEBAPI: $('[name="NBCRMWEBAPI"]').val(),
            USERID: $('[name="USERID"]').val(),
            nextStep: '4',
            page: '1'
        }, function(data, status) {
            if (status === "success") {
                location.reload();
            } else {
                $('.prev-btn').show();
                $('.btn-loading-prev').hide();
            }
        });
    });

    function getContent() {
        var contentDt = {
            'job_details': {
                'more_details': {
                    'length_of_work': "{{isset($DataContent->job_details) && !empty($DataContent->job_details->more_details->length_of_work)?$DataContent->job_details->more_details->length_of_work:''}}",
                    'previous_employer': "{{isset($DataContent->job_details) && !empty($DataContent->job_details->more_details->previous_employer)?$DataContent->job_details->more_details->previous_employer:0}}",
                    'employer_name': "{{isset($DataContent->job_details) && !empty($DataContent->job_details->more_details->employer_name)?$DataContent->job_details->more_details->employer_name:''}}",
                    'business_fields': "{{isset($DataContent->job_details) && !empty($DataContent->job_details->more_details->business_fields)?$DataContent->job_details->more_details->business_fields:''}}",
                    'title': "{{isset($DataContent->job_details) && !empty($DataContent->job_details->more_details->title)?$DataContent->job_details->more_details->title:''}}",
                    'office_address': "{{isset($DataContent->job_details) && !empty($DataContent->job_details->more_details->office_address)?$DataContent->job_details->more_details->office_address:''}}",
                    'office_tele': "{{isset($DataContent->job_details) && !empty($DataContent->job_details->more_details->office_tele)?$DataContent->job_details->more_details->office_tele:''}}",
                    'office_fax': 0
                },
                'position': {
                    'value': "{{isset($DataContent->job_details)&&!empty($DataContent->job_details->position->value)?$DataContent->job_details->position->value:''}}",
                    'input': "{{isset($DataContent->job_details)&&!empty($DataContent->job_details->position->input)?$DataContent->job_details->position->input:''}}",
                }
            },
            'wealth_list': {
                'income_per_year': "{{isset($DataContent->wealth_list)&&!empty($DataContent->wealth_list->income_per_year)?$DataContent->wealth_list->income_per_year:''}}",
                'residence_location': "{{isset($DataContent->wealth_list)&&!empty($DataContent->wealth_list->residence_location)?$DataContent->wealth_list->residence_location:''}}",
                'tax_object_selling_value': "{{isset($DataContent->wealth_list)&&!empty($DataContent->wealth_list->tax_object_selling_value)?$DataContent->wealth_list->tax_object_selling_value:''}}",
                'bank_time_deposit_value': "{{isset($DataContent->wealth_list)&&!empty($DataContent->wealth_list->bank_time_deposit_value)?$DataContent->wealth_list->bank_time_deposit_value:''}}",
                'bank_saving_value': "{{isset($DataContent->wealth_list)&&!empty($DataContent->wealth_list->bank_saving_value)?$DataContent->wealth_list->bank_saving_value:''}}",
                'others': "{{isset($DataContent->wealth_list)&&!empty($DataContent->wealth_list->others)?$DataContent->wealth_list->others:''}}",
            },
            'bank_details_for_withdrawls': {
                'full_name': "{{isset($DataContent->bank_details_for_withdrawls)&&!empty($DataContent->bank_details_for_withdrawls->full_name)?$DataContent->bank_details_for_withdrawls->full_name:''}}",
                'branch': "{{isset($DataContent->bank_details_for_withdrawls)&&!empty($DataContent->bank_details_for_withdrawls->branch)?$DataContent->bank_details_for_withdrawls->branch:''}}",
                'account_number': "{{isset($DataContent->bank_details_for_withdrawls)&&!empty($DataContent->bank_details_for_withdrawls->account_number)?$DataContent->bank_details_for_withdrawls->account_number:''}}",
                'phone_number': 0,
                'account_type': {
                    'value': "{{isset($DataContent->bank_details_for_withdrawls)&&!empty($DataContent->bank_details_for_withdrawls->account_type->value)?$DataContent->bank_details_for_withdrawls->account_type->value:''}}",
                    'input': "{{isset($DataContent->bank_details_for_withdrawls)&&!empty($DataContent->bank_details_for_withdrawls->account_type->input)?$DataContent->bank_details_for_withdrawls->account_type->input:''}}",
                }
            },
            'emergency_contact_data': {
                'name': "{{isset($DataContent->emergency_contact_data)&&!empty($DataContent->emergency_contact_data->name)?$DataContent->emergency_contact_data->name:''}}",
                'address': "{{isset($DataContent->emergency_contact_data)&&!empty($DataContent->emergency_contact_data->address)?$DataContent->emergency_contact_data->address:''}}",
                'post_code': "{{isset($DataContent->emergency_contact_data)&&!empty($DataContent->emergency_contact_data->post_code)?$DataContent->emergency_contact_data->post_code:''}}",
                'home_phone_number': "{{isset($DataContent->emergency_contact_data)&&!empty($DataContent->emergency_contact_data->home_phone_number)?$DataContent->emergency_contact_data->home_phone_number:''}}",
                'relationship_with_the_contacts': "{{isset($DataContent->emergency_contact_data)&&!empty($DataContent->emergency_contact_data->relationship_with_the_contacts)?$DataContent->emergency_contact_data->relationship_with_the_contacts:''}}"
            },
            'attached_documents': {
                'id_card': true,
                'coloured_photo': true,
                'specimen_signature': true,
                'other_count': 1,
                'required_image': [{
                        'type': 'id_card',
                        'title': 'id_card_or_driving_license_passport',
                        'action_type': 0,
                        'src': {
                            'name': "{{!empty($DataContent->attached_documents)?$DataContent->attached_documents->required_image[0]->src->name:''}}",
                            'size': 116,
                            'url': "{{!empty($DataContent->attached_documents)?$DataContent->attached_documents->required_image[0]->src->url:''}}",
                        }
                    },
                    {
                        'type': 'coloured_photo',
                        'title': 'coloured_photo',
                        'action_type': 0,
                        'src': {
                            'name': "{{!empty($DataContent->attached_documents)?$DataContent->attached_documents->required_image[1]->src->name:''}}",
                            'size': 116,
                            'url': "{{!empty($DataContent->attached_documents)?$DataContent->attached_documents->required_image[1]->src->url:''}}",
                        }
                    },
                    {
                        'type': 'specimen_signature',
                        'title': 'specimen_signature',
                        'action_type': 0,
                        'src': {
                            'name': "{{!empty($DataContent->attached_documents)?$DataContent->attached_documents->required_image[2]->src->name:''}}",
                            'size': 116,
                            'url': "{{!empty($DataContent->attached_documents)?$DataContent->attached_documents->required_image[2]->src->url:''}}",
                        }
                    },
                    {
                        'type': 'other',
                        'title': 'other',
                        'action_type': 0,
                        'src': {
                            'name': "{{!empty($DataContent->attached_documents)?$DataContent->attached_documents->required_image[3]->src->name:''}}",
                            'size': 116,
                            'url': "{{!empty($DataContent->attached_documents)?$DataContent->attached_documents->required_image[3]->src->url:''}}",
                        }
                    }
                ],
                'option': 'yes'
            },
            'trading_statement': {
                'statement_for_trading_simulation_on_demo_account': "{{isset($DataContent->trading_statement)&&!empty($DataContent->trading_statement->statement_for_trading_simulation_on_demo_account)?$DataContent->trading_statement->statement_for_trading_simulation_on_demo_account:''}}",
                'statement_letter_has_been_experienced_on_trading': "{{isset($DataContent->trading_statement)&&!empty($DataContent->trading_statement->statement_letter_has_been_experienced_on_trading)?$DataContent->trading_statement->statement_letter_has_been_experienced_on_trading:''}}",
                'statement_has_been_received_information_futures_company': "{{isset($DataContent->trading_statement)&&!empty($DataContent->trading_statement->statement_has_been_received_information_futures_company)?$DataContent->trading_statement->statement_has_been_received_information_futures_company:''}}",
            },
            'risk_document': {
                'risk_1_title_en_ref': "{{isset($DataContent->risk_document)&&!empty($DataContent->risk_document->risk_1_title_en_ref)?$DataContent->risk_document->risk_1_title_en_ref:''}}",
                'risk_2_title_en_ref': "{{isset($DataContent->risk_document)&&!empty($DataContent->risk_document->risk_2_title_en_ref)?$DataContent->risk_document->risk_2_title_en_ref:''}}",
                'risk_3_title_en_ref': "{{isset($DataContent->risk_document)&&!empty($DataContent->risk_document->risk_3_title_en_ref)?$DataContent->risk_document->risk_3_title_en_ref:''}}",
                'risk_4_title_en_ref': "{{isset($DataContent->risk_document)&&!empty($DataContent->risk_document->risk_4_title_en_ref)?$DataContent->risk_document->risk_4_title_en_ref:''}}",
                'risk_5_title_en_ref': "{{isset($DataContent->risk_document)&&!empty($DataContent->risk_document->risk_5_title_en_ref)?$DataContent->risk_document->risk_5_title_en_ref:''}}",
                'risk_6_title_en_ref': "{{isset($DataContent->risk_document)&&!empty($DataContent->risk_document->risk_6_title_en_ref)?$DataContent->risk_document->risk_6_title_en_ref:''}}",
                'risk_7_title_en_ref': "{{isset($DataContent->risk_document)&&!empty($DataContent->risk_document->risk_7_title_en_ref)?$DataContent->risk_document->risk_7_title_en_ref:''}}",
                'risk_8_title_en_ref': "{{isset($DataContent->risk_document)&&!empty($DataContent->risk_document->risk_8_title_en_ref)?$DataContent->risk_document->risk_8_title_en_ref:''}}",
                'risk_9_title_en_ref': "{{isset($DataContent->risk_document)&&!empty($DataContent->risk_document->risk_9_title_en_ref)?$DataContent->risk_document->risk_9_title_en_ref:''}}",
                'risk_10_title_en_ref': "{{isset($DataContent->risk_document)&&!empty($DataContent->risk_document->risk_10_title_en_ref)?$DataContent->risk_document->risk_10_title_en_ref:''}}",
                'risk_11_title_en_ref': "{{isset($DataContent->risk_document)&&!empty($DataContent->risk_document->risk_11_title_en_ref)?$DataContent->risk_document->risk_11_title_en_ref:''}}",
                'risk_12_title_en_ref': "{{isset($DataContent->risk_document)&&!empty($DataContent->risk_document->risk_12_title_en_ref)?$DataContent->risk_document->risk_12_title_en_ref:''}}",
                'risk_13_title_en_ref': "{{isset($DataContent->risk_document)&&!empty($DataContent->risk_document->risk_13_title_en_ref)?$DataContent->risk_document->risk_13_title_en_ref:''}}",
                'radio': "{{isset($DataContent->risk_document)&&!empty($DataContent->risk_document->radio)?$DataContent->risk_document->radio:''}}",
            },
            'trading_condition': {
                'trading_conditions_1_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_1_title_en_ref)?$DataContent->trading_condition->trading_conditions_1_title_en_ref:''}}",
                'trading_conditions_2_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_2_title_en_ref)?$DataContent->trading_condition->trading_conditions_2_title_en_ref:''}}",
                'trading_conditions_3_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_3_title_en_ref)?$DataContent->trading_condition->trading_conditions_3_title_en_ref:''}}",
                'trading_conditions_4_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_4_title_en_ref)?$DataContent->trading_condition->trading_conditions_4_title_en_ref:''}}",
                'trading_conditions_5_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_5_title_en_ref)?$DataContent->trading_condition->trading_conditions_5_title_en_ref:''}}",
                'trading_conditions_6_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_6_title_en_ref)?$DataContent->trading_condition->trading_conditions_6_title_en_ref:''}}",
                'trading_conditions_7_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_7_title_en_ref)?$DataContent->trading_condition->trading_conditions_7_title_en_ref:''}}",
                'trading_conditions_8_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_8_title_en_ref)?$DataContent->trading_condition->trading_conditions_8_title_en_ref:''}}",
                'trading_conditions_9_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_9_title_en_ref)?$DataContent->trading_condition->trading_conditions_9_title_en_ref:''}}",
                'trading_conditions_10_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_10_title_en_ref)?$DataContent->trading_condition->trading_conditions_10_title_en_ref:''}}",
                'trading_conditions_11_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_11_title_en_ref)?$DataContent->trading_condition->trading_conditions_11_title_en_ref:''}}",
                'trading_conditions_12_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_12_title_en_ref)?$DataContent->trading_condition->trading_conditions_12_title_en_ref:''}}",
                'trading_conditions_13_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_13_title_en_ref)?$DataContent->trading_condition->trading_conditions_13_title_en_ref:''}}",
                'trading_conditions_14_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_14_title_en_ref)?$DataContent->trading_condition->trading_conditions_14_title_en_ref:''}}",
                'trading_conditions_15_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_15_title_en_ref)?$DataContent->trading_condition->trading_conditions_15_title_en_ref:''}}",
                'trading_conditions_16_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_16_title_en_ref)?$DataContent->trading_condition->trading_conditions_16_title_en_ref:''}}",
                'trading_conditions_17_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_17_title_en_ref)?$DataContent->trading_condition->trading_conditions_17_title_en_ref:''}}",
                'trading_conditions_18_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_18_title_en_ref)?$DataContent->trading_condition->trading_conditions_18_title_en_ref:''}}",
                'trading_conditions_19_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_19_title_en_ref)?$DataContent->trading_condition->trading_conditions_19_title_en_ref:''}}",
                'trading_conditions_20_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_20_title_en_ref)?$DataContent->trading_condition->trading_conditions_20_title_en_ref:''}}",
                'trading_conditions_21_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_21_title_en_ref)?$DataContent->trading_condition->trading_conditions_21_title_en_ref:''}}",
                'trading_conditions_22_title_1': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_22_title_1)?$DataContent->trading_condition->trading_conditions_22_title_1:'0'}}",
                'trading_conditions_22_title_2': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_22_title_2)?$DataContent->trading_condition->trading_conditions_22_title_2:'0'}}",
                'trading_conditions_22_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_22_title_en_ref)?$DataContent->trading_condition->trading_conditions_22_title_en_ref:''}}",
                'trading_conditions_23_title_en_ref': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->trading_conditions_23_title_en_ref)?$DataContent->trading_condition->trading_conditions_23_title_en_ref:''}}",
                'step_4_footer': "{{isset($DataContent->trading_condition)&&!empty($DataContent->trading_condition->step_4_footer)?$DataContent->trading_condition->step_4_footer:''}}",
            },
            'pap_agreement': "{{!empty($DataContent->pap_agreement)?$DataContent->pap_agreement:''}}",
            'truth_resposibility_agreement': "{{!empty($DataContent->truth_resposibility_agreement)?$DataContent->truth_resposibility_agreement:''}}",
            'demoAccount': "{{(Session::get('user.MTUserDemo')->Login)}}",
            'full_name': "{{!empty($DataContent->full_name)?$DataContent->full_name:''}}",
            'identity_number': "{{!empty($DataContent->identity_number)?$DataContent->identity_number:''}}",
            'future_contract': {
                'value': "{{isset($DataContent->future_contract)&&!empty($DataContent->future_contract->value)?$DataContent->future_contract->value:''}}",
                'input': "{{isset($DataContent->future_contract)&&!empty($DataContent->future_contract->input)?$DataContent->future_contract->input:''}}",
            },
            'account_type': "{{!empty($DataContent->account_type)?$DataContent->account_type:''}}",
            'place_of_birth': "{{!empty($DataContent->place_of_birth)?$DataContent->place_of_birth:''}}",
            'date_of_birth': "{{!empty($DataContent->date_of_birth)?$DataContent->date_of_birth:''}}",
            'post_code': "{{!empty($DataContent->post_code)?$DataContent->post_code:''}}",
            'page': "{{!empty($DataContent->page)?$DataContent->page:0}}",
            'activeStep': "{{!empty($DataContent->activeStep)?$DataContent->activeStep:''}}",
            'tax_file_number': "{{!empty($DataContent->tax_file_number)?$DataContent->tax_file_number:''}}",
            'mother_maiden_name': "{{!empty($DataContent->mother_maiden_name)?$DataContent->mother_maiden_name:''}}",
            'status': "{{!empty($DataContent->status)?$DataContent->status:''}}",
            'gender': "{{!empty($DataContent->gender)?$DataContent->gender:''}}",
            'home_address': "{{!empty($DataContent->home_address)?$DataContent->home_address:''}}",
            'home_ownership_status': {
                'value': "{{isset($DataContent->home_ownership_status)&&!empty($DataContent->home_ownership_status->value)?$DataContent->home_ownership_status->value:''}}",
                'input': "{{isset($DataContent->home_ownership_status)&&!empty($DataContent->home_ownership_status->input)?$DataContent->home_ownership_status->input:''}}"
            },
            'openint_account_purpose': {
                'value': "{{isset($DataContent->openint_account_purpose)&&!empty($DataContent->openint_account_purpose->value)?$DataContent->openint_account_purpose->value:''}}",
                'input': "{{isset($DataContent->openint_account_purpose)&&!empty($DataContent->openint_account_purpose->input)?$DataContent->openint_account_purpose->input:''}}"
            },
            'experince_on_investment': {
                'value': "{{isset($DataContent->experince_on_investment)&&!empty($DataContent->experince_on_investment->value)?$DataContent->experince_on_investment->value:''}}",
                'input': "{{isset($DataContent->experince_on_investment)&&!empty($DataContent->experince_on_investment->input)?$DataContent->experince_on_investment->input:''}}"
            },
            'is_family_work_in_related_company': {
                'value': "{{isset($DataContent->is_family_work_in_related_company)&&!empty($DataContent->is_family_work_in_related_company->value)?$DataContent->is_family_work_in_related_company->value:''}}",
                'input': "{{isset($DataContent->is_family_work_in_related_company)&&!empty($DataContent->is_family_work_in_related_company->input)?$DataContent->is_family_work_in_related_company->input:''}}"
            },
            'is_declared_bankrupt': {
                'value': "{{isset($DataContent->is_declared_bankrupt)&&!empty($DataContent->is_declared_bankrupt->value)?$DataContent->is_declared_bankrupt->value:''}}",
                'input': "{{isset($DataContent->is_declared_bankrupt)&&!empty($DataContent->is_declared_bankrupt->input)?$DataContent->is_declared_bankrupt->input:''}}"
            },
            'trading_rules': $($("input[name='trading_rules']:checked")).val() || '',
            'ae_name_or_branch': "{{!empty($DataContent->ae_name_or_branch)?$DataContent->ae_name_or_branch:''}}",
            'husband_wife_spouse': "{{!empty($DataContent->husband_wife_spouse)?$DataContent->husband_wife_spouse:''}}",
            'home_phone': "{{!empty($DataContent->home_phone)?$DataContent->home_phone:''}}",
            'statement_data_correctly': "{{!empty($DataContent->statement_data_correctly)?$DataContent->statement_data_correctly:''}}",
            'KycStatus':0
        };
        return contentDt;
    }

    function savekyc() {
        var contentDt = getContent();
        if(contentDt.wealth_list.income_per_year==="&gt;500"){
            contentDt.wealth_list.income_per_year=">500";
        }
        const content = JSON.stringify(contentDt)
        $.post("{{route('savekyc')}}", {
            content,
            NBCRMWEBAPI: $('[name="NBCRMWEBAPI"]').val(),
            USERID: $('[name="USERID"]').val(),
        }, function(data, status) {
            var {
                Code,
                Message
            } = data;
            if (data.Data) {
                var Data = data.Data;
                var MTUserRef = Data.MTUserRef;
                var idx = MTUserRef.findIndex(x => x.IsSimulate === false);
                if (MTUserRef[idx]) {
                    var liveAcc = MTUserRef[idx];
                    $('[data-bind="ac_live_no"]').text(liveAcc.Login);
                }
            }
        });
    };

    // });
    $(function() {
        $("#form-step-0").validate({
            ignore: [],
            // errorElement: "div",
            rules: {
                trading_rules: {
                    required: true
                },
            },
            messages: {
                trading_rules: 'Klik "Ya" untuk melanjutkan',
            },
            submitHandler: function() {
                var val = $($("input[name='trading_rules']:checked")).val() || '';
                if (val === "yes") {
                    $('.next-btn').hide();
                    $('.btn-loading-next').show();
                    $.post("{{route('postNextStepOpenLiveAccount')}}", {
                        NBCRMWEBAPI: $('[name="NBCRMWEBAPI"]').val(),
                        USERID: $('[name="USERID"]').val(),
                        nextStep: '4',
                        page: '3'
                    }, function(data, status) {
                        if (status === "success") {
                            location.reload();
                        } else {
                            $('.next-btn').show();
                            $('.btn-loading-next').hide();
                        }
                    });
                } else {
                    $('#trading_rules-error').show();
                    $('#trading_rules-error').text('Klik "Ya" untuk melanjutkan.');
                }
            }
        });


    });

    var renderImgAttc = false;
    var renderImgAttcOther = false;
    var minAge = new Date();
    var getYear = minAge.getFullYear();
    minAge.setFullYear(getYear - 21);
    var dd = minAge.getDate();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    var mm = minAge.getMonth() + 1; //January is 0!
    var yyyy = minAge.getFullYear();
    var minDate = yyyy + '-' + mm + '-' + dd;
    $('[name="date_of_birth"]').prop('max', minDate);

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    const type = sessionStorage.getItem("register_account");
    if (type === "miniaccount") {
        $('.account_type_mini').prop('checked', true);
        $('.forex_idr').prop('checked', true);
        $('[name="future_contract"]').val('forex_idr');
        $('[name="account_type"]').val('mini');
    } else if (type === "usdaccount") {
        $('.account_type_reguler').prop('checked', true);
        $('.forex_usd').prop('checked', true);
        $('[name="future_contract"]').val('forex_usd');
        $('[name="account_type"]').val('regular');
    } else {
        // profesional
        $('.account_type_reguler').prop('checked', true);
        $('.forex_idr').prop('checked', true);
        $('[name="future_contract"]').val('forex_idr');
        $('[name="account_type"]').val('regular');
    }

    $('[name="statement_data_correctly"]').on('change', function(event) {
        var val = event.currentTarget.value;
        if (val === 'No') {
            $('.next-btn').prop('disabled', true);
            $('#statement_data_correctly-error').show();
        } else {
            $('.next-btn').prop('disabled', false);
            $('#statement_data_correctly-error').hide();
        }
    });

    $('[name="future_contract_opt"]').on('change', function(event) {
        const val = event.currentTarget.value;
        $('[name="future_contract"]').val(val);
    });


    $('[name="date_of_birth"]').on('change', function(event) {
        var date = event.currentTarget.value;
        var today = new Date();
        var birthday = new Date(date);
        var year = 0;
        if (today.getMonth() < birthday.getMonth()) {
            year = 1;
        } else if ((today.getMonth() == birthday.getMonth()) && today.getDate() < birthday.getDate()) {
            year = 1;
        }
        var age = today.getFullYear() - birthday.getFullYear() - year;
        if (age < 0) {
            age = 0;
        }
        if (age < 21) {
            $(this).val('');
            swal("Perhatian!", "Untuk memenuhi persyaratan dan peraturan  kami hanya dapat menerima klien yang berusia lebih dari 21 tahun.!", "warning");
        } else {

        }
    });
</script>
@endsection