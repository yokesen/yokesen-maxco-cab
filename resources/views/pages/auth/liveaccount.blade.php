@extends('templates.master')

@section('content')
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <h1>Live Account</h1>
                <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                    <ol class="breadcrumb pt-0">
                        <!-- <li class="breadcrumb-item active" aria-current="page">Data</li> -->
                    </ol>
                </nav>
                <div class="separator mb-5"></div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="row mb-4">
                    <div class="col-xs-6 col-lg-4 col-12 mb-4">
                        <div class="card">
                            <!-- <div class="position-relative">
                                <img class="card-img-top" src="{{url('/')}}/cabinet/img/webpage/account_mini.jpg" alt="Card image cap" />
                                <span class="badge badge-pill badge-theme-1 position-absolute badge-top-left">NEW</span>
                            </div> -->
                            <div class="card-body">
                                <p class="list-item-heading text-center">Mini Account</p>
                                <h4 class="mb-4 text-center">$500</h4>
                                <ul>
                                    <li>Forex & Gold & Index</li>
                                    <li>IDR fixed rate 10.000</li>
                                    <li>Leverage 1:100</li>
                                    <li>Contract size 100.000</li>
                                    <li>Start from 0.1 lot</li>
                                </ul>
                                @if(Session::get('user.KycStatus')==0 || Session::get('user.KycStatus')==3)
                                <footer class="text-center">
                                    <button class="btn btn-primary btn-lg btn-shadow btn-maxco-blue borderradius-0" onclick="openModal('miniaccount')" style="color:#fff;">Open Account Now</button>
                                </footer>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-lg-4 col-12 mb-4">
                        <div class="card">
                            <!-- <div class="position-relative">
                                <img class="card-img-top" src="{{url('/')}}/cabinet/img/webpage/account_profesional.jpg" alt="Card image cap" />
                                <span class="badge badge-pill badge-theme-1 position-absolute badge-top-left">TRENDING</span>
                            </div> -->
                            <div class="card-body">
                                <p class="list-item-heading text-center">Professional Account</p>
                                <h4 class="mb-4 text-center">$10,000</h4>
                                <ul>
                                    <li>Forex & Gold & Index</li>
                                    <li>IDR fixed rate 10.000</li>
                                    <li>Leverage 1:100</li>
                                    <li>Contract size 100.000</li>
                                    <li>Start from 1 lot</li>
                                </ul>
                                @if(Session::get('user.KycStatus')==0 || Session::get('user.KycStatus')==3)
                                <footer class="text-center">
                                    <button class="btn btn-primary btn-lg btn-shadow btn-maxco-blue borderradius-0" onclick="openModal('profesionalaccount')" style="color:#fff;" {{Session::get('user.KycStatus')===1?'disabled':''}}>Open Account Now</button>
                                </footer>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-lg-4 col-12 mb-4">
                        <div class="card">
                            <!-- <div class="position-relative">
                                <img class="card-img-top" src="{{url('/')}}/cabinet/img/webpage/account_dollar.jpg" alt="Card image cap" />
                                <span class="badge badge-pill badge-theme-1 position-absolute badge-top-left">TRENDING</span>
                            </div> -->
                            <div class="card-body">
                                <p class="list-item-heading text-center">USD Account</p>
                                <h4 class="mb-4 text-center">$10,000</h4>
                                <ul>
                                    <li>Forex & Gold & Index</li>
                                    <li>USD floating Rate</li>
                                    <li>Leverage 1:100</li>
                                    <li>Contract size 100.000</li>
                                    <li>Start from 1 lot</li>
                                </ul>
                                @if(Session::get('user.KycStatus')==0 || Session::get('user.KycStatus')==3)
                                <footer class="text-center">
                                    <button class="btn btn-primary btn-lg btn-shadow btn-maxco-blue borderradius-0" onclick="openModal('usdaccount')" style="color:#fff;" {{Session::get('user.KycStatus')===1?'disabled':''}}>Open Account Now</button>
                                </footer>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Pemberitahuan</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <ol>
                            <li>Pastikan anda telah melakukan simulasi trading di platform meta trader PT. Maxco Futures dan simpan nomer rekening demo tersebut guna kelengkapan pengisian aplikasi.</li>
                            <li>Siapkan Soft Copy (scan) KTP/SIM/Pasport (pilih salah satu).</li>
                            <li>Siapkan Soft Copy (scan) Photo Berwarna terbaru.</li>
                            <li>
                                Siapkan Soft Copy (scan) salah satu pilihan dibawah ini :
                                <ul>
                                    <li>a. Rekening Koran Bank 3 (tiga) bulan terakhir yang dipergunakan sebagai tempat penyetoran dan penarikan margin (hanya dapat dilakukan ke rekening bank yang tercantum dalam Aplikasi Pembukaan rekening Transaksi Secara Elektronik Online ini. Dalam hal ada lebih dari 1 (satu) rekening Bank yang akan digunakan, maka wajib melampirkan seluruh rekening Bank dimaksud)</li>
                                    <li>b. Surat keterangan bekerja</li>
                                    <li>c. Surat keterangan pendapatan untuk calon nasabah yang bersatus pegawai atau laporan Keuangan 3 (tiga) bulan terakhir (untuk calon Nasabah berstatus wiraswasta atau pemilik usaha)</li>
                                    <li>d. Tagihan Kartu Kredit;</li>
                                    <li>e. Bukti kepemilikan tanah atau bangunan atau kendaraan bermotor; atau</li>
                                    <li>f. Dokumen lain yang terkait, yang relevan berdasarkan pertimbangan Wakil Pialang Berjangka</li>
                                </ul>
                            </li>
                            <li>Lakukan Proses pengisian Aplikasi Pembukaan Rekening Transaksi Secara Elektronik Online.</li>
                            <li>Apabila Proses Aplikasi On-Liine ini telah selesai maka pihak wakil pialang dari PT. Maxco Futures akan segera menghubungi anda untuk proses verifikasi</li>
                            <li>Setelah proses verifikasi wakil pialang selesai dan aplikasi anda dinyatakan "verified" maka anda dipersilahkan melakukan transfer ke rekening terpisah PT. Maxco Futures yang tercantum pada Formulir Nomor 107.PBK.01.</li>
                        </ol>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary borderradius-0" data-dismiss="modal">Close</button>
                        <a href="/open-live-account" type="button" class="btn btn-primary btn-maxco-blue borderradius-0">Lanjutkan</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection

@section('jsonpage')
<script>
    var atvStep = 0;

    function openModal(account) {
        sessionStorage.setItem("register_account", account);
        $('#exampleModalCenter').modal()
    }

    function initDataKYC() {
        const contentDt = {
            'full_name': "{{$IdCard->RealName}}",
            'identity_number': "{{$IdCard->IDNO}}",
            'place_of_birth': "{{$User->PlaceOfBirth}}",
            'date_of_birth': "{{!empty($User->DateOfBirth)?$User->DateOfBirth:''}}",
            'activeStep': 0,
            'gender': "{{$User->Gender}}",
            'home_address': "{{$User->Address}}",
        };
        const content = JSON.stringify(contentDt)
        $.post("{{route('savekyc')}}", {
            content,
            NBCRMWEBAPI: $('[name="NBCRMWEBAPI"]').val(),
            USERID: $('[name="USERID"]').val(),
        }, function(data, status) {
            // console.log("Data: " + data + "\nStatus: " + status);
        });

    }
    // initDataKYC();
    function getDetailKYC() {
        $.get("{{route('apigetKycDetail')}}", {}, function(data, status) {
            if (!data.Data)  {
                console.log('tidak ada data');
        //     initDataKYC();
            }else{
                console.log('ada data');
            }
        });
    };
    // getDetailKYC();
</script>
@endsection