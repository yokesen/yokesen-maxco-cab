@extends('templates.master')


@section('csslinkonhead')
<link rel="stylesheet" href="{{url('/')}}/cabinet/css/vendor/select2.min.css" />
<link rel="stylesheet" href="{{url('/')}}/cabinet/css/vendor/select2-bootstrap.min.css" />
@endsection

@section('content')
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <h1>Deposit</h1>
                <div class="top-right-button-container">
                    <button class="btn btn-primary btn-lg top-right-button mr-1 btn-maxco-blue borderradius-0 text-uppercase" onclick="gotodeposit()">Deposit Request</button>
                </div>
                <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                    <ol class="breadcrumb pt-0">
                        <li class="breadcrumb-item active" aria-current="page">Data</li>
                    </ol>
                </nav>
                <div class="separator mb-5"></div>
            </div>
        </div>

        <div class="card">
            <div class="card-body">
                <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">DATE TIME</th>
                                <th scope="col">TO MT4</th>
                                <th scope="col">BANK</th>
                                <th scope="col">CURRENCY</th>
                                <th scope="col">AMOUNT</th>
                                <th scope="col">METHOD</th>
                                <th scope="col">STATUS</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $item)
                            <?php
                            switch ($item->Status) {
                                case '0':
                                    $statusDepo = "Pending";
                                    break;
                                case '1':
                                    $statusDepo = "Approved";
                                    break;
                                case '2':
                                    $statusDepo = "Denied";
                                    break;
                            }

                            switch ($item->Currency) {
                                case '360':
                                    $currency = "IDR";
                                    break;
                                case '840':
                                    $currency = "USD";
                                    break;
                                default:
                                    $currency ='';
                                    break;
                            }

                            switch ($item->BankId) {
                                case '35':
                                    $bankname = "Panin Bank";
                                    break;
                                case '36':
                                    $bankname = "BCA";
                                    break;
                                default:
                                    $bankname ='';
                                    break;
                            }

                            ?>

                            <tr>
                                <td>{{$item->CreateTime}}</td>
                                <td>{{$item->MTId}}</td>
                                <td>{{$bankname}}</td>
                                <td>{{$currency}}</td>
                                <td>{{number_format($item->Amount,0)}}</td>
                                <td>{{$item->Method}}</td>
                                <td>{{$statusDepo}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="row view-pager">
                        <div class="col-sm-12">
                            <div class="text-center">
                                <div class="dataTables_paginate paging_simple_numbers" id="DataTables_Table_0_paginate">
                                    @php
                                        $page =!empty($_GET['page'])?$_GET['page']:1;
                                    @endphp
                                    <ul class="pagination pagination-sm">
                                        <li class="paginate_button page-item previous {{$page>1?'':'disabled'}}" id="DataTables_Table_0_previous"><a href="{{route('historydeposit').'?page='.($page-1)}}" aria-controls="DataTables_Table_0" data-dt-idx="0" tabindex="0" class="page-link prev"><i class="simple-icon-arrow-left"></i></a></li>
                                        @for($i = 1; $i < ($totalPages+1); $i++)
                                            <li class="paginate_button page-item {{$page==$i?'active':''}}"><a href="{{route('historydeposit').'?page='.$i}}" aria-controls="DataTables_Table_{{$i}}" data-dt-idx="1" tabindex="0" class="page-link">{{$i}}</a></li>
                                        @endfor
                                        <li class="paginate_button page-item next {{$page<$totalPages?'':'disabled'}}" id="DataTables_Table_0_next"><a href="{{route('historydeposit').'?page='.($page+1)}}" aria-controls="DataTables_Table_0" data-dt-idx="3" tabindex="0" class="page-link next"><i class="simple-icon-arrow-right"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection

@section('jsonpage')
<script src="{{url('/')}}/cabinet/js/vendor/sweetalert.min.js"></script>
<script>
    var atvStep;

    function gotodeposit() {
        var kycStatus = "{{Session::get('user.KycStatus')}}";
        if (kycStatus === '2') {
            window.location.href = "{{url('/')}}/deposit-request";
        } else {
            swal("Perhatian!", "Anda belum memiliki akun live.", "warning");
        }
    };
</script>
<script src="{{url('/')}}/cabinet/js/vendor/select2.full.js"></script>
<script src="{{url('/')}}/cabinet/js/vendor/jquery.validate/jquery.validate.min.js"></script>
<script src="{{url('/')}}/cabinet/js/vendor/jquery.validate/additional-methods.min.js"></script>
@endsection