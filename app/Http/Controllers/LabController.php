<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Alert;
class LabController extends Controller
{
    //
    public function alert(){
        alert()->success('Message', 'Optional Title');
        return view('pages.labsalert');
    }

}
