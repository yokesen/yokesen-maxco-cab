@extends('templates.master')

@section('csslinkonhead')
<link rel="stylesheet" href="{{url('/')}}/cabinet/css/vendor/smart_wizard.min.css" />
<link rel="stylesheet" href="{{url('/')}}/cabinet/css/vendor/dropzone.min.css" />
<meta name="csrf-token" content="{{ csrf_token() }}" />
@endsection

@section('content')
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <h1>Opening Account Wizard</h1>
                <div class="separator mb-5"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 mb-4">
                <!-- <h5 class="mb-4">Open Live Account</h5> -->
                <div class="card mb-4">
                    <div id="smartWizardValidation">
                        <ul class="card-header mb-4">
                            <li class=" {{intval($DataContent->activeStep?$DataContent->activeStep:0)}} @if(0<intval($DataContent->activeStep?$DataContent->activeStep:0)) done @endif">
                                <a href="#step-0">
                                    Step 1
                                    <br />
                                    <!-- <small>First step description</small>-->
                                </a>
                            </li>
                            <li>
                                <a href="#step-1">
                                    Step 2
                                    <br />
                                    <!-- <small>Second step description</small>-->
                                </a>
                            </li>
                            <li>
                                <a href="#step-2">
                                    Step 3
                                    <br />
                                    <!-- <small>Third step description</small>-->
                                </a>
                            </li>
                            <li>
                                <a href="#step-3">
                                    Step 4
                                    <br />
                                    <!-- <small>Third step description</small>-->
                                </a>
                            </li>
                            <li>
                                <a href="#step-4">
                                    Step 5
                                    <br />
                                    <!-- <small>Third step description</small>-->
                                </a>
                            </li>
                        </ul>

                        <div class="card-body">

                            <div id="step-0" class="mt-4">
                                <form id="form-step-0" class="needs-validation" novalidate style="padding: 0px 22px;">
                                    <input name="NBCRMWEBAPI" type="hidden" value="{{$UserToken}}" />
                                    <input name="USERID" type="hidden" value="{{$UserId}}" />
                                    <input name="activeStep" type="hidden" value="{{$DataContent->activeStep?$DataContent->activeStep:0}}" />
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group position-relative error-l-100">
                                                <label for="email">Kontrak Berjangka</label>
                                                <div class="form-check">
                                                    <input class="form-check-input forex_usd" type="radio" name="future_contract_opt" value="forex_usd" checked />
                                                    <label class="form-check-label" for="regular">
                                                        Forex & Gold & Index (USD)
                                                    </label>
                                                </div>
                                                <div class="form-check">
                                                    <input class="form-check-input forex_idr" type="radio" name="future_contract_opt" value="forex_idr" />
                                                    <label class="form-check-label" for="mini">
                                                        Forex & Gold & Index (IDR) - (Rp 10.000)
                                                    </label>
                                                </div>
                                            </div>
                                            <input type="hidden" name="future_contract" value="forex_idr" />

                                            <div class="form-group position-relative error-l-100">
                                                <label for="email">Jenis Akun</label>
                                                <div class="form-check">
                                                    <input class="form-check-input account_type_reguler" type="radio" name="account_type_opt" id="account_type_opt1" value="regular" checked />
                                                    <label class="form-check-label" for="regular">
                                                        Reguler
                                                        <br />* Minimum Open : 1.0 Lot
                                                        <br />* Minimal Setoran Awal : 10.000 USD
                                                        <br />
                                                    </label>
                                                </div>
                                                <div class="form-check">
                                                    <input class="form-check-input account_type_mini" type="radio" name="account_type_opt" id="account_type_opt2" value="mini" />
                                                    <label class="form-check-label" for="mini">
                                                        Mini
                                                        <br />* Minimum Open : 0.1 Lot
                                                        <br />* Minimal Setoran Awal : 300 USD
                                                        <br />
                                                    </label>
                                                </div>
                                            </div>
                                            <input type="hidden" name="account_type" value="reguler" />


                                        </div>
                                        <div class="col-md-4">
                                            <div class="alert alert-dark" role="alert">Data Pribadi</div>
                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Nama Lengkap</label>
                                                <!-- <span><sup>(*</sup></span> -->
                                                <input type="text" class="form-control" name="full_name" placeholder="Masukkan nama lengkap" onchange="updateData" onkeypress="updateData" required value="{{!empty($DataContent->full_name)?$DataContent->full_name:''}}" />
                                            </div>

                                            <div class="form-group position-relative error-l-100">
                                                <label for="idno" class="required">No Identitas</label>
                                                <input type="text" class="form-control" name="identity_number" placeholder="KTP/SIM/PASSPOR/KITAS" required onchange="updateData" onkeypress="updateData" value="{{!empty($DataContent->identity_number)?$DataContent->identity_number:''}}" />
                                            </div>

                                            <div class="form-group position-relative error-l-100">
                                                <label for="placeofbirth" class="required">Tempat Lahir</label>
                                                <input type="text" class="form-control" name="place_of_birth" placeholder="Masukkan nama kota disini" required onchange="updateData" onkeypress="updateData" value="{{!empty($DataContent->place_of_birth)?$DataContent->place_of_birth:''}}" />
                                                <div class="invalid-tooltip"> Tempat Lahir is required!</div>
                                            </div>

                                            <div class="form-group position-relative error-l-100">
                                                <label for="dateofbirth" class="required">Tanggal Lahir</label>
                                                <input type="date" class="form-control" name="date_of_birth" placeholder="Your dateofbirth address" required onchange="updateData" onkeypress="updateData" value="{{!empty($DataContent->date_of_birth)?$DataContent->date_of_birth:''}}" />
                                                <small class="form-text text-muted">Untuk memenuhi persyaratan dan peraturan kami hanya dapat menerima klien yang berusia lebih dari 21 tahun.</small>
                                            </div>

                                            <div class="form-group position-relative error-l-100">
                                                <label for="address" class="required">Alamat</label>
                                                <input type="text" class="form-control" name="home_address" placeholder="Masukkan alamat lengkap disini" required onchange="updateData" onkeypress="updateData" value="{{!empty($DataContent->home_address)?$DataContent->home_address:''}}" />
                                            </div>

                                            <div class="form-group position-relative error-l-100">
                                                <label for="poscode" class="required">Kode Pos</label>
                                                <input type="text" class="form-control" name="post_code" placeholder="Kode pos" required onchange="updateData" onkeypress="updateData" value="{{!empty($DataContent->post_code)?$DataContent->post_code:''}}" />
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group position-relative error-l-100">
                                                <label for="aename">AE Nama / Cabang</label>
                                                <input type="text" class="form-control" name="ae_name_or_branch" placeholder="Masukkan AE Nama / Cabang" onchange="updateData" onkeypress="updateData" value="@isset($DataContent->ae_name_or_branch){{trim($DataContent->ae_name_or_branch)}}@endisset" />
                                            </div>

                                            <div class="form-group position-relative error-l-100 row">
                                                <label class="col-sm-4 col-form-label">No Demo Acc.</label>
                                                <div class="col-sm-8">
                                                    <span class="form-control-plaintext">{{(Session::get('user.MTUserDemo')->Login)}}</span>
                                                </div>
                                                <div class="col-sm-12">
                                                    <p class="text-justify">
                                                        Semua data wajib diisi dan harus benar
                                                        adanya/sesuai dengan dokumen
                                                        pendukung yang sah. Semua data dan Pengaturan adalah merupakan
                                                        ketentuan yang mutlak dari Badan Pengawas Perdagangn Berjangka
                                                        Komoditi (BAPPEBTI) dan akan dikelola seta disimpan secara
                                                        Rahasia
                                                        (Highky Confidential) oleh Maxco Futures sebagai Wakil
                                                        Perusahaan
                                                        Pialang di Indonesia. Sebelum memulai proses Pembukaan Rekening
                                                        Transaksi Online bersama Maxco Futures.
                                                    </p>
                                                    <div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name='statement_data_correctly' required value="Yes" onchange="updateData" onkeypress="updateData" @isset($DataContent->statement_data_correctly) @if($DataContent->statement_data_correctly==='Yes') checked @endif @endisset/>
                                                            <label class="form-check-label required">Ya</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name='statement_data_correctly' required value="No" onchange="updateData" onkeypress="updateData" @isset($DataContent->statement_data_correctly) @if($DataContent->statement_data_correctly==='No') checked @endif @endisset/>
                                                            <label class="form-check-label">Tidak</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div id="step-1">
                                <form id="form-step-1" class="needs-validation" novalidate style="padding: 0px 22px;">
                                    <h3 class="mb-4">
                                        Ketentuan Penyajian Aplikasi Pembukaan Rekening Transaksi Secara Elektronik
                                        Online - Formulir Nomor 107.PBK.03
                                    </h3>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="alert alert-dark" role="alert">Data Pribadi</div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Nama Lengkap</label>
                                                <div class="col-sm-8">
                                                    <span class="form-control-plaintext" data-bind="fullname"></span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Nomor Identitas</label>
                                                <div class="col-sm-8">
                                                    <span class="form-control-plaintext" data-bind="identity_number"></span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Tempat Lahir</label>
                                                <div class="col-sm-8">
                                                    <span class="form-control-plaintext" data-bind="place_of_birth"></span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Tangal Lahir</label>
                                                <div class="col-sm-8">
                                                    <span class="form-control-plaintext" data-bind="date_of_birth"></span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Alamat</label>
                                                <div class="col-sm-8">
                                                    <span class="form-control-plaintext" data-bind="home_address"></span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Kode Pos</label>
                                                <div class="col-sm-8">
                                                    <span class="form-control-plaintext" data-bind="post_code"></span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Alamat Email</label>
                                                <div class="col-sm-8">
                                                    <span class="form-control-plaintext">{{Session::get('user.email')}}</span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">No Handphone</label>
                                                <div class="col-sm-8">
                                                    <span class="form-control-plaintext">{{Session::get('user.mobile')}}</span>
                                                </div>
                                            </div>
                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">No NPWP</label>
                                                <input type="text" class="form-control" name="tax_file_number" placeholder="No NPWP" value="@isset($DataContent->tax_file_number){{trim($DataContent->tax_file_number)}}@endisset" required onchange="updateData" onkeypress="updateData" />
                                            </div>
                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Jenis Kelamin</label>
                                                <div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="genderOptions" value="Male" required onchange="updateData" onkeypress="updateData" @isset($DataContent->gender) @if($DataContent->gender==='Male') checked @endif @endisset/>
                                                        <label class="form-check-label" for="gender1">Laki-laki</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="genderOptions" value="Female" required onchange="updateData" onkeypress="updateData" @isset($DataContent->gender) @if($DataContent->gender==='Female') checked @endif @endisset/>
                                                        <label class="form-check-label" for="gender2">Perempuan</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Nama Ibu Kandung</label>
                                                <input type="text" class="form-control" name="mother_maiden_name" placeholder="Masukkan nama lengkap disini" value="@isset($DataContent->mother_maiden_name){{trim($DataContent->mother_maiden_name)}}@endisset" required onchange="updateData" onkeypress="updateData" />
                                            </div>
                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Status Perkawinan</label>
                                                <div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="maritalstatusOptions" value="Married" onchange="updateData" onkeypress="updateData" required @isset($DataContent->status) @if($DataContent->status==='Married') checked @endif @endisset/>
                                                        <label class="form-check-label" for="maritalstatus1">
                                                            Tidak
                                                            Kawin
                                                        </label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="maritalstatusOptions" value="Single" onchange="updateData" onkeypress="updateData" required @isset($DataContent->status) @if($DataContent->status==='Single') checked @endif @endisset/>
                                                        <label class="form-check-label" for="maritalstatus2">Kawin</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="maritalstatusOptions" value="Divorced" onchange="updateData" onkeypress="updateData" required @isset($DataContent->status) @if($DataContent->status==='Divorced') checked @endif @endisset/>
                                                        <label class="form-check-label" for="maritalstatus3">
                                                            Janda /
                                                            Duda
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group position-relative error-l-100">
                                                <label>Nama Istri/Suami</label>
                                                <input type="text" class="form-control" name="husband_wife_spouse" placeholder="Masukkan nama lengkap (Opsional)" value="@isset($DataContent->husband_wife_spouse){{trim($DataContent->husband_wife_spouse)}}@endisset" onchange="updateData" onkeypress="updateData" />
                                            </div>
                                            <div class="form-group position-relative error-l-100">
                                                <label>No Tlp Rumah</label>
                                                <input type="text" class="form-control" name="home_phone" placeholder="(Kode Area) No. (Opsional)" value="@isset($DataContent->home_phone){{trim($DataContent->home_phone)}}@endisset" onchange="updateData" onkeypress="updateData" />
                                            </div>
                                            <div class="form-group position-relative error-l-100">
                                                <label>No Fax Rumah</label>
                                                <input type="text" class="form-control" name="home_fax" placeholder="(Kode Area) No. (Opsional)" value="@isset($DataContent->home_fax){{trim($DataContent->home_fax)}}@endisset" onchange="updateData" onkeypress="updateData" />
                                            </div>
                                            
                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Status Kepemilikan Rumah</label>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="houseownstatusOptions" required value="personal" @isset($DataContent->home_ownership_status) @if($DataContent->home_ownership_status->value==='personal') checked @endif @endisset
                                                            onchange="updateData" onkeypress="updateData" />
                                                            <label class="form-check-label" for="houseownstatus1">Pribadi</label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="houseownstatusOptions" required value="family" @isset($DataContent->home_ownership_status) @if($DataContent->home_ownership_status->value==='family') checked @endif @endisset onchange="updateData" onkeypress="updateData" />
                                                            <label class="form-check-label" for="houseownstatus2">Keluarga</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="houseownstatusOptions" required value="rent" @isset($DataContent->home_ownership_status) @if($DataContent->home_ownership_status->value==='rent') checked @endif @endisset onchange="updateData" onkeypress="updateData" />
                                                            <label class="form-check-label" for="houseownstatus3">Sewa/Kontrak</label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="houseownstatusOptions" required value="other" @isset($DataContent->home_ownership_status) @if($DataContent->home_ownership_status->value==='other') checked @endif @endisset onchange="updateData" onkeypress="updateData" />
                                                            <label class="form-check-label" for="houseownstatus4">Lainnya</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12" id="houseownstatusdescriptiondiv">
                                                        <input type="text" class="form-control" name="houseownstatusdescription" placeholder="Sebutkan" value="@isset($DataContent->home_ownership_status){{trim($DataContent->home_ownership_status->input)}}@endisset" onchange="updateData" onkeypress="updateData" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Tujuan Pembukaan Rekening</label>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="openint_account_purpose" required value="protecvalue" onchange="updateData" onkeypress="updateData" @isset($DataContent->openint_account_purpose)@if($DataContent->openint_account_purpose->value==='protecvalue') checked @endif @endisset />
                                                            <label class="form-check-label" for="openaccountfor1">
                                                                Lindungi
                                                                Nilai
                                                            </label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="openint_account_purpose" required value="gain" onchange="updateData" onkeypress="updateData" @isset($DataContent->openint_account_purpose) @if($DataContent->openint_account_purpose->value==='gain') checked @endif @endisset />
                                                            <label class="form-check-label" for="openaccountfor2">Gain</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="openint_account_purpose" required value="specultion" onchange="updateData" onkeypress="updateData" @isset($DataContent->openint_account_purpose) @if($DataContent->openint_account_purpose->value==='specultion') checked @endif @endisset />
                                                            <label class="form-check-label" for="openaccountfor3">Spekulasi</label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="openint_account_purpose" required value="other" onchange="updateData" onkeypress="updateData"
                                                            @isset($DataContent->openint_account_purpose) @if($DataContent->openint_account_purpose->value==='other') checked @endif @endisset />
                                                            <label class="form-check-label" for="openaccountfor4">Lainnya</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <input type="text" class="form-control" name="openint_account_purpose_input" placeholder="Sebutkan" onchange="updateData" onkeypress="updateData"
                                                        value="@isset($DataContent->openint_account_purpose){{trim($DataContent->openint_account_purpose->input)}}@endisset" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Pengalaman Investasi</label>
                                                <div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="experince_on_investment" required value="Yes" onchange="updateData" onkeypress="updateData" @isset($DataContent->experince_on_investment) @if($DataContent->experince_on_investment->value==='Yes') checked @endif @endisset/>
                                                        <label class="form-check-label">Ya</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="experince_on_investment" required value="No" onchange="updateData" onkeypress="updateData" @isset($DataContent->experince_on_investment) @if($DataContent->experince_on_investment->value==='No') checked @endif @endisset/>
                                                        <label class="form-check-label">Tidak</label>
                                                    </div>
                                                    <input type="text" class="form-control" name="experince_on_investment_input" placeholder="Sebutkan Bidang" onchange="updateData" onkeypress="updateData" value="@isset($DataContent->experince_on_investment){{trim($DataContent->experince_on_investment->input)}}@endisset" />
                                                </div>
                                            </div>

                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">
                                                    Apakah Anda Memiliki Anggota Keluarga Yang
                                                    Bekerja Di
                                                    Bappebti / Bursa Berjangka / Kliring Berjangka
                                                </label>
                                                <div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" required name="is_family_work_in_related_company" value="Yes" onchange="updateData" onkeypress="updateData" @isset($DataContent->is_family_work_in_related_company) @if($DataContent->is_family_work_in_related_company->value==='Yes') checked @endif @endisset/>
                                                        <label class="form-check-label">Ya</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" required name="is_family_work_in_related_company" value="No" onchange="updateData" onkeypress="updateData" @isset($DataContent->is_family_work_in_related_company) @if($DataContent->is_family_work_in_related_company->value==='No') checked @endif @endisset/>
                                                        <label class="form-check-label">Tidak</label>
                                                    </div>
                                                    <input type="text" class="form-control" name="is_family_work_in_related_company_input" placeholder="Sebutkan Nama" onchange="updateData" onkeypress="updateData" value="@isset($DataContent->is_family_work_in_related_company){{trim($DataContent->is_family_work_in_related_company->input)}}@endisset" />
                                                </div>
                                            </div>

                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">
                                                    Apakah Anda Dinyatakan Pailit Oleh Pengadilan
                                                    ?
                                                </label>
                                                <div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="is_declared_bankrupt" value="Yes" required onchange="updateData" onkeypress="updateData" @isset($DataContent->is_declared_bankrupt) @if($DataContent->is_declared_bankrupt->value==='Yes') checked @endif @endisset/>
                                                        <label class="form-check-label" for="bankrupt1">Ya</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="is_declared_bankrupt" value="No" required onchange="updateData" onkeypress="updateData" @isset($DataContent->is_declared_bankrupt) @if($DataContent->is_declared_bankrupt->value==='No') checked @endif @endisset/>
                                                        <label class="form-check-label" for="bankrupt2">Tidak</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="alert alert-dark" role="alert">Pihak Yang Dihubungi Dalam Keadaan Darurat</div>
                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Nama</label>
                                                <input type="text" class="form-control" name="contactEmergencyName" value="@isset($DataContent->emergency_contact_data){{trim($DataContent->emergency_contact_data->name)}}@endisset" placeholder="Masukkan nama lengkap" onchange="updateData" onkeypress="updateData" required />
                                            </div>

                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Alamat</label>
                                                <input type="text" class="form-control" name="contactEmergencyAddress" value="@isset($DataContent->emergency_contact_data){{trim($DataContent->emergency_contact_data->address)}}@endisset" placeholder="Masukkan Alamat" onchange="updateData" onkeypress="updateData" required />
                                            </div>

                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Kode Pos</label>
                                                <input type="text" class="form-control" name="contactEmergencyPoscode" value="@isset($DataContent->emergency_contact_data){{trim($DataContent->emergency_contact_data->post_code)}}@endisset" placeholder="Kode Pos" onchange="updateData" onkeypress="updateData" required />
                                            </div>

                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">No Telepon</label>
                                                <input type="text" class="form-control" name="contactEmergencyPhone" value="@isset($DataContent->emergency_contact_data){{trim($DataContent->emergency_contact_data->home_phone_number)}}@endisset" placeholder="(Kode Area) No" onchange="updateData" onkeypress="updateData" required />
                                            </div>

                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Hubungan Dengan Anda</label>
                                                <input type="text" class="form-control" name="contactEmergencyRelationship" placeholder="Ketik Disini" value="@isset($DataContent->emergency_contact_data){{trim($DataContent->emergency_contact_data->relationship_with_the_contacts)}}@endisset" onchange="updateData" onkeypress="updateData" required />
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="alert alert-dark" role="alert">Pekerjaan</div>
                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Pekerjaan</label>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="position_value" value="swasta" onchange="updateData" onkeypress="updateData" required @isset($DataContent->job_details) @if($DataContent->job_details->position->value==='swasta') checked @endif @endisset />
                                                            <label class="form-check-label" for="jobtitle1">Swasta</label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="position_value" value="wiraswasta" onchange="updateData" onkeypress="updateData" required @isset($DataContent->job_details) @if($DataContent->job_details->position->value==='wiraswasta') checked @endif @endisset />
                                                            <label class="form-check-label" for="jobtitle2">Wiraswata</label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="position_value" value="iburt" onchange="updateData" onkeypress="updateData" required @isset($DataContent->job_details) @if($DataContent->job_details->position->value==='iburt') checked @endif @endisset/>
                                                            <label class="form-check-label" for="jobtitle3">
                                                                Ibu
                                                                RT
                                                            </label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="position_value" value="profesional" onchange="updateData" onkeypress="updateData" required @isset($DataContent->job_details) @if($DataContent->job_details->position->value==='profesional') checked @endif @endisset/>
                                                            <label class="form-check-label" for="jobtitle4">Profesional</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="position_value" value="pns" onchange="updateData" onkeypress="updateData" required @isset($DataContent->job_details) @if($DataContent->job_details->position->value==='pns') checked @endif @endisset/>
                                                            <label class="form-check-label" for="jobtitle5">PNS</label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="position_value" value="student" onchange="updateData" onkeypress="updateData" required @isset($DataContent->job_details) @if($DataContent->job_details->position->value==='student') checked @endif @endisset/>
                                                            <label class="form-check-label" for="jobtitle6">Mahasiswa</label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="position_value" value="other" onchange="updateData" onkeypress="updateData" required @isset($DataContent->job_details) @if($DataContent->job_details->position->value==='other') checked @endif @endisset/>
                                                            <label class="form-check-label" for="jobtitle7">Lainnya</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12" id="position_value_div">
                                                        <input type="text" class="form-control" name="position_input" placeholder="Sebutkan" onchange="updateData" onkeypress="updateData" value="@isset($DataContent->job_details){{trim($DataContent->job_details->position->input)}}@endisset" />
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Nama Perusahaan</label>
                                                <input type="text" class="form-control" name="employer_name" placeholder="Ketik Disini" value="@isset($DataContent->job_details){{trim($DataContent->job_details->more_details->employer_name)}}@endisset" onchange="updateData" onkeypress="updateData" required />
                                            </div>

                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Bidang Usaha</label>
                                                <input type="text" class="form-control" name="business_fields" placeholder="Ketik Disini" value="@isset($DataContent->job_details){{trim($DataContent->job_details->more_details->business_fields)}}@endisset" onchange="updateData" onkeypress="updateData" required />
                                            </div>

                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Jabatan</label>
                                                <input type="text" class="form-control" name="title" placeholder="Ketik Disini" onchange="updateData" onkeypress="updateData" value="@isset($DataContent->job_details){{trim($DataContent->job_details->more_details->title)}}@endisset" required />
                                            </div>
                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Lama Bekerja</label>
                                                <div class="input-group">
                                                    <input type="number" class="form-control" name="length_of_work" placeholder="Ketik Disini" onchange="updateData" onkeypress="updateData" required value="@isset($DataContent->job_details){{trim($DataContent->job_details->more_details->length_of_work)}}@endisset" />
                                                    <div class="input-group-append">
                                                        <div class="input-group-text" id="btnGroupAddon">Tahun</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Kantor Sebelumnya</label>
                                                <div class="input-group">
                                                    <input type="number" class="form-control" name="previous_employer" placeholder="Ketik Disini" onchange="updateData" onkeypress="updateData" value="@isset($DataContent->job_details){{trim($DataContent->job_details->more_details->previous_employer)}}@endisset" />
                                                    <div class="input-group-append">
                                                        <div class="input-group-text" id="btnGroupAddon">Tahun</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Alamat Kantor</label>
                                                <input type="text" class="form-control" name="office_address" placeholder="Masukkan Alamat Lengkap Disini" onchange="updateData" onkeypress="updateData" value="@isset($DataContent->job_details){{trim($DataContent->job_details->more_details->office_address)}}@endisset" required />
                                            </div>
                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">No Tlp Kantor</label>
                                                <input type="text" class="form-control" name="office_tele" placeholder="Masukkan No Tlp Kantor" onchange="updateData" onkeypress="updateData" required value="@isset($DataContent->job_details){{trim($DataContent->job_details->more_details->office_tele)}}@endisset" />
                                            </div>
                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">No Fax Kantor</label>
                                                <input type="text" class="form-control" name="office_fax" placeholder="Masukkan No Fax Kantor" onchange="updateData" onkeypress="updateData" required value="@isset($DataContent->job_details){{trim($DataContent->job_details->more_details->office_fax)}}@endisset" />
                                            </div>
                                            <div class="alert alert-dark" role="alert">Daftar Kekayaan</div>

                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Penghasilan Per Tahun ?</label>
                                                <div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="income_per_year" value="100-200" onchange="updateData" onkeypress="updateData" required @isset($DataContent->wealth_list)@if($DataContent->wealth_list->income_per_year==='100-200') checked @endif @endisset
                                                        />
                                                        <label class="form-check-label" for="income1">
                                                            100-250
                                                            Juta
                                                        </label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="income_per_year" value="250-500" onchange="updateData" onkeypress="updateData" required @isset($DataContent->wealth_list)@if($DataContent->wealth_list->income_per_year==='250-500') checked @endif @endisset/>
                                                        <label class="form-check-label" for="income2">
                                                            250-500
                                                            Juta
                                                        </label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="income_per_year" value=">500" onchange="updateData" onkeypress="updateData" required @isset($DataContent->wealth_list)@if($DataContent->wealth_list->income_per_year==='>500') checked @endif @endisset/>
                                                        <label class="form-check-label" for="income3">
                                                            >500
                                                            Juta
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Lokasi Rumah</label>
                                                <input type="text" class="form-control" name="residence_location" placeholder="Masukkan Masukkan Lokasi" value="@isset($DataContent->wealth_list){{trim($DataContent->wealth_list->residence_location)}}@endisset" onchange="updateData" onkeypress="updateData" required />
                                            </div>
                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Nilai NJOP</label>
                                                <input type="text" class="form-control" name="tax_object_selling_value" placeholder="Total Semua NJOP Rumah" value="@isset($DataContent->wealth_list){{trim($DataContent->wealth_list->tax_object_selling_value)}}@endisset" onchange="updateData" onkeypress="updateData" required />
                                            </div>
                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Deposito Bank</label>
                                                <input type="text" class="form-control" name="bank_time_deposit_value" placeholder="Sebutkan Bank-bank Deposito" value="@isset($DataContent->wealth_list){{trim($DataContent->wealth_list->bank_time_deposit_value)}}@endisset" onchange="updateData" onkeypress="updateData" required />
                                            </div>
                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Jumlah Deposito</label>
                                                <input type="text" class="form-control" name="bank_saving_value" placeholder="Sebutkan Total Nilai Deposito" value="@isset($DataContent->wealth_list){{trim($DataContent->wealth_list->bank_saving_value)}}@endisset" onchange="updateData" onkeypress="updateData" required />
                                            </div>
                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Lainnya</label>
                                                <input type="text" class="form-control" name="wealth_list_other" placeholder="Sebutkan Aset-aset lainnya" value="@isset($DataContent->wealth_list){{trim($DataContent->wealth_list->others)}}@endisset" onchange="updateData" onkeypress="updateData" required />
                                            </div>
                                            <div class="alert alert-dark" role="alert">
                                                Rekening Bank Untuk Penarikan
                                                <p>
                                                    Rekening Bank Nasabah Untuk Penyetoren Dana Penarikan Margin
                                                    (Hanya
                                                    Rekening Dibawah Ini Yang Dapat Saudara Pergunakan Untuk
                                                    Lalulintas
                                                    Margin)
                                                </p>
                                            </div>
                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Nama Bank</label>
                                                <input type="text" class="form-control" name="withdrwlbankname" placeholder="Masukkan Nama Lengkap Disini" value="@isset($DataContent->bank_details_for_withdrawls){{trim($DataContent->bank_details_for_withdrawls->full_name)}}@endisset" onchange="updateData" onkeypress="updateData" required />
                                            </div>
                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Cabang</label>
                                                <input type="text" class="form-control" name="withdrwlbankbranch" placeholder="Masukkan Nama Cabang Disini" value="@isset($DataContent->bank_details_for_withdrawls){{trim($DataContent->bank_details_for_withdrawls->branch)}}@endisset" onchange="updateData" onkeypress="updateData" required />
                                            </div>
                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Nomor AC</label>
                                                <input type="text" class="form-control" name="withdrwlbankaccno" placeholder="Sebutkan Nomor Rekening Dengan Benar" value="@isset($DataContent->bank_details_for_withdrawls){{trim($DataContent->bank_details_for_withdrawls->account_number)}}@endisset" onchange="updateData" onkeypress="updateData" required />
                                            </div>
                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Nomor Telepon</label>
                                                <input type="text" class="form-control" name="withdrwlbankphone" placeholder="(Kode Area) No." value="@isset($DataContent->bank_details_for_withdrawls){{trim($DataContent->bank_details_for_withdrawls->phone_number)}}@endisset" onchange="updateData" onkeypress="updateData" required />
                                            </div>
                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Jenis Akun</label>
                                                <div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="withdrwltypeaccbank" value="giro" onchange="updateData" onkeypress="updateData" required @isset($DataContent->bank_details_for_withdrawls) @if($DataContent->bank_details_for_withdrawls->account_type->value==='giro') checked @endif @endisset"/>
                                                        <label class="form-check-label" for="typeakunbankforwithdrawl1">Giro</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="withdrwltypeaccbank" value="saving" onchange="updateData" onkeypress="updateData" required @isset($DataContent->bank_details_for_withdrawls) @if($DataContent->bank_details_for_withdrawls->account_type->value==='saving') checked @endif @endisset"/>
                                                        <label class="form-check-label" for="typeakunbankforwithdrawl2">Tabungan</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="withdrwltypeaccbank" value="other" onchange="updateData" onkeypress="updateData" required @isset($DataContent->bank_details_for_withdrawls) @if($DataContent->bank_details_for_withdrawls->account_type->value==='other') checked @endif @endisset"/>
                                                        <label class="form-check-label" for="typeakunbankforwithdrawl3">Lainnya</label>
                                                    </div>
                                                </div>
                                                <input type="text" class="form-control" name="withdrwltypeaccbankinput" placeholder="Sebutkan" value="@isset($DataContent->bank_details_for_withdrawls){{trim($DataContent->bank_details_for_withdrawls->account_type->input)}}@endisset" onchange="updateData" onkeypress="updateData" />
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="alert alert-dark" role="alert">Dokumen Yang Dilampirkan</div>
                                            <div class="form-group" style="display: none;">
                                                <div class="card mb-4">
                                                    <div class="card-body">
                                                        <form action="{{route('uploadimg')}}" enctype="multipart/form-data" class="dropzone coloured_photo">
                                                            @csrf
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="required">Fotokopi KTP/SIM/KITAS/PASSPOR</label>
                                                <div class="card mb-4">
                                                    <div class="card-body">
                                                        <h5 class="mb-4">Fotokopi KTP/SIM/KITAS/PASSPOR</h5>
                                                        <form action="{{route('uploadimg')}}" enctype="multipart/form-data" class="dropzone id_card">
                                                            @csrf
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Pasfoto Berwarna</label>
                                                <div class="card mb-4">
                                                    <div class="card-body">
                                                        <h5 class="mb-4">Pasfoto Berwarna</h5>
                                                        <form action="{{route('uploadimg')}}" enctype="multipart/form-data" class="dropzone coloured_photo">
                                                            @csrf
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Tanda Tangan Spesimen</label>
                                                <div class="card mb-4">
                                                    <div class="card-body">
                                                        <h5 class="mb-4">Tanda Tangan Spesimen</h5>
                                                        <form action="{{route('uploadimg')}}" enctype="multipart/form-data" class="dropzone specimen_signature">
                                                            @csrf
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">Cover Buku Tabungan</label>
                                                <div class="card mb-4">
                                                    <div class="card-body">
                                                        <h5 class="mb-4">Cover Buku Tabungan</h5>
                                                        <form action="{{route('uploadimg')}}" enctype="multipart/form-data" class="dropzone bank_statement">
                                                            @csrf
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group position-relative error-l-100">
                                                <label>Lainnya</label>
                                                <div class="card mb-4">
                                                    <div class="card-body">
                                                        <h5 class="mb-4">Lainnya</h5>
                                                        <form action="{{route('uploadimg')}}" enctype="multipart/form-data" class="dropzone other_image">
                                                            @csrf
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group position-relative error-l-100">
                                                <label>
                                                    PERNYATAAN KEBENARAN DAN TANGGUNG
                                                    JAWAB
                                                </label>
                                                <p class="text-justify">
                                                    Dengan mengisi kolom "YA" di bawah ini, saya menyatakan bahwa
                                                    semua
                                                    informasi dan semua dokumen yang saya lampirkan dalam APLIKASI
                                                    PEMBUKAAN
                                                    REKENING TRANSAKSI SECARA ELEKTRONIK ON-LINE adalah benar dan
                                                    tepat.
                                                    Saya akan bertanggung jawab penuh apabila dikemudian hari
                                                    terjadi
                                                    sesuatu hal sebuhungan dengan ketidakbenaran data yang saya
                                                    berikan.
                                                </p>
                                                <div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="truth_resposibility_agreement" value="yes" onchange="updateData" onkeypress="updateData" required @isset($DataContent->truth_resposibility_agreement) @if($DataContent->truth_resposibility_agreement==='yes') checked @endif @endisset/>
                                                        <label class="form-check-label required">Ya</label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="truth_resposibility_agreement" value="no" onchange="updateData" onkeypress="updateData" required @isset($DataContent->truth_resposibility_agreement) @if($DataContent->truth_resposibility_agreement==='no') checked @endif @endisset/>
                                                        <label class="form-check-label">Tidak</label>
                                                    </div>
                                                </div>
                                                <p>Menyatakan pada tanggal : {{date('d-m-Y',strtotime(date("m.d.y")))}}</p>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div id="step-2">
                                <form id="form-step-2" class="tooltip-label-right" novalidate style="padding: 0px 22px;">
                                    <h3>
                                        Ketentuan Penyajian Profil Perusahaan Pialang Berjangka - Formulir Nomor
                                        107.PBK.01
                                    </h3>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="alert alert-dark" role="alert">PROFIL PERUSAHAAN PIALANG BERJANGKA</div>

                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Nama</label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">PT. Maxco Futures</div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Alamat</label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">
                                                        Gedung Panin Pusat Lantai Dasar
                                                        <br />Jl. Jend. Sudirman Kav. 1. Senayan, Jakarta 10270
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">No Telepon</label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">021-7205868 (Hunting)</div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Faksimili</label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">021-5710974</div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Email</label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">cs@maxcofutures.co.id</div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Home Page</label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">www.maxcofutures.co.id</div>
                                                </div>
                                            </div>
                                            <div class="alert alert-dark" role="alert">Susunan Pengurus PT MAXCO FUTURES</div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Direktur Utama</label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">Muhammad Yanwar Pohan</div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Direktur</label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">Ade Manuel Ortega</div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Direktur Kepatuhan</label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">Rudi Anto, S, Sos</div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Komisaris Utama</label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">Ronald H.Lalamentik</div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Komisaris</label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">Tofvin</div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">
                                                    Sususnan Pemegang Saham
                                                    Perusahaan
                                                </label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">
                                                        Ronald H.Lalamentik
                                                        <br />Tofvin
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">
                                                    Nomor dan Tanggal Izin Usaha
                                                    dari
                                                    Bappebti
                                                </label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">353/BAPPEBTI/SI/IV/2004 – Tanggal 30 April 2004</div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">
                                                    Nomor dan Tanggal Keanggotaan
                                                    Bursa Berjangka
                                                </label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">SPAB-057/BBJ/12/03 – Tanggal 30 Desember 2003</div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">
                                                    Nomor dan Tanggal Keanggotaan
                                                    Lembaga Kliring
                                                    Berjangka
                                                </label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">23/AK-KBI/V/2004 – Tanggal 16 November 2014</div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">
                                                    Nomor dan Tanggal Persetujuan
                                                    sebagai Peserta Sistem
                                                    Perdagangan Alternatif
                                                </label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">23/AK-KBI/V/2004 – Tanggal 16 November 2014</div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">
                                                    Nama Penyelengara Sistem
                                                    Perdangan Alternatif
                                                </label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">PT. PAN EMPEROR</div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">
                                                    Kontrak Berjangka Yang
                                                    Diperdangangkan *)
                                                </label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext"></div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">
                                                    Kontrak Derivatif Syariah
                                                    Yang
                                                    Diperdangangkan *)
                                                </label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="alert alert-dark" role="alert">Kontrak Derivatif dalam Sistem Perdangangan ALternatif *)</div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Forex</label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">
                                                        GBP/USD, EUR/USD, AUD/USD, NZD/USD, USD/JPY, USD/CHF,
                                                        USD/CAD
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Cross Rate</label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">
                                                        GBP/JPY, GBP/AUD, EUR/AUD, AUD/JPY, AUD/NZD, EUR/CHF,
                                                        EUR/CAD,
                                                        Etc
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Loco</label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">London Gold & Precious Metals</div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Crude Oil</label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">NYMEX crude Oil</div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Stock Indexes</label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">Dow Jones, S&P500, Nasdaq, Indeks Saham Hong Kong, etc</div>
                                                </div>
                                            </div>

                                            <div class="alert alert-dark" role="alert">
                                                Kontrak Derivatif dalam Sistem Perdagangan Alternatif dengan volume
                                                minimum 0,1
                                                (nol koma satu) lot yang Diperdagangkan *)
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Forex</label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">
                                                        GBP/USD, EUR/USD, AUD/USD, NZD/USD, USD/JPY, USD/CHF,
                                                        USD/CAD
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Cross Rate</label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">
                                                        GBP/JPY, GBP/AUD, EUR/AUD, AUD/JPY, AUD/NZD, EUR/CHF,
                                                        EUR/CAD,
                                                        Etc
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Loco</label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">London Gold & Precious Metals</div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Crude Oil</label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">NYMEX crude Oil</div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Stock Indexes</label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">Dow Jones, S&P500, Nasdaq, Indeks Saham Hong Kong, etc</div>
                                                </div>
                                            </div>

                                            <div class="alert alert-dark" role="alert">
                                                Nama-Nama Wakil Pialang Berjangka yang Bekerja di Perusahaan Pialang
                                                Berjangka
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-sm-12">
                                                    <div class="form-control-plaintext">
                                                        MUHAMMAD YANWAR POHAN
                                                        <!-- <br />EUIS FERAWATI -->
                                                        <br />IRWAN JULIAN
                                                        <br />IRVAN SUTANTO
                                                        <br />WIDIASTUTI
                                                        <br />MELINA OCTOVIA
                                                        <br />SULUH ADIL WICAKSONO
                                                        <br>LENNY KARTIKA
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="alert alert-dark" role="alert">
                                                Nama-Nama Wakil Pialang Berjangka yang secara khusus ditunjuk oleh
                                                Pialang
                                                Berjangka untuk melakukakn verifikaasi dalam rangka penerimaan
                                                Nasabah
                                                elektronik online
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-sm-12">
                                                    <div class="form-control-plaintext">
                                                        LENNY KARTIKA
                                                        <br />WIDIASTUTI
                                                        <br />MELINA OCTOVIA
                                                        <br />IRWAN JULIAN
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <!-- ================================================================ -->
                                        <div class="col-sm-4">
                                            <div class="alert alert-dark" role="alert">
                                                Nomor Rekening Terpisah (Segregeted Account) Perusahaan Pialang
                                                berjangka
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Nama</label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">PT. Maxco Futures</div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Alamat</label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">
                                                        Gedung Panin Pusat Lantai Dasar
                                                        <br />Jl. Jend. Sudirman Kav. 1. Senayan, Jakarta 10270
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Bank</label>
                                                <div class="col-sm-8">
                                                    <div class="form-control-plaintext">
                                                        BCA Sudirman (Jakarta)
                                                        <br />No. Rekening Terpisah
                                                        <br />035 311 5666 (Rupiah)
                                                        <br />035 311 9777 (USD)
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="alert alert-dark" role="alert">PERNYATAAN TELAH MEMBACA PROFIL PERUSAHAAN PIALANG BERJANGKA</div>
                                            <div class="form-group position-relative error-l-100">
                                                <label>
                                                    <p class="text-justify">
                                                        Dengan mengisi kolom "YA" di bawah ini, saya menyatakan bahwa
                                                        saya
                                                        telah
                                                        membaca dan menerima informasi PROFIL PERUSAHAAN PIALANG
                                                        BERJANGKA,
                                                        mengeri
                                                        dan memahami isinya.
                                                    </p>
                                                </label>

                                                <div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="statement_has_been_received_information_futures_company" value="Yes" required @isset($DataContent->trading_statement) @if($DataContent->trading_statement->statement_has_been_received_information_futures_company==='Yes') checked @endif @endisset />
                                                        <label class="form-check-label required">Ya</label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="statement_has_been_received_information_futures_company" value="No" required @isset($DataContent->trading_statement) @if($DataContent->trading_statement->statement_has_been_received_information_futures_company==='No') checked @endif @endisset />
                                                        <label class="form-check-label">Tidak</label>
                                                    </div>
                                                </div>
                                                <p>Menyatakan pada tanggal : {{date('d-m-Y',strtotime(date("m.d.y")))}}</p>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- next -->
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <h6>Yang mengisi formulir dibawah ini :</h6>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Nama Lengkap</label>
                                                <div class="col-sm-8">
                                                    <span class="form-control-plaintext" data-bind="fullname"></span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Tempat Lahir</label>
                                                <div class="col-sm-8">
                                                    <span class="form-control-plaintext" data-bind="place_of_birth"></span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Tangal Lahir</label>
                                                <div class="col-sm-8">
                                                    <span class="form-control-plaintext" data-bind="date_of_birth"></span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Alamat</label>
                                                <div class="col-sm-8">
                                                    <span class="form-control-plaintext" data-bind="home_address"></span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Kode Pos</label>
                                                <div class="col-sm-8">
                                                    <span class="form-control-plaintext" data-bind="post_code"></span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Nomor Identitas</label>
                                                <div class="col-sm-8">
                                                    <span class="form-control-plaintext" data-bind="identity_number"></span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">No Demo Acc.</label>
                                                <div class="col-sm-8">
                                                    <span class="form-control-plaintext" data-bind="no_demo_acc"></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <h3>
                                                Pernyataan Telah Melakukan Simulasi Perdagangan
                                                Berjangka
                                                Komoditi - Formulir : 107.PBK.02.1
                                            </h3>
                                            <div class="form-group position-relative error-l-100">
                                                <label></label>
                                                <p class="text-justify required">
                                                    Dengan mengisi kolom "YA" di bawah ini, saya menyatakan bahawa
                                                    saya
                                                    telah
                                                    melakukan simulasi bertransaksi di bidang Perdagangan Berjangka
                                                    Komoditi
                                                    pada PT. Maxco Futures, dan telah memahami tata cara
                                                    bertransaksidi
                                                    bidang
                                                    Perdagangan Berjangka Komoditi.
                                                    <br />Demikian Pernyataan ini dibuat dengan sebenarnya dalam keadaan
                                                    sadar, sehat
                                                    jasmani dan rohani serta tanpa paksaan apapun dari pihak
                                                    manapun.
                                                </p>
                                                <div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="statement_for_trading_simulation_on_demo_account" value="yes" required @isset($DataContent->trading_statement) @if($DataContent->trading_statement->statement_for_trading_simulation_on_demo_account==='yes') checked @endif @endisset />
                                                        <label class="form-check-label" for="trxsimulationyes">Ya</label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="statement_for_trading_simulation_on_demo_account" value="no" required @isset($DataContent->trading_statement) @if($DataContent->trading_statement->statement_for_trading_simulation_on_demo_account==='no') checked @endif @endisset/>
                                                        <label class="form-check-label" for="trxsimulationno">Tidak</label>
                                                    </div>
                                                </div>
                                                <p>Menyatakan pada tanggal : {{date('d-m-Y',strtotime(date("m.d.y")))}}</p>
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <h3>
                                                Surat Penyataan Telah Berpengalaman Melaksanakan
                                                Transaksi
                                                Perdagangan Berjangka Komoditi - Formulir : 107.PBK.02.2
                                            </h3>
                                            <div class="form-group position-relative error-l-100">
                                                <label></label>
                                                <p class="text-justify required">
                                                    Dengan mengisi kolom "YA" di bawah ini, saya menyatakan bahwa
                                                    saya
                                                    telah
                                                    memiliki pengalaman yang mencukupi dalam melaksanakan transaksi
                                                    Perdagangan
                                                    Berjangka karena pernah bertransaksi pada Perusahaan Pialang
                                                    Berjangka **),
                                                    dan telah memahami tentang tata cara bertransaksi Perdagangan
                                                    Berjangka.
                                                    Demikian Peryatanan ini dibuat denga sebenarnya dalam keadaan
                                                    sadar,
                                                    sehat
                                                    jasmani dan rohani serta tanpa paksaan apapun dari pihak
                                                    manapun.
                                                    <br />Demikian Pernyataan ini dibuat dengan sebenarnya dalam keadaan
                                                    sadar, sehat
                                                    jasmani dan rohani serta tanpa paksaan apapun dari pihak
                                                    manapun.
                                                </p>
                                                <div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="statement_letter_has_been_experienced_on_trading" value="yes" required @isset($DataContent->trading_statement) @if($DataContent->trading_statement->statement_letter_has_been_experienced_on_trading==='yes') checked @endif @endisset/>
                                                        <label class="form-check-label" for="experttradingyes">Ya</label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="statement_letter_has_been_experienced_on_trading" value="no" required @isset($DataContent->trading_statement) @if($DataContent->trading_statement->statement_letter_has_been_experienced_on_trading==='no') checked @endif @endisset/>
                                                        <label class="form-check-label" for="experttradingno">Tidak</label>
                                                    </div>
                                                </div>
                                                <p>Menyatakan pada tanggal : {{date('d-m-Y',strtotime(date("m.d.y")))}}</p>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div id="step-3">
                                <form id="form-step-3" class="tooltip-label-right" novalidate style="padding: 0px 22px;">
                                    <div class="row">
                                        <h3>
                                            Dokumen Pemberitahuan Adanya Resiko Yang Harus Disampaikan Oleh Pialang
                                            Berjangka Untuk Transaksi Kontrak Derivatif Dalam Sistem Perdagangan
                                            Alternatif
                                            - Formulir Nomor 107.PBK.04.2
                                        </h3>
                                        <div class="col-md-4">
                                            <div class="alert alert-dark" role="alert">
                                                Dokumen Pemberitahuan Adanya Resiko Yang Harus Disampaikan Oleh
                                                Pialang
                                                Berjangka Untuk Transaksi Kontrak Derivatif Dalam Sistem Perdagangan
                                                Alternatif
                                                - Formulir Nomor 107.PBK.04.2
                                            </div>

                                            <h5>PERHATIAN!</h5>
                                            <h5 class="text-justify">
                                                MOHON UNTUK MEMBACA DENGAN TELITI DAN
                                                MENYETUJUI
                                                SETIAP
                                                ISI DOKUMEN DENGAN CARA
                                                "TICK / CENTANG" DI KOTAK YANG TERSEDIA
                                            </h5>
                                            <p class="text-justify">
                                                Dokumen Pemberitahuan Adanya Risiko ini disampaikan kepada Anda
                                                sesuai
                                                dengan
                                                Pasal
                                                50 ayat (2) Undang-Undang Nomor 32 Tahun 1997 tentang Perdagangan
                                                Berjangka
                                                Komoditi
                                                sebagaimana telah diubah dengan Undang-Undang Nomor 10 Tahun 2011
                                                tentang
                                                Perubahan
                                                Atas Undang-Undang Nomor 32 Tahun 1997 Tentang Perdagangan Berjangka
                                                Komoditi.
                                                <br />Maksud dokumen ini adalah memberitahukan bahwa kemungkinan kerugian
                                                atau
                                                keuntungan
                                                dalam perdagangan Kontrak Derivatif dalam Sistem Perdagangan
                                                Alternatif bisa
                                                mencapai jumlah yang sangat besar. Oleh karena itu, Anda harus
                                                berhati-hati
                                                dalam
                                                memutuskan untuk melakukan transaksi, apakah kondisi keuangan Anda
                                                mencukupi.
                                            </p>

                                            <div class="form-group">
                                                <label class="required">
                                                    • 1.
                                                    Perdagangan Kontrak Derivatif dalam Sistem Perdagangan
                                                    Alternatif belum
                                                    tentu layak
                                                    bagi semua investor.
                                                    <br />Anda dapat menderita kerugian dalam jumlah besar dan dalam
                                                    jangka waktu
                                                    singkat.
                                                    Jumlah kerugian uang dimungkinkan dapat melebihi jumlah uang
                                                    yang
                                                    pertama
                                                    kali Anda
                                                    setor (Margin Awal) ke Pialang Berjangka Anda. Anda mungkin
                                                    menderita
                                                    kerugian
                                                    seluruh Margin dan Margin tambahan yang ditempatkan pada Pialang
                                                    Berjangka
                                                    untuk
                                                    mempertahankan posisi Kontrak Derivatif dalam Sistem Perdagangan
                                                    Alternatif
                                                    Anda.
                                                    Hal ini disebabkan Perdagangan Berjangka sangat dipengaruhi oleh
                                                    mekanisme
                                                    leverage,
                                                    dimana dengan jumlah investasi dalam bentuk yang relatif kecil
                                                    dapat
                                                    digunakan untuk
                                                    membuka posisi dengan aset yang bernilai jauh lebih tinggi.
                                                    Apabila Anda
                                                    tidak siap
                                                    dengan risiko seperti ini, sebaiknya Anda tidak melakukan
                                                    perdagangan
                                                    Kontrak
                                                    Derivatif dalam Sistem Perdagangan Alternatif.
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="risk_1_title_en_ref" value="1" required @isset($DataContent->risk_document) @if($DataContent->risk_document->risk_1_title_en_ref==='1') checked @endif @endisset />
                                                    <label class="form-check-label">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="required">
                                                    • 2.
                                                    Perdagangan Kontrak Derivatif dalam Sistem Perdagangan
                                                    Alternatif
                                                    mempunyai
                                                    risiko
                                                    dan mempunyai kemungkinan kerugian yang tidak terbatas yang jauh
                                                    lebih
                                                    besar
                                                    dari
                                                    jumlah uang yang disetor (Margin) ke Pialang Berjangka.
                                                    <br />Kontrak Derivatif dalam Sistem Perdagangan Alternatif sama
                                                    dengan produk
                                                    keuangan
                                                    lainnya yang mempunyai risiko tinggi, Anda sebaiknya tidak
                                                    menaruh
                                                    risiko
                                                    terhadap
                                                    dana yang Anda tidak siap untuk menderita rugi, seperti tabungan
                                                    pensiun,
                                                    dana
                                                    kesehatan atau dana untuk keadaan darurat, dana yang disediakan
                                                    untuk
                                                    pendidikan
                                                    atau kepemilikan rumah, dana yang diperoleh dari pinjaman
                                                    pendidikan
                                                    atau
                                                    gadai,
                                                    atau dana yang digunakan untuk memenuhi kebutuhan
                                                    sehari-hari.
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="risk_2_title_en_ref" value="1" required @isset($DataContent->risk_document) @if($DataContent->risk_document->risk_2_title_en_ref==='1') checked @endif @endisset />
                                                    <label class="form-check-label" for="pbk0402point2read">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>
                                                    • 3.
                                                    Berhati-hatilah terhadap pernyataan bahwa Anda pasti mendapatkan
                                                    keuntungan
                                                    besar
                                                    dari perdagangan Kontrak Derivatif dalam Sistem Perdagangan
                                                    Alternatif.
                                                    <br />Meskipun perdagangan Kontrak Derivatif dalam Sistem Perdagangan
                                                    Alternatif
                                                    dapat
                                                    memberikan keuntungan yang besar dan cepat, namun hal tersebut
                                                    tidak
                                                    pasti,
                                                    bahkan
                                                    dapat menimbulkan kerugian yang besar dan cepat juga. Seperti
                                                    produk
                                                    keuangan
                                                    lainnya, tidak ada yang dinamakan “pasti untung”.
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="risk_3_title_en_ref" value="1" required @isset($DataContent->risk_document) @if($DataContent->risk_document->risk_3_title_en_ref==='1') checked @endif @endisset />
                                                    <label class="form-check-label" for="pbk0402point3read">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="required">
                                                    • 4.
                                                    Disebabkan adanya mekanisme leverage dan sifat dari transaksi
                                                    Kontrak
                                                    Derivatif
                                                    dalam Sistem Perdagangan Alternatif, Anda dapat merasakan dampak
                                                    bahwa
                                                    Anda
                                                    menderita kerugian dalam waktu cepat.
                                                    <br />Keuntungan maupun kerugian dalam transaksi akan langsung
                                                    dikredit atau
                                                    didebet ke
                                                    rekening Anda, paling lambat secara harian. Apabila pergerakan
                                                    di pasar
                                                    terhadap
                                                    Kontrak Derivatif dalam Sistem Perdagangan Alternatif menurunkan
                                                    nilai
                                                    posisi Anda
                                                    dalam Kontrak Derivatif dalam Sistem Perdagangan Alternatif,
                                                    dengan kata
                                                    lain
                                                    berlawanan dengan posisi yang Anda ambil, Anda diwajibkan untuk
                                                    menambah
                                                    dana untuk
                                                    pemenuhan kewajiban Margin ke perusahaan Pialang Berjangka.
                                                    Apabila
                                                    rekening
                                                    Anda
                                                    berada dibawah minimum Margin yang telah ditetapkan Lembaga
                                                    Kliring
                                                    Berjangka atau
                                                    Pialang Berjangka, maka posisi Anda dapat dilikuidasi pada saat
                                                    rugi,
                                                    dan
                                                    Anda wajib
                                                    menyelesaikan defisit (jika ada) dalam rekening Anda.
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="risk_4_title_en_ref" value="1" required @isset($DataContent->risk_document) @if($DataContent->risk_document->risk_4_title_en_ref==='1') checked @endif @endisset/>
                                                    <label class="form-check-label" for="pbk0402point4read">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="required">
                                                    • 5.
                                                    Pada saat pasar dalam keadaan tertentu, Anda mungkin akan sulit
                                                    atau
                                                    tidak
                                                    mungkin
                                                    melikuidasi posisi.
                                                    <br />Pada umumnya Anda harus melakukan transaksi mengambil posisi
                                                    yang
                                                    berlawanan
                                                    dengan
                                                    maksud melikuidasi posisi (offset) jika ingin melikuidasi posisi
                                                    dalam
                                                    Kontrak
                                                    Derivatif dalam Sistem Perdagangan Alternatif. Apabila Anda
                                                    tidak dapat
                                                    melikuidasi
                                                    posisi Kontrak Derivatif dalam Sistem Perdagangan Alternatif,
                                                    Anda tidak
                                                    dapat
                                                    merealisasikan keuntungan pada nilai posisi tersebut atau
                                                    mencegah
                                                    kerugian
                                                    yang
                                                    lebih tinggi. Kemungkinan tidak dapat melikuidasi dapat terjadi,
                                                    antara
                                                    lain: jika
                                                    perdagangan berhenti dikarenakan aktivitas perdagangan yang
                                                    tidak lazim
                                                    pada
                                                    Kontrak
                                                    Derivatif atau subjek Kontrak Derivatif, atau terjadi kerusakan
                                                    sistem
                                                    pada
                                                    Pialang
                                                    Berjangka Peserta Sistem Perdagangan Alternatif atau Pedagang
                                                    Berjangka
                                                    Penyelenggara Sistem Perdagangan Alternatif. Bahkan apabila Anda
                                                    dapat
                                                    melikuidasi
                                                    posisi tersebut, Anda mungkin terpaksa melakukannya pada harga
                                                    yang
                                                    menimbulkan
                                                    kerugian besar.
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="risk_5_title_en_ref" value="1" required @isset($DataContent->risk_document) @if($DataContent->risk_document->risk_5_title_en_ref==='1') checked @endif @endisset/>
                                                    <label class="form-check-label" for="pbk0402point5read">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="required">
                                                    • 6.
                                                    Pada saat pasar dalam keadaan tertentu, Anda mungkin akan sulit
                                                    atau
                                                    tidak
                                                    mungkin
                                                    mengelola risiko atas posisi terbuka Kontrak Derivatif dalam
                                                    Sistem
                                                    Perdagangan
                                                    Alternatif dengan cara membuka posisi dengan nilai yang sama
                                                    namun
                                                    dengan
                                                    posisi
                                                    yang berlawanan dalam kontrak bulan yang berbeda, dalam pasar
                                                    yang
                                                    berbeda
                                                    atau
                                                    dalam “subjek Kontrak Derivatif dalam Sistem Perdagangan
                                                    Alternatif”
                                                    yang
                                                    berbeda.
                                                    <br />Kemungkinan untuk tidak dapat mengambil posisi dalam rangka
                                                    membatasi
                                                    risiko
                                                    yang
                                                    timbul, contohnya; jika perdagangan dihentikan pada pasar yang
                                                    berbeda
                                                    disebabkan
                                                    aktivitas perdagangan yang tidak lazim pada Kontrak Derivatif
                                                    dalam
                                                    Sistem
                                                    Perdagangan Alternatif atau “Kontrak Derivatif dalam Sistem
                                                    Perdagangan
                                                    Alternatif”.
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="risk_6_title_en_ref" value="1" required @isset($DataContent->risk_document) @if($DataContent->risk_document->risk_6_title_en_ref==='1') checked @endif @endisset/>
                                                    <label class="form-check-label" for="pbk0402point6read">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="required">
                                                    • 7.
                                                    Anda dapat menderita kerugian yang disebabkan kegagalan sistem
                                                    informasi.
                                                    <br />Sebagaimana yang terjadi pada setiap transaksi keuangan, Anda
                                                    dapat
                                                    menderita
                                                    kerugian jika amanat untuk melaksanakan transaksi Kontrak
                                                    Derivatif
                                                    dalam
                                                    Sistem
                                                    Perdagangan Alternatif tidak dapat dilakukan karena kegagalan
                                                    sistem
                                                    informasi di
                                                    Bursa Berjangka, Pedagang Penyelenggara Sistem Perdagangan
                                                    Alternatif ,
                                                    maupun
                                                    sistem di Pialang Berjangka Peserta Sistem Perdagangan
                                                    Alternatif yang
                                                    mengelola
                                                    posisi Anda. Kerugian Anda akan semakin besar jika Pialang
                                                    Berjangka
                                                    yang
                                                    mengelola
                                                    posisi Anda tidak memiliki sistem informasi cadangan atau
                                                    prosedur yang
                                                    layak.
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="risk_7_title_en_ref" value="1" required @isset($DataContent->risk_document) @if($DataContent->risk_document->risk_7_title_en_ref==='1') checked @endif @endisset/>
                                                    <label class="form-check-label" for="pbk0402point7read">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="required">
                                                    • 8.
                                                    Semua Kontrak Derivatif dalam Sistem Perdagangan Alternatif
                                                    mempunyai
                                                    risiko, dan
                                                    tidak ada strategi berdagang yang dapat menjamin untuk
                                                    menghilangkan
                                                    risiko
                                                    tersebut.
                                                    <br />Strategi dengan menggunakan kombinasi posisi seperti spread,
                                                    dapat sama
                                                    berisiko
                                                    seperti posisi long atau short. Melakukan Perdagangan Berjangka
                                                    memerlukan
                                                    pengetahuan mengenai Kontrak Derivatif dalam Sistem Perdagangan
                                                    Alternatif
                                                    dan pasar
                                                    berjangka.
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="risk_8_title_en_ref" value="1" required @isset($DataContent->risk_document) @if($DataContent->risk_document->risk_8_title_en_ref==='1') checked @endif @endisset/>
                                                    <label class="form-check-label" for="pbk0402point8read">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="required">
                                                    • 9.
                                                    Strategi perdagangan harian dalam Kontrak Derivatif dalam Sistem
                                                    Perdagangan
                                                    Alternatif dan produk lainnya memiliki risiko khusus.
                                                    <br />Seperti pada produk keuangan lainnya, pihak yang ingin membeli
                                                    atau
                                                    menjual
                                                    Kontrak
                                                    Derivatif dalam Sistem Perdagangan Alternatif yang sama dalam
                                                    satu hari
                                                    untuk
                                                    mendapat keuntungan dari perubahan harga pada hari tersebut
                                                    (“day
                                                    traders”)
                                                    akan
                                                    memiliki beberapa risiko tertentu antara lain jumlah komisi yang
                                                    besar,
                                                    risiko
                                                    terkena efek pengungkit (“exposure to leverage”), dan persaingan
                                                    dengan
                                                    pedagang
                                                    profesional. Anda harus mengerti risiko tersebut dan memiliki
                                                    pengalaman
                                                    yang
                                                    memadai sebelum melakukan perdagangan harian (“day
                                                    trading”).
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="risk_9_title_en_ref" value="1" required @isset($DataContent->risk_document) @if($DataContent->risk_document->risk_9_title_en_ref==='1') checked @endif @endisset/>
                                                    <label class="form-check-label" for="pbk0402point9read">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="required">
                                                    • 10.
                                                    Menetapkan amanat bersyarat, Kontrak Derivatif dalam Sistem
                                                    Perdagangan
                                                    Alternatif
                                                    dilikuidasi pada keadaan tertentu untuk membatasi rugi (stop
                                                    loss),
                                                    mungkin
                                                    tidak
                                                    akan dapat membatasi kerugian Anda sampai jumlah tertentu
                                                    saja.
                                                    <br />Amanat bersyarat tersebut mungkin tidak dapat dilaksanakan
                                                    karena
                                                    terjadi
                                                    kondisi
                                                    pasar yang tidak memungkinkan melikuidasi Kontrak Derivatif
                                                    dalam Sistem
                                                    Perdagangan
                                                    Alternatif.
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="risk_10_title_en_ref" value="1" required @isset($DataContent->risk_document) @if($DataContent->risk_document->risk_10_title_en_ref==='1') checked @endif @endisset/>
                                                    <label class="form-check-label" for="pbk0402point10read">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="required">
                                                    • 11.
                                                    Anda harus membaca dengan seksama dan memahami Perjanjian
                                                    Pemberian
                                                    Amanat
                                                    Nasabah
                                                    dengan Pialang Berjangka Anda sebelum melakukan transaksi
                                                    Kontrak
                                                    Derivatif
                                                    dalam
                                                    Sistem Perdagangan Alternatif.
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="risk_11_title_en_ref" value="1" required @isset($DataContent->risk_document) @if($DataContent->risk_document->risk_11_title_en_ref==='1') checked @endif @endisset/>
                                                    <label class="form-check-label" for="pbk0402point11read">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="required">
                                                    • 12.
                                                    Pernyataan singkat ini tidak dapat memuat secara rinci seluruh
                                                    risiko
                                                    atau
                                                    aspek
                                                    penting lainnya tentang Perdagangan Berjangka. Oleh karena itu
                                                    Anda
                                                    harus
                                                    mempelajari kegiatan Perdagangan Berjangka secara cermat sebelum
                                                    memutuskan
                                                    melakukan transaksi.
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="risk_12_title_en_ref" value="1" required @isset($DataContent->risk_document) @if($DataContent->risk_document->risk_12_title_en_ref==='1') checked @endif @endisset />
                                                    <label class="form-check-label" for="pbk0402point12read">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="required">
                                                    • 13.
                                                    Dokumen Pemberitahuan Adanya Risiko (Risk Disclosure) ini dibuat
                                                    dalam
                                                    Bahasa
                                                    Indonesia.
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="risk_13_title_en_ref" value="1" required @isset($DataContent->risk_document) @if($DataContent->risk_document->risk_13_title_en_ref==='1') checked @endif @endisset/>
                                                    <label class="form-check-label" for="pbk0402point13read">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="required">
                                                    SAYA SUDAH MEMBACA DAN MEMAHAMI
                                                    PERNYATAAN MENERIMA PEMBERITAHUAN ADANYA RISIKO
                                                    <br />Dengan mengisi kolom “YA” di bawah, saya menyatakan bahwa saya
                                                    telah
                                                    menerima
                                                    “DOKUMEN PEMBERITAHUAN ADANYA RISIKO” mengerti dan menyetujui
                                                    isinya.
                                                    Pernyataan menerima
                                                </label>
                                                <div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="risk_document_radio" value="yes" @isset($DataContent->risk_document) @if($DataContent->risk_document->radio==='yes') checked @endif @endisset />
                                                        <label class="form-check-label">Ya</label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="risk_document_radio" value="no" @isset($DataContent->risk_document) @if($DataContent->risk_document->radio==='no') checked @endif @endisset />
                                                        <label class="form-check-label">Tidak</label>
                                                    </div>
                                                </div>
                                                <p>Menyatakan pada tanggal : {{date('d-m-Y',strtotime(date("m.d.y")))}}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="alert alert-dark" role="alert">
                                                Perjanjian Pemberian Amanat Secara Elektronik Online Untuk Transaksi
                                                Kontrak
                                                Derivatif Dalam Sistem Perdagangan Alternatif - Formulir Nomor
                                                107.PBK.05.2
                                                - Formulir Nomor 107.PBK.05.2
                                            </div>
                                            <h5>PERHATIAN!</h5>
                                            <h5 class="text-justify">
                                                PERJANJIAN INI MERUPAKAN KONTRAK HUKUM. HARAP
                                                DIBACA
                                                DENGAN SEKSAMA.
                                                "TICK / CENTANG" DI KOTAK YANG TERSEDIA
                                            </h5>
                                            <p>
                                                Pada hari ini ,
                                                <b>
                                                    {{date('l',strtotime(date("m.d.y")))}}, tanggal {{date('d',strtotime(date("m.d.y")))}},bulan {{date('F',strtotime(date("m.d.y")))}}, tahun bulan {{date('Y',strtotime(date("m.d.y")))}} kami
                                                    yang
                                                    mengisi
                                                    perjanjian dibawah ini:
                                                </b>
                                            </p>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Nama</label>
                                                <div class="col-sm-8">
                                                    <span class="form-control-plaintext" data-bind="fullname"></span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Pekerjaan/Jabatan</label>
                                                <div class="col-sm-8">
                                                    <span class="form-control-plaintext" data-bind="job_details_title"></span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Alamat</label>
                                                <div class="col-sm-8">
                                                    <span class="form-control-plaintext" data-bind="home_address"></span>
                                                </div>
                                            </div>
                                            <div class="form-group position-relative error-l-100">
                                                <label>
                                                    dalam hal ini bertindak untuk dan atas nama PT. Maxco Futures
                                                    yang
                                                    selanjutnya disebut Pialang Berjangka,
                                                    Nasabah dan Pialang Berjangka secara bersama – sama selanjutnya
                                                    disebut
                                                    Para Pihak.
                                                    Para pihak sepakat untuk mengadakan Perjanjian Pemberian Amanat
                                                    untuk
                                                    melakukan transaksi penjualan maupun pembelian Kontrak Derivatif
                                                    dalam
                                                    Sistem Perdagangan Alternatif dengan ketentuan sebagai
                                                    berikut:
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <label class="required">
                                                    1. Margin dan Pembayaran Lainnya
                                                    <br />(1)Nasabah menempatkan sejumlah dana(Margin) ke Rekening
                                                    Terpisah
                                                    (Segregated Account) Pialang Berjangka sebagai Margin Awal dan
                                                    wajib
                                                    mempertahankannya sebagaimana ditetapkan.
                                                    <br />(2)membayar biaya-biaya yang diperlukan untuk transaksi, yaitu
                                                    biaya
                                                    transaksi, pajak, komisi, dan biaya pelayanan, biaya bunga
                                                    sesuai
                                                    tingkat yang berlaku, dan biaya lainnya yang dapat
                                                    dipertanggungjawabkan berkaitan dengan transaksi sesuai amanat
                                                    Nasabah, maupun biaya rekening Nasabah.
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="trading_conditions_1_title_en_ref" value="1" required @isset($DataContent->trading_condition) @if($DataContent->trading_condition->trading_conditions_1_title_en_ref==='1') checked @endif @endisset/>
                                                    <label class="form-check-label">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="required">
                                                    2. Pelaksanaan Transaksi
                                                    <br />(1)Setiap transaksi Nasabah dilaksanakan secara elektronik
                                                    on-line oleh
                                                    Nasabah yang bersangkutan;
                                                    <br />(2)Setiap amanat Nasabah yang diterima dapat langsung
                                                    dilaksanakan
                                                    sepanjang nilai Margin yang tersedia pada rekeningnya mencukupi
                                                    dan eksekusinya dapat menimbulkan perbedaan waktu terhadap
                                                    proses pelaksanaan transaksi tersebut. Nasabah harus mengetahui
                                                    posisi Margin dan posisi terbuka sebelum memberikan amanat untuk
                                                    transaksi berikutnya.
                                                    <br />(3)Setiap transaksi Nasabah secara bilateral dilawankan dengan
                                                    Penyelenggara Sistem Perdagangan Alternatif PT. Maxco Futures
                                                    yang
                                                    bekerjasama dengan Pialang Berjangka.
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="trading_conditions_2_title_en_ref" value="1" required @isset($DataContent->trading_condition) @if($DataContent->trading_condition->trading_conditions_2_title_en_ref==='1') checked @endif @endisset />
                                                    <label class="form-check-label" for="dotrax">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="required">
                                                    3. Kewajiban Memelihara Margin
                                                    <br />(1)Nasabah wajib memelihara/memenuhi tingkat Margin yang harus
                                                    tersedia di rekening pada Pialang Berjangka sesuai dengan jumlah
                                                    yang telah ditetapkan baik diminta ataupun tidak oleh Pialang
                                                    Berjangka.
                                                    <br />(2)Apabila jumlah Margin memerlukan penambahan maka Pialang
                                                    Berjangka wajib memberitahukan dan memintakan kepada Nasabah
                                                    untuk menambah Margin segera.
                                                    <br />(3)Apabila jumlah Margin memerlukan tambahan (Call Margin) maka
                                                    Nasabah wajib melakukan penyerahan Call Margin selambat-
                                                    lambatnya sebelum dimulai hari perdagangan berikutnya. Kewajiban
                                                    Nasabah sehubungan dengan penyerahan Call Margin tidak terbatas
                                                    pada jumlah Margin awal.
                                                    <br />(4)Pialang Berjangka tidak berkewajiban melaksanakan amanat
                                                    untuk
                                                    melakukan transaksi yang baru dari Nasabah sebelum Call Margin
                                                    dipenuhi.
                                                    <br />(5)Untuk memenuhi kewajiban Call Margin dan keuangan lainnya
                                                    dari
                                                    Nasabah, Pialang Berjangka dapat mencairkan dana Nasabah yang
                                                    ada di Pialang Berjangka.
                                                    <br />
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="trading_conditions_3_title_en_ref" value="1" required @isset($DataContent->trading_condition) @if($DataContent->trading_condition->trading_conditions_3_title_en_ref==='1') checked @endif @endisset />
                                                    <label class="form-check-label" for="margin1">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="required">
                                                    4. Hak Pialang Berjangka Melikuidasi Posisi Nasabah
                                                    <br />Nasabah bertanggung jawab memantau/mengetahui posisi terbukanya
                                                    secara terus- menerus dan memenuhi kewajibannya. Apabila dalam
                                                    jangka
                                                    waktu tertentu dana pada rekening Nasabah kurang dari yang
                                                    dipersyaratkan, Pialang Berjangka dapat menutup posisi terbuka
                                                    Nasabah
                                                    secara keseluruhan atau sebagian, membatasi transaksi, atau
                                                    tindakan
                                                    lain untuk melindungi diri dalam pemenuhan Margin tersebut
                                                    dengan
                                                    terlebih dahulu memberitahu atau tanpa memberitahu Nasabah dan
                                                    Pialang Berjangka tidak bertanggung jawab atas kerugian yang
                                                    timbul
                                                    akibat tindakan tersebut.
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="trading_conditions_4_title_en_ref" required value="1" @isset($DataContent->trading_condition) @if($DataContent->trading_condition->trading_conditions_4_title_en_ref==='1') checked @endif @endisset />
                                                    <label class="form-check-label" for="likuiditas1">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="required">
                                                    5. Penggantian Kerugian Tidak Adanya Penutupan Posisi
                                                    Apabila Nasabah tidak mampu melakukan penutupan atas transaksi
                                                    yang
                                                    jatuh tempo, Pialang Berjangka dapat melakukan penutupan atas
                                                    transaksi
                                                    Nasabah yang terjadi. Nasabah wajib membayar biaya-biaya,
                                                    termasuk
                                                    biaya kerugian dan premi yang telah dibayarkan oleh Pialang
                                                    Berjangka,
                                                    dan apabila Nasabah lalai untuk membayar biaya-biaya tersebut,
                                                    Pialang
                                                    Berjangka berhak untuk mengambil pembayaran dari dana Nasabah.
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="trading_conditions_5_title_en_ref" value="1" required @isset($DataContent->trading_condition) @if($DataContent->trading_condition->trading_conditions_5_title_en_ref==='1') checked @endif @endisset />
                                                    <label class="form-check-label" for="closeposition1">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="required">
                                                    6. Pialang Berjangka Dapat Membatasi Posisi
                                                    <br />Nasabah mengakui hak Pialang Berjangka untuk membatasi posisi
                                                    terbuka
                                                    Kontrak dan Nasabah tidak melakukan transaksi melebihi batas
                                                    yang telah
                                                    ditetapkan tersebut.
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="trading_conditions_6_title_en_ref" value="1" required @isset($DataContent->trading_condition) @if($DataContent->trading_condition->trading_conditions_6_title_en_ref==='1') checked @endif @endisset />
                                                    <label class="form-check-label" for="limitposisi1">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="required">
                                                    7. Tidak Ada Jaminan atas Informasi atau Rekomendasi
                                                    Nasabah mengakui bahwa:
                                                    <br />(1)Informasi dan rekomendasi yang diberikan oleh Pialang
                                                    Berjangka
                                                    kepada Nasabah tidak selalu lengkap dan perlu diverifikasi.
                                                    <br />(2)Pialang Berjangka tidak menjamin bahwa informasi dan
                                                    rekomendasi
                                                    yang diberikan merupakan informasi yang akurat dan lengkap.
                                                    <br />(3)Informasi dan rekomendasi yang diberikan oleh Wakil Pialang
                                                    Berjangka
                                                    yang satu dengan yang lain mungkin berbeda karena perbedaan
                                                    analisis fundamental atau teknikal. Nasabah menyadari bahwa ada
                                                    kemungkinan Pialang Berjangka dan pihak terafiliasinya memiliki
                                                    posisi di pasar dan memberikan rekomendasi tidak konsisten
                                                    kepada
                                                    Nasabah.
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="trading_conditions_7_title_en_ref" required value="1" @isset($DataContent->trading_condition) @if($DataContent->trading_condition->trading_conditions_7_title_en_ref==='1') checked @endif @endisset />
                                                    <label class="form-check-label" for="accurateinfo1">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="required">
                                                    8. Pembatasan Tanggung Jawab Pialang Berjangka.
                                                    <br />(1)Pialang Berjangka tidak bertanggung jawab untuk memberikan
                                                    penilaian
                                                    kepada Nasabah mengenai iklim, pasar, keadaan politik dan
                                                    ekonomi
                                                    nasional dan internasional, nilai Kontrak Derivatif, kolateral,
                                                    atau
                                                    memberikan nasihat mengenai keadaan pasar. Pialang Berjangka
                                                    hanya memberikan pelayanan untuk melakukan transaksi secara
                                                    jujur
                                                    serta memberikan laporan atas transaksi tersebut.
                                                    <br />(2)Perdagangan sewaktu-waktu dapat dihentikan oleh pihak yang
                                                    memiliki
                                                    otoritas (Bappebti/Bursa Berjangka) tanpa pemberitahuan terlebih
                                                    dahulu kepada Nasabah. Atas posisi terbuka yang masih dimiliki
                                                    oleh
                                                    Nasabah pada saat perdagangan tersebut dihentikan, maka akan
                                                    diselesaikan (likuidasi) berdasarkan pada peraturan/ketentuan
                                                    yang
                                                    dikeluarkan dan ditetapkan oleh pihak otoritas tersebut, dan
                                                    semua
                                                    kerugian serta biaya yang timbul sebagai akibat dihentikannya
                                                    transaksi oleh pihak otoritas perdagangan tersebut, menjadi
                                                    beban
                                                    dan tanggung jawab Nasabah sepenuhnya.
                                                    <br />
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="trading_conditions_8_title_en_ref" required value="1" @isset($DataContent->trading_condition) @if($DataContent->trading_condition->trading_conditions_8_title_en_ref==='1') checked @endif @endisset />
                                                    <label class="form-check-label" for="limitationofliability1">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="required">
                                                    9. Transaksi Harus Mematuhi Peraturan Yang Berlaku
                                                    <br />Semua transaksi dilakukan sendiri oleh Nasabah dan wajib
                                                    mematuhi
                                                    peraturan perundang-undangan di bidang Perdagangan Berjangka,
                                                    kebiasaan dan interpretasi resmi yang ditetapkan oleh Bappebti
                                                    atau
                                                    Bursa
                                                    Berjangka.
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="trading_conditions_9_title_en_ref" value="1" required @isset($DataContent->trading_condition) @if($DataContent->trading_condition->trading_conditions_9_title_en_ref==='1') checked @endif @endisset />
                                                    <label class="form-check-label" for="complyregulations1">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="required">
                                                    10. Pialang Berjangka tidak Bertanggung jawab atas Kegagalan
                                                    Komunikasi
                                                    <br />Pialang Berjangka tidak bertanggung jawab atas keterlambatan
                                                    atau tidak
                                                    tepat waktunya pengiriman amanat atau informasi lainnya yang
                                                    disebabkan oleh kerusakan fasilitas komunikasi atau sebab lain
                                                    diluar
                                                    kontrol Pialang Berjangka.
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="trading_conditions_10_title_en_ref" value="1" required @isset($DataContent->trading_condition) @if($DataContent->trading_condition->trading_conditions_10_title_en_ref==='1') checked @endif @endisset />
                                                    <label class="form-check-label" for="pbk0402point10read">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="required">
                                                    11. Konfirmasi
                                                    <br />(1)Konfirmasi dari Nasabah dapat berupa surat, telex, media
                                                    lain, surat
                                                    elektronik, secara tertulis ataupun rekaman suara.
                                                    <br />(2)Pialang Berjangka berkewajiban menyampaikan konfirmasi
                                                    transaksi,
                                                    laporan rekening, permintaan Call Margin, dan pemberitahuan
                                                    lainnya
                                                    kepada Nasabah secara akurat, benar dan secepatnya pada alamat
                                                    (email) Nasabah sesuai dengan yang tertera dalam rekening
                                                    Nasabah.
                                                    Apabila dalam jangka waktu 2 x 24 jam setelah amanat jual atau
                                                    beli
                                                    disampaikan, tetapi Nasabah belum menerima konfirmasi melalui
                                                    alamat email Nasabah dan/atau sistem transaksi, Nasabah segera
                                                    memberitahukan hal tersebut kepada Pialang Berjangka melalui
                                                    telepon dan disusul dengan pemberitahuan tertulis.
                                                    <br />(3)Jika dalam waktu 2 x 24 jam sejak tanggal penerimaan
                                                    konfirmasi
                                                    tersebut tidak ada sanggahan dari Nasabah maka konfirmasi
                                                    Pialang
                                                    Berjangka dianggap benar dan sah.
                                                    <br />(4)Kekeliruan atas konfirmasi yang diterbitkan Pialang Berjangka
                                                    akan
                                                    diperbaiki oleh Pialang Berjangka sesuai keadaan yang sebenarnya
                                                    dan demi hukum konfirmasi yang lama batal.
                                                    <br />(5)Nasabah tidak bertanggung jawab atas transaksi yang
                                                    dilaksanakan atas
                                                    rekeningnya apabila konfirmasi tersebut tidak disampaikan secara
                                                    benar dan akurat.
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="trading_conditions_11_title_en_ref" value="1" required @isset($DataContent->trading_condition) @if($DataContent->trading_condition->trading_conditions_11_title_en_ref==='1') checked @endif @endisset />
                                                    <label class="form-check-label" for="pbk0402point11read">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="required">
                                                    12. Kebenaran Informasi Nasabah
                                                    <br />Nasabah memberikan informasi yang benar dan akurat mengenai data
                                                    Nasabah yang diminta oleh Pialang Berjangka dan akan
                                                    memberitahukan
                                                    paling lambat dalam waktu 3 (tiga) hari kerja setelah terjadi
                                                    perubahan,
                                                    termasuk perubahan kemampuan keuangannya untuk terus
                                                    melaksanakan
                                                    transaksi.
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="trading_conditions_12_title_en_ref" required value="1" @isset($DataContent->trading_condition) @if($DataContent->trading_condition->trading_conditions_12_title_en_ref==='1') checked @endif @endisset />
                                                    <label class="form-check-label" for="pbk0402point12read">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="required">
                                                    13. Komisi Transaksi
                                                    <br />Nasabah mengetahui dan menyetujui bahwa Pialang Berjangka berhak
                                                    untuk memungut komisi atas transaksi yang telah dilaksanakan,
                                                    dalam
                                                    jumlah sebagaimana akan ditetapkan dari waktu ke waktu oleh
                                                    Pialang
                                                    Berjangka. Perubahan beban (fees) dan biaya lainnya harus
                                                    disetujui
                                                    secara tertulis oleh Para Pihak.
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="trading_conditions_13_title_en_ref" required value="1" @isset($DataContent->trading_condition) @if($DataContent->trading_condition->trading_conditions_13_title_en_ref==='1') checked @endif @endisset />
                                                    <label class="form-check-label" for="pbk0402point13read">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="required">
                                                    14. Pemberian Kuasa
                                                    <br />Nasabah memberikan kuasa kepada Pialang Berjangka untuk
                                                    menghubungi bank, lembaga keuangan, Pialang Berjangka lain, atau
                                                    institusi lain yang terkait untuk memperoleh keterangan atau
                                                    verifikasi
                                                    mengenai informasi yang diterima dari Nasabah. Nasabah mengerti
                                                    bahwa
                                                    penelitian mengenai data hutang pribadi dan bisnis dapat
                                                    dilakukan oleh
                                                    Pialang Berjangka apabila diperlukan.Nasabah diberikan
                                                    kesempatan
                                                    untuk memberitahukan secara tertulis dalam jangka waktu yang
                                                    telah
                                                    disepakati untuk melengkapi persyaratan yang diperlukan.
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="trading_conditions_14_title_en_ref" value="1" required @isset($DataContent->trading_condition) @if($DataContent->trading_condition->trading_conditions_14_title_en_ref==='1') checked @endif @endisset />
                                                    <label class="form-check-label" for="pbk0402point14read">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="required">
                                                    15. Pemindahan Dana
                                                    <br />Pialang Berjangka dapat setiap saat mengalihkan dana dari satu
                                                    rekening
                                                    ke rekening lainnya berkaitan dengan kegiatan transaksi yang
                                                    dilakukan
                                                    Nasabah seperti pembayaran komisi, pembayaran biaya transaksi,
                                                    kliring
                                                    dan keterlambatan dalam memenuhi kewajibannya, tanpa terlebih
                                                    dahulu
                                                    memberitahukan kepada Nasabah. Transfer yang telah dilakukan
                                                    akan
                                                    segera diberitahukan secara tertulis kepada Nasabah.
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="trading_conditions_15_title_en_ref" value="1" required @isset($DataContent->trading_condition) @if($DataContent->trading_condition->trading_conditions_15_title_en_ref==='1') checked @endif @endisset />
                                                    <label class="form-check-label" for="pbk0402point15read">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="required">
                                                    16. Pemberitahuan
                                                    <br />(1)Semua komunikasi, uang, surat berharga, dan kekayaan lainnya
                                                    harus
                                                    dikirimkan langsung ke alamat Nasabah seperti tertera dalam
                                                    rekeningnya atau alamat lain yang ditetapkan/diberitahukan
                                                    secara
                                                    tertulis oleh Nasabah.
                                                    <br />(2)Semua uang, harus disetor atau ditransfer langsung oleh
                                                    Nasabah ke
                                                    Rekening Terpisah (Segregated Account) Pialang Berjangka:
                                                </label>
                                                <label>Nama</label>
                                                <div class="form-control-plaintext">PT. Maxco Futures</div>
                                                <label>Alamat</label>
                                                <div class="form-control-plaintext">
                                                    Gedung Panin Pusat Lantai Dasar Jl. Jend. Sudirman kav. 1
                                                    Senayan
                                                    Jakarta
                                                    10270
                                                </div>
                                                <label>Bank</label>
                                                <div class="form-control-plaintext">BCA Sudirman (Jakarta)</div>
                                                <label>No. Rekening Terpisah</label>
                                                <div class="form-control-plaintext">
                                                    035 311 5666 (IDR)
                                                    <br />035 311 9777 (USD)
                                                </div>
                                                <label>
                                                    dan dianggap sudah diterima oleh Pialang Berjangka apabila sudah
                                                    ada tanda terima bukti setor atau transfer dari pegawai Pialang
                                                    Berjangka.
                                                    (3)Semua surat berharga, kekayaan lainnya, atau komunikasi harus
                                                    dikirim kepadaPialang Berjangka:
                                                </label>
                                                <label>Nama</label>
                                                <div class="form-control-plaintext">PT. Maxco Futures</div>
                                                <label>Alamat</label>
                                                <div class="form-control-plaintext">
                                                    Gedung Panin Pusat Lantai Dasar Jl. Jend. Sudirman kav. 1
                                                    Senayan
                                                    Jakarta
                                                    10270
                                                </div>
                                                <label>Telepon</label>
                                                <div class="form-control-plaintext">021-720 5868 (Hunting)</div>
                                                <label>Faksimili</label>
                                                <div class="form-control-plaintext">021-571 0974</div>
                                                <label>Email</label>
                                                <div class="form-control-plaintext">cs@maxcofutures.co.id</div>
                                                <label>
                                                    dan dianggap sudah diterima oleh Pialang Berjangka apabila sudah
                                                    ada tanda bukti penerimaan dari pegawai Pialang Berjangka.
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="trading_conditions_16_title_en_ref" value="1" required @isset($DataContent->trading_condition) @if($DataContent->trading_condition->trading_conditions_16_title_en_ref==='1') checked @endif @endisset />
                                                    <label class="form-check-label" for="pbk0402point16read">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="required">
                                                    17. Dokumen Pemberitahuan Adanya Risiko
                                                    <br />Nasabah mengakui menerima dan mengerti Dokumen Pemberitahuan
                                                    Adanya Risiko.
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="trading_conditions_17_title_en_ref" value="1" required @isset($DataContent->trading_condition) @if($DataContent->trading_condition->trading_conditions_17_title_en_ref==='1') checked @endif @endisset />
                                                    <label class="form-check-label" for="pbk0402point17read">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="required">
                                                    18. Jangka Waktu Perjanjian dan Pengakhiran
                                                    <br />(1)Perjanjian ini mulai berlaku terhitung sejak tanggal
                                                    dilakukannya
                                                    konfirmasi oleh Pialang Berjangka dengan diterimanya Bukti
                                                    Konfirmasi
                                                    Penerimaan Nasabah dari Pialang Berjangka oleh Nasabah.
                                                    <br />(2)Nasabah dapat mengakhiri Perjanjian ini hanya jika Nasabah
                                                    sudah
                                                    tidak lagi memiliki posisi terbuka dan tidak ada kewajiban
                                                    Nasabah
                                                    yang diemban oleh atau terhutang kepada Pialang Berjangka.
                                                    <br />(3)Pengakhiran tidak membebaskan salah satu Pihak dari tanggung
                                                    jawab
                                                    atau kewajiban yang terjadi sebelum pemberitahuan tersebut.
                                                    <br />
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="trading_conditions_18_title_en_ref" value="1" required @isset($DataContent->trading_condition) @if($DataContent->trading_condition->trading_conditions_18_title_en_ref==='1') checked @endif @endisset />
                                                    <label class="form-check-label" for="pbk0402point18read">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="required">
                                                    19. Berakhirnya Perjanjian
                                                    <br />Perjanjian dapat berakhir dalam hal Nasabah:
                                                    <br />(1)dinyatakan pailit, memiliki hutang yang sangat besar, dalam
                                                    proses
                                                    peradilan, menjadi hilang ingatan, mengundurkan diri atau
                                                    meninggal;
                                                    <br />(2)tidak dapat memenuhi atau mematuhi perjanjian ini dan/atau
                                                    melakukan pelanggaran terhadapnya;
                                                    <br />(3)berkaitan dengan butir (1) dan (2) tersebut diatas, Pialang
                                                    Berjangka
                                                    dapat :
                                                    <br />i) meneruskan atau menutup posisi Nasabah tersebut
                                                    setelahmempertimbangkannya secara cermat dan jujur ; dan
                                                    <br />ii) menolaktransaksidari Nasabah.
                                                    <br />(4)Pengakhiran Perjanjian sebagaimana dimaksud dengan angka (1)
                                                    dan (2)
                                                    tersebut di atas tidak melepaskan kewajiban dari Para Pihak yang
                                                    berhubungan dengan penerimaan atau kewajiban pembayaran atau
                                                    pertanggungjawaban kewajiban lainnya yang timbul dari
                                                    Perjanjian.
                                                    <br />
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="trading_conditions_19_title_en_ref" value="1" required @isset($DataContent->trading_condition) @if($DataContent->trading_condition->trading_conditions_19_title_en_ref==='1') checked @endif @endisset />
                                                    <label class="form-check-label" for="pbk0402point19read">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="required">
                                                    20. Force Majeur
                                                    <br />Tidak ada satupun pihak di dalam Perjanjian dapat diminta
                                                    pertanggungjawabannya untuk suatu keterlambatan atau
                                                    terhalangnya
                                                    memenuhi kewajiban berdasarkan Perjanjian yang diakibatkan oleh
                                                    suatu
                                                    sebab yang berada di luar kemampuannya atau kekuasaannya (force
                                                    majeur), sepanjang pemberitahuan tertulis mengenai sebab itu
                                                    disampaikannya kepada pihak lain dalam Perjanjian dalam waktu
                                                    tidak
                                                    lebih dari 24 (dua puluh empat) jam sejak timbulnya sebab itu.
                                                    Yang dimaksud dengan Force Majeur dalam Perjanjian adalah
                                                    peristiwa
                                                    kebakaran, bencana alam (seperti gempa bumi, banjir, angin
                                                    topan,
                                                    petir),
                                                    pemogokan umum, huru hara, peperangan, perubahan terhadap
                                                    peraturan
                                                    perundang-undangan yang berlaku dan kondisi di bidang ekonomi,
                                                    keuangan dan Perdagangan Berjangka, pembatasan yang dilakukan
                                                    oleh
                                                    otoritas Perdagangan Berjangka dan Bursa Berjangka serta
                                                    terganggunya
                                                    sistem perdagangan, kliring dan penyelesaian transaksi Kontrak
                                                    Berjangka
                                                    di mana transaksi dilaksanakan yang secara langsung mempengaruhi
                                                    pelaksanaan pekerjaan berdasarkan Perjanjian.
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="trading_conditions_20_title_en_ref" value="1" required @isset($DataContent->trading_condition) @if($DataContent->trading_condition->trading_conditions_20_title_en_ref==='1') checked @endif @endisset />
                                                    <label class="form-check-label" for="pbk0402point20read">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="required">
                                                    21. Perubahan atas Isian dalam Perjanjian Pemberian Amanat
                                                    Perubahan atas isian dalam Perjanjian ini hanya dapat dilakukan
                                                    atas
                                                    persetujuan Para Pihak, atau Pialang Berjangka telah
                                                    memberitahukan
                                                    secara tertulis perubahan yang diinginkan, dan Nasabah tetap
                                                    memberikan
                                                    perintah untuk transaksi dengan tanpa memberikan tanggapan
                                                    secara
                                                    tertulis atas usul perubahan tersebut. Tindakan Nasabah tersebut
                                                    dianggap
                                                    setuju atas usul perubahan tersebut.
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="trading_conditions_21_title_en_ref" value="1" required @isset($DataContent->trading_condition) @if($DataContent->trading_condition->trading_conditions_21_title_en_ref==='1') checked @endif @endisset />
                                                    <label class="form-check-label" for="pbk0402point21read">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="required">
                                                    22. Penyelesaian Perselisihan
                                                    <br />(1)Semua perselisihan dan perbedaan pendapat yang timbul dalam
                                                    pelaksanaan Perjanjian ini wajib diselesaikan terlebih dahulu
                                                    secara
                                                    musyawarah untuk mencapai mufakat antara Para Pihak.
                                                    <br />(2)Apabila perselisihan dan perbedaan pendapat yang timbul tidak
                                                    dapat
                                                    diselesaikan secara musyawarah untuk mencapai mufakat, Para
                                                    Pihak
                                                    wajib memanfaatkan sarana penyelesaian perselisihan yang
                                                    tersedia di
                                                    Bursa Berjangka.
                                                    <br />(3)Apabila perselisihan dan perbedaan pendapat yang timbul tidak
                                                    dapat
                                                    diselesaikan melalui cara sebagaimana dimaksud pada angka (1)
                                                    dan
                                                    angka (2), maka Para Pihak sepakat untuk menyelesaikan
                                                    perselisihan
                                                    melalui *):
                                                    <br />a. Badan Arbitrase Perdagangan Berjangka Komoditi (BAKTI)
                                                    berdasarkan Peraturan dan Prosedur Badan Arbitrase Perdagangan
                                                    Berjangka Komoditi (BAKTI);atau
                                                    <br />b. Pengadilan Negeri Jakarta Selatan
                                                    <br />(4)Kantor atau kantor cabang Pialang Berjangka terdekat dengan
                                                    domisili
                                                    Nasabah tempat penyelesaian dalam hal terjadi perselisihan.
                                                    Daftar Kantor Kantor yang dipilih (salah
                                                    satu):
                                                    <br />a. Head Office - Address : Panin Bank Centre - First Floor Jl.
                                                    Jend.
                                                    Sudirman Kav. 1. Senayan, Jakarta Selatan 10270 Indonesia
                                                    <br />b. Surabaya - Address : Jl. Dharmahusada  no 60A, Mojo Gubeng,
                                                    Surabaya,Jawa Timur 60285 Indonesia
                                                    <br />
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="trading_conditions_22_title_en_ref" value="1" required @isset($DataContent->trading_condition) @if($DataContent->trading_condition->trading_conditions_22_title_en_ref==='1') checked @endif @endisset />
                                                    <label class="form-check-label" for="pbk0402point22read">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="required">
                                                    23. Bahasa
                                                    <br />Perjanjian ini dibuat dalam Bahasa Indonesia.
                                                </label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="trading_conditions_23_title_en_ref" value="1" required @isset($DataContent->trading_condition) @if($DataContent->trading_condition->trading_conditions_23_title_en_ref==='1') checked @endif @endisset />
                                                    <label class="form-check-label" for="pbk0402point23read">SAYA SUDAH MEMBACA DAN MEMAHAMI</label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="required">
                                                    Dengan mengisi kolom “YA” di bawah, saya menyatakan bahwa saya
                                                    telah
                                                    menerima "PERJANJIAN PEMBERIAN AMANAT TRANSAKSI KONTRAK
                                                    DERIVARIF SISTEM
                                                    PERDANGANGAN ALTERNATIF" mengerti dan menyetujui isinya.
                                                </label>
                                                <div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="step_4_footer" value="yes" @isset($DataContent->trading_condition) @if($DataContent->trading_condition->step_4_footer==='yes') checked @endif @endisset />
                                                        <label class="form-check-label" for="authorizationTrxYes">Ya</label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="step_4_footer" value="no" @isset($DataContent->trading_condition) @if($DataContent->trading_condition->step_4_footer==='no') checked @endif @endisset />
                                                        <label class="form-check-label" for="authorizationTrxNo">Tidak</label>
                                                    </div>
                                                </div>
                                                <p>Menyatakan pada tanggal : {{date('d-m-Y',strtotime(date("m.d.y")))}}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="alert alert-dark" role="alert">
                                                Peraturan Perdangangan (Trading Rules) Dalam Sistem Aplikasi
                                                Penerimaan
                                                Nasabah Secara Elektronik On-Line - Formulir Nomor 107.PBK.06
                                            </div>
                                            <div class="form-group">
                                                <h5>PT MAXCO FUTURES Online Trading Rules</h5>
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th scope="col">No</Fth>
                                                            <th scope="col">Nama Kontrak</th>
                                                            <th scope="col">Satuan Kontrak</th>
                                                            <th scope="col">Kode Kontrak</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <th>1</th>
                                                            <td>Kontrak EUR/USD (FOREX)</td>
                                                            <td>EUR 100,000 Fixed rate 10,000 EUR 100,000 Floating</td>
                                                            <td>
                                                                EU 1010_BBJ
                                                                <br />EU 10F_BBJ
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th>2</th>
                                                            <td>Kontrak GBP/USD (FOREX)</td>
                                                            <td>GBP 100,000 Fixed rate 10,000 GBP 100,000 Floating</td>
                                                            <td>
                                                                GU 1010_BBJ
                                                                <br />GU 10F_BBJ
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th>3</th>
                                                            <td>Kontrak AUD/USD (FOREX)</td>
                                                            <td>AUD 100,000 Fixed rate 10,000 AUD 100,000 Floating</td>
                                                            <td>
                                                                AU 1010_BBJ
                                                                <br />AU 10F_BBJ
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th>4</th>
                                                            <td>Kontrak NZD/USD (FOREX)</td>
                                                            <td>NZD 100,000 Fixed rate 10,000 NZD 100,000 Floating</td>
                                                            <td>
                                                                NU 1010_BBJ
                                                                <br />NU 10F_BBJ
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th>5</th>
                                                            <td>Kontrak USD/JPY (FOREX)</td>
                                                            <td>JPY 100,000 Fixed rate 10,000 JPY 100,000 Floating</td>
                                                            <td>
                                                                UJ 1010_BBJ
                                                                <br />UJ 10F_BBJ
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th>6</th>
                                                            <td>Kontrak USD/CHF (FOREX)</td>
                                                            <td>CHF 100,000 Fixed rate 10,000 CHF 100,000 Floating</td>
                                                            <td>
                                                                UC 1010_BBJ
                                                                <br />UC 10F_BBJ
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th>7</th>
                                                            <td>Kontrak USD/CAD (FOREX)</td>
                                                            <td>CAD 100,000 Fixed rate 10,000 CAD 100,000 Floating</td>
                                                            <td>
                                                                UK 1010_BBJ
                                                                <br />UK 10F_BBJ
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th>8</th>
                                                            <td>Kontrak UAD/NZD (Crossrate)</td>
                                                            <td>AUD 100,000 Fixed rate 10,000 AUD 100,000 Floating</td>
                                                            <td>
                                                                AN 1010_BBJ
                                                                <br />AN 10F_BBJ
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th>9</th>
                                                            <td>Kontrak EUR/JPY (Crossrate)</td>
                                                            <td>EUR 100,000 Fixed rate 10,000 EUR 100,000 Floating</td>
                                                            <td>
                                                                EJ 1H10_BBJ
                                                                <br />EJ 10F_BBJ
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th>10</th>
                                                            <td>Kontrak EUR/GBP (Crossrate)</td>
                                                            <td>GBP 100,000 Fixed rate 10,000 GBP 100,000 Floating</td>
                                                            <td>
                                                                EG 1H10_BBJ
                                                                <br />EG 10F_BBJ
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th>11</th>
                                                            <td>Kontrak EUR/AUD(Crossrate)</td>
                                                            <td>EUR 100,000 Fixed rate 10,000 EUR 100,000 Floating</td>
                                                            <td>
                                                                EA 1H10_BBJ
                                                                <br />EA 10F_BBJ
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th>12</th>
                                                            <td>Kontrak EUR/CAD (Crossrate)</td>
                                                            <td>
                                                                EUR 100,000 Fixed rate 10,000/USD EUR 100,000
                                                                Floating
                                                            </td>
                                                            <td>
                                                                EU 1010_BBJ
                                                                <br />EU 10F_BBJ
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th>13</th>
                                                            <td>Kontrak NZD/JPY (Crossrate)</td>
                                                            <td>
                                                                NZD 100,000 Fixed rate 10,000/USD NZD 100,000
                                                                Floating
                                                            </td>
                                                            <td>
                                                                NJ 1010_BBJ
                                                                <br />NJ 10F_BBJ
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th>14</th>
                                                            <td>Kontrak EUR/CHF (Crossrate)</td>
                                                            <td>EUR 100,000 Fixed rate 10,000 EUR 100,000 Floating</td>
                                                            <td>
                                                                EC 1010_BBJ
                                                                <br />EC 10F_BBJ
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th>15</th>
                                                            <td>Kontrak AUD/JPY(Crossrate)</td>
                                                            <td>AUD 100,000 Fixed rate 10,000 AUD 100,000 Floating</td>
                                                            <td>
                                                                AJ 1010_BBJ
                                                                <br />AJ 10F_BBJ
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th>16</th>
                                                            <td>Kontrak CHF/JPY (Crossrate)</td>
                                                            <td>
                                                                CHF 100,000 Fixed rate 10,000/USD CHF 100,000
                                                                Floating
                                                            </td>
                                                            <td>
                                                                CJ 1H10_BBJ
                                                                <br />CJ 10F_BBJ
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th>17</th>
                                                            <td>Kontrak GBP/JPY (Crossrate)</td>
                                                            <td>GBP 100,000 Fixed rate 10,000 GBP 100,000 Floating</td>
                                                            <td>
                                                                GJ 1H10_BBJ
                                                                <br />GJ 10F_BBJ
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th>18</th>
                                                            <td>Kontrak GBP/AUD (Crossrate)</td>
                                                            <td>GBP 100,000 Fixed rate 10,000 GBP 100,000 Floating</td>
                                                            <td>
                                                                GA 1H10_BBJ
                                                                <br />GA 10F_BBJ
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th>19</th>
                                                            <td>Kontrak GBP/CHF (Crossrate)</td>
                                                            <td>GBP 100,000 Fixed rate 10,000 GBP 100,000 Floating</td>
                                                            <td>
                                                                GC 1010_BBJ
                                                                <br />GC 10F_BBJ
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th>20</th>
                                                            <td>Kontrak XAU/USD (Precious Metal)</td>
                                                            <td>
                                                                XAU 100 Troy Ounce Fixed rate 10,000 XAU 100 Troy
                                                                Ounce
                                                                Floating
                                                            </td>
                                                            <td>
                                                                XUL10
                                                                <br />XULF
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th>21</th>
                                                            <td>Kontrak XAG/USD (Precious Metal)</td>
                                                            <td>
                                                                XAU 5000 Troy Ounce Fixed rate 10,000 XAU 5000 Troy
                                                                Ounce
                                                                Floating
                                                            </td>
                                                            <td>
                                                                XAG10_BBJ
                                                                <br />XAGF_BBJ
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                            <div class="form-group position-relative error-l-100">
                                                <label>
                                                    Dengan menigisi kolom "YA" di bawah ini, saya
                                                    menyatakan
                                                    bahwa sya telah membaca tentang PERATURAN PERDAGANGAN (Trading
                                                    Rules)
                                                    mengerti dan menerima ketentuan dalam bertransaksi. Demikian
                                                    Pernyataan
                                                    ini
                                                    dibuat dengan sebenarnya dalam keadaan sadar,sehat jasmani dan
                                                    rohani
                                                    serta
                                                    tanpa paksaan apapun dari pihak manapun. Penyataan
                                                    menerima
                                                </label>
                                                <div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="trading_rules" value="yes" @isset($DataContent->trading_rules) @if($DataContent->trading_rules==='yes') checked @endif @endisset/>
                                                        <label class="form-check-label">Ya</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="trading_rules" value="no" @isset($DataContent->trading_rules) @if($DataContent->trading_rules==='no') checked @endif @endisset/>
                                                        <label class="form-check-label">Tidak</label>
                                                    </div>
                                                </div>
                                                <label>Menyatakan pada tanggal : {{date('d-m-Y',strtotime(date("m.d.y")))}}</label>
                                            </div>
                                            <div class="alert alert-dark" role="alert">
                                                Pernyataan Bertanggung Jawab Atas Kode Akses Transaksi Nasabah
                                                (Personal
                                                Access Password) - Formulir Nomor 107.PBK.07
                                            </div>
                                            <div class="form-group">
                                                <h5>PT MAXCO FUTURES Online Trading Rules</h5>
                                                <table class="table">
                                                    <tbody>
                                                        <tr>
                                                            <th>Nama Lengkap</th>
                                                            <td><span data-bind="fullname"></span></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Tempat Lahir</th>
                                                            <td><span data-bind="place_of_birth"></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Tanggal Lahir</th>
                                                            <td><span data-bind="date_of_birth"></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Alamat</th>
                                                            <td><span data-bind="home_address"></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Kode Pos</th>
                                                            <td><span data-bind="post_code"></td>
                                                        </tr>
                                                        <tr>
                                                            <th>No. Indentitas</th>
                                                            <td><span data-bind="identity_number"></td>
                                                        </tr>
                                                        <tr>
                                                            <th>No Demo Acc.</th>
                                                            <td></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                            <div class="form-group position-relative error-l-100">
                                                <label class="required">
                                                    PERINGATAN !!! Pialang Berjangka, Wakil
                                                    Pialang
                                                    Berjangka,
                                                    pegawai Pialang Berjangka, atau pihak yang mimiliki kepentingan
                                                    dengan
                                                    Pialang Berjangka dilarang menerima kode akses transaksi Nasabah
                                                    (Personal
                                                    Accesss Password).
                                                    <br />Dengan mengisi kolom "YA" di bawah ini, saya menyatakan bahwa
                                                    saya
                                                    bertanggungjawab sepenuhnya terhadap kode akses transaksi
                                                    Nasabah
                                                    (Personal
                                                    Access Password) dan tidak menyerahkan kode akses transaksi
                                                    Nasabah
                                                    (Personal Access Password) ke pihak lain, terutama kepada
                                                    pegawai
                                                    Pialang
                                                    Berjangka atau piahk yang memiliki kepentingan dengan Pialang
                                                    Berjangka.
                                                    <br />Demikian Pernyataan ini dibuat dengan sebenarnya dalam keadaan
                                                    sadar,
                                                    sehat
                                                    jasmani dan rohani serta tanpa paksaan apapun dari pihak manapun.
                                                </label>
                                                <div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="pap_agreement" value="yes" @isset($DataContent->pap_agreement) @if($DataContent->pap_agreement==='yes') checked @endif @endisset/>
                                                        <label class="form-check-label">Ya</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="pap_agreement" value="no" @isset($DataContent->pap_agreement) @if($DataContent->pap_agreement==='no') checked @endif @endisset/>
                                                        <label class="form-check-label">Tidak</label>
                                                    </div>
                                                </div>
                                                <label>Menyatakan pada tanggal : {{date('d-m-Y',strtotime(date("m.d.y")))}}</label>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div id="step-4">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <h4 class="text-center">Buka Akun Live</h4>
                                        <!-- <div class="glyph">
                      <div class="glyph-icon simple-icon-check"></div>
                      <div class="class-name">simple-icon-check</div>
                    </div> -->
                                        <h2 class="text-center">Menunggu Persetujuan</h2>
                                        <h4 class="text-center">MT LIVE A/C NO. : <span data-bind="ac_live_no"></span></h4>
                                        <!-- <h4 class="text-center">Server : mt4-live.maxcofutures.co.id</h4> -->
                                        <div class="text-center">
                                            <a href="{{url('/')}}/deposit" class="btn btn-primary">DEPOSIT SEKARANG</a>
                                        </div>
                                        <h4 class="text-center">Anda telah berhasil mengirimkan permintaan Untuk membuka akun live MT4.</h4>
                                        <h4 class="text-center">MAXCO FUTURES akan menghubungin Anda untuk langkah selanjutnya dalam kurun waktu 24 jam. (tidak termasuk hari libur)</h4>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="btn-toolbar custom-toolbar text-center card-body pt-0 form-footer-wizard mb-4">
                            <button class="btn btn-secondary prev-btn" type="button">Previous</button>
                            <button class="btn btn-secondary next-btn" type="button">Next</button>
                            <button class="btn btn-secondary finish-btn" type="submit">Finish</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection

@section('jsonpage')
<script src="{{url('/')}}/cabinet/js/vendor/jquery.smartWizard.min.js"></script>
<script src="{{url('/')}}/cabinet/js/vendor/dropzone.min.js"></script>
<script src="{{url('/')}}/cabinet/js/vendor/sweetalert.min.js"></script>

<script>
    var renderImgAttc = false;
    var renderImgAttcOther = false;
    var minAge = new Date();
    var getYear = minAge.getFullYear();
    minAge.setFullYear(getYear - 21);
    var dd = minAge.getDate();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    var mm = minAge.getMonth() + 1; //January is 0!
    var yyyy = minAge.getFullYear();
    var minDate = yyyy + '-' + mm + '-' + dd;
    $('[name="date_of_birth"]').prop('max', minDate);


    var atvStep = "{{$DataContent->activeStep?$DataContent->activeStep:0}}";
    localStorage.setItem('activestepopnliveacc', atvStep);
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    const type = sessionStorage.getItem("register_account");
    if (type === "miniaccount") {
        $('.account_type_mini').prop('checked', true);
        $('.forex_idr').prop('checked', true);
        $('[name="future_contract"]').val('forex_idr');
        $('[name="account_type"]').val('mini');
    } else if (type === "usdaccount") {
        $('.account_type_reguler').prop('checked', true);
        $('.forex_usd').prop('checked', true);
        $('[name="future_contract"]').val('forex_usd');
        $('[name="account_type"]').val('regular');
    } else {
        // profesional
        $('.account_type_reguler').prop('checked', true);
        $('.forex_idr').prop('checked', true);
        $('[name="future_contract"]').val('forex_idr');
        $('[name="account_type"]').val('regular');
    }

    $('[name="future_contract_opt"]').on('change', function(event) {
        const val = event.currentTarget.value;
        $('[name="future_contract"]').val(val);
    });

    var position_value = "{{$DataContent->job_details?$DataContent->job_details->position->value:''}}";
    if (position_value !== "other") {
        $('#position_value_div').hide();
    } else {
        $('[name="position_input"]').prop('required', true);
    }
    $('[name="position_value"]').on('click', function(event) {
        const value = event.currentTarget.value;
        if (value === "other") {
            $('#position_value_div').show();
            $('[name="position_input"]').prop('required', true);
        } else {
            $('#position_value_div').hide();
            $('[name="position_input"]').removeAttr('required');
        }
    });

    var home_ownership_status = "{{$DataContent->home_ownership_status?$DataContent->home_ownership_status->value:''}}";
    if (home_ownership_status !== "other") {
        $('#houseownstatusdescriptiondiv').hide();
    } else {
        $('[name="houseownstatusdescription"]').prop('required', true);
    }
    $('[name="houseownstatusOptions"]').on('click', function(event) {
        const value = event.currentTarget.value;
        if (value === "other") {
            $('#houseownstatusdescriptiondiv').show();
            $('[name="houseownstatusdescription"]').prop('required', true);
        } else {
            $('#houseownstatusdescriptiondiv').hide();
            $('[name="houseownstatusdescription"]').removeAttr('required');
            $('#houseownstatusdescription-error').remove();
        }
    });

    var openint_account_purpose = "{{$DataContent->openint_account_purpose?$DataContent->openint_account_purpose->value:''}}";
    if (openint_account_purpose !== "other") {
        $('[name="openint_account_purpose_input"]').hide();
    } else {
        $('[name="openint_account_purpose_input"]').prop('required', true);
    }
    $('[name="openint_account_purpose"]').on('change', function() {
        const val = $(this).val();
        if (val === 'other') {
            $('[name="openint_account_purpose_input"]').show();
            $('[name="openint_account_purpose_input"]').prop('required', true);
        } else {
            $('[name="openint_account_purpose_input"]').hide();
            $('[name="openint_account_purpose_input"]').removeAttr('required');
            $('#openint_account_purpose_input-error').remove();

        }
    });

    var experince_on_investment_input = "{{$DataContent->experince_on_investment?$DataContent->experince_on_investment->value:''}}";
    if (experince_on_investment_input !== "Yes") {
        $('[name="experince_on_investment_input"]').hide();
    } else {
        $('[name="experince_on_investment_input"]').prop('required', true);
    }
    $('[name="experince_on_investment"]').on('change', function() {
        const val = $(this).val();
        if (val === 'Yes') {
            $('[name="experince_on_investment_input"]').show();
            $('[name="experince_on_investment_input"]').prop('required', true);
        } else {
            $('[name="experince_on_investment_input"]').hide();
            $('[name="experince_on_investment_input"]').removeAttr('required');
        }
    });

    var is_family_work_in_related_company = "{{$DataContent->is_family_work_in_related_company?$DataContent->is_family_work_in_related_company->value:''}}";
    if (is_family_work_in_related_company !== "Yes") {
        $('[name="is_family_work_in_related_company_input"]').hide();
    } else {
        $('[name="is_family_work_in_related_company_input"]').prop('required', true);
    }
    $('[name="is_family_work_in_related_company"]').on('change', function() {
        const val = $(this).val();
        if (val === 'Yes') {
            $('[name="is_family_work_in_related_company_input"]').show();
            $('[name="is_family_work_in_related_company_input"]').prop('required', true);
        } else {
            $('[name="is_family_work_in_related_company_input"]').hide();
            $('[name="is_family_work_in_related_company_input"]').removeAttr('required');

        }
    });

    var withdrwltypeaccbank = "{{$DataContent->bank_details_for_withdrawls?$DataContent->bank_details_for_withdrawls->account_type->value:''}}";
    if (withdrwltypeaccbank !== "other") {
        $('[name="withdrwltypeaccbankinput"]').hide();
    } else {
        $('[name="withdrwltypeaccbankinput"]').prop('required', true);
    }
    $('[name="withdrwltypeaccbank"]').on('change', function() {
        const val = $(this).val();
        if (val === 'other') {
            $('[name="withdrwltypeaccbankinput"]').show();
            $('[name="withdrwltypeaccbankinput"]').prop('required', true);
        } else {
            $('[name="withdrwltypeaccbankinput"]').hide();
            $('[name="withdrwltypeaccbankinput"]').removeAttr('required');
        }
    });

    $('[name="date_of_birth"]').on('change', function(event) {
        var date = event.currentTarget.value;
        var today = new Date();
        var birthday = new Date(date);
        var year = 0;
        if (today.getMonth() < birthday.getMonth()) {
            year = 1;
        } else if ((today.getMonth() == birthday.getMonth()) && today.getDate() < birthday.getDate()) {
            year = 1;
        }
        var age = today.getFullYear() - birthday.getFullYear() - year;
        if (age < 0) {
            age = 0;
        }
        if (age < 21) {
            $(this).val('');
            swal("Perhatian!", "Untuk memenuhi persyaratan dan peraturan  kami hanya dapat menerima klien yang berusia lebih dari 21 tahun.!", "warning");
        } else {

        }
    });

    function updateData() {};


    $(document).ready(function() {
        var idCard_imgUrl = '';
        var id_card = false;
        var colouredPhoto_imgUrl = '';
        var coloured_photo = false;
        var specimenSignature_imgUrl = '';
        var specimen_signature = false;
        var bankStatement_imgUrl = ''
        var bank_statement = false;
        var other_imgUrl = '';
        var attachedImage = [];

        function getDetailKYC() {
            var contentDt = getContent();
            var content = JSON.stringify(contentDt)
            $.get("{{route('apigetKycDetail')}}", {}, function(data, status) {

                var dataKyc = data.Data;
                $('[data-bind="fullname"]').text(dataKyc.full_name);
                $('[data-bind="identity_number"]').text(dataKyc.identity_number);
                $('[data-bind="place_of_birth"]').text(dataKyc.place_of_birth);
                $('[data-bind="date_of_birth"]').text(dataKyc.date_of_birth);
                $('[data-bind="home_address"]').text(dataKyc.home_address);
                $('[data-bind="post_code"]').text(dataKyc.post_code);
                $('[data-bind="job_details_title"]').text(dataKyc.job_details ? dataKyc.job_details.more_details.title : '');
                $('[data-bind="identity_number"]').text(dataKyc.identity_number);

                if (dataKyc.attached_documents.required_image.length > 0 && renderImgAttc !== true) {
                    var attchments = dataKyc.attached_documents.required_image;
                    console.log('attchments', attchments);
                    var findIdxIdCardImg = attchments.findIndex(x => x.type === "id_card");
                    if (attchments[findIdxIdCardImg]) {
                        var dataImageIdCard = attchments[findIdxIdCardImg];
                        if (dataImageIdCard.src.url !== "") {
                            idCard_imgUrl = dataImageIdCard.src.url;
                            var imgName = idCard_imgUrl.split('/');
                            var idCardHtml = `
                            <div class="dz-preview mb-3 dz-processing dz-image-preview dz-success dz-complete">
                                <div class="d-flex flex-row ">
                                    <div class="p-0 w-30 position-relative">
                                        <div class="dz-error-mark"><span><i></i></span></div>
                                        <div class="dz-success-mark"><span><i></i></span></div>
                                        <div class="preview-container"><img data-dz-thumbnail="" class="img-thumbnail border-0"
                                                alt="${imgName[2]}"
                                                src="http://maxco-api.bull-b.com/${dataImageIdCard.src.url}"><i
                                                class="simple-icon-doc preview-icon"></i></div>
                                    </div>
                                    <div class="pl-3 pt-2 pr-2 pb-1 w-70 dz-details position-relative">
                                        <div><span data-dz-name="">${imgName[2]}</span></div>
                                        <!-- <div class="text-primary text-extra-small" data-dz-size=""><strong>0.8</strong> MB</div> -->
                                    </div>
                                    <!--- <a href="#/" class="remove" data-dz-remove=""><i class="glyph-icon simple-icon-trash"></i></a> -->
                                </div>
                            </div>
                            `;
                            $('.id_card').append(idCardHtml);
                            $('.id_card').addClass('dz-started');

                        }
                    }

                    var findIdxIdClrphto = attchments.findIndex(x => x.type === "coloured_photo");
                    if (attchments[findIdxIdClrphto]) {
                        var dataImageClrPhoto = attchments[findIdxIdClrphto];
                        if (dataImageClrPhoto.src.url !== "") {
                            colouredPhoto_imgUrl = dataImageClrPhoto.src.url;
                            var imgName = colouredPhoto_imgUrl.split('/');
                            var idCardHtml = `
                            <div class="dz-preview mb-3 dz-processing dz-image-preview dz-success dz-complete">
                                <div class="d-flex flex-row ">
                                    <div class="p-0 w-30 position-relative">
                                        <div class="dz-error-mark"><span><i></i></span></div>
                                        <div class="dz-success-mark"><span><i></i></span></div>
                                        <div class="preview-container"><img data-dz-thumbnail="" class="img-thumbnail border-0"
                                                alt="${imgName[2]}"
                                                src="http://maxco-api.bull-b.com/${dataImageClrPhoto.src.url}"><i
                                                class="simple-icon-doc preview-icon"></i></div>
                                    </div>
                                    <div class="pl-3 pt-2 pr-2 pb-1 w-70 dz-details position-relative">
                                        <div><span data-dz-name="">${imgName[2]}</span></div>
                                        <!-- <div class="text-primary text-extra-small" data-dz-size=""><strong>0.8</strong> MB</div>-->
                                    </div>
                                    <!-- <a href="#/" class="remove" data-dz-remove=""><i class="glyph-icon simple-icon-trash"></i></a> -->
                                </div>
                            </div>
                            `;
                            $('.coloured_photo').append(idCardHtml);
                            $('.coloured_photo').addClass('dz-started');
                        }
                    }

                    var findIdxIdSign = attchments.findIndex(x => x.type === "specimen_signature");
                    if (attchments[findIdxIdSign]) {
                        console.log()
                        var dataImageSignPhoto = attchments[findIdxIdSign];
                        if (dataImageSignPhoto.src.url !== "") {
                            specimenSignature_imgUrl = dataImageSignPhoto.src.url;
                            var imgName = specimenSignature_imgUrl.split('/');
                            var idCardHtml = `
                            <div class="dz-preview mb-3 dz-processing dz-image-preview dz-success dz-complete">
                                <div class="d-flex flex-row ">
                                    <div class="p-0 w-30 position-relative">
                                        <div class="dz-error-mark"><span><i></i></span></div>
                                        <div class="dz-success-mark"><span><i></i></span></div>
                                        <div class="preview-container"><img data-dz-thumbnail="" class="img-thumbnail border-0"
                                                alt="${imgName[2]}"
                                                src="http://maxco-api.bull-b.com/${dataImageSignPhoto.src.url}"><i
                                                class="simple-icon-doc preview-icon"></i></div>
                                    </div>
                                    <div class="pl-3 pt-2 pr-2 pb-1 w-70 dz-details position-relative">
                                        <div><span data-dz-name="">${imgName[2]}</span></div>
                                        <!-- <div class="text-primary text-extra-small" data-dz-size=""><strong>0.8</strong> MB</div> -->
                                    </div>
                                    <!-- <a href="#/" class="remove" data-dz-remove=""><i class="glyph-icon simple-icon-trash"></i></a> -->
                                </div>
                            </div>
                            `;
                            $('.specimen_signature').append(idCardHtml);
                            $('.specimen_signature').addClass('dz-started');
                        }
                    }

                    var findIdxBankStatement = attchments.findIndex(x => x.type === "bank_statement");
                    if (attchments[findIdxBankStatement]) {
                        var dataImageBankStatement = attchments[findIdxBankStatement];
                        if (dataImageBankStatement.src.url !== "") {
                            bankStatement_imgUrl = dataImageBankStatement.src.url;
                            var imgName = bankStatement_imgUrl.split('/');
                            var idCardHtml = `
                            <div class="dz-preview mb-3 dz-processing dz-image-preview dz-success dz-complete">
                                <div class="d-flex flex-row ">
                                    <div class="p-0 w-30 position-relative">
                                        <div class="dz-error-mark"><span><i></i></span></div>
                                        <div class="dz-success-mark"><span><i></i></span></div>
                                        <div class="preview-container"><img data-dz-thumbnail="" class="img-thumbnail border-0"
                                                alt="${imgName[2]}"
                                                src="http://maxco-api.bull-b.com/${dataImageBankStatement.src.url}"><i
                                                class="simple-icon-doc preview-icon"></i></div>
                                    </div>
                                    <div class="pl-3 pt-2 pr-2 pb-1 w-70 dz-details position-relative">
                                        <div><span data-dz-name="">${imgName[2]}</span></div>
                                        <!-- <div class="text-primary text-extra-small" data-dz-size=""><strong>0.8</strong> MB</div> -->
                                    </div>
                                    <!--- <a href="#/" class="remove" data-dz-remove=""><i class="glyph-icon simple-icon-trash"></i></a> -->
                                </div>
                            </div>
                            `;
                            $('.bank_statement').append(idCardHtml);
                            $('.bank_statement').addClass('dz-started');
                        }
                    }
                    renderImgAttc = true;
                }

                if (dataKyc.attached_documents.other_image && dataKyc.attached_documents.other_image.src) {
                    if (dataKyc.attached_documents.other_image.src.url !== "") {
                        other_imgUrl = dataKyc.attached_documents.other_image.src.url;
                    }
                }
                if (dataKyc.attached_documents.other_image && dataKyc.attached_documents.other_image.src && renderImgAttcOther !== true) {
                    if (dataKyc.attached_documents.other_image.src.url !== "") {
                        var imgName = other_imgUrl.split('/');
                        var idCardHtml = `
                            <div class="dz-preview mb-3 dz-processing dz-image-preview dz-success dz-complete">
                                <div class="d-flex flex-row ">
                                    <div class="p-0 w-30 position-relative">
                                        <div class="dz-error-mark"><span><i></i></span></div>
                                        <div class="dz-success-mark"><span><i></i></span></div>
                                        <div class="preview-container"><img data-dz-thumbnail="" class="img-thumbnail border-0"
                                                alt="${imgName[2]}"
                                                src="http://maxco-api.bull-b.com/${other_imgUrl}"><i
                                                class="simple-icon-doc preview-icon"></i></div>
                                    </div>
                                    <div class="pl-3 pt-2 pr-2 pb-1 w-70 dz-details position-relative">
                                        <div><span data-dz-name="">${imgName[2]}</span></div>
                                    <!-- </div><a href="#/" class="remove" data-dz-remove=""><i class="glyph-icon simple-icon-trash"></i></a> -->
                                </div>
                            </div>
                            `;
                        $('.other_image').append(idCardHtml);
                        $('.other_image').addClass('dz-started');
                    }
                    renderImgAttcOther = true;
                }

            });
        };

        function getContent() {
            var contentDt = {
                'job_details': {
                    'more_details': {
                        'length_of_work': parseInt($('[name="length_of_work"]').val()),
                        'previous_employer': parseInt($('[name="previous_employer"]').val()),
                        'employer_name': $('[name="employer_name"]').val(),
                        'business_fields': $('[name="business_fields"]').val(),
                        'title': $('[name="title"]').val(),
                        'office_address': $('[name="office_address"]').val(),
                        'office_tele': $('[name="office_tele"]').val(),
                        'office_fax': $('[name="office_fax"]').val()
                    },
                    'position': {
                        'value': $($("input[name='position_value']:checked")).val() || '',
                        'input': $('[name="position_input"]').val()
                    }
                },
                'wealth_list': {
                    'income_per_year': $($("input[name='income_per_year']:checked")).val() || '',
                    'residence_location': $('[name="residence_location"]').val(),
                    'tax_object_selling_value': $('[name="tax_object_selling_value"]').val(),
                    'bank_time_deposit_value': $('[name="bank_time_deposit_value"]').val(),
                    'bank_saving_value': $('[name="bank_saving_value"]').val(),
                    'others': $('[name="wealth_list_other"]').val()
                },
                'bank_details_for_withdrawls': {
                    'full_name': $('[name="withdrwlbankname"]').val(),
                    'branch': $('[name="withdrwlbankbranch"]').val(),
                    'account_number': $('[name="withdrwlbankaccno"]').val(),
                    'phone_number': $('[name="withdrwlbankphone"]').val(),
                    'account_type': {
                        'value': $($("input[name='withdrwltypeaccbank']:checked")).val() || '',
                        'input': $('[name="withdrwltypeaccbankinput"]').val()
                    }
                },
                'emergency_contact_data': {
                    'name': $('[name="contactEmergencyName"]').val(),
                    'address': $('[name="contactEmergencyAddress"]').val(),
                    'post_code': $('[name="contactEmergencyPoscode"]').val(),
                    'home_phone_number': $('[name="contactEmergencyPhone"]').val(),
                    'relationship_with_the_contacts': $('[name="contactEmergencyRelationship"]').val()
                },
                'attached_documents': {
                    'id_card': true,
                    'coloured_photo': true,
                    'specimen_signature': true,
                    'other_count': 1,
                    'required_image': [{
                            'type': 'id_card',
                            'title': 'id_card_or_driving_license_passport',
                            'action_type': 0,
                            'src': {
                                'name': 'A8DE88AE-206B-4A94-A6D4-A6FDBEF2347A.png',
                                'size': 116,
                                'url': ''
                            }
                        },
                        {
                            'type': 'coloured_photo',
                            'title': 'coloured_photo',
                            'action_type': 0,
                            'src': {
                                'name': '72A0861E-4771-42DE-87D0-7D57A8CAA6C7.png',
                                'size': 116,
                                'url': colouredPhoto_imgUrl
                            }
                        },
                        {
                            'type': 'specimen_signature',
                            'title': 'specimen_signature',
                            'action_type': 0,
                            'src': {
                                'name': 'E85FAA5E-7219-4B9D-910D-3E3FB740EA1B.png',
                                'size': 116,
                                'url': specimenSignature_imgUrl
                            }
                        },
                        {
                            'type': 'bank_statement',
                            'title': 'bank_statement',
                            'action_type': 0,
                            'src': {
                                'name': 'A3F0FF6D-1A6E-4DAC-9D07-CF76D93C5BC9.png',
                                'size': 116,
                                'url': bankStatement_imgUrl
                            }
                        }
                    ],
                    'option': 'yes',
                    'other_image': {
                        'type': 'other_image',
                        'title': 'other_image',
                        'action_type': 0,
                        'src': {
                            'name': 'A8DE88AE-206B-4A94-A6D4-A6FDBEF2347A.png',
                            'size': 116,
                            'url': other_imgUrl
                        }
                    },
                },
                'trading_statement': {
                    'statement_for_trading_simulation_on_demo_account': $($("input[name='statement_for_trading_simulation_on_demo_account']:checked")).val() || '',
                    'statement_letter_has_been_experienced_on_trading': $($("input[name='statement_letter_has_been_experienced_on_trading']:checked")).val() || '',
                    'statement_has_been_received_information_futures_company': $($("input[name='statement_has_been_received_information_futures_company']:checked")).val() || ''
                },
                'risk_document': {
                    'risk_1_title_en_ref': $($("input[name='risk_1_title_en_ref']:checked")).val() || '',
                    'risk_2_title_en_ref': $($("input[name='risk_2_title_en_ref']:checked")).val() || '',
                    'risk_3_title_en_ref': $($("input[name='risk_3_title_en_ref']:checked")).val() || '',
                    'risk_4_title_en_ref': $($("input[name='risk_4_title_en_ref']:checked")).val() || '',
                    'risk_5_title_en_ref': $($("input[name='risk_5_title_en_ref']:checked")).val() || '',
                    'risk_6_title_en_ref': $($("input[name='risk_6_title_en_ref']:checked")).val() || '',
                    'risk_7_title_en_ref': $($("input[name='risk_7_title_en_ref']:checked")).val() || '',
                    'risk_8_title_en_ref': $($("input[name='risk_8_title_en_ref']:checked")).val() || '',
                    'risk_9_title_en_ref': $($("input[name='risk_9_title_en_ref']:checked")).val() || '',
                    'risk_10_title_en_ref': $($("input[name='risk_10_title_en_ref']:checked")).val() || '',
                    'risk_11_title_en_ref': $($("input[name='risk_11_title_en_ref']:checked")).val() || '',
                    'risk_12_title_en_ref': $($("input[name='risk_12_title_en_ref']:checked")).val() || '',
                    'risk_13_title_en_ref': $($("input[name='risk_13_title_en_ref']:checked")).val() || '',
                    'radio': $($("input[name='risk_document_radio']:checked")).val() || ''
                },
                'trading_condition': {
                    'trading_conditions_1_title_en_ref': $($("[name='trading_conditions_1_title_en_ref']:checked")).val() || '',
                    'trading_conditions_2_title_en_ref': $($("[name='trading_conditions_2_title_en_ref']:checked")).val() || '',
                    'trading_conditions_3_title_en_ref': $($("[name='trading_conditions_3_title_en_ref']:checked")).val() || '',
                    'trading_conditions_4_title_en_ref': $($("[name='trading_conditions_4_title_en_ref']:checked")).val() || '',
                    'trading_conditions_5_title_en_ref': $($("[name='trading_conditions_5_title_en_ref']:checked")).val() || '',
                    'trading_conditions_6_title_en_ref': $($("[name='trading_conditions_6_title_en_ref']:checked")).val() || '',
                    'trading_conditions_7_title_en_ref': $($("[name='trading_conditions_7_title_en_ref']:checked")).val() || '',
                    'trading_conditions_8_title_en_ref': $($("[name='trading_conditions_8_title_en_ref']:checked")).val() || '',
                    'trading_conditions_9_title_en_ref': $($("[name='trading_conditions_9_title_en_ref']:checked")).val() || '',
                    'trading_conditions_10_title_en_ref': $($("[name='trading_conditions_10_title_en_ref']:checked")).val() || '',
                    'trading_conditions_11_title_en_ref': $($("[name='trading_conditions_11_title_en_ref']:checked")).val() || '',
                    'trading_conditions_12_title_en_ref': $($("[name='trading_conditions_12_title_en_ref']:checked")).val() || '',
                    'trading_conditions_13_title_en_ref': $($("[name='trading_conditions_13_title_en_ref']:checked")).val() || '',
                    'trading_conditions_14_title_en_ref': $($("[name='trading_conditions_14_title_en_ref']:checked")).val() || '',
                    'trading_conditions_15_title_en_ref': $($("[name='trading_conditions_15_title_en_ref']:checked")).val() || '',
                    'trading_conditions_16_title_en_ref': $($("[name='trading_conditions_16_title_en_ref']:checked")).val() || '',
                    'trading_conditions_17_title_en_ref': $($("[name='trading_conditions_17_title_en_ref']:checked")).val() || '',
                    'trading_conditions_18_title_en_ref': $($("[name='trading_conditions_18_title_en_ref']:checked")).val() || '',
                    'trading_conditions_19_title_en_ref': $($("[name='trading_conditions_19_title_en_ref']:checked")).val() || '',
                    'trading_conditions_20_title_en_ref': $($("[name='trading_conditions_20_title_en_ref']:checked")).val() || '',
                    'trading_conditions_21_title_en_ref': $($("[name='trading_conditions_21_title_en_ref']:checked")).val() || '',
                    'trading_conditions_22_title_1': 0,
                    'trading_conditions_22_title_2': 0,
                    'trading_conditions_22_title_en_ref': $($("[name='trading_conditions_22_title_en_ref']:checked")).val() || '',
                    'trading_conditions_23_title_en_ref': $($("[name='trading_conditions_23_title_en_ref']:checked")).val() || '',
                    'step_4_footer': $($("[name='step_4_footer']:checked")).val() || '',
                },
                'pap_agreement': $($("input[name='pap_agreement']:checked")).val() || '',
                'truth_resposibility_agreement': $($("input[name='truth_resposibility_agreement']:checked")).val() || '',
                'demoAccount': 0,
                'full_name': $('[name="full_name"]').val(),
                'identity_number': $('[name="identity_number"]').val(),
                'future_contract': {
                    'value': $('[name="future_contract"]').val(),
                    'input': ''
                },
                'account_type': $('[name="account_type"]').val(),
                'place_of_birth': $('[name="place_of_birth"]').val(),
                'date_of_birth': $('[name="date_of_birth"]').val(),
                'post_code': $('[name="post_code"]').val(),
                'page': 11,
                'activeStep': atvStep,
                'tax_file_number': $('[name="tax_file_number"]').val(),
                'mother_maiden_name': $('[name="mother_maiden_name"]').val(),
                'status': $($("input[name='maritalstatusOptions']:checked")).val() || '',
                'gender': $($("input[name='genderOptions']:checked")).val() || '',
                'home_address': $('[name="home_address"]').val(),
                'home_ownership_status': {
                    'value': $($("input[name='houseownstatusOptions']:checked")).val() || '',
                    'input': $('[name="houseownstatusdescription"]').val()
                },
                'openint_account_purpose': {
                    'value': $($("input[name='openint_account_purpose']:checked")).val() || '',
                    'input': $("input[name='openint_account_purpose_input']").val(),
                },
                'experince_on_investment': {
                    'value': $($("input[name='experince_on_investment']:checked")).val() || '',
                    'input': $("input[name='experince_on_investment_input']").val()
                },
                'is_family_work_in_related_company': {
                    'value': $($("input[name='is_family_work_in_related_company']:checked")).val() || '',
                    'input': $("input[name='is_family_work_in_related_company_input']").val()
                },
                'is_declared_bankrupt': {
                    'value': $($("input[name='is_declared_bankrupt']:checked")).val() || '',
                    'input': ''
                },
                'trading_rules': $($("input[name='trading_rules']:checked")).val() || '',
                'ae_name_or_branch': $('[name="ae_name_or_branch"]').val(),
                'husband_wife_spouse': $('[name="husband_wife_spouse"]').val(),
                'home_phone': $('[name="home_phone"]').val(),
                'home_fax': $('[name="home_fax"]').val(),
                'statement_data_correctly': $($("input[name='statement_data_correctly']:checked")).val() || '',
            };

            return contentDt;
        }


        if ($().dropzone && !$(".dropzone").hasClass("disabled")) {
            $(".id_card").dropzone({
                url: "https://dev-maxco-cabinet-2.yokesen.com/upload-img",
                init: function() {
                    this.on("success", function(file, responseText) {
                        idCard_imgUrl = responseText.Data;
                        id_card = true;
                    });
                    this.on("error", function(error) {
                        console.log(error);
                    });
                },
                thumbnailWidth: 160,
                previewTemplate: '<div class="dz-preview dz-file-preview mb-3"><div class="d-flex flex-row "><div class="p-0 w-30 position-relative"><div class="dz-error-mark"><span><i></i></span></div><div class="dz-success-mark"><span><i></i></span></div><div class="preview-container"><img data-dz-thumbnail class="img-thumbnail border-0" /><i class="simple-icon-doc preview-icon" ></i></div></div><div class="pl-3 pt-2 pr-2 pb-1 w-70 dz-details position-relative"><div><span data-dz-name></span></div><div class="text-primary text-extra-small" data-dz-size /><div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div><div class="dz-error-message"><span data-dz-errormessage></span></div></div></div><a href="#/" class="remove" data-dz-remove><i class="glyph-icon simple-icon-trash"></i></a></div>'
            });
            $(".coloured_photo").dropzone({
                url: "https://dev-maxco-cabinet-2.yokesen.com/upload-img",
                init: function() {
                    this.on("success", function(file, responseText) {
                        colouredPhoto_imgUrl = responseText.Data;
                        coloured_photo = true;
                    });
                    this.on("error", function(error) {
                        console.log(error);
                    });
                },
                thumbnailWidth: 160,
                previewTemplate: '<div class="dz-preview dz-file-preview mb-3"><div class="d-flex flex-row "><div class="p-0 w-30 position-relative"><div class="dz-error-mark"><span><i></i></span></div><div class="dz-success-mark"><span><i></i></span></div><div class="preview-container"><img data-dz-thumbnail class="img-thumbnail border-0" /><i class="simple-icon-doc preview-icon" ></i></div></div><div class="pl-3 pt-2 pr-2 pb-1 w-70 dz-details position-relative"><div><span data-dz-name></span></div><div class="text-primary text-extra-small" data-dz-size /><div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div><div class="dz-error-message"><span data-dz-errormessage></span></div></div></div><a href="#/" class="remove" data-dz-remove><i class="glyph-icon simple-icon-trash"></i></a></div>'
            });
            $(".specimen_signature").dropzone({
                url: "https://dev-maxco-cabinet-2.yokesen.com/upload-img",
                init: function() {
                    this.on("success", function(file, responseText) {
                        console.log('specimen_signature', responseText.Data);
                        specimenSignature_imgUrl = responseText.Data;
                        specimen_signature = true;
                    });
                    this.on("error", function(error) {
                        console.log(error);
                    });
                },
                thumbnailWidth: 160,
                previewTemplate: '<div class="dz-preview dz-file-preview mb-3"><div class="d-flex flex-row "><div class="p-0 w-30 position-relative"><div class="dz-error-mark"><span><i></i></span></div><div class="dz-success-mark"><span><i></i></span></div><div class="preview-container"><img data-dz-thumbnail class="img-thumbnail border-0" /><i class="simple-icon-doc preview-icon" ></i></div></div><div class="pl-3 pt-2 pr-2 pb-1 w-70 dz-details position-relative"><div><span data-dz-name></span></div><div class="text-primary text-extra-small" data-dz-size /><div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div><div class="dz-error-message"><span data-dz-errormessage></span></div></div></div><a href="#/" class="remove" data-dz-remove><i class="glyph-icon simple-icon-trash"></i></a></div>'
            });
            $(".bank_statement").dropzone({
                url: "https://dev-maxco-cabinet-2.yokesen.com/upload-img",
                init: function() {
                    this.on("success", function(file, responseText) {
                        bankStatement_imgUrl = responseText.Data;
                        bank_statement = true;
                    });
                    this.on("error", function(error) {
                        console.log(error);
                    });
                },
                thumbnailWidth: 160,
                previewTemplate: '<div class="dz-preview dz-file-preview mb-3"><div class="d-flex flex-row "><div class="p-0 w-30 position-relative"><div class="dz-error-mark"><span><i></i></span></div><div class="dz-success-mark"><span><i></i></span></div><div class="preview-container"><img data-dz-thumbnail class="img-thumbnail border-0" /><i class="simple-icon-doc preview-icon" ></i></div></div><div class="pl-3 pt-2 pr-2 pb-1 w-70 dz-details position-relative"><div><span data-dz-name></span></div><div class="text-primary text-extra-small" data-dz-size /><div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div><div class="dz-error-message"><span data-dz-errormessage></span></div></div></div><a href="#/" class="remove" data-dz-remove><i class="glyph-icon simple-icon-trash"></i></a></div>'
            });
            $(".other_image").dropzone({
                url: "https://dev-maxco-cabinet-2.yokesen.com/upload-img",
                init: function() {
                    this.on("success", function(file, responseText) {
                        console.log(responseText);
                        other_imgUrl = responseText.Data;
                        other_image = true;
                    });
                    this.on("error", function(error) {
                        console.log(error);
                    });
                },
                thumbnailWidth: 160,
                previewTemplate: '<div class="dz-preview dz-file-preview mb-3"><div class="d-flex flex-row "><div class="p-0 w-30 position-relative"><div class="dz-error-mark"><span><i></i></span></div><div class="dz-success-mark"><span><i></i></span></div><div class="preview-container"><img data-dz-thumbnail class="img-thumbnail border-0" /><i class="simple-icon-doc preview-icon" ></i></div></div><div class="pl-3 pt-2 pr-2 pb-1 w-70 dz-details position-relative"><div><span data-dz-name></span></div><div class="text-primary text-extra-small" data-dz-size /><div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div><div class="dz-error-message"><span data-dz-errormessage></span></div></div></div><a href="#/" class="remove" data-dz-remove><i class="glyph-icon simple-icon-trash"></i></a></div>'
            });
        }

        function savekyc() {
            var contentDt = getContent();
            contentDt.attached_documents.required_image[0].src.url = idCard_imgUrl;
            contentDt.attached_documents.required_image[1].src.url = colouredPhoto_imgUrl;
            contentDt.attached_documents.required_image[2].src.url = specimenSignature_imgUrl;
            contentDt.attached_documents.required_image[3].src.url = bankStatement_imgUrl;
            contentDt.attached_documents.other_image.src.url = other_imgUrl;
            const content = JSON.stringify(contentDt)
            $.post("{{route('savekyc')}}", {
                content,
                NBCRMWEBAPI: $('[name="NBCRMWEBAPI"]').val(),
                USERID: $('[name="USERID"]').val(),
            }, function(data, status) {
                var {
                    Code,
                    Message
                } = data;
                if (data.Data) {
                    var Data = data.Data;
                    var MTUserRef = Data.MTUserRef;
                    var idx = MTUserRef.findIndex(x => x.IsSimulate === false);
                    if (MTUserRef[idx]) {
                        var liveAcc = MTUserRef[idx];
                        $('[data-bind="ac_live_no"]').text(liveAcc.Login);
                    }
                }
            });
        };

        function addliveaccount() {
            var contentDt = getContent();
            const content = JSON.stringify(contentDt)
            $.post("{{route('addliveaccount')}}", {
                content,
                NBCRMWEBAPI: $('[name="NBCRMWEBAPI"]').val(),
                USERID: $('[name="USERID"]').val(),
            }, function(data, status) {
                console.log("Data: " + data + "\nStatus: " + status);
            });
        };

        // Initialize Smart Wizard
        //   $('#smartWizardValidation').smartWizardValidation();

        // Initialize the leaveStep event
        //   $("#smartWizardValidation").on("leaveStep", function(e, anchorObject, stepNumber, stepDirection) {
        //      return confirm("Do you want to leave the step "+stepNumber+"?");
        //   });

        // Initialize the showStep event
        $("#smartWizardValidation").on("showStep", function(e, anchorObject, stepNumber, stepDirection) {
            atvStep = stepNumber;
            localStorage.setItem('activestepopnliveacc', stepNumber);
            if (stepDirection !== "") {
                savekyc();
            }
            if (stepNumber === 4) {
                addliveaccount();
            }
            getDetailKYC();
        });

        //   // Initialize the beginReset event
        //   $("#smartWizardValidation").on("beginReset", function(e) {
        //  return confirm("Do you want to reset the wizard?");
        //   });

        //   // Initialize the endReset event
        //   $("#smartWizardValidation").on("endReset", function(e) {
        //  alert("endReset called");
        //   });

        //   // Initialize the themeChanged event
        //   $("#smartWizardValidation").on("themeChanged", function(e, theme) {
        //  alert("Theme changed. New theme name: " + theme);
        //   });



    });
</script>
@endsection
