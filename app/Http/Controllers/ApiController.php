<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Storage;
use Illuminate\Contracts\Filesystem\Factory;
use URL;
use Jenssegers\Agent\Agent;


use Session;
use Cookie;
use Redirect;
use Image;
use Illuminate\Support\Facades\File;
use DB;
use Alert;

class ApiController extends Controller
{

  public function lab2()
  {
    /*
      $curl = curl_init();

      $data1 = [
        'IsLoadComm' => false,
        'LoadStatistics'=> true,
];


      curl_setopt_array($curl, array(
          CURLOPT_URL => "http://maxco-api.bull-b.com/api/account/Detail",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30000,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => json_encode($data1),
          CURLOPT_HTTPHEADER => array(
          	// Set here requred headers

              "accept-language: en-US,en;q=0.8",
              "Cookie: NBCRMWEBAPI.SESSION=CfDJ8HafDXNoayJHpYEDDzbbgr5V7%2Brbdc%2BYlB%2FThlbrVDYpPgyTuF8WHl3Ad5QHdghHAMGPIwY0vxQUEI72nQxjZ2lAU2gbgNnZMitcAf%2BherjGP9%2FUL3%2Bzn1LusLBYEx9CIq%2FyzXbRMgT%2BK0wKebU2VoB7dhMDRvsFLm1vZGCjMjSn",
              "Content-Type: application/json"
          ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);
      dd($response,$err);
      curl_close($curl);
*/



    $api_url = env('API_URL');
    $url = $api_url . 'api/account/Detail';

    //dd($token);
    $client = new Client();
    $account_detail = $client->post($url, [
      'headers' => [
        'Content-Type' => 'application/json',
        'Language' => 'en',
        'Cookie' => Session::get('user.token')
      ],
      'json' => [
        'IsLoadComm' => false,
        'LoadStatistics' => true,
      ]
    ]);
    $accounts = json_decode($account_detail->getBody());
  }

  public function lab3()
  {
    $agent = new Agent();
    //dd(Cookie::get('ref'),Cookie::get('so'),Cookie::get('campaign'),URL::previous(),request()->ip(),$agent->platform(),$agent->device(),$agent->languages()[0],Cookie::get('uuid'));

    $client = new Client();
    $insertcrm = $client->post(env('CRM_URL') . '/insert_client', [
      'headers' => [
        "X-Authorization-Time" => time(),
        "X-Authorization-Token" => env('CRM_TOKEN'),
        "useragent" => $_SERVER['HTTP_USER_AGENT']
      ],
      'form_params' => [
        'name' => 'name-test-hard-dcode',
        'email' => 'email@testhardcode.com',
        'phone' => '627738724834',
        'ktp' => '3723895748754',
        'parent' => Cookie::get('ref'),
        'origin' => Cookie::get('so'),
        'campaign' => Cookie::get('campaign'),
        'media_iklan' => 'none',
        'previous_url' => URL::previous(),
        'ipaddress' => request()->ip(),
        'desktop' => $agent->platform(),
        'device' => $agent->device(),
        'lang' => $agent->languages()[0],
        'uuid' => Cookie::get('uuid')
      ]
    ]);
    $responsecrm = json_decode($insertcrm->getBody());
  }

  public function lab()
  {
    $api_url = env('API_URL');
    $url = $api_url . 'api/account/listmatchusers';


    $client = new Client();
    $account_list = $client->post($url, [
      'headers' => [
        'Content-Type' => 'application/json',
        'Language' => 'en'
      ],
      'json' => [
        'Username' => "yoke.endarto@gmail.com",
        'Password' => "Richie020908",
        'InputType' => "email"
      ]
    ]);

    $header = $account_list->getHeaders();
    $cookie = $header['Set-Cookie'][1];
    $cookie = explode(";", $cookie);
    $session = trim($cookie[0]);
    $accounts = json_decode($account_list->getBody());
    Session::put('user.shadow', 'shadow');
    Session::put('user.token', $session);
    Session::put('user.id', $accounts->Data->User->UserId);
    Session::put('user.parent', $accounts->Data->User->ParentId);
    Session::put('user.type', $accounts->Data->User->UserType);
    Session::put('user.email', $accounts->Data->User->Email);
    Session::put('user.mobile', $accounts->Data->User->Mobile);
    Session::put('user.gender', $accounts->Data->User->Gender);
    Session::put('user.name', $accounts->Data->IdCard->RealName);

    return redirect()->route('lab2');
  }

  public function getnotification()
  {
    $data = DB::table('notifications')->where(['user_id' => Session::get('user.id'), 'isRead' => 0])->orderby('created_at', 'desc')->get();
    return response()->json([
      'Code' => 0,
      'Data' => $data,
    ]);
  }

  public function save(Request $request)
  {
    Session::put('demouser.Password', $request->Password);
    Session::put('demouser.ConfirmPassword', $request->ConfirmPassword);
    Session::put('demouser.Email', $request->Email);
    Session::put('demouser.Realname', $request->Realname);
    Session::put('demouser.AreaCode', $request->AreaCode);
    Session::put('demouser.Mobile', $request->Mobile);
    Session::put('demouser.IDNO', $request->IDNO);
    Session::put('demouser.Nationality', $request->Nationality);
    Session::put('demouser.VerifyCode', $request->VerifyCode);
    Session::put('demouser.ParentId', $request->ParentId);
    Session::put('demouser.InfoType', $request->InfoType);
    return redirect()->route('getRegisterVerification');
  }
  public function sendVerificationCodeRegistration()
  {

    $api_url = env('API_URL');
    $url = $api_url . 'api/message/sendvalidatecaptcha';
    $client = new Client();
    $requestCode = $client->post($url, [
      'headers' => [
        'Content-Type' => 'application/json',
        'Language' => 'en'
      ],
      'json' => [
        'InfoType' => Session::get('demouser.InfoType'),
        'ValidateType' => "1",
        'AreaCode' => Session::get('demouser.AreaCode'),
        'Mobile' => Session::get('demouser.Mobile'),
        'Email' => Session::get('demouser.Email')
      ]
    ]);
    $response = json_decode($requestCode->getBody());
    if ($response->Code === 0) {
      return view('pages.auth.registrationverificationcode');
    } else {
      return redirect()->route('formregister')->with('ErrorMessage', $response->Message);
    }
  }

  public function sendVerificationCodeForgotPass(Request $request)
  {
    $api_url = env('API_URL');
    $url = $api_url . 'api/message/sendvalidatecaptcha';
    $client = new Client();
    // ValidateType
    // 34 => Email forgot password verification code
    // 43 => SMS forgot password verification code
    $requestPost = $client->post($url, [
      'headers' => [
        'Content-Type' => 'application/json',
        'Language' => 'en'
      ],
      'json' => [
        'InfoType' => $request->InfoType,
        'ValidateType' => $request->ValidateType,
        'AreaCode' => str_replace("+ ", "", $request->AreaCode),
        'Mobile' => $request->Mobile,
        'Email' => $request->Email
      ]
    ]);
    $response = json_decode($requestPost->getBody());
    return array('Code' => $response->Code, 'Message' => $response->Message);
    // $requestPost->getBody()->Message;
    //  response()->json(['Code'=>$response->Code,'Message'=>$response->Message]);
  }

  public function forgotPassword(Request $request)
  {
    $UserId = Session::get('user.id');
    $api_url = env('API_URL');
    $url = $api_url . 'api/account/resetpassword';
    $client = new Client();
    $jsonDt = array(
      'VerifyCode' => $request->VerifyCode,
      'InfoType' => $request->InfoType,
      'AreaCode' => str_replace("+ ", "", $request->AreaCode),
      'UserId' => $UserId,
      'NewPassword' => $request->Password,
    );
    if ($request->InfoType === '0') {
      $jsonDt = array_add($jsonDt, 'Mobile', $request->InputText);
    } else {
      $jsonDt = array_add($jsonDt, 'Email', $request->InputText);
    }
    $requestPost = $client->post($url, [
      'headers' => [
        'Content-Type' => 'application/json',
        'Language' => 'en'
      ],
      'json' => $jsonDt
    ]);
    $response = json_decode($requestPost->getBody());
    if ($response->Code === 0) {
      return redirect()->route('login')->withInput()->with('SuccessMessage', 'Password has been changed.');
    } else {
      // return Redirect::route('forgotpasswordform')->withInput()->with('ErrorMessage', $response->Message);
      return redirect()->back()->with('ErrorMessage', $response->Message);
    }
  }

  public function adddemouser(Request $request)
  {
    $dataDemoUser = Session::get('demouser');
    $api_url = env('API_URL');
    $url = $api_url . 'api/account/adddemouser';
    $client = new Client();
    $req_register = $client->post($url, [
      'headers' => [
        'Content-Type' => 'application/json',
        'Language' => 'en'
      ],
      'json' => [
        'Password' => $dataDemoUser['Password'],
        'ConfirmPassword' => $dataDemoUser['ConfirmPassword'],
        'Email' => $dataDemoUser['Email'],
        'Realname' => $dataDemoUser['Realname'],
        'AreaCode' => $dataDemoUser['AreaCode'],
        'Mobile' => $dataDemoUser['Mobile'],
        'IDNO' => $dataDemoUser['IDNO'],
        'Nationality' => $dataDemoUser['Nationality'],
        'VerifyCode' => $request->VerifyCode,
        'ParentId' => $dataDemoUser['ParentId'],
        'RegChannel' => 1
      ]
    ]);
    $response = json_decode($req_register->getBody());

    if ($response->Code === 0) {
      $agent = new Agent();
      $client = new Client();

      $insertcrm = $client->post(env('CRM_URL') . '/insert_client', [
        'headers' => [
          "X-Authorization-Time" => time(),
          "X-Authorization-Token" => env('CRM_TOKEN'),
          "useragent" => $_SERVER['HTTP_USER_AGENT']
        ],
        'form_params' => [
          'name' => $dataDemoUser['Realname'],
          'email' => $dataDemoUser['Email'],
          'phone' => $dataDemoUser['AreaCode'] . $dataDemoUser['Mobile'],
          'ktp' => $dataDemoUser['IDNO'],
          'parent' => Cookie::get('ref'),
          'origin' => Cookie::get('so'),
          'campaign' => Cookie::get('campaign'),
          'media_iklan' => 'none',
          'previous_url' => URL::previous(),
          'ipaddress' => request()->ip(),
          'desktop' => $agent->platform(),
          'device' => $agent->device(),
          'lang' => $agent->languages()[0],
          'uuid' => Cookie::get('uuid')
        ]
      ]);
      $responsecrm = json_decode($insertcrm->getBody());

      // Session::forget('demouser.Password');
      // Session::forget('demouser.ConfirmPassword');
      // Session::forget('demouser.Email');
      // Session::forget('demouser.Realname');
      // Session::forget('demouser.AreaCode');
      // Session::forget('demouser.Mobile');
      // Session::forget('demouser.IDNO');
      // Session::forget('demouser.Nationality');
      // Session::forget('demouser.VerifyCode');
      // Session::forget('demouser.ParentId');
      // Session::forget('demouser.InfoType');

      // alert()->success('Success', 'Anda telah dikirimkan melalui email dan sms');
      // return redirect()->route('login');

      // otomatis login saat berhasil registrasi
      $urllogin = $api_url . 'api/account/listmatchusers';
      $client_login = $client->post($urllogin, [
        'headers' => [
          'Content-Type' => 'application/json',
          'Language' => 'en'
        ],
        'json' =>  [
          'Username' => $dataDemoUser['Email'],
          'Password' => $dataDemoUser['Password'],
          'InputType' => 'email'
        ]
      ]);
      $accounts = json_decode($client_login->getBody());
      if ($accounts->Code === 0) {
        $header = $client_login->getHeaders();

        if (count($header['Set-Cookie']) > 1) {
          $cfuid = $header['Set-Cookie'][0];
          Cookie::queue('__cfduid', substr($cfuid, 9));
          $cookie = $header['Set-Cookie'][1];
        } else {
          $cookie = $header['Set-Cookie'][0];
        }

        $cookie = explode(";", $cookie);
        $session = trim($cookie[0]);
        Cookie::queue('NBCRMWEBAPI.SESSION', substr($session, 20));
        Session::put('user.token', $session);
        Session::put('user.id', $accounts->Data->User->UserId);
        Session::put('user.parent', $accounts->Data->User->ParentId);
        Session::put('user.type', $accounts->Data->User->UserType);
        Session::put('user.email', $accounts->Data->User->Email);
        Session::put('user.mobile', $accounts->Data->User->Mobile);
        Session::put('user.gender', $accounts->Data->User->Gender);
        Session::put('user.name', $accounts->Data->IdCard->RealName);
        Session::put('user.IDNO', $accounts->Data->IdCard->IDNO);
        Session::put('user.PlaceOfBirth', $accounts->Data->User->PlaceOfBirth);
        Session::put('user.DateOfBirth', $accounts->Data->User->DateOfBirth);
        Session::put('user.Address', $accounts->Data->User->Address);
        Session::put('user.KycStatus', $accounts->Data->User->KycStatus);
        Session::put('user.Currency', $accounts->Data->User->Currency);
        if (isset($accounts->Data->User->Summary)) {
          Session::put('user.Summary', $accounts->Data->User->Summary);
        }
        $findIndexLiveAcc = array_search(false, array_column($accounts->Data->MTUserRef, 'IsSimulate'));
        $findIndexDemoAcc = array_search(true, array_column($accounts->Data->MTUserRef, 'IsSimulate'));

        Session::put('user.MTUserRefObj', $accounts->Data->MTUserRef[0]);
        Session::put('user.MTUserLive', $accounts->Data->MTUserRef[$findIndexLiveAcc]);
        Session::put('user.MTUserDemo', $accounts->Data->MTUserRef[$findIndexDemoAcc]);

        // $lastpath = Session::get('lastpath');
        // if($lastpath !== ""){
        //   return redirect()->to($lastpath);
        // }else{
        return redirect()->route('dashboardProfile');
        // }
      } else {
        alert()->error('Perhatian !', 'Data yang Anda masukkan masih salah,silahkan perbaiki sesuai petunjuk berwarna merah.');
        return redirect()->route('formregister')->with('ErrorMessage', $accounts->Message);
      }
    } else {
      alert()->error('Perhatian !', 'Data yang Anda masukkan masih salah,silahkan perbaiki sesuai petunjuk berwarna merah.');
      return redirect()->route('formregister')->with('ErrorMessage', $response->Message);
    }
  }

  public function login(Request $request)
  {
    $api_url = env('API_URL');
    $url = $api_url . 'api/account/listmatchusers';
    $client = new Client();

    $dataLogin =  [
      'Username' => $request->Username,
      'Password' => $request->Password,
      'InputType' => $request->InputType
    ];

    if ($request->InputType == "phone") {
      $dataLogin['AreaCode'] =  str_replace("+ ", "", $request->AreaCode);
    }

    $account_list = $client->post($url, [
      'headers' => [
        'Content-Type' => 'application/json',
        'Language' => 'en'
      ],
      'json' => $dataLogin
    ]);

    $accounts = json_decode($account_list->getBody());
    if ($accounts->Code === 0) {
      $header = $account_list->getHeaders();

      if (count($header['Set-Cookie']) > 1) {
        $cfuid = $header['Set-Cookie'][0];
        Cookie::queue('__cfduid', substr($cfuid, 9));
        $cookie = $header['Set-Cookie'][1];
      } else {
        $cookie = $header['Set-Cookie'][0];
      }

      $cookie = explode(";", $cookie);
      $session = trim($cookie[0]);
      Cookie::queue('NBCRMWEBAPI.SESSION', substr($session, 20));
      Session::put('user.token', $session);
      Session::put('user.id', $accounts->Data->User->UserId);
      Session::put('user.parent', $accounts->Data->User->ParentId);
      Session::put('user.type', $accounts->Data->User->UserType);
      Session::put('user.email', $accounts->Data->User->Email);
      Session::put('user.mobile', $accounts->Data->User->Mobile);
      Session::put('user.gender', $accounts->Data->User->Gender);
      Session::put('user.name', $accounts->Data->IdCard->RealName);
      Session::put('user.IDNO', $accounts->Data->IdCard->IDNO);
      Session::put('user.PlaceOfBirth', $accounts->Data->User->PlaceOfBirth);
      Session::put('user.DateOfBirth', $accounts->Data->User->DateOfBirth);
      Session::put('user.Address', $accounts->Data->User->Address);
      Session::put('user.KycStatus', $accounts->Data->User->KycStatus);
      Session::put('user.Currency', $accounts->Data->User->Currency);
      if (isset($accounts->Data->User->Summary)) {
        Session::put('user.Summary', $accounts->Data->User->Summary);
      }
      $findIndexLiveAcc = array_search(false, array_column($accounts->Data->MTUserRef, 'IsSimulate'));
      $findIndexDemoAcc = array_search(true, array_column($accounts->Data->MTUserRef, 'IsSimulate'));

      Session::put('user.MTUserRefObj', $accounts->Data->MTUserRef[0]);
      Session::put('user.MTUserLive', $accounts->Data->MTUserRef[$findIndexLiveAcc]);
      Session::put('user.MTUserDemo', $accounts->Data->MTUserRef[$findIndexDemoAcc]);

      // $lastpath = Session::get('lastpath');
      // if($lastpath !== ""){
      //   return redirect()->to($lastpath);
      // }else{
      return redirect()->route('dashboardProfile');
      // }
    } else {
      return redirect()->back()->with('ErrorMessage', $accounts->Message);
    }
  }

  public function profile()
  {
    try {
      $api_url = env('API_URL');
      $url = $api_url . 'api/account/Detail';
      $client = new Client();
      $account_detail = $client->post($url, [
        'headers' => [
          'Content-Type' => 'application/json',
          'Language' => 'en',
          'Cookie' => Session::get('user.token')
        ],
        'json' => [
          'IsLoadComm' => false,
          'LoadStatistics' => true,
        ]
      ]);
      $accounts = json_decode($account_detail->getBody());
      $Data = $accounts->Data;
      $User = $Data->User;
      $UserBankCard = $Data->UserBankCard;
      $IdCard = $Data->IdCard;

      if (count($UserBankCard) > 0) {
        Session::put('userbank.BankName', $UserBankCard[0]->BankName);
        Session::put('userbank.AccountNo', $UserBankCard[0]->AccountNo);
        Session::put('userbank.AccountName', $UserBankCard[0]->AccountName);
        Session::put('userbank.BranchName', $UserBankCard[0]->BranchName);
      }
      if (isset($accounts->Data->User->Summary)) {
        Session::put('user.Summary', $accounts->Data->User->Summary);
      }
      return view('pages.auth.profile', compact('User', 'UserBankCard', 'IdCard'));
    } catch (\Exception $e) {
      // dd($e->getResponse());
      if ($e->getResponse()->getStatusCode() == '401') {
        return redirect()->route('login');
      } else if ($e->getResponse()->getStatusCode() == '500') {
      } else {
        dd('error code : ' . $e->getResponse()->getStatusCode());
      }
    }
  }

  public function changepassword(Request $request)
  {
    try {
      $api_url = env('API_URL');
      $url = $api_url . 'api/account/updatepassword';
      $client = new Client();
      $updatePass = $client->post($url, [
        'headers' => [
          'Content-Type' => 'application/json',
          'Language' => 'en',
          'Cookie' => Session::get('user.token')
        ],
        'json' => [
          'UserId' => $request->UserId,
          'OldPassword' => $request->OldPassword,
          'NewPassword' => $request->NewPassword
        ]
      ]);
      $resp = json_decode($updatePass->getBody());
      if ($resp->Code === 0) {
        alert()->success('Success', 'Password Anda berhasil dirubah.');
        return redirect()->route('dashboardProfile');
      } else {
        // alert()->error('Error', 'Anda telah dikirimkan melalui email dan sms');
        return redirect()->back()->with('ErrorMessage', $resp->Message);
      }
    } catch (\Exception $e) {
      if ($e->getResponse()->getStatusCode() == '401') {
        return redirect()->route('login');
      } else if ($e->getResponse()->getStatusCode() == '500') {
      } else {
        dd('error code : ' . $e->getResponse()->getStatusCode());
      }
    }
  }

  public function apigetKycDetail(Request $request)
  {
    $api_url = env('API_URL');
    $url = $api_url . 'api/account/getanswer';
    $token = trim(Session::get('user.token'));
    if ($token === "") {
      Session::put('lastpath', $request->path());
      return redirect()->route('login');
    }
    Cookie::queue('NBCRMWEBAPI.SESSION', substr($token, 20));
    $client = new Client();
    $requestPost = $client->post($url, [
      'headers' => [
        'Content-Type' => 'application/json',
        'Language' => 'en',
        'Cookie' => Session::get('user.token')
      ],
      'json' => []
    ]);
    $UserToken = Session::get('user.token');
    $UserId = Session::get('user.id');
    $response = json_decode($requestPost->getBody());
    if ($response->Code === 0) {
      $isTouch = isset($response->Data);
      if ($isTouch) {
        return array("Data" => json_decode($response->Data->Content));
      } else {
        return response()->json([
          'Code' => 0,
          'Message' => ''
        ]);
      }
    } else {
    }
  }

  public function postNextStepOpenLiveAccount(Request $request)
  {
    try {
      $api_url = env('API_URL');
      $url = $api_url . 'api/account/getanswer';
      $token = trim(Session::get('user.token'));
      if ($token === "") {
        Session::put('lastpath', $request->path());
        return redirect()->route('login');
      }
      Cookie::queue('NBCRMWEBAPI.SESSION', substr($token, 20));
      $client = new Client();
      $requestPost = $client->post($url, [
        'headers' => [
          'Content-Type' => 'application/json',
          'Language' => 'en',
          'Cookie' => Session::get('user.token')
        ],
        'json' => []
      ]);
      $response = json_decode($requestPost->getBody());
      $DataContent = json_decode($response->Data->Content);
      $DataContent->activeStep = $request->nextStep;
      if (isset($request->page)) {
        $DataContent->page = $request->page;
      }
      $UserId = Session::get('user.id');
      $api_url = env('API_URL');
      $url = $api_url . 'api/account/savekyc';
      $client = new Client();
      $requestPost = $client->post($url, [
        'headers' => [
          'Content-Type' => 'application/json',
          'Language' => 'en',
          'Cookie' => $request->NBCRMWEBAPI
        ],
        'json' => [
          'UserId' =>  $UserId,
          'Content' => json_encode($DataContent)
        ]
      ]);
      $response = json_decode($requestPost->getBody());
      if ($response->Code == 0) {
        return redirect()->route('openLiveAccount');
      }
    } catch (\Exception $e) {
      if ($e->getResponse()->getStatusCode() == '401') {
        return redirect()->route('login');
      } else if ($e->getResponse()->getStatusCode() == '500') {
      } else {
        dd('error code : ' . $e->getResponse()->getStatusCode());
      }
    }
  }

  public function getKycDetail(Request $request)
  {
    try {
      $agent = new Agent();
      $api_url = env('API_URL');
      $url = $api_url . 'api/account/getanswer';
      $token = trim(Session::get('user.token'));
      if ($token === "") {
        Session::put('lastpath', $request->path());
        return redirect()->route('login');
      }
      Cookie::queue('NBCRMWEBAPI.SESSION', substr($token, 20));
      $client = new Client();
      $requestPost = $client->post($url, [
        'headers' => [
          'Content-Type' => 'application/json',
          'Language' => 'en',
          'Cookie' => Session::get('user.token')
        ],
        'json' => []
      ]);
      $UserToken = Session::get('user.token');
      $UserId = Session::get('user.id');
      $response = json_decode($requestPost->getBody());

      $url2 = $api_url . 'api/account/Detail';

      $client = new Client();
      $requestDetailPost = $client->post($url2, [
        'headers' => [
          'Content-Type' => 'application/json',
          'Language' => 'en',
          'Cookie' => Session::get('user.token')
        ],
        'json' => [
          'IsLoadComm' => false,
          'LoadStatistics' => true
        ]
      ]);

      $res = json_decode($requestDetailPost->getBody());
      $UserDetail = $res->Data;
      if ($response->Code == 0) {
        $dataExist = isset($response->Data);
        if ($dataExist) {
          $databaru = false;
          $DataContent = json_decode($response->Data->Content);
        } else {
          $databaru = true;
          $DataContent = collect([]);
          $DataContent->activeStep = 1;
        }
        if (!isset($DataContent->activeStep)) {
          $DataContent->activeStep = 1;
        }

        if (!isset($DataContent->full_name)) {
          $DataContent->full_name = $UserDetail->IdCard->RealName;
        }

        if (!isset($DataContent->identity_number)) {
          $DataContent->identity_number = $UserDetail->IdCard->IDNO;
        }

        if (!isset($DataContent->place_of_birth)) {
          $DataContent->place_of_birth = $UserDetail->User->PlaceOfBirth;
        }

        if (!isset($DataContent->date_of_birth)) {
          $DataContent->date_of_birth = isset($UserDetail->User->DateOfBirth) ? $UserDetail->User->DateOfBirth : '';
        }

        if (!isset($DataContent->home_address)) {
          $DataContent->home_address = $UserDetail->User->Address;
        }

        if (!isset($DataContent->post_code)) {
          $DataContent->post_code = '';
        }
        $activeStep = $DataContent->activeStep;
        if (!isset($DataContent->page)) {
          $page = 0;
        } else {
          $page = $DataContent->page;
        }


        if ($databaru) {
          // dd('databaru', $databaru);
          $getdataerror = false;
          if (
            $getdataerror == false && (empty($DataContent->full_name) ||
              empty($DataContent->identity_number) ||
              (isset($DataContent->future_contract) && empty($DataContent->future_contract->value)) ||
              empty($DataContent->account_type) ||
              empty($DataContent->place_of_birth) ||
              empty($DataContent->date_of_birth) ||
              empty($DataContent->post_code) ||
              empty($DataContent->home_address) ||
              $DataContent->statement_data_correctly != 'Yes')
          ) {
            if ($activeStep != "1") {
              alert()->info('Info', 'Harap periksa kembali form pembukaan live akun')->persistent('Close');
            }
            $getdataerror = true;
            $activeStep = 1;
          }

          if (
            $getdataerror == false &&
            (!isset($DataContent->job_details) ||
              (isset($DataContent->job_details) && empty($DataContent->job_details->more_details->length_of_work)) ||
              (isset($DataContent->job_details) && empty($DataContent->job_details->more_details->previous_employer)) ||
              (isset($DataContent->job_details) && empty($DataContent->job_details->more_details->employer_name)) ||
              (isset($DataContent->job_details) && empty($DataContent->job_details->more_details->business_fields)) ||
              (isset($DataContent->job_details) && empty($DataContent->job_details->more_details->title)) ||
              (isset($DataContent->job_details) && empty($DataContent->job_details->more_details->office_address)) ||
              (isset($DataContent->job_details) && empty($DataContent->job_details->more_details->office_tele)) ||
              (isset($DataContent->job_details) && empty($DataContent->job_details->position->value)) ||
              (isset($DataContent->wealth_list) && empty($DataContent->wealth_list->income_per_year)) ||
              (isset($DataContent->wealth_list) && empty($DataContent->wealth_list->residence_location)) ||
              (isset($DataContent->wealth_list) && empty($DataContent->wealth_list->tax_object_selling_value)) ||
              (isset($DataContent->wealth_list) && empty($DataContent->wealth_list->bank_time_deposit_value)) ||
              (isset($DataContent->wealth_list) && empty($DataContent->wealth_list->bank_saving_value)) ||
              (isset($DataContent->wealth_list) && empty($DataContent->wealth_list->others)) ||
              (isset($DataContent->bank_details_for_withdrawls) && empty($DataContent->bank_details_for_withdrawls->full_name)) ||
              (isset($DataContent->bank_details_for_withdrawls) && empty($DataContent->bank_details_for_withdrawls->branch)) ||
              (isset($DataContent->bank_details_for_withdrawls) && empty($DataContent->bank_details_for_withdrawls->account_number)) ||
              (isset($DataContent->bank_details_for_withdrawls) && empty($DataContent->bank_details_for_withdrawls->account_type->value)) ||
              (isset($DataContent->emergency_contact_data) && empty($DataContent->emergency_contact_data->name)) ||
              (isset($DataContent->emergency_contact_data) && empty($DataContent->emergency_contact_data->address)) ||
              (isset($DataContent->emergency_contact_data) && empty($DataContent->emergency_contact_data->post_code)) ||
              (isset($DataContent->emergency_contact_data) && empty($DataContent->emergency_contact_data->home_phone_number)) ||
              (isset($DataContent->emergency_contact_data) && empty($DataContent->emergency_contact_data->relationship_with_the_contacts)) ||
              empty($DataContent->tax_file_number) ||
              empty($DataContent->mother_maiden_name) ||
              empty($DataContent->status) ||
              empty($DataContent->gender) ||
              empty($DataContent->home_address) ||
              empty($DataContent->home_ownership_status->value) ||
              (isset($DataContent->openint_account_purpose) && empty($DataContent->openint_account_purpose->value)) ||
              (isset($DataContent->experince_on_investment) && empty($DataContent->experince_on_investment->value)) ||
              (isset($DataContent->is_family_work_in_related_company) && empty($DataContent->is_family_work_in_related_company->value)) ||
              (isset($DataContent->is_declared_bankrupt) && empty($DataContent->is_declared_bankrupt->value)) ||
              empty($DataContent->truth_resposibility_agreement) || ($DataContent->truth_resposibility_agreement != 'yes'))
          ) {
            if ($activeStep != "2") {
              alert()->info('Info', 'Harap periksa kembali form pembukaan live akun')->persistent('Close');
            }
            $getdataerror = true;
            $activeStep = 2;
          }

          if (
            $getdataerror == false &&
            (!isset($DataContent->trading_statement) ||
              (isset($DataContent->trading_statement) && $DataContent->trading_statement->statement_for_trading_simulation_on_demo_account == 'no') ||
              (isset($DataContent->trading_statement) && $DataContent->trading_statement->statement_letter_has_been_experienced_on_trading == 'no') ||
              (isset($DataContent->trading_statement) && $DataContent->trading_statement->statement_has_been_received_information_futures_company == 'no'))
          ) {
            if ($activeStep != "3") {
              alert()->info('Info', 'Harap periksa kembali form pembukaan live akun')->persistent('Close');
            }
            $getdataerror = true;
            $activeStep = 3;
          }


          if (
            $getdataerror == false && $databaru == false && (empty($DataContent->risk_document->risk_1_title_en_ref) || ($DataContent->risk_document->risk_1_title_en_ref != "1") ||
              empty($DataContent->risk_document->risk_2_title_en_ref) || ($DataContent->risk_document->risk_2_title_en_ref != "1") ||
              empty($DataContent->risk_document->risk_3_title_en_ref) || ($DataContent->risk_document->risk_3_title_en_ref != "1") ||
              empty($DataContent->risk_document->risk_4_title_en_ref) || ($DataContent->risk_document->risk_4_title_en_ref != "1") ||
              empty($DataContent->risk_document->risk_5_title_en_ref) || ($DataContent->risk_document->risk_5_title_en_ref != "1") ||
              empty($DataContent->risk_document->risk_6_title_en_ref) || ($DataContent->risk_document->risk_6_title_en_ref != "1") ||
              empty($DataContent->risk_document->risk_7_title_en_ref) || ($DataContent->risk_document->risk_7_title_en_ref != "1") ||
              empty($DataContent->risk_document->risk_8_title_en_ref) || ($DataContent->risk_document->risk_8_title_en_ref != "1") ||
              empty($DataContent->risk_document->risk_9_title_en_ref) || ($DataContent->risk_document->risk_9_title_en_ref != "1") ||
              empty($DataContent->risk_document->risk_10_title_en_ref) || ($DataContent->risk_document->risk_10_title_en_ref != "1") ||
              empty($DataContent->risk_document->risk_11_title_en_ref) || ($DataContent->risk_document->risk_11_title_en_ref != "1") ||
              empty($DataContent->risk_document->risk_12_title_en_ref) || ($DataContent->risk_document->risk_12_title_en_ref != "1") ||
              empty($DataContent->risk_document->risk_13_title_en_ref) || ($DataContent->risk_document->risk_13_title_en_ref != "1") ||
              empty($DataContent->risk_document->radio) || ($DataContent->risk_document->radio != "yes")) ||
            (isset($DataContent->trading_condition) && empty($DataContent->trading_condition->trading_conditions_1_title_en_ref)) || (isset($DataContent->trading_condition) && ($DataContent->trading_condition->trading_conditions_1_title_en_ref != "1")) ||
            (isset($DataContent->trading_condition) && empty($DataContent->trading_condition->trading_conditions_2_title_en_ref)) || (isset($DataContent->trading_condition) && ($DataContent->trading_condition->trading_conditions_2_title_en_ref != "1")) ||
            (isset($DataContent->trading_condition) && empty($DataContent->trading_condition->trading_conditions_3_title_en_ref)) || (isset($DataContent->trading_condition) && ($DataContent->trading_condition->trading_conditions_3_title_en_ref != "1")) ||
            (isset($DataContent->trading_condition) && empty($DataContent->trading_condition->trading_conditions_4_title_en_ref)) || (isset($DataContent->trading_condition) && ($DataContent->trading_condition->trading_conditions_4_title_en_ref != "1")) ||
            (isset($DataContent->trading_condition) && empty($DataContent->trading_condition->trading_conditions_5_title_en_ref)) || (isset($DataContent->trading_condition) && ($DataContent->trading_condition->trading_conditions_5_title_en_ref != "1")) ||
            (isset($DataContent->trading_condition) && empty($DataContent->trading_condition->trading_conditions_6_title_en_ref)) || (isset($DataContent->trading_condition) && ($DataContent->trading_condition->trading_conditions_6_title_en_ref != "1")) ||
            (isset($DataContent->trading_condition) && empty($DataContent->trading_condition->trading_conditions_7_title_en_ref)) || (isset($DataContent->trading_condition) && ($DataContent->trading_condition->trading_conditions_7_title_en_ref != "1")) ||
            (isset($DataContent->trading_condition) && empty($DataContent->trading_condition->trading_conditions_8_title_en_ref)) || (isset($DataContent->trading_condition) && ($DataContent->trading_condition->trading_conditions_8_title_en_ref != "1")) ||
            (isset($DataContent->trading_condition) && empty($DataContent->trading_condition->trading_conditions_9_title_en_ref)) || (isset($DataContent->trading_condition) && ($DataContent->trading_condition->trading_conditions_9_title_en_ref != "1")) ||
            (isset($DataContent->trading_condition) && empty($DataContent->trading_condition->trading_conditions_10_title_en_ref)) || (isset($DataContent->trading_condition) && ($DataContent->trading_condition->trading_conditions_10_title_en_ref != "1")) ||
            (isset($DataContent->trading_condition) && empty($DataContent->trading_condition->trading_conditions_11_title_en_ref)) || (isset($DataContent->trading_condition) && ($DataContent->trading_condition->trading_conditions_11_title_en_ref != "1")) ||
            (isset($DataContent->trading_condition) && empty($DataContent->trading_condition->trading_conditions_12_title_en_ref)) || (isset($DataContent->trading_condition) && ($DataContent->trading_condition->trading_conditions_12_title_en_ref != "1")) ||
            (isset($DataContent->trading_condition) && empty($DataContent->trading_condition->trading_conditions_13_title_en_ref)) || (isset($DataContent->trading_condition) && ($DataContent->trading_condition->trading_conditions_13_title_en_ref != "1")) ||
            (isset($DataContent->trading_condition) && empty($DataContent->trading_condition->trading_conditions_14_title_en_ref)) || (isset($DataContent->trading_condition) && ($DataContent->trading_condition->trading_conditions_14_title_en_ref != "1")) ||
            (isset($DataContent->trading_condition) && empty($DataContent->trading_condition->trading_conditions_15_title_en_ref)) || (isset($DataContent->trading_condition) && ($DataContent->trading_condition->trading_conditions_15_title_en_ref != "1")) ||
            (isset($DataContent->trading_condition) && empty($DataContent->trading_condition->trading_conditions_16_title_en_ref)) || (isset($DataContent->trading_condition) && ($DataContent->trading_condition->trading_conditions_16_title_en_ref != "1")) ||
            (isset($DataContent->trading_condition) && empty($DataContent->trading_condition->trading_conditions_17_title_en_ref)) || (isset($DataContent->trading_condition) && ($DataContent->trading_condition->trading_conditions_17_title_en_ref != "1")) ||
            (isset($DataContent->trading_condition) && empty($DataContent->trading_condition->trading_conditions_18_title_en_ref)) || (isset($DataContent->trading_condition) && ($DataContent->trading_condition->trading_conditions_18_title_en_ref != "1")) ||
            (isset($DataContent->trading_condition) && empty($DataContent->trading_condition->trading_conditions_19_title_en_ref)) || (isset($DataContent->trading_condition) && ($DataContent->trading_condition->trading_conditions_19_title_en_ref != "1")) ||
            (isset($DataContent->trading_condition) && empty($DataContent->trading_condition->trading_conditions_20_title_en_ref)) || (isset($DataContent->trading_condition) && ($DataContent->trading_condition->trading_conditions_20_title_en_ref != "1")) ||
            (isset($DataContent->trading_condition) && empty($DataContent->trading_condition->trading_conditions_21_title_en_ref)) || (isset($DataContent->trading_condition) && ($DataContent->trading_condition->trading_conditions_21_title_en_ref != "1")) ||
            (isset($DataContent->trading_condition) && empty($DataContent->trading_condition->trading_conditions_22_title_1)) || (isset($DataContent->trading_condition) && ($DataContent->trading_condition->trading_conditions_22_title_1 != "1")) ||
            (isset($DataContent->trading_condition) && empty($DataContent->trading_condition->trading_conditions_22_title_2)) || (isset($DataContent->trading_condition) && ($DataContent->trading_condition->trading_conditions_22_title_2 != "1")) ||
            (isset($DataContent->trading_condition) && empty($DataContent->trading_condition->trading_conditions_22_title_en_ref)) || (isset($DataContent->trading_condition) && ($DataContent->trading_condition->trading_conditions_22_title_en_ref != "1")) ||
            (isset($DataContent->trading_condition) && empty($DataContent->trading_condition->trading_conditions_23_title_en_ref)) || (isset($DataContent->trading_condition) && ($DataContent->trading_condition->trading_conditions_23_title_en_ref != "1")) ||
            (isset($DataContent->trading_condition) && empty($DataContent->trading_condition->step_4_footer)) || (isset($DataContent->trading_condition) && ($DataContent->trading_condition->step_4_footer != "yes"))
          ) {
            if ($activeStep != "4") {
              alert()->info('Info', 'Harap periksa kembali form pembukaan live akun')->persistent('Close');
            }
            $getdataerror = true;
            $activeStep = 4;
            $page == 1;
          }

          if ($getdataerror == false && (empty($DataContent->trading_rules) ||
            $DataContent->trading_rules != 'yes')) {
            if ($activeStep != "4") {
              alert()->info('Info', 'Harap periksa kembali form pembukaan live akun')->persistent('Close');
            }
            $getdataerror = true;
            $activeStep = 4;
            $page == 2;
          }

          if ($getdataerror == false && (empty($DataContent->pap_agreement) ||
            $DataContent->pap_agreement != 'yes')) {
            if ($activeStep != "4") {
              alert()->info('Info', 'Harap periksa kembali form pembukaan live akun')->persistent('Close');
            }
            $getdataerror = true;
            $activeStep = 4;
            $page == 3;
          }

          // end data baru
        }

        // $activeStep = 2;
        // $page=2;
        $count_required_image = 0;
        if ($activeStep > 2) {
          foreach ($DataContent->attached_documents->required_image as $doc) {
            // dd($doc->src->url);
            if (isset($doc->src->url) && $doc->src->url != "") {
              $count_required_image = $count_required_image + 1;
            }
          }

          if ($count_required_image < 3) {
            alert()->info('Info', 'Harap lampirkan seluruh dokumen yang diperlukan')->persistent('Close');
            $activeStep = 2;
          }
        }

        if ($activeStep == 1) {
          return view('pages.auth.openliveaccountstep.step1', compact('UserToken', 'UserId', 'DataContent', 'UserDetail', 'agent'));
        } else if ($activeStep == 2) {
          return view('pages.auth.openliveaccountstep.step2', compact('UserToken', 'UserId', 'DataContent', 'UserDetail', 'agent'));
        } else if ($activeStep == 3) {
          return view('pages.auth.openliveaccountstep.step3-1', compact('UserToken', 'UserId', 'DataContent', 'UserDetail', 'agent'));
        } else if ($activeStep == 4) {
          if ($page == 1 || $page == 0) {
            return view('pages.auth.openliveaccountstep.step4-1', compact('UserToken', 'UserId', 'DataContent', 'UserDetail', 'agent'));
          } else if ($page == 2) {
            return view('pages.auth.openliveaccountstep.step4-2', compact('UserToken', 'UserId', 'DataContent', 'UserDetail', 'agent'));
          } else if ($page == 3) {
            return view('pages.auth.openliveaccountstep.step4-3', compact('UserToken', 'UserId', 'DataContent', 'UserDetail', 'agent'));
          }
        } else if ($activeStep == 5) {
          $api_url = env('API_URL');

          // for fix typo account_type=="reguler"
          if ($DataContent->account_type == "reguler") {
            $DataContent->account_type = "regular";
          }
          // end fix typo

          // save ulang KYC 
          $DataContent->KycStatus = 1;
          $url_savekyc = $api_url . 'api/account/savekyc';
          $client = new Client();
          $requestSaveKYC = $client->post($url_savekyc, [
            'headers' => [
              'Content-Type' => 'application/json',
              'Language' => 'en',
              'Cookie' => Session::get('user.token')
            ],
            'json' => [
              'UserId' =>  $UserId,
              'Content' => json_encode($DataContent)
            ]
          ]);
          $response = json_decode($requestSaveKYC->getBody());
          // end save ulang KYC

          // Add live account 
          $DataContent->KycStatus = 1;
          $url = $api_url . 'api/account/addliveaccount';
          $client = new Client();
          $requestPost = $client->post($url, [
            'headers' => [
              'Content-Type' => 'application/json',
              'Language' => 'en',
              'Cookie' => Session::get('user.token')
            ],
            'json' => [
              'UserId' =>  $UserId,
              'Content' => json_encode($DataContent)
            ]
          ]);
          $response = json_decode($requestPost->getBody());
          // end add live account

          // to do : get detail account ->kycstatus
          $urlaccdetail_2 = $api_url . 'api/account/Detail';
          $reqaccdetail_2 = $client->post($urlaccdetail_2, [
            'headers' => [
              'Content-Type' => 'application/json',
              'Language' => 'en',
              'Cookie' => Session::get('user.token')
            ],
            'json' => [
              'IsLoadComm' => false,
              'LoadStatistics' => true
            ]
          ]);

          $resaccdetail_2 = json_decode($reqaccdetail_2->getBody());
          if($resaccdetail_2->Code==0){
            $dtUser = $resaccdetail_2->Data->User;
            Session::put('user.KycStatus', $dtUser->KycStatus);
          }
          
          // end;
          if ($response->Code === 0) {
            $MTUserRef = $response->Data->MTUserRef[0];
            return view('pages.auth.openliveaccountstep.step5', compact('UserToken', 'UserId', 'DataContent', 'UserDetail', 'agent', 'MTUserRef'));
          } else {
            return view('pages.auth.openliveaccountstep.step5', compact('UserToken', 'UserId', 'DataContent', 'UserDetail', 'agent'));
          }
        }
      } else {
        return redirect()->route('login');
      }
    } catch (\Exception $e) {
      if ($e->getResponse()->getStatusCode() == '401') {
        return redirect()->route('login');
      } else if ($e->getResponse()->getStatusCode() == '500') {
      } else {
        dd('error code : ' . $e->getResponse()->getStatusCode());
      }
    }
  }

  public function liveaccountproduct()
  {
    try {
      $api_url = env('API_URL');
      $url = $api_url . 'api/account/Detail';
      $client = new Client();
      $account_detail = $client->post($url, [
        'headers' => [
          'Content-Type' => 'application/json',
          'Language' => 'en',
          'Cookie' => Session::get('user.token')
        ],
        'json' => [
          'IsLoadComm' => false,
          'LoadStatistics' => true,
        ]
      ]);
      $accounts = json_decode($account_detail->getBody());
      $Data = $accounts->Data;
      $User = $Data->User;
      $UserBankCard = $Data->UserBankCard;
      $IdCard = $Data->IdCard;
      return view('pages.auth.liveaccount', compact('User', 'UserBankCard', 'IdCard'));
    } catch (\Exception $e) {
      if ($e->getResponse()->getStatusCode() == '401') {
        return redirect()->route('login');
      } else if ($e->getResponse()->getStatusCode() == '500') {
      } else {
        dd('error code : ' . $e->getResponse()->getStatusCode());
      }
    }
  }

  public function savekyc(Request $request)
  {
    $api_url = env('API_URL');
    $url = $api_url . 'api/account/savekyc';
    $client = new Client();
    $requestPost = $client->post($url, [
      'headers' => [
        'Content-Type' => 'application/json',
        'Language' => 'en',
        'Cookie' => $request->NBCRMWEBAPI
      ],
      'json' => [
        'UserId' =>  $request->USERID,
        'Content' => $request->content
      ]
    ]);
    $response = json_decode($requestPost->getBody());

    $url_client_info_update = $api_url . 'api/account/updateuserinfo';
    $requestupdateclient = $client->post($url_client_info_update, [
      'headers' => [
        'Content-Type' => 'application/json',
        'Language' => 'en',
        'Cookie' => $request->NBCRMWEBAPI
      ],
      'json' => [
        'Realname' =>  json_decode($request->content)->full_name,
      ]
    ]);
    $responseupdateclient = json_decode($requestupdateclient->getBody());
    // dd($responseupdateclient);
    return $response->Code;
  }

  public function addliveaccount(Request $request)
  {
    $api_url = env('API_URL');
    $url = $api_url . 'api/account/addliveaccount';
    $client = new Client();
    $requestPost = $client->post($url, [
      'headers' => [
        'Content-Type' => 'application/json',
        'Language' => 'en',
        'Cookie' => $request->NBCRMWEBAPI
      ],
      'json' => [
        'UserId' =>  $request->USERID,
        'Content' => $request->content
      ]
    ]);
    $response = json_decode($requestPost->getBody());
    return $response->Code;
    if ($response->Code === 0) {
      return array("Code" => json_decode($response->Code), "Message" => json_decode($response->Message), "Data" => json_decode($response->Data));
    } else {
      return array("Code" => json_decode($response->Code), "Message" => json_decode($response->Message));
    }
  }

  public function withdrawal(Request $request)
  {
    try {
      $pageSize = 10;
      $pageIndex = 1;
      $api_url = env('API_URL');
      $url = $api_url . 'api/bill/pagemaxcowithdraws';
      $client = new Client();
      $MT = Session::get('user.MTUserLive');
      if (!empty($request->query('page'))) {
        $pageIndex = $request->query('page');
      }
      $requestPost = $client->post($url, [
        'headers' => [
          'Content-Type' => 'application/json',
          'Language' => 'en',
          'Cookie' => Session::get('user.token')
        ],
        'json' => [
          'MTId' => $MT->Login,
          'PageIndex' => $pageIndex,
          'PageSize' => $pageSize
        ]
      ]);
      $response = json_decode($requestPost->getBody());
      $data = $response->Data;
      $page = $response->Page;
      $totals = $response->Totals;

      $totalCount = $page->TotalCount;
      $totalPages = ceil($totalCount / $pageSize);
      $totalPages = number_format($totalPages, 0);
      // dd($data);
      return view('pages.auth.withdrawal', compact('data', 'page', 'totals', 'totalPages'));
    } catch (\Exception $e) {
      if ($e->getResponse()->getStatusCode() == '401') {
        return redirect()->route('login');
      } else if ($e->getResponse()->getStatusCode() == '500') {
      } else {
        dd('error code : ' . $e->getResponse()->getStatusCode());
      }
    }
  }

  public function withdrawalrequest(Request $request)
  {
    try {
      $api_url = env('API_URL');
      $url = $api_url . 'api/bill/applymaxcowithdraw';
      $client = new Client();
      $MT = Session::get('user.MTUserLive');
      $amountDecimal = !empty($request->AmountDecimal) ? $request->AmountDecimal : 0;
      $amount = $request->Amount . "." . $amountDecimal;
      // dd($request,$amount);
      $requestPost = $client->post($url, [
        'headers' => [
          'Content-Type' => 'application/json',
          'Language' => 'en',
          'Cookie' => Session::get('user.token')
        ],
        'json' => [
          'Login' => $MT->Login,
          'MType' => $MT->MType,
          'Amount' => $amount,
          'BankCardId' => $request->BankCardId,
          'Currency' => $request->Currency,
          'Remark' => $request->Remark,
        ]
      ]);
      $response = json_decode($requestPost->getBody());
      if ($response->Code === 0) {
        alert()->success('Success', 'Permintaan penarikan anda berhasil');
        return redirect()->route('historywithdrawal');
      } else {
        return redirect()->back()->with('ErrorMessage', $response->Message);
      }
    } catch (\Exception $e) {
      if ($e->getResponse()->getStatusCode() == '401') {
        return redirect()->route('login');
      } else if ($e->getResponse()->getStatusCode() == '500') {
      } else {
        dd('error code : ' . $e->getResponse()->getStatusCode());
      }
    }
  }
  public function deposit(Request $request)
  {
    $api_url = env('API_URL');
    $url = $api_url . 'api/bill/pagemaxcodeposits';
    $client = new Client();
    $MT = Session::get('user.MTUserLive');
    if (empty($MT)) {
      return redirect()->route('login');
    }
    try {
      $pageSize = 10;
      $pageIndex = 1;
      if (!empty($request->query('page'))) {
        $pageIndex = $request->query('page');
      }
      $requestPost = $client->post($url, [
        'headers' => [
          'Content-Type' => 'application/json',
          'Language' => 'en',
          'Cookie' => Session::get('user.token')
        ],
        'json' => [
          'MTId' => $MT->Login,
          'PageIndex' => $pageIndex,
          'PageSize' => $pageSize
        ]
      ]);
      $response = json_decode($requestPost->getBody());
      $data = $response->Data;
      $page = $response->Page;
      $totals = $response->Totals;

      $totalCount = $page->TotalCount;
      $totalPages = ceil($totalCount / $pageSize);
      $totalPages = number_format($totalPages, 0);
      return view('pages.auth.deposit', compact('data', 'page', 'totals', 'totalPages'));
    } catch (\Exception $e) {
      if ($e->getResponse()->getStatusCode() == '401') {
        return redirect()->route('login');
      } else if ($e->getResponse()->getStatusCode() == '500') {
      } else {
        dd('error code : ' . $e->getResponse()->getStatusCode());
      }
    }
  }
  public function depositrequestForm()
  {
    return view('pages.auth.depositrequest');
  }
  public function depositrequest(Request $request)
  {
    try {
      $api_url = env('API_URL');
      $url = $api_url . 'api/bill/applymaxcodeposit';
      $client = new Client();
      $MT = Session::get('user.MTUserLive');
      $requestPost = $client->post($url, [
        'headers' => [
          'Content-Type' => 'application/json',
          'Language' => 'en',
          'Cookie' => Session::get('user.token')
        ],
        'json' => [
          'UserId' => Session::get('user.id'),
          'MTId' => $MT->Login,
          'MType' => $MT->MType,
          'Amount' => $request->Amount,
          'BankId' => $request->BankId,
          'Receipt' => $request->Receipt,
          'Method' => $request->Method,
        ]
      ]);
      $response = json_decode($requestPost->getBody());
      if ($response->Code === 0) {
        alert()->success('Success', 'Permintaan setoran anda berhasil');
        return redirect()->route('historydeposit');
      } else {
        return redirect()->back()->with('ErrorMessage', $response->Message);
      }
    } catch (\Exception $e) {
      if ($e->getResponse()->getStatusCode() == '401') {
        return redirect()->route('login');
      } else if ($e->getResponse()->getStatusCode() == '500') {
      } else {
        dd('error code : ' . $e->getResponse()->getStatusCode());
      }
    }
  }

  public function uploadimg(Request $request)
  {
    $image = $request->file('file');
    $time = time();
    $img1  = str_replace(" ", "", $time . "-" . $image->getClientOriginalName());
    $image_normal = Image::make($image)->widen(600, function ($constraint) {
      $constraint->upsize();
    });
    $image_normal = $image_normal->stream();

    Storage::disk('local')->put('public/' . $img1, $image_normal->__toString());

    $api_url = env('API_URL');
    $url = $api_url . 'api/file/uploadimg';
    $client = new Client();
    $requestPost = $client->post($url, [
      'headers' => [
        'Language' => 'en',
        'Cookie' => Session::get('user.token'),

      ],
      'multipart' => [
        [
          'name' => 'Image',
          'contents' => fopen(url('/') . '/storage/' . $img1, 'r'),
          'headers' => ['Content-Type' => 'image/jpeg']
        ]

      ]
    ]);
    $response = json_decode($requestPost->getBody());
    $myFile = url('/') . '/storage/' . $img1;
    $try = File::delete($myFile);
    if ($response->Code === 0) {
      return array("Code" => $response->Code, "Message" => $response->Message, "Data" => $response->Data);
    } else {
      return array("Code" => $response->Code, "Message" => $response->Message);
    }
  }

  // public function savetokenfromweb(Request $request){
  //   Session::put('user.bytoken',$request->tokenweb);
  // }
}

/*
'headers' => [
  //'Content-Type' => 'multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW',
  'Language' => 'en',
  'Cookie' => Session::get('user.token')
],
'multipart' => [
 [
     'name'     => 'Image',
     'contents' => fopen($request->file('file'), 'r'),
     'headers'  => ['Content-Type' => 'image/jpeg']
 ],
 */
