@extends('templates.master')

@section('content')
<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <h1>Change Password</h1>
                <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                    <ol class="breadcrumb pt-0"></ol>
                </nav>
                <div class="separator mb-5"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-6">
                <form id="change-password-form" class="needs-validation" novalidate method="POST" action="{{route('postChangePassword')}}">
                    @csrf
                    <input class="form-control" name="UserId" type="hidden" value="{{Session::get('user.id')}}" />
                    <label class="form-group has-float-label mb-4">
                        <input class="form-control" type="password" name="OldPassword" required />
                        <span>Old Password</span>
                        <label id="OldPassword-error" class="error" for="OldPassword" style="top: 33px;position: absolute;color: red;opacity: 2;background: rgb(255, 255, 255);left: 0.75rem;"></label>
                    </label>
                    <label class="form-group has-float-label mb-4">
                        <input class="form-control" type="password" name="NewPassword" required />
                        <span>New Password</span>
                        <label id="NewPassword-error" class="error" for="NewPassword" style="top: 33px;position: absolute;color: red;opacity: 2;background: rgb(255, 255, 255);left: 0.75rem;"></label>
                    </label>
                    <label class="form-group has-float-label mb-4">
                        <input class="form-control" type="password" name="ConfirmPassword" required />
                        <span>Confirm Password</span>
                        <label id="ConfirmPassword-error" class="error" for="ConfirmPassword" style="top: 33px;position: absolute;color: red;opacity: 2;background: rgb(255, 255, 255);left: 0.75rem;"></label>
                    </label>

                    @if (session('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>Success!</strong> Password changed.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif

                    @if (session('ErrorMessage'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <strong>Error!</strong> {{ session('ErrorMessage') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif
                    <div class="d-flex justify-content-between align-items-center">
                        <input id="btn-change-password" class="btn btn-primary btn-lg btn-shadow btn-maxco-blue borderradius-0" type="submit" value="CHANGE PASSWORD" />
                        <button id="btn-loading" onclick="event.preventDefault();" class="btn btn-primary btn-lg btn-shadow hidden" style="border-radius: 0px;background-color: #017dc7 !important;"><span class="loader"></span> CHANGE PASSWORD</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</main>
@endsection

@section('jsonpage')
<script>
    $(function() {
        $.validator.addMethod(
            "regex",
            function(value, element, regexp) {
                var re = new RegExp(regexp);
                return this.optional(element) || re.test(value);
            },
            "Gunakan minimal 8 karakter dengan campuran huruf kapital dan angka."
        );
        $("#change-password-form").validate({
            ignore: [],
            // errorElement: "div",
            rules: {
                OldPassword: {
                    required: true
                },
                NewPassword: {
                    required: true,
                    // minlength: 8,
                    // maxlength: 30,
                    regex: "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{5,30}$"
                },
                ConfirmPassword: {
                    required: true,
                    // minlength: 6,
                    equalTo: "[name='NewPassword']"
                },

            },
            messages: {
                ConfirmPassword: {
                    equalTo: "Passwords must match!",
                }
            }
        });
    });
    $('[type="submit"]').on('click', function() {
        if ($('form').valid()) {
           $('#btn-change-password').hide();
           $('#btn-loading').show();
        }
    });
</script>

@endsection