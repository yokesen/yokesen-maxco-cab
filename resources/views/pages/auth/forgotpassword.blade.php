<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-BMTCL2Z4WL"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'G-BMTCL2Z4WL');
    </script>
    <meta charset="UTF-8">
    <title>Maxco Futures | Prestigious Global Brokerage House</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="stylesheet" href="{{url('/')}}/cabinet/font/iconsmind-s/css/iconsminds.css" />
    <link rel="stylesheet" href="{{url('/')}}/cabinet/font/simple-line-icons/css/simple-line-icons.css" />
    <link rel="stylesheet" href="{{url('/')}}/cabinet/css/vendor/select2.min.css" />
    <link rel="stylesheet" href="{{url('/')}}/cabinet/css/vendor/select2-bootstrap.min.css" />
    <link rel="stylesheet" href="{{url('/')}}/cabinet/css/vendor/bootstrap.min.css" />
    <link rel="stylesheet" href="{{url('/')}}/cabinet/css/vendor/bootstrap.rtl.only.min.css" />
    <link rel="stylesheet" href="{{url('/')}}/cabinet/css/vendor/bootstrap-float-label.min.css" />
    <link rel="stylesheet" href="{{url('/')}}/cabinet/css/main.css" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <style>
        .btn-active {
            background-color: #fff !important;
            color: #017dc7 !important;
            border-color: #017dc7 !important;
        }
        

        /* .verify-code {
            max-width: 400px;
            min-width: 300px;
        } */
    </style>
     @if($agent->isMobile())
    <style>
      .loader{
        margin-top: -75px;
      }
    </style>
  @endif
</head>

<body class="background show-spinner no-footer">
    <div class="fixed-background" style="background:url({{url('/')}}/cabinet/img/webpage/background.jpg)"></div>

    <main>
        <div class="container">
            <div class="row h-100">
                <div class="col-12 col-md-10 mx-auto my-auto">
                    <div class="card auth-card">
                        <div class="position-relative image-side" style="background:url({{url('/')}}/cabinet/img/webpage/background-kecil-final.jpg)">
                            <p class=" text-white h2">MAXCOFUTURES CLIENT AREA</p>
                            <p class="white mb-0">
                                Please use your e-mail to reset your password.
                                <br>If you are not a member, please
                                <a href="/register" class="white">register</a>.
                            </p>
                        </div>
                        <div class="form-side">
                            <a href="#">
                                <span class="logo-single" style="background:url({{url('/')}}/cabinet/img/webpage/logo_maxco_W300px.png);background-size: cover;width: 300px;height: 66px;"></span>
                            </a>
                            <h6 class="mb-4">Forgot Password</h6>
                            <div class="mb-4">
                                <div class="row">
                                    <div class="col-md-6">
                                        <button class="btn btn-primary btn-shadow btn-option-login optionTel" style="width:100% !important; border-radius: 0px;background-color: #fff;color:#9e9e9e;" onclick="renderText('Telephone');$('#areaCodeGrp').show();changeBtn(event);">Telephone</button>
                                    </div>
                                    <div class="col-md-6">
                                        <button class="btn btn-primary btn-shadow btn-option-login optionEmail btn-active" style="width:100% !important; border-radius: 0px;background-color: #fff;color:#9e9e9e;" onclick="renderText('Email');$('#areaCodeGrp').hide();changeBtn(event);">Email</button>
                                    </div>
                                </div>
                            </div>
                            <form id="forgotpasswordform" method="post" action="{{route('postForgotPassword')}}" autocomplete="off">
                                @csrf
                                <div style="display: flex">
                                    <label id="areaCodeGrp" class="form-group has-float-label mb-4" style="display: none;width: 118px;">
                                        <select id="areaCode" name="AreaCode" class="form-control select2-single" style="min-width: 100%;">
                                        </select>
                                        <span>Code Area</span>
                                    </label>
                                    <label class="form-group has-float-label mb-4" style="width:100%;">
                                        <input class="form-control" name="InputText" id="InputText" value="{{old('InputText')}}" />
                                        <span id="label-text">E-mail</span>
                                    </label>
                                </div>
                                <div class="row">
                                    <div id="div-input-verify-code" class="col-md-6 col-sm-6 hidden">
                                        <label class="form-group has-float-label">
                                            <input class="form-control verify-code" type="text" name="VerifyCode" required autocomplete="verify-code" placeholder="Verify Code" value="{{old('VerifyCode')}}">
                                            <span>Verify Code</span>
                                        </label>
                                    </div>
                                    <div class="col-sm-12 alertVerCode" style="display: none;">
                                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                            <strong>Error!</strong> <span id="errorMsg"></span>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    </div>
                                    <div id="div-button-send-code" class="col-md-8 col-sm-8">
                                        <a id="btn-send-code" class="btn btn-primary btn-shadow btn-maxco-blue" style="border-radius: 0px;color:#fff;background-color: #017dc7 !important;" onclick="sendVerificationCodeForgotPass()">SEND VERIFY CODE</a>
                                        <button id="btn-loading-send-code" onclick="event.preventDefault();" class="btn btn-primary btn-lg btn-shadow hidden" value="LOGIN" style="border-radius: 0px;background-color: #017dc7 !important;"><span class="loader-small"></span> SEND VERIFY CODE</button>
                                    </div>
                                </div>
                                <div id="div-password" class="hidden">
                                    <label class="form-group has-float-label mb-4">
                                        <input class="form-control" type="password" name="Password" autocomplete="off" required value="{{old('Password')}}" />
                                        <span>Password</span>
                                    </label>
                                    <label class="form-group has-float-label mb-4">
                                        <input class="form-control" type="password" name="ConfirmPassword" required value="{{old('ConfirmPassword')}}" />
                                        <span>Confirm Password</span>
                                    </label>
                                </div>

                                @if (session('ErrorMessage'))
                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                    <strong>Error!</strong> {{ session('ErrorMessage') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                @endif
                                <input type="hidden" id="InfoType" name="InfoType" value="1">
                                <div class="d-flex justify-content-end align-items-center">
                                    <input id="reset-btn" class="btn btn-primary btn-lg btn-shadow hidden" type="submit" style="border-radius: 0px;background-color: #017dc7 !important;" value="RESET" />
                                    <button id="reset-btn-loading" onclick="event.preventDefault();" class="btn btn-primary btn-lg btn-shadow hidden" value="LOGIN" style="border-radius: 0px;background-color: #017dc7 !important;"><span class="loader"></span> RESET</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <script src="{{url('/')}}/cabinet/data/nationality.js"></script>
    <script src="{{url('/')}}/cabinet/js/vendor/jquery-3.3.1.min.js"></script>
    <script src="{{url('/')}}/cabinet/js/vendor/jquery.validate/jquery.validate.min.js"></script>
    <script src="{{url('/')}}/cabinet/js/vendor/bootstrap.bundle.min.js"></script>
    <script src="{{url('/')}}/cabinet/js/vendor/select2.full.js"></script>
    <script src="{{url('/')}}/cabinet/js/dore.script.js"></script>
    <script src="{{url('/')}}/cabinet/js/scripts.js"></script>
    <script>
        var validator;

        function renderValidation() {
            validator = $("#forgotpasswordform").validate({
                ignore: [],
                errorElement: "div",
                rules: {
                    InputText: {
                        required: true,
                        email: true,
                        number: false
                    },
                    VerifyCode: {
                        required: true
                    },
                    Password: {
                        required: true,
                        // minlength: 6,
                    },
                    ConfirmPassword: {
                        required: true,
                        // minlength: 6,
                        equalTo: "[name='Password']"
                    },
                }
            });
        }

        $(function() {
            renderValidation();
            // $("form").on('submit', function(event) {
            //     event.preventDefault();
            // });
            setTimeout(function() {
                $('[name="VerifyCode"]').val('');
                $('[name="Password"]').val('');
            }, 500);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        });
        const InfoType = "{{old('Password')}}";
        if (InfoType) {
            if (InfoType === "0") {
                $('.optionTel').trigger('click');
            } else {
                $('.optionEmail').trigger('click');
            }
        }

        function changeBtn(event) {
            $('.btn-option-login').each(function() {
                $(this).removeClass('btn-active');
            })
            $(event.target).addClass('btn-active');
        }

        function renderText(txt) {
            $('#label-text').text(txt);
            var text;
            if (txt === "Telephone") {
                text = '0';
                $('#InputText-error').remove();
                if (InfoType === "") {
                    $('[name="InputText"]').val('');
                }
                validator.settings.rules.InputText.email = false;
                validator.settings.rules.InputText.number = true;

            } else {
                text = '1'
                $('#InputText-error').remove();
                if (InfoType === "") {
                    $('[name="InputText"]').val('');
                }
                validator.settings.rules.InputText.email = true;
                validator.settings.rules.InputText.number = false;
            }
            $('#InfoType').val(text);
        }
        var optionAreaCd = "";
        nationalityDt.map(function(item) {
            const dt = `<option value="${item.AreaCode}">${item.AreaCode}</option>`;
            optionAreaCd += dt;
        });
        $('#areaCode').append(optionAreaCd)
        $('#areaCode').val('+ 62');

        function sendVerificationCodeForgotPass(event) {
            var InputText = $('#InputText').val();
            var InfoType = $('#InfoType').val();
            if ((InputText).trim() === "") {
                $('.alertVerCode').show();
                $('#errorMsg').text('Email or phone cannot be empty.');
            } else {
                $('.alertVerCode').hide();
                $('#btn-send-code').hide();
                $('#btn-loading-send-code').show();
                var ValidateType = '43';
                var Mobile = '';
                var Email = '';
                if (InfoType === '1') {
                    ValidateType = '34';
                    Email = InputText;
                } else {
                    Mobile = InputText;
                }
                $.post("/send-code-forgot-password", {
                    InfoType,
                    ValidateType,
                    AreaCode: $('#areaCode').val(),
                    Mobile,
                    Email,
                }, function(data, status) {
                    console.log('data', data)
                    if (parseInt(data.Code) > 0) {
                        $('.alertVerCode').show();
                        $('#errorMsg').text(data.Message);
                        $('#btn-send-code').show();
                        $('#btn-loading-send-code').hide();
                    } else {
                        $('#div-button-send-code').hide();
                        $('#div-input-verify-code').show();
                        $('#div-password').show();
                        $('#reset-btn').show();
                    }
                });
            }

        };

        $('[type="submit"]').on('click', function() {
            console.log('onclick');
            if($('#forgotpasswordform').valid()){
                $('#reset-btn').hide();   
                $('#reset-btn-loading').show(); 
            }
        });
    </script>
</body>

</html>