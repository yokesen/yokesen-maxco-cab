<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-BMTCL2Z4WL"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'G-BMTCL2Z4WL');
    </script>

    <meta charset="UTF-8">
    <title>Maxco Futures | Prestigious Global Brokerage House</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="icon" type="image/png" sizes="16x16" href="favicon.ico">

    <link rel="stylesheet" href="{{url('/')}}/cabinet/font/iconsmind-s/css/iconsminds.css" />
    <link rel="stylesheet" href="{{url('/')}}/cabinet/font/simple-line-icons/css/simple-line-icons.css" />

    <link rel="stylesheet" href="{{url('/')}}/cabinet/css/vendor/bootstrap.min.css" />
    <link rel="stylesheet" href="{{url('/')}}/cabinet/css/vendor/bootstrap.rtl.only.min.css" />
    <link rel="stylesheet" href="{{url('/')}}/cabinet/css/vendor/fullcalendar.min.css" />
    <link rel="stylesheet" href="{{url('/')}}/cabinet/css/vendor/dataTables.bootstrap4.min.css" />
    <link rel="stylesheet" href="{{url('/')}}/cabinet/css/vendor/datatables.responsive.bootstrap4.min.css" />
    <link rel="stylesheet" href="{{url('/')}}/cabinet/css/vendor/select2.min.css" />
    <link rel="stylesheet" href="{{url('/')}}/cabinet/css/vendor/bootstrap-float-label.min.css" />
    <link rel="stylesheet" href="{{url('/')}}/cabinet/css/vendor/bootstrap-tagsinput.css" />
    <link rel="stylesheet" href="{{url('/')}}/cabinet/css/vendor/perfect-scrollbar.css" />
    <link rel="stylesheet" href="{{url('/')}}/cabinet/css/vendor/glide.core.min.css" />
    <link rel="stylesheet" href="{{url('/')}}/cabinet/css/vendor/bootstrap-stars.css" />
    <link rel="stylesheet" href="{{url('/')}}/cabinet/css/vendor/nouislider.min.css" />
    <link rel="stylesheet" href="{{url('/')}}/cabinet/css/vendor/bootstrap-datepicker3.min.css" />
    <link rel="stylesheet" href="{{url('/')}}/cabinet/css/vendor/component-custom-switch.min.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/')}}/cabinet/css/dore.light.blue.min.css">
    <link rel="stylesheet" href="{{url('/')}}/cabinet/css/main.css" />
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <meta name="csrf-token" content="{{ csrf_token() }}">

    @yield('csslinkonhead')
    <!-- Facebook Pixel Code -->
    <style>
        .logo-mobile {
            background: url("{{url('/')}}/cabinet/img/webpage/logo_maxco_W300px_mobile.png") no-repeat !important;
            width: 26px !important;
        }
    </style>

</head>