@extends('templates.master')

@section('content')
<main id="app">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <h1>Demo Account</h1>
                <div class="text-zero top-right-button-container">
                    <a href="/open-demo-account" class="btn btn-primary btn-lg btn-shadow btn-maxco-blue borderradius-0" type="submit">Open Demo Account</a>
                </div>
                <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                    <ol class="breadcrumb pt-0">
                        <!-- <li class="breadcrumb-item">
                            <a href="#">Home</a>
                        </li> -->
                        <!-- <li class="breadcrumb-item">
                            <a href="#">Library</a>
                        </li> -->
                        <!-- <li class="breadcrumb-item active" aria-current="page">Data</li> -->
                    </ol>
                </nav>
                <div class="separator mb-5"></div>
            </div>
        </div>

    </div>
</main>
@endsection